//
//  UIApplicationExtenstion.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIApplication {
     func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                application.open(URL(string: url)!, options: [:], completionHandler: nil)
                return
            }
        }
    }
}
