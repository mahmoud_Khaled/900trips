//
//  UITextFieldExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum IconDirection {
    case left, right
}

extension UITextField {
    
    func setIcon(direction: IconDirection, image: UIImage?, width: CGFloat, height: CGFloat, customLeading: CGFloat = 0, tintColor: UIColor? = nil) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width + (customLeading == 0 ? 20 : customLeading), height: height))
        
        let imageView: UIImageView
        if let tintColor = tintColor {
            imageView = UIImageView(image: image?.withRenderingMode(.alwaysTemplate))
            imageView.tintColor = tintColor
        } else {
             imageView = UIImageView(image: image)
        }
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        if customLeading != 0 {
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        } else {
            imageView.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: direction == .left ? -6 : -10).isActive = true
        }
        NSLayoutConstraint.activate([
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: width),
            imageView.heightAnchor.constraint(equalToConstant: height)
        ])
        
        if direction == .left {
            leftView = view
            leftViewMode = .always
        } else {
            rightView = view
            rightViewMode = .always
        }
    }
    
    func setIconWithoutPadding(direction: IconDirection, image: UIImage?, width: CGFloat, height: CGFloat, tintColor: UIColor? = nil) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width + 4, height: height))
        
        let imageView: UIImageView
        if let tintColor = tintColor {
            imageView = UIImageView(image: image?.withRenderingMode(.alwaysTemplate))
            imageView.tintColor = tintColor
        } else {
            imageView = UIImageView(image: image)
        }
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: width),
            imageView.heightAnchor.constraint(equalToConstant: height)
        ])
        
        if direction == .left {
            leftView = view
            leftViewMode = .always
        } else {
            rightView = view
            rightViewMode = .always
        }
    }
}
