//
//  UIImageViewExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func load(url: URL?, placeholder: UIImage? = nil) {
        if url?.absoluteString.hasPrefix("file://") == true {
            guard let url = url else { return }
            let provider = LocalFileImageDataProvider(fileURL: url)
            kf.setImage(with: provider, placeholder: placeholder)
        } else {
            var kf = self.kf
            kf.indicatorType = .activity
            kf.setImage(with: url, placeholder: placeholder)
        }
    }
    
}
