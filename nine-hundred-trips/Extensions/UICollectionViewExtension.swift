//
//  UICollectionViewExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UICollectionView {
    func emptyMessage(message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 5))
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = message
        messageLabel.textColor = .coral
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = DinNextFont.bold.getFont(ofSize: 17)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        backgroundView = view
        backgroundView!.addSubview(messageLabel)
        NSLayoutConstraint.activate([
            messageLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            messageLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            messageLabel.widthAnchor.constraint(equalToConstant: frame.width - 16)
        ])
    }
}
