//
//  UIColorExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let coral = UIColor(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0)
    static let deepDarkOrange = UIColor(red: 243/255, green: 126/255, blue: 72/255, alpha: 1.0)
    static let darkOrange = UIColor(red: 255/255, green: 149/255, blue: 0/255, alpha: 1.0)
    static let gray = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
    static let babyGray = UIColor(red: 248/255, green: 246/255, blue: 246/255, alpha: 1.0)
    static let mediumGray = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 0.94)
    static let darkGray = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
    static let mediumTurquoise = UIColor(red: 29/255, green: 160/255, blue: 192/255, alpha: 1.0)
    static let lightCyan = UIColor(red: 29/255, green: 160/255, blue: 192/255, alpha: 0.16)
    static let cyan = UIColor(red: 29/255, green: 160/255, blue: 192/255, alpha: 1.0)
    static let textColor = UIColor(red: 109/255, green: 109/255, blue: 109/255, alpha: 1.0)
    static let customBlue = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
    static let babyGreen = UIColor(red: 117/255, green: 181/255, blue: 181/255, alpha: 1.0)
    static let veryLightGray = UIColor(red: 216/255, green: 216/255, blue: 216/254, alpha: 1.0)
    static let shadow =  UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
    static let darkShadow = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.92)
    
    public convenience init(_ rgbValue : UInt32, alpha: CGFloat = 1.0) {
        
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8) / 256.0
        let blue = CGFloat(rgbValue & 0xFF) / 256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
