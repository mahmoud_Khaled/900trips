//
//  NSNotificationNameExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let updateOfferLikeStatus = NSNotification.Name("updateOfferLikeStatus")
    static let updateProfile = NSNotification.Name("updateProfile")
    static let newMessage = NSNotification.Name("newMessage")
    static let updateConversations = NSNotification.Name("updateConversations")
    static let statusBarTapped = NSNotification.Name("statusBarTapped")
    static let handleOpenOfferDetails = NSNotification.Name("handleOpenOfferDetails")
}
