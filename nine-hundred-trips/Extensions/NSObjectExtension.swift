//
//  NSObjectExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var className: String {
        return "\(self)"
    }
    
}
