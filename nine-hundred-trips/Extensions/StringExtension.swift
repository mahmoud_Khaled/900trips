//
//  StringExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    var englishDigits: String {
        var string = self
        let map = [
            "٠": "0",
            "١": "1",
            "٢": "2",
            "٣": "3",
            "٤": "4",
            "٥": "5",
            "٦": "6",
            "٧": "7",
            "٨": "8",
            "٩": "9"
        ]
        map.forEach { string = string.replacingOccurrences(of: $0, with: $1) }
        return string
    }
    
}
