//
//  UINavigationControllerExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UINavigationController {
    class func create(rootViewController: UIViewController) -> UINavigationController {
        let navigationController = CustomNavigationController(rootViewController: rootViewController)
        return navigationController
    }
}
