//
//  UIImageExtension.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIImage {
    func with(size: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: size)
        let image = renderer.image { _ in
            self.draw(in: .init(origin: .zero, size: size))
        }
        
        return image
    }
}

