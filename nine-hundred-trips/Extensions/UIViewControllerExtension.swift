//
//  UIViewControllerExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(with message: String) {
        AlertBuilder(title: "", message: message, preferredStyle: .alert)
            .addAction(title: "ok".localized(), style: .default)
            .build()
            .show()
    }
    
    func show<V: PopupViewProtocol>(popupView: V, model: V.Model, on view: UIView?, for duration: TimeInterval, completionHandler: @escaping () -> ()) {
        
        addPopupView(popupView: popupView, on: view)
        
        if popupView.isConfigurable {
            popupView.configure(model: model)
        }
        
        if popupView.dismissesAutomtically {
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                completionHandler()
                popupView.fadeOut(duration: 0.25) {
                    popupView.removeFromSuperview()
                }
            }
        }
    }
    
    func show<V: PopupViewActionProtocol>(popupView: V, model: V.Model? = nil, on view: UIView?, confirmAction: @escaping () -> (), cancelAction: @escaping () -> ()) {
        var popupView = popupView
        addPopupView(popupView: popupView, on: view)
        
        if popupView.isConfigurable {
            guard let model = model else { return }
            popupView.configure(model: model)
        }
        
        popupView.confirmAction = {
            confirmAction()
        }
        
        popupView.cancelAction = {
            cancelAction()
        }
    }
    
    func addPopupView(popupView: UIView, on view: UIView?) {
        guard let view = view else { return }
        popupView.alpha = 0
        popupView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(popupView)
        NSLayoutConstraint.activate([
            popupView.topAnchor.constraint(equalTo: view.topAnchor),
            popupView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            popupView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        popupView.fadeIn(duration: 0.25)
    }
    
    func setNavigationBarTransparent(_ transparent: Bool) {
        navigationController?.navigationBar.isTranslucent = true
        if transparent {
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
        } else {
            navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            navigationController?.navigationBar.shadowImage = nil
        }
    }
    
    func hideBackButtonTitle() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

}
