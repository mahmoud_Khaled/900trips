//
//  OfferType.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class OfferType: CustomStringConvertible {
    let id: Int
    let name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    var description: String {
        return name
    }
    
    static func createOfferTypes() -> [OfferType] {
        return [
            OfferType(id: 0, name: "normalOffer".localized()),
            OfferType(id: 1, name: "seasonalOffer".localized())
        ]
    }
}
