//
//  Image.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Image: Codable {
    let id: Int?
    let placeID: String?
    let imageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case placeID = "place_id"
        case imageURL = "image_url"
    }
}

struct ImageViewModel {
    let id: Int
    let placeID: String
    let imageURL: URL?
    
    init(image: Image) {
        id = image.id ?? 0
        placeID = image.placeID ?? ""
        imageURL = URL(string: image.imageURL ?? "")
    }
}
