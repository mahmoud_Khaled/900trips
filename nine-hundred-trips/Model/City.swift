//
//  City.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class City: Codable {
    var id: Int?
    var name: String?
    var imageUrl: String?
    var countryId: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case imageUrl = "image_url"
        case countryId = "country_id"
    }
}

struct CityViewModel: CustomStringConvertible {
    
    let id: Int
    let name: String
    let countryId: String?
    
    init(city: City) {
        id = city.id ?? 0
        name = city.name ?? ""
        countryId = city.countryId ?? ""
    }
    
    var description: String {
        return name
    }
    
}
