//
//  PushNotification.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/31/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum PushNotificationType: String {
    case message, other
}

struct PushNotification {
    let type: PushNotificationType
    let data: [String: Any]?
    
    init?(userInfo: [AnyHashable: Any]) {
        guard
            let stringData = userInfo["data"] as? String,
            let data = stringData.data(using: .utf8),
            let dictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
            let notificationType = dictionary?["notification_type"] as? String,
            let pushNotificationType = PushNotificationType(rawValue: notificationType)
            else { return nil }
        
        self.type = pushNotificationType
        self.data = dictionary?["notification_data"] as? [String: Any]
        
    }
}
