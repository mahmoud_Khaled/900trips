//
//  OfferDetails.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OfferDetails: Codable {
    let id: Int
    let title: String
    let hasResidence: Bool
    let hasFlight: Bool
    let hotelName: String
    let hotelStars: String
    let fromDate: Int
    let toDate: Int
    let adultPrice: Double
    let adultPriceCurrency: String
    let childPrice: String
    let nights: String
    let isLiked: Bool
    let fromCity: City
    let toCity: City
    let flightCompany: FlightCompany
    let flightSeatType: String
    let isTransit: Bool
    let transitDetails: TransitDetails?
    let images: [String]
    let services: [Service]
    let company: User
    let offerRate: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case hasResidence = "has_residence"
        case hasFlight = "has_flight"
        case hotelName = "hotel_name"
        case hotelStars = "hotel_stars"
        case fromDate = "from_date"
        case toDate = "to_date"
        case adultPrice = "adult_price"
        case adultPriceCurrency = "adult_price_currency"
        case childPrice = "child_price"
        case nights
        case isLiked = "is_liked"
        case fromCity = "from_city"
        case toCity = "to_city"
        case flightCompany = "flight_company"
        case flightSeatType = "flight_seat_type"
        case isTransit = "is_transit"
        case transitDetails = "transit_details"
        case images, services, company
        case offerRate = "offer_rate"
    }
}


class OfferDetailsViewModel {
    
    private var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    let images: [URL?]
    let services: [ServiceViewModel]
    let flightDetails: [FlightDetail]
    let id: Int
    
    var isLiked: Bool {
        didSet {
            favouriteButtonImage = isLiked ? UIImage(named: "ic_favourite_red")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "ic_favourite_white")?.withRenderingMode(.alwaysOriginal)
        }
    }
    
    let title: String
    let hasResidence, hasFlight: Bool
    let fromCityName: String
    let fromCityImageUrl: URL?
    let toCityName: String
    let toCityImageUrl: URL?
    let offerPeriod: String
    let isTransit: Bool
    let transitCountryName: String
    let transitCountryImageUrl: URL?
    let transitTime: String
    let hotelName: String
    let hotelRate: Double
    let hotelRateText: String
    let adultPriceString: String
    let childPriceString: String
    let adultPrice: Double
    let companyName: String
    let companyImageUrl: URL?
    let companyId: Int
    let countryOfCompany: String
    let companyNumber1: String
    let companyNumber2: String
//    let offerRate: Int
    
    var favouriteButtonImage: UIImage?
    
    init(offerDetails: OfferDetails) {
        
        images = offerDetails.images.map(URL.init)
        services = offerDetails.services.map(ServiceViewModel.init)
        id = offerDetails.id
        title = offerDetails.title
        hasResidence = offerDetails.hasResidence
        hasFlight = offerDetails.hasFlight
        fromCityName = offerDetails.fromCity.name ?? ""
        fromCityImageUrl = URL(string: offerDetails.fromCity.imageUrl ?? "")
        toCityName = offerDetails.toCity.name ?? ""
        toCityImageUrl = URL(string: offerDetails.toCity.imageUrl ?? "")
 
        let fromDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(offerDetails.fromDate)))
        let toDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(offerDetails.toDate)))
        
        offerPeriod = "\("periodFrom".localized()) \(fromDate) \("to".localized()) \(toDate)"
        
        isTransit = offerDetails.isTransit
        transitCountryName = offerDetails.transitDetails?.country ?? ""
        transitCountryImageUrl = URL(string: offerDetails.transitDetails?.flag ?? "")
        
        let stopMinutes = Float(offerDetails.transitDetails?.stopMinutes ?? "")
        let transitHours = (stopMinutes ?? 0) / 60
        
        transitTime = "\("stoppedFor".localized()) \(transitHours.rounded(toPlaces: 2)) \("hours".localized())"
        
        let flightCompanyName = offerDetails.flightCompany.name
        let flightCompanyImage = offerDetails.flightCompany.imageUrl
        let offerNights = offerDetails.nights
        let offerNumber = offerDetails.id
        let flightType = offerDetails.flightSeatType
        let offerRate = Double(offerDetails.offerRate ?? "")
        
        flightDetails = FlightDetail.createDetails(offerNumber: offerNumber, flightCompanyName: flightCompanyName ?? "", flightType: flightType, offerNights: offerNights, flightCompanyImage: flightCompanyImage ?? "", offerRate: offerRate ?? 0.0)
        
        hotelName = offerDetails.hotelName
        hotelRate = Double(offerDetails.hotelStars) ?? 0
        hotelRateText = "\(hotelRate.cleanValue) \("stars".localized())"
        adultPriceString = "\(offerDetails.adultPrice.cleanValue) \(offerDetails.adultPriceCurrency)"
        childPriceString = offerDetails.childPrice
        adultPrice = offerDetails.adultPrice
        companyName = offerDetails.company.username ?? ""
        companyId = offerDetails.company.id ?? 0
        companyImageUrl = URL(string: offerDetails.company.imageUrl ?? "")
        countryOfCompany = offerDetails.company.country?.name ?? ""
        companyNumber1 = offerDetails.company.phone ?? ""
        companyNumber2 = offerDetails.company.phone1 ?? ""
        
        isLiked = offerDetails.isLiked
        favouriteButtonImage = isLiked ? UIImage(named: "ic_favourite_red")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "ic_favourite_white")?.withRenderingMode(.alwaysOriginal)
        
    }
}
