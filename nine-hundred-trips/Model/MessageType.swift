//
//  MessageType.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class MessageType: Codable {
    
    let id: Int?
    let name: String?
    
}

struct MessageTypeViewModel: CustomStringConvertible {
    
    var id: Int
    var name: String
    
    init(messageType: MessageType) {
        id = messageType.id ?? 0
        name = messageType.name ?? ""
    }
    
    var description: String {
        return name
    }
    
}
