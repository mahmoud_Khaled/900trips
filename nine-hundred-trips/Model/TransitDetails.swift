//
//  TransitDetails.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TransitDetails: Codable {
    let country: String?
    let flag: String?
    let stopMinutes: String?
    
    enum CodingKeys: String, CodingKey {
        case country, flag
        case stopMinutes = "stop_minutes"
    }
}
