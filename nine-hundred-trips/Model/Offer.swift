//
//  Offer.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class Offer: Codable {
    
    let id: Int?
    let title: String?
    let fromDate: Int?
    let toDate: Int?
    let adultPrice: String?
    let hasResidence: Bool?
    let hasFlight: Bool?
    let isLiked: Bool?
    let imageUrl: String?
    let userId: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case fromDate = "from_date"
        case toDate = "to_date"
        case adultPrice = "adult_price"
        case hasResidence = "has_residence"
        case hasFlight = "has_flight"
        case isLiked = "is_liked"
        case imageUrl = "image_url"
        case userId = "user_id"
    }
    
}

class OfferViewModel {
    
    let id: Int
    let title: String
    let offerPeriod: String
    let adultPrice: String
    let hasResidence: Bool
    let hasFlight: Bool
    var isMyOffer: Bool = false
    var isLiked: Bool {
        didSet {
            favouriteButtonImage = isLiked ? UIImage(named: "ic_fav_colored")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "ic_fav")?.withRenderingMode(.alwaysOriginal)
        }
    }
    var favouriteButtonImage: UIImage?
    let imageUrl: URL?
    let userId: Int
    
    init(offer: Offer, dateFormatter: DateFormatter, stringFormat: String) {
        id = offer.id ?? 0
        title = offer.title ?? ""
        
        let fromDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(offer.fromDate ?? 0)))
        let toDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(offer.toDate ?? 0)))
        offerPeriod = stringFormat
            .replacingOccurrences(of: "{{fromDate}}", with: fromDate)
            .replacingOccurrences(of: "{{toDate}}", with: toDate)
        
        adultPrice = offer.adultPrice ?? ""
        hasResidence = offer.hasResidence ?? false
        hasFlight = offer.hasFlight ?? false
        isLiked = offer.isLiked ?? false
        favouriteButtonImage = isLiked ? UIImage(named: "ic_fav_colored")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "ic_fav")?.withRenderingMode(.alwaysOriginal)
        imageUrl = URL(string: offer.imageUrl ?? "")
        userId = Int(offer.userId ?? "") ?? 0
    }
    
}
