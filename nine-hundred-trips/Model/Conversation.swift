//
//  Conversation.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Conversation: Codable {
    let id: Int
    let otherUserId: Int
    let otherUserName: String
    let otherUserImage: String
    let time: Int
    let lastMessage: String
    let rate: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case otherUserId = "other_user_id"
        case otherUserName = "other_user_name"
        case otherUserImage = "other_user_image"
        case time
        case lastMessage = "last_message"
        case rate = "other_user_rate"
    }
    
    init(id: Int, otherUserId: Int, otherUserName: String, otherUserImage: String, time: Int, lastMessage: String, rate: String) {
        self.id = id
        self.otherUserId = otherUserId
        self.otherUserName = otherUserName
        self.otherUserImage = otherUserImage
        self.time = time
        self.lastMessage = lastMessage
        self.rate = rate
    }
}

class ConversationViewModel {
    
    let id: Int
    let otherUserId: Int
    let otherUserName: String
    let otherUserImage: URL?
    var time: String
    var lastMessage: String
    var rate: Double
    
    init(conversation: Conversation, dateFormatter: DateFormatter) {
        id = conversation.id
        otherUserId = conversation.otherUserId
        otherUserName = conversation.otherUserName
        rate = Double(conversation.rate ?? "") ?? 0.0
        otherUserImage = URL(string: conversation.otherUserImage)
        if conversation.lastMessage == "" {
            lastMessage = "photo".localized()
        } else {
            lastMessage = conversation.lastMessage
        }
        time = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(conversation.time)))
    }
}
