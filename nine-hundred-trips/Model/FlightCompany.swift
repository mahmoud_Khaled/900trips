//
//  FlightCompany.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class FlightCompany: Codable, CustomStringConvertible {
    
    let name: String?
    let imageUrl: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
        case imageUrl = "image_url"
    }
    
    var description: String {
        return name ?? ""
    }
}
