//
//  TourismPlaces.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TourismPlace: Codable {
    
    let id: Int?
    let title: String?
    let description: String?
    let longitude: String?
    let latitude: String?
    let images: [String]
    let city: City?
    
    enum CodingKeys: String, CodingKey {
        case id, title, description, longitude, latitude
        case images, city
    }
}

struct TourismPlaceViewModel {
    let id: Int
    let title: String
    let description: String
    let longitude: Double
    let latitude: Double
    let imageUrls: [URL?]
    let cityName: String
    
    init(tourismPlace: TourismPlace) {
        id = tourismPlace.id ?? 0
        title = tourismPlace.title ?? ""
        description = tourismPlace.description ?? ""
        longitude = Double(tourismPlace.longitude ?? "") ?? 0.0
        latitude = Double(tourismPlace.latitude ?? "") ?? 0.0
        cityName = tourismPlace.city?.name ?? ""
        imageUrls = tourismPlace.images.map(URL.init)
    }
}
