//
//  Service.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Service: Codable {
    let name: String
    let photo: String
    let id: Int?
}

struct  ServiceViewModel: CustomStringConvertible {

    let name: String
    let imageUrl: URL?
    let id: Int
    var isSelected: Bool
    
    init(service: Service) {
        name = service.name
        imageUrl = URL(string: service.photo)
        id = service.id ?? 0
        isSelected = false
    }
    
    var description: String {
        return name
    }
    
}
