//
//  OrderPriceQuota.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RequestQuotationPrice: Codable {
    let price: Double
    let currency: String  
}

struct RequestQuotationPriceViewModel {
    
    let price: Double
    let description: String
    
    init(model: RequestQuotationPrice) {
        price = model.price
        if model.price == 0 {
            description = "free".localized()
        } else {
            description = "\(model.price.cleanValue) \(model.currency)"
        }
    }
}
