//
//  ProfileItem.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct ProfileItem {
    var name: String
    var image: String
    var editImage: String
    var action: () -> ()
}
