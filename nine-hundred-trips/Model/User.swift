//
//  User.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
class User: Codable {
    
    let id: Int?
    let username: String?
    let domain: String?
    let email: String?
    let country: Country?
    let currency: Currency?
    let bio: String?
    let likesCount: Int?
    let offersCount: Int?
    let facebook: String?
    let twitter: String?
    let instagram: String?
    let userType: UserType?
    let imageUrl: String?
    let phone: String?
    let phone1: String?
    let token: String?
    let companyType: String?
    let rate: String?
    
    enum CodingKeys: String, CodingKey {
        case id, username,email, country, currency, domain, bio, facebook, twitter, instagram, phone, phone1, token
        case userType = "user_type_id"
        case imageUrl = "photo"
        case offersCount = "offers_count"
        case likesCount = "likes_count"
        case companyType = "company_type"
        case rate = "user_rate"
    }
}

struct UserViewModel {
    let id: Int
    let username: String
    let email: String
    let domain: String
    let countryName: String
    let imageUrl: URL?
    let countryFlagUrl: URL?
    let offerCount: Int
    let likesCount: Int
    let facebook: String?
    let twitter: String?
    let instagram: String?
    var phone: String
    var phone1: String
    var isFacebookButtonHidden: Bool = false
    var isInstagramButtonHidden: Bool = false
    var isTwitterButtonHidden: Bool = false
    var isSocialMediaViewHidden: Bool = false
    let bio: String
    var currency: String
    var companyType: String
    var rate: String
    
    init(user: User) {
        id = user.id ?? 0
        username = user.username ?? ""
        email = user.email ?? ""
        domain = (user.domain ?? "") == "" ? "N/A" : user.domain ?? ""
        countryName = user.country?.name ?? ""
        countryFlagUrl = URL(string: user.country?.photo ?? "")
        currency = user.currency?.name ?? ""
        companyType = user.companyType ?? ""
        imageUrl = URL(string: user.imageUrl ?? "" )
        offerCount = user.offersCount ?? 0
        likesCount = user.likesCount ?? 0
        facebook = user.facebook ?? ""
        twitter = user.twitter ?? ""
        instagram = user.instagram ?? ""
        phone = user.phone ?? ""
        phone1 = user.phone1 ?? ""
        isFacebookButtonHidden = facebook == ""
        isInstagramButtonHidden = instagram == ""
        isTwitterButtonHidden = twitter == ""
        bio = (user.bio ?? "") == "" ? "bioNotAvailable".localized() : user.bio ?? ""
        currency = user.currency?.name ?? ""
        rate = user.rate ?? ""
        isSocialMediaViewHidden = facebook == "" && twitter == "" && instagram == ""
    }
}

