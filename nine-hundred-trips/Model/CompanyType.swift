//
//  CompanyType.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompanyType: Codable {
    
    let id: Int?
    let name: String?
    
}

struct CompanyTypeViewModel: CustomStringConvertible {
    
    let id: Int
    let name: String
    
    init(companyType: CompanyType) {
        id = companyType.id ?? 0
        name = companyType.name ?? ""
    }
    
    var description: String {
        return name
    }
    
}
