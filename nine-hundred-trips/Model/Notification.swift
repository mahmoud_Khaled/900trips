//
//  Notification.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation


class Notification: Codable {
    let id: Int
    let senderID, message: String
    let createdAt: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case senderID = "sender_id"
        case message
        case createdAt = "created_at"
    }
}


struct NotificationViewModel {
    
    let id: Int
    let senderID: String
    let message: String
    let createdAt: Int
    
    init(notification: Notification) {
        id = notification.id
        senderID = notification.senderID
        message = notification.message
        createdAt = notification.createdAt
    }
}
