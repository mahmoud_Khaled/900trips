//
//  RequestOffer.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/10/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct AddOfferModel {
    
    var title = ""
    var departureCountryId = 0
    var departureCityId = 0
    var arrivalCountryId = 0
    var arrivalCityId = 0
    var flightCompanyId = 0
    var flightSeatTypId = 0
    var currencyId = 0
    var offerTypeId = 0
    var transitCountryId = 0
    var transit: Trasnsit = .yes
    var fromDate = ""
    var toDate = ""
    var offerDays = ""
    var transitTime = ""
    var hotelName = ""
    var hotelRate = 0.0
    var adultPrice = ""
    var childPrice = ""
    var photos = [File]()
    
}
