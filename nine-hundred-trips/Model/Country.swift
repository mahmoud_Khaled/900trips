//
//  Country.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Country: Codable {
    let id: Int?
    let name: String?
    let photo: String?
    let cities: [City]?
}

class CountryViewModel: CustomStringConvertible {
    
    let id: Int
    let name: String
    let photoUrl: URL?
    let cities: [CityViewModel]
    var isSelected: Bool
    
    init(country: Country) {
        id = country.id ?? 0
        name = country.name ?? ""
        photoUrl = URL(string: country.photo ?? "")
        isSelected = false
        cities = country.cities?.map({CityViewModel(city: $0)}) ?? []
    }
    
    var description: String {
        return name
    }
}
