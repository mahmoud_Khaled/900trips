//
//  Currency.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Currency: Codable {

    let id: Int?
    let name: String?
    let photo: String?
    let code: String?
}

class CurrencyViewModel: CustomStringConvertible {
    
    let id: Int
    let name: String
    let photoUrl: URL?
    let code: String
    var isSelected: Bool
    
    init(country: Currency) {
        id = country.id ?? 0
        name = country.name ?? ""
        photoUrl = URL(string: country.photo ?? "")
        isSelected = false
        code = country.code ?? ""
    }
    
    var description: String {
        return name
    }
}
