//
//  Message.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum ChatMessageType: String {
    case text = "0"
    case photo = "1"
}

extension ChatMessageType: Codable {
    public init(from decoder: Decoder) throws {
        self = try ChatMessageType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .text
    }
}

class Message: Codable {
    let id: Int
    let type: ChatMessageType
    let message: String
    let time: Int
    let senderUser: User
    
    enum CodingKeys: String, CodingKey {
        case id, message, time, senderUser
        case type = "message_type"
    }
}

class MessageViewModel {
    
    let id = UUID().uuidString
    let content: String
    let time: String
    let timestamp: Int
    let isSender: Bool
    let type: ChatMessageType
    var isSent = false
    
    init(message: Message, dateFormatter: DateFormatter) {
        content = message.message
        time = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(message.time)))
        timestamp = message.time
        isSender = (message.senderUser.id ?? 0) == UserDefaultsHelper.id
        type = message.type
    }
    
    init(content: String, time: String, timestamp: Int, type: ChatMessageType) {
        self.content = content
        self.time = time
        self.timestamp = timestamp
        self.isSender = true
        self.type = type
    }
    
}
