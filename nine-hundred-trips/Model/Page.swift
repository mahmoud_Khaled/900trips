//
//  Content.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct Page: Codable {
    var content: String
}
