//
//  FlightSeatCompany.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class FlightSeatType: Codable, CustomStringConvertible {
    let id: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
    }
    
    var description: String {
        return name
    }
}
