//
//  AllServiceModel.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AddOfferData: Codable {
    
    let services: [Service]
    let flightCompanies: [FlightCompany]
    let flightSeatTypes: [FlightSeatType]
    let currencies: [Currency]
    let countries: [Country]
    
    enum CodingKeys: String, CodingKey {
        case services
        case flightCompanies = "flight_companies"
        case flightSeatTypes = "flight_seat_types"
        case currencies, countries
    }
}

struct AddOfferDataViewModel {
    
    let services: [ServiceViewModel]
    let flightCompnaies: [FlightCompany]
    let flightSeatTypes: [FlightSeatType]
    let currencies: [CurrencyViewModel]
    let countries: [CountryViewModel]
    
    init(data: AddOfferData) {
        services = data.services.map { ServiceViewModel(service: $0) }
        flightCompnaies = data.flightCompanies
        flightSeatTypes = data.flightSeatTypes
        currencies = data.currencies.map { CurrencyViewModel(country: $0) }
        countries = data.countries.map { CountryViewModel(country: $0) }
    }
}
