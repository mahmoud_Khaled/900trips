//
//  Company.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Company: Codable {
    let id: Int
    let name: String
    let imageUrl: String
    let country: String
    let phone: String
    let phone1: String
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case imageUrl = "image_url"
        case country, phone, phone1
    }
}
