//
//  CountryAndCurrency.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 18/08/1440 AH.
//  Copyright © 1440 Ibtdi. All rights reserved.
//

import Foundation

class CountryAndCurrency: Codable {
    let countries: [Country]
    let currencies: [Currency]
}
