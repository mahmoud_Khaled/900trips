//
//  PlaceCatgory.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class PlaceCategory: Codable {
    var id: Int?
    var name: String?
    var photo: String?
}

struct PlaceCategoryViewModel: CustomStringConvertible {
    let id: Int
    let name: String
    let photo: URL?
    
    init(placeCategory: PlaceCategory) {
        id = placeCategory.id ?? 0
        name = placeCategory.name ?? ""
        photo = URL(string: placeCategory.photo ?? "")
    }
    
    var description: String {
        return name 
    }

}
