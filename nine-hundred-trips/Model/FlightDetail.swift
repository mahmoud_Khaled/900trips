//
//  FlightDetail.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class FlightDetail {
    
    let title: String
    let value: String
    let imageUrl: URL?
    let imageIsHidden: Bool
    let offerRate: Double?
    
    init(title: String, value: String, imageUrl: String?, imageIsHidden: Bool, offerRate: Double?) {
        self.title = title
        self.value = value
        self.imageUrl = URL(string: imageUrl ?? "")
        self.imageIsHidden = imageIsHidden
        self.offerRate = offerRate
    }
    
    static func createDetails(offerNumber: Int, flightCompanyName: String, flightType: String, offerNights: String, flightCompanyImage: String, offerRate: Double ) -> [FlightDetail] {
        
        var flightDetailsArray: [FlightDetail] = []
        let offerNumberString = String(offerNumber)
        
        let offerNumber = FlightDetail(title: "offerNumber".localized(), value: offerNumberString, imageUrl: nil, imageIsHidden: true, offerRate: nil)
        
        let flightCompany = FlightDetail(title: "flightCompany".localized(), value: flightCompanyName, imageUrl: flightCompanyImage, imageIsHidden: false, offerRate: nil)
        
        let flightType = FlightDetail(title: "flightType".localized(), value: flightType, imageUrl: nil, imageIsHidden: true, offerRate: nil)
        
        let offerNights = FlightDetail(title: "offerNights".localized(), value: offerNights, imageUrl: nil, imageIsHidden: true, offerRate: nil)
        
        let offerRate = FlightDetail(title: "offerRate".localized(), value: "", imageUrl: nil, imageIsHidden: true, offerRate: offerRate)
        
        flightDetailsArray.append(offerNumber)
        flightDetailsArray.append(flightCompany)
        flightDetailsArray.append(flightType)
        flightDetailsArray.append(offerNights)
        flightDetailsArray.append(offerRate)
        
        return flightDetailsArray
        
    }
    
}
