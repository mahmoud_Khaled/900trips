//
//  RequestQuotation.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct RequestQuotation {
    let departureCityId: Int
    let arrivalCityId: Int
    let isTransit: Bool
    let adultPassengersCount: String
    let childPassengersCount: String
    let babyPassengersCount: String
    let residenceType: String
    let roomsCount: String
    let personsInRoom: String
    let description: String
    let name: String
    let phone: String
    let email: String
    
    func toParameters() -> [String: Any] {
        return [
            "from_city": departureCityId,
            "to_city": arrivalCityId,
            "transit": isTransit ? 1 : 0,
            "adult_passengers": adultPassengersCount,
            "child_passengers": childPassengersCount,
            "baby_passengers": babyPassengersCount,
            "residence_type": residenceType,
            "rooms": roomsCount,
            "persons_in_room": personsInRoom,
            "description": description,
            "name": name,
            "phone": phone.englishDigits,
            "email": email
        ]
    }
}
