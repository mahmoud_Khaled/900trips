//
//  Packages.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/29/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class PackageAndSubscription: Codable {
    let current: SubscriptionPackage
    let packages: [Package]
}

// MARK: - Current
class SubscriptionPackage: Codable {
    
    let packageName: String
    let packageDays: Int
    let packagePrice: Int
    let daysLeft: Int
    let daysOfPackage: Int
    let offers: Int
    let advOffers: Int
    let totalPackageoffers: Int
    let totalSpecialPackageOffers: Int
    
    enum CodingKeys: String, CodingKey {
        case packageName = "package_name"
        case packageDays = "package_days"
        case packagePrice = "package_price"
        case daysLeft = "days_left"
        case daysOfPackage = "days_of_package"
        case offers
        case advOffers = "adv_offers"
        case totalPackageoffers = "package_offers"
        case totalSpecialPackageOffers = "package_adv_offers"
    }
}

// MARK: - Package
class Package: Codable {
    let id: Int
    let currencyID, name, price, days: String
    let offers, specialOffers: String
    let currency: Currency
    
    enum CodingKeys: String, CodingKey {
        case id
        case currencyID = "currency_id"
        case name, price, days, offers
        case specialOffers = "adv_offers"
        case currency = "get_currency"
    }
}


struct SubscriptionPackageViewModel {

    let name: String
    let totalDays: Int
    let remainingDays: Int
    let remainingDaysText: String
    let strokeEnd: Float
    let price: String
    let doneOffers: String
    let specialDoneOffers: String
    let remainingOffers: String
    let specialRemainingOffers: String
    
    init(subscriptionPackage: SubscriptionPackage) {
        name = subscriptionPackage.packageName == "" ? "thereIsNoSubscriptions".localized() : subscriptionPackage.packageName
        totalDays = subscriptionPackage.packageDays == 0 ? 0 : subscriptionPackage.packageDays
        remainingDays = subscriptionPackage.daysLeft == 0 ?  0 : subscriptionPackage.daysLeft
        strokeEnd = totalDays == 0 ? 0.0 : Float(Float(remainingDays) / Float(totalDays)).rounded(toPlaces: 2)
        price = "\(subscriptionPackage.packagePrice) \("aed".localized())"
        remainingDaysText = subscriptionPackage.daysLeft == 0 ?  "0" : String(subscriptionPackage.daysLeft)
        doneOffers = "\(subscriptionPackage.offers)"
        specialDoneOffers = "\(subscriptionPackage.advOffers)"
        remainingOffers = "\(subscriptionPackage.totalPackageoffers - subscriptionPackage.offers)"
        specialRemainingOffers = "\(subscriptionPackage.totalSpecialPackageOffers - subscriptionPackage.advOffers)"
    }

}

struct PackageViewModel {
    let id: Int
    let name: String
    let price: String
    let currencyName: String
    let currencyId: Int
    let currencyCode: String
    let days: String
    let offers: String
    let specialOffers: String
    
    init(package: Package) {
        id = package.id
        name = package.name
        currencyCode = package.currency.code ?? ""
        currencyId = package.currency.id ?? 0
        currencyName = package.currency.name ?? ""
        price = "\(package.price) \(currencyCode)"
        days = package.days
        offers = "\(package.offers) \("packageOffers".localized()) \("for".localized()) \(days) \("days".localized())"
        specialOffers = "\(package.specialOffers) \("special".localized()) \("packageOffers".localized())"
    }
}
