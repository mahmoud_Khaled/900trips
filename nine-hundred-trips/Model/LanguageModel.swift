//
//  Language.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct LanguageModel {
    let code: String
    let name: String
    var isSelected: Bool
}
