//
//  Reservations.swift
//  nine-hundred-trips
//
//  Created by Alshimaa on 12/11/1440 AH.
//  Copyright © 1440 Ibtdi. All rights reserved.
//

import Foundation


class Reservation: Codable {
    
    let id: Int
    let title: String
    let persons: String
    let phone: String
    let time: Int
    let price: String
    let imageUrl: String?
    let username: String
    let companyName: String
    let userPhoto: String
    let userId: String
    let companyId: String
    let offerId: Int
    let offerRating: Int
    let isRated: Int
    
    enum CodingKeys: String, CodingKey {
        case id, title, persons, phone, time, price
        case imageUrl = "image_url"
        case username
        case companyName = "company_name"
        case userPhoto = "user_photo"
        case userId = "user_id"
        case companyId = "company_id"
        case offerId = "offer_id"
        case offerRating = "offer_rating"
        case isRated = "is_rated"
    }
    
    
}


struct ReservationViewModel {
    
    let id: Int
    let title: String
    let persons: String
    let phone: String
    let price: String
    let imageUrl: URL?
    let username: String
    let companyName: String
    let userId: Int
    let companyId: Int
    let offerId: Int
    
    var isRated: Bool = false
    
    init(reservation: Reservation) {
        id = reservation.id
        title = reservation.title
        persons = reservation.persons
        phone = reservation.phone
        price = reservation.price
        imageUrl = URL(string: reservation.imageUrl ?? "")
        username  = reservation.username
        companyName = reservation.companyName
        userId = Int(reservation.userId) ?? 0
        companyId = Int(reservation.companyId) ?? 0
        offerId = reservation.offerId
        isRated = reservation.isRated == 1
    }
}
