//
//  Order.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Order: Codable {
    let id: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "order_id"
    }
}
