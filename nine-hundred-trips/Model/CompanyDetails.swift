//
//  CompanyDetails.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompanyDetails: Codable {
    
    let company: User
    let offers: [Offer]
    
}
