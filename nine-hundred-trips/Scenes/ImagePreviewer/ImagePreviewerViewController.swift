//
//  ImagePreviewerViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/31/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ImagePreviewerViewController: UIViewController {
    
    private let mainView = ImagePreviewerView()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    init(imageUrl: URL?) {
        mainView.configure(url: imageUrl)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        setupGestureRecgonizers()
        addTargets()
    }
    
    private func setupScrollView() {
        mainView.scrollView.delegate = self
        mainView.scrollView.minimumZoomScale = 1.0
        mainView.scrollView.maximumZoomScale = 6.0
    }
    
    private func setupGestureRecgonizers() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGesture.cancelsTouchesInView = false
        tapGesture.numberOfTapsRequired = 1
        mainView.scrollView.addGestureRecognizer(tapGesture)
        mainView.scrollView.isUserInteractionEnabled = true
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageDoubleTapped(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        mainView.scrollView.addGestureRecognizer(doubleTapGesture)
        
        tapGesture.require(toFail: doubleTapGesture)
    }
    
    @objc private func imageTapped() {
        mainView.scrollView.setZoomScale(0.0, animated: true)
    }
    
    @objc private func imageDoubleTapped(_ recognizer: UITapGestureRecognizer) {
        if mainView.scrollView.zoomScale == 1 {
            mainView.scrollView.zoom(to: zoomRectForScale(scale: mainView.scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            mainView.scrollView.setZoomScale(1, animated: true)
        }
    }
    
    private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = mainView.imageView.frame.size.height / scale
        zoomRect.size.width  = mainView.imageView.frame.size.width  / scale
        let newCenter = mainView.imageView.convert(center, from: mainView.scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    private func addTargets() {
        mainView.closeButton.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ImagePreviewerViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mainView.imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        let boundsSize = scrollView.bounds.size
        var frameToCenter = mainView.imageView.frame
        
        let widthDiff  = boundsSize.width  - frameToCenter.size.width
        let heightDiff = boundsSize.height - frameToCenter.size.height
        frameToCenter.origin.x = (widthDiff  > 0) ? widthDiff  / 2 : 0
        frameToCenter.origin.y = (heightDiff > 0) ? heightDiff / 2 : 0
        
        mainView.imageView.frame = frameToCenter
    }
}
