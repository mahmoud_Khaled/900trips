//
//  ReservationRequestInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestReservationInteractor: RequestReservationInteractorInputProtocol {
    
    weak var presenter: RequestReservationInteractorOutputProtocol?
    private let reservationWorker = ReservationWorker()
    
    func requestReservation(parameters: [String: Any]) {
        reservationWorker.requestReservation(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRequestReservation(with: result)
        }
    }
}
