//
//  ReservationRequestViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestReservationViewController: UIViewController, RequestReservationViewProtocol {
    
    private let mainView = RequestReservationView()
    var presenter: RequestReservationPresenterProtocol!
    
    private var offerDetails: OfferDetailsViewModel
    
    init(offerDetails: OfferDetailsViewModel) {
        self.offerDetails = offerDetails
        mainView.updateUI(offerDetails: offerDetails)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "requestReservation".localized()
        setupNavigationBarStyle()
        addtargets()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func addtargets() {
        mainView.sendRequestButton.addTarget(self, action: #selector(reservationRequestButtonTapped), for: .touchUpInside)
        mainView.personsNumberTextField.addTarget(self, action: #selector(personsNumberTextFieldDidEndEditing), for: .editingChanged)
    }
    
    @objc private func reservationRequestButtonTapped() {
        presenter.requestReservation(offerId: offerDetails.id, personsNumber: mainView.personsNumberTextField.text!, phone: mainView.phoneNumberTextField.text!)
    }
    
    @objc private func personsNumberTextFieldDidEndEditing() {
        let totalPrice = presenter.getTotalPriceOfPersons(personsNumber: Int(mainView.personsNumberTextField.text!) ?? 0, offerPrice: offerDetails.adultPrice)
        mainView.updateTotalPrice(totalPrice: totalPrice)
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func setEmptyFields() {
        mainView.setEmptyFields()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "confirmedReservationRequest".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.mainView.setEmptyFields()
        }
    }
}
