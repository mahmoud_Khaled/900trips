//
//  ReservationRequestProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol RequestReservationRouterProtocol: class {
    func showAlert(with message: String)
}

protocol RequestReservationPresenterProtocol: class {
    var view: RequestReservationViewProtocol? { get set }
    func getTotalPriceOfPersons(personsNumber: Int, offerPrice: Double) -> Double
    func requestReservation(offerId: Int, personsNumber: String, phone: String)
}

protocol RequestReservationInteractorInputProtocol: class {
    var presenter: RequestReservationInteractorOutputProtocol? { get set }
    func requestReservation(parameters: [String: Any])
}

protocol RequestReservationInteractorOutputProtocol: class {
    func didRequestReservation(with result: Result<Data>)
}

protocol RequestReservationViewProtocol: class {
    var presenter: RequestReservationPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func setEmptyFields()
    func showSuccessView()
}
