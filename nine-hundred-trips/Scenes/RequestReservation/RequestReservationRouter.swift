//
//  ReservationRequestRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestReservationRouter: RequestReservationRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(offerDetails: OfferDetailsViewModel) -> UIViewController {
        let view = RequestReservationViewController(offerDetails: offerDetails)
        let interactor = RequestReservationInteractor()
        let router = RequestReservationRouter()
        let presenter = RequestReservationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}
