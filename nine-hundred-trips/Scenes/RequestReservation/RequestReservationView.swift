//
//  ReservationRequestView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestReservationView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        scrolView.showsHorizontalScrollIndicator = false
        scrolView.showsVerticalScrollIndicator = false
        return scrolView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var offerContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var offerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "offerimage")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 30
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 20)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var firstLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    private lazy var personPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "personPrice".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var secondLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0).withAlphaComponent(0.15)
        return view
    }()
    
    private lazy var fieldsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "enterReservationData".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    lazy var personsNumberTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "numberofPersons".localized()
        textField.keyboardType = .asciiCapableNumberPad
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var totalPriceTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "totalPrice".localized()
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.numberOfLines = 2
        return label
    }()
    
    lazy var totalPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.numberOfLines = 2
        label.isHidden = true
        return label
    }()
    
    lazy var phoneNumberTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "phoneNumber".localized()
        textField.keyboardType = .asciiCapableNumberPad
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var sendRequestButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.setImage(UIImage(named: "ic_tag")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("sendRequest".localized(), for: .normal)
        button.tintColor = .white
        button.contentHorizontalAlignment = .center
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(offerContainerView)
        offerContainerView.addSubview(offerImageView)
        offerContainerView.addSubview(titleLabel)
        offerContainerView.addSubview(firstLineView)
        offerContainerView.addSubview(personPriceLabel)
        offerContainerView.addSubview(priceLabel)
        containerView.addSubview(secondLineView)
        containerView.addSubview(fieldsLabel)
        containerView.addSubview(personsNumberTextField)
        containerView.addSubview(totalPriceTextLabel)
        containerView.addSubview(totalPriceLabel)
        containerView.addSubview(phoneNumberTextField)
        containerView.addSubview(sendRequestButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupOfferContainerViewConstraints() {
        NSLayoutConstraint.activate([
            offerContainerView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 30),
            offerContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            offerContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            offerContainerView.heightAnchor.constraint(equalToConstant: 156)
        ])
    }
    
    private func setupOfferImageViewConstraints() {
        NSLayoutConstraint.activate([
            offerImageView.topAnchor.constraint(equalTo: offerContainerView.topAnchor, constant: 15),
            offerImageView.leadingAnchor.constraint(equalTo: offerContainerView.leadingAnchor, constant: 17),
            offerImageView.widthAnchor.constraint(equalToConstant: 60),
            offerImageView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: offerImageView.trailingAnchor, constant: 17),
            titleLabel.trailingAnchor.constraint(equalTo: offerContainerView.trailingAnchor, constant: -8),
            titleLabel.centerYAnchor.constraint(equalTo: offerImageView.centerYAnchor)
        ])
    }
    
    private func setupFirstLineViewConstraints() {
        NSLayoutConstraint.activate([
            firstLineView.topAnchor.constraint(equalTo: offerImageView.bottomAnchor, constant: 18),
            firstLineView.leadingAnchor.constraint(equalTo: offerContainerView.leadingAnchor, constant: 0),
            firstLineView.trailingAnchor.constraint(equalTo: offerContainerView.trailingAnchor, constant: 0),
            firstLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupPersonPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            personPriceLabel.leadingAnchor.constraint(equalTo: offerContainerView.leadingAnchor, constant: 17),
            personPriceLabel.topAnchor.constraint(equalTo: firstLineView.bottomAnchor, constant: 10),
        ])
    }
    
    private func setupPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            priceLabel.leadingAnchor.constraint(equalTo: personPriceLabel.trailingAnchor, constant: 0),
            priceLabel.centerYAnchor.constraint(equalTo: personPriceLabel.centerYAnchor)
        ])
    }
    
    private func setupSecondLineViewConstraints() {
        NSLayoutConstraint.activate([
            secondLineView.topAnchor.constraint(equalTo: offerContainerView.bottomAnchor, constant: 37),
            secondLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            secondLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            secondLineView.heightAnchor.constraint(equalToConstant: 2)
        ])
    }
    
    private func setupFieldsLabelConstraints() {
        NSLayoutConstraint.activate([
            fieldsLabel.topAnchor.constraint(equalTo: secondLineView.bottomAnchor, constant: 25),
            fieldsLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            fieldsLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20)
        ])
    }
    
    private func setupPersonsNumberTextFieldConstraints() {
        NSLayoutConstraint.activate([
            personsNumberTextField.topAnchor.constraint(equalTo: fieldsLabel.bottomAnchor, constant: 20),
            personsNumberTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            personsNumberTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            personsNumberTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupTotalPriceTextLabelConstraints() {
        NSLayoutConstraint.activate([
            totalPriceTextLabel.topAnchor.constraint(equalTo: personsNumberTextField.bottomAnchor, constant: 20),
            totalPriceTextLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20)
        ])
    }
    
    private func setupTotalPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            totalPriceLabel.leadingAnchor.constraint(equalTo: totalPriceTextLabel.trailingAnchor, constant: 0),
            totalPriceLabel.centerYAnchor.constraint(equalTo: totalPriceTextLabel.centerYAnchor)
        ])
    }
    
    private func setupPhoneNumberTextFieldConstraints() {
        NSLayoutConstraint.activate([
            phoneNumberTextField.topAnchor.constraint(equalTo: totalPriceTextLabel.bottomAnchor, constant: 20),
            phoneNumberTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            phoneNumberTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            phoneNumberTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupSendRequestButtonConstraints() {
        NSLayoutConstraint.activate([
            sendRequestButton.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 30),
            sendRequestButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            sendRequestButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            sendRequestButton.heightAnchor.constraint(equalToConstant: 50),
            sendRequestButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupOfferContainerViewConstraints()
        setupOfferImageViewConstraints()
        setupTitleLabelConstraints()
        setupFirstLineViewConstraints()
        setupPersonPriceLabelConstraints()
        setupPriceLabelConstraints()
        setupSecondLineViewConstraints()
        setupFieldsLabelConstraints()
        setupPersonsNumberTextFieldConstraints()
        setupTotalPriceTextLabelConstraints()
        setupTotalPriceLabelConstraints()
        setupPhoneNumberTextFieldConstraints()
        setupSendRequestButtonConstraints()
    }
    
    func updateUI(offerDetails: OfferDetailsViewModel) {
        offerImageView.load(url: offerDetails.images.compactMap { $0 }.first)
        titleLabel.text = offerDetails.title
        priceLabel.text = offerDetails.adultPriceString
    }
    
    func setEmptyFields() {
        personsNumberTextField.text = ""
        phoneNumberTextField.text = ""
        totalPriceLabel.isHidden = true
    }
    
    func updateTotalPrice(totalPrice: Double) {
        if totalPrice == 0 {
            totalPriceLabel.isHidden = true
        } else {
            totalPriceLabel.isHidden = false
            totalPriceLabel.text = "\(totalPrice.cleanValue)\(" / ")\(personsNumberTextField.text!)\(" persons".localized())"
        }
    }
}
