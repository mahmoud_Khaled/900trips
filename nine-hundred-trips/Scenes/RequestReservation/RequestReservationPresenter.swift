//
//  ReservationRequestPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 7/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RequestReservationPresenter: RequestReservationPresenterProtocol, RequestReservationInteractorOutputProtocol {    
    
    weak var view: RequestReservationViewProtocol?
    private let interactor: RequestReservationInteractorInputProtocol
    private let router: RequestReservationRouterProtocol
    
    init(view: RequestReservationViewProtocol, interactor: RequestReservationInteractorInputProtocol, router: RequestReservationRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getTotalPriceOfPersons(personsNumber: Int, offerPrice: Double) -> Double {
        return Double(personsNumber) * offerPrice
    }
    
    func requestReservation(offerId: Int, personsNumber: String, phone: String) {
        guard !(personsNumber.isEmpty) else {
            return router.showAlert(with: "")
        }
        guard !(phone.isEmpty) else {
            return router.showAlert(with: "emptyPhone".localized())
        }
        view?.showActivityIndicator()
        let parameters: [String: Any] = [
            "offer_id": offerId,
            "persons": personsNumber,
            "phone": phone
        ]
        interactor.requestReservation(parameters: parameters)
    }
    
    func didRequestReservation(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
