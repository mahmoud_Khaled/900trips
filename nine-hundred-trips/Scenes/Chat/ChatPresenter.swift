//
//  ChatPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ChatPresenter: ChatPresenterProtocol, ChatInteractorOutputProtocol {
    
    weak var view: ChatViewProtocol?
    private let interactor: ChatInteractorInputProtocol
    private let router: ChatRouterProtocol
    private let userId: Int
    private let rate: String
    
    private var messages = [MessageViewModel]()

    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    var numberOfRows: Int {
        return messages.count
    }
    
    private var currentPage = 1
    private var totalPages = 1
    private var conversationId: Int?
    private let userImage: String
    private let userName: String
    
    init(view: ChatViewProtocol, interactor: ChatInteractorInputProtocol, router: ChatRouterProtocol, userId: Int, conversationId: Int?, userImage: URL?, userName: String, rate: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.userId = userId
        self.conversationId = conversationId
        self.userImage = userImage?.absoluteString ?? ""
        self.userName = userName
        self.rate = rate
        NotificationCenter.default.addObserver(self, selector: #selector(handleNewMessage(_:)), name: .newMessage, object: nil)
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getMessages(with: userId, page: currentPage)
    }
    
    func didFetchMessages(with result: Result<[Message]>, totalPages: Int) {
        view?.hideActivityIndicator()
        self.totalPages = totalPages
        view?.toggleIsPaginating()
        switch result {
        case .success(let messages):
            self.messages.append(contentsOf: messages.map {
                let viewModel = MessageViewModel(message: $0, dateFormatter: dateFormatter)
                viewModel.isSent = viewModel.isSender
                return viewModel
            })
            view?.reloadData()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<MessageViewModel>, at indexPath: IndexPath) {
        cell.configure(model: messages[indexPath.row])
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            interactor.getMessages(with: userId, page: currentPage)
        }
    }
    
    func contentForMessage(at indexPath: IndexPath) -> String {
        return messages[indexPath.row].content
    }
    
    func uuidForMessage(at indexPath: IndexPath) -> String {
        return messages[indexPath.row].id
    }
    
    func sendMessage(message: String) {
        let currentDate = Date()
        let timestamp = currentDate.timeIntervalSince1970
        let viewModel = MessageViewModel(content: message, time: dateFormatter.string(from: currentDate), timestamp: Int(timestamp), type: .text)
        messages.insert(viewModel, at: 0)
        view?.reloadData()
        view?.setEmptyTextView()
        view?.scrollToFirstItem()
        interactor.sendMessage(to: userId, content: viewModel.content, messageIdentifier: viewModel.id)
    }
    
    func sendImage(path: URL, file: File) {
        let currentDate = Date()
        let timestamp = currentDate.timeIntervalSince1970
        let viewModel = MessageViewModel(content: path.absoluteString, time: dateFormatter.string(from: currentDate), timestamp: Int(timestamp), type: .photo)
        
        messages.insert(viewModel, at: 0)
        view?.reloadData()
        view?.scrollToFirstItem()
        interactor.sendImage(to: userId, file: file, messageIdentifier: viewModel.id)
    }
    
    func didSendMessage(with result: Result<Data>, messageIdentifier: String) {
        switch result {
        case .success(let data):
            guard let dictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else { return }
            guard let messageId = dictionary?["message_id"] as? Int else { return }
            guard let message = messages.first(where: { $0.id == messageIdentifier }) else { return }
            message.isSent = true
            conversationId = messageId
            NotificationCenter.default.post(name: .updateConversations, object: nil, userInfo: [
                "conversationId": conversationId ?? 0,
                "userId": userId,
                "userName": userName,
                "userImage": userImage,
                "time": message.timestamp,
                "content": message.type == .photo ? "photo".localized() : message.content,
                "rate": rate
            ])
            view?.reloadData()
        case .failure:
            break
        }
    }
    
    func backButtonTapped() {
        router.dismiss()
    }
    
    func cameraButtonTapped() {
        router.presentImagePicker()
    }
    
    @objc private func handleNewMessage(_ notification: NSNotification) {
        guard
            let message = notification.userInfo?["message"] as? Message,
            let otherUserId = message.senderUser.id
        else { return }
        
        if otherUserId == userId {
            let viewModel = MessageViewModel(message: message, dateFormatter: dateFormatter)
            messages.insert(viewModel, at: 0)
            view?.reloadData()
        }
    }
    
    func messageType(at indexPath: IndexPath) -> ChatMessageType {
        return messages[indexPath.row].type
    }
    
    func isSender(at indexPath: IndexPath) -> Bool {
        return messages[indexPath.row].isSender
    }
    
    func imageSelected(at indexPath: IndexPath) {
        let imagePath = messages[indexPath.row].content
        router.presentImagePreviewer(with: URL(string: imagePath))
        
        
    }
    
    func rateButtonTapped(rate: Double) {
        view?.showActivityIndicator()
        interactor.rate(userId: userId, rate: rate)
    }
    
    func didRateUser(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            break
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
}
