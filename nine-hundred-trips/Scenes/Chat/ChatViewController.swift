//
//  ChatViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, ChatViewProtocol {
    
    private let mainView = ChatView()
    var presenter: ChatPresenterProtocol!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var cachedSizes = [String: CGFloat]()
    private var cachedSizeCounter = 0
    private var isPaginating = false
    let keyboard = Typist()
    let userId: Int
    
    init(userId: Int, userImage: URL?, username: String) {
        self.userId = userId
        mainView.configure(userImage: userImage, username: username)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handleStatusBarTapped), name: .statusBarTapped, object: nil)
        setupCollectionView()
        addTargets()
        presenter.viewDidLoad()
        setupDismissTapGesture()
        setupKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    @objc private func handleStatusBarTapped() {
        //Actually do nothing since it's bugged and I've no idea why.
    }
    
    private func setupCollectionView() {
        mainView.chatCollectionView.dataSource = self
        mainView.chatCollectionView.delegate = self
    }
    
    private func addTargets() {
        mainView.sendButton.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
        mainView.backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        mainView.cameraButton.addTarget(self, action: #selector(cameraButtonTapped), for: .touchUpInside)
        mainView.rateButton.addTarget(self, action: #selector(rateButtonTapped), for: .touchUpInside)
    }
    
    private func setupDismissTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    private func setupKeyboardObservers() {
        keyboard
            .toolbar(scrollView: mainView.chatCollectionView)
            .on(event: .willChangeFrame) { [weak self] options in
                guard let self = self else { return }
                guard self.mainView.sendMessageTextView.isFirstResponder else { return }
                let height = options.endFrame.height
                self.mainView.bottomContainerViewBottomConstraint.constant = -max(0, height - self.view.safeAreaInsets.bottom)
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseIn, animations: {
                    self.mainView.layoutIfNeeded()
                }, completion: { _ in })
            }
            .on(event: .willHide) { [weak self] options in
                guard let self = self else { return }
                self.mainView.bottomContainerViewBottomConstraint.constant = 0
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseIn, animations: {
                    self.mainView.layoutIfNeeded()
                }, completion: { _ in })
            }
            .start()
    }
    
    @objc private func sendButtonTapped() {
        guard !mainView.sendMessageTextView.text.isEmpty else { return }
        presenter.sendMessage(message: mainView.sendMessageTextView.text)
    }
    
    @objc private func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    @objc private func cameraButtonTapped() {
        presenter.cameraButtonTapped()
    }
    
    @objc private func rateButtonTapped() {
        presenter.rateButtonTapped(rate: mainView.starsView.rating)
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func reloadData() {
        mainView.chatCollectionView.reloadData()
    }
    
    func setEmptyTextView() {
        mainView.sendMessageTextView.text = ""
    }
    
    func scrollToFirstItem() {
        mainView.chatCollectionView.setContentOffset(.zero, animated: false)
    }
    
    func toggleIsPaginating() {
        isPaginating = false
    }
    
    deinit {
        keyboard.stop()
    }
}


extension ChatViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if presenter.isSender(at: indexPath) {
            switch presenter.messageType(at: indexPath) {
            case .text:

                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OutgoingTextCell.className, for: indexPath) as! OutgoingTextCell
                presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
                return cell

            case .photo:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OutgoingImageCell.className, for: indexPath) as! OutgoingImageCell
                presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
                return cell
            }
        } else {
            switch presenter.messageType(at: indexPath) {
            case .text:

                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IncomingTextCell.className, for: indexPath) as! IncomingTextCell
                presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
                return cell

            case .photo:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IncomingImageCell.className, for: indexPath) as! IncomingImageCell
                presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if presenter.messageType(at: indexPath) == .photo {
            return CGSize(width: view.frame.width, height: 250)
        } else {
            if let height = cachedSizes[presenter.uuidForMessage(at: indexPath)] {
                return CGSize(width: view.frame.width, height: height)
            } else {
                let height = calculateTextHeight(at: indexPath)
                cachedSizes[presenter.uuidForMessage(at: indexPath)] = height
                return CGSize(width: view.frame.width, height: height)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        UIView.performWithoutAnimation {
            cell.layoutIfNeeded()
        }
        
        if indexPath.row == presenter.numberOfRows - 1 && !isPaginating {
            presenter.loadNextPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if presenter.messageType(at: indexPath) == .text {
            dismissKeyboard()
        } else {
            presenter.imageSelected(at: indexPath)
        }
    }
    
    private func calculateTextHeight(at indexPath: IndexPath) -> CGFloat {
        let approximateSize = CGSize(width: view.frame.width - 88, height: .greatestFiniteMagnitude)
        let estimatedHeight = NSString(string: presenter.contentForMessage(at: indexPath)).boundingRect(with: approximateSize, options: .usesLineFragmentOrigin, attributes: [.font: DinNextFont.regular.getFont(ofSize: 14)], context: nil).height
        return estimatedHeight + 40
    }
    
}

extension ChatViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isKind(of: UIButton.self) == true || touch.view?.isDescendant(of: mainView.bottomContainerView) == true || touch.view?.isDescendant(of: mainView.chatCollectionView) == true {
            return false
        } else {
            return true
        }
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            guard let imagePath = info[.imageURL] as? URL else { return }
            guard let editedImage = info[.editedImage] as? UIImage else { return }
            self.presenter.sendImage(path: imagePath, file: ("photo.jpg", "photo", MimeType.jpg, editedImage.jpegData(compressionQuality: 0.5)!))
        }
    }
    
}
