//
//  ChatView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class ChatView: UIView {
    
    lazy var navigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        return view
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_back")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        button.imageView!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.imageView!.heightAnchor.constraint(equalToConstant: 20),
            button.imageView!.widthAnchor.constraint(equalToConstant: 20)
        ])
        return button
    }()
    
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true
        imageView.image = UIImage()
        return imageView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 12)
        label.textColor = .white
        label.text = ""
        return label
    }()
    
    private lazy var rateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor(red: 29/255, green: 160/255, blue: 192/255, alpha: 1.0), alpha: 0.14, x: 0, y: 3, blur: 6, spread: 0)
        return view
    }()
    
    private lazy var rateConversationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "rateConversation".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.minimumScaleFactor = 0.75
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1.0)
        return label
    }()
    
    lazy var starsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var starsView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.starSize = 23
        view.settings.fillMode = .half
        view.settings.filledColor = .coral
        view.settings.filledBorderColor = .coral
        view.settings.emptyColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.emptyBorderColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.starMargin = 0
        view.settings.totalStars = 5
        view.rating = 1.0
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    lazy var rateButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 15
        button.setTitle("done".localized(), for: .normal)
        return button
    }()
    
    lazy var chatCollectionView: UICollectionView = {
        let collectionViewFlowLayout = CustomFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(OutgoingTextCell.self, forCellWithReuseIdentifier: OutgoingTextCell.className)
        collectionView.register(IncomingTextCell.self, forCellWithReuseIdentifier: IncomingTextCell.className)
        collectionView.register(OutgoingImageCell.self, forCellWithReuseIdentifier: OutgoingImageCell.className)
        collectionView.register(IncomingImageCell.self, forCellWithReuseIdentifier: IncomingImageCell.className)
        collectionView.transform = CGAffineTransform(scaleX: 1, y: -1)
        collectionView.semanticContentAttribute = .forceLeftToRight
        collectionView.keyboardDismissMode = .interactive
        collectionView.scrollsToTop = true
        return collectionView
    }()
    
    lazy var bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .darkShadow
        return view
    }()
    
    private lazy var horizontalLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var sendMessageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 23
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0).cgColor
        view.backgroundColor = .white
        return view
    }()
    
    lazy var sendMessageTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .white
        textView.font = DinNextFont.regular.getFont(ofSize: 20)
        textView.textColor = .textColor
        textView.placeholder = "yourMessageHere".localized()
        textView.minHeight = 46
        textView.maxHeight = 100
        textView.scrollsToTop = false
        return textView
    }()
    
    lazy var cameraButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "camera")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "send")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    var bottomContainerViewBottomConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
        if UserDefaultsHelper.userType == .company {
            rateView.isHidden = true
            navigationBarView.layer.applySketchShadow(color: .black, alpha: 0.14, x: 0, y: 3, blur: 6, spread: 0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(navigationBarView)
        navigationBarView.addSubview(userImageView)
        navigationBarView.addSubview(usernameLabel)
        navigationBarView.addSubview(backButton)
        addSubview(rateView)
        rateView.addSubview(rateConversationLabel)
        starsContainerView.addSubview(starsView)
        rateView.addSubview(starsContainerView)
        rateView.addSubview(rateButton)
        addSubview(chatCollectionView)
        addSubview(horizontalLineView)
        addSubview(bottomContainerView)
        bottomContainerView.addSubview(sendButton)
        bottomContainerView.addSubview(sendMessageContainerView)
        sendMessageContainerView.addSubview(sendMessageTextView)
        sendMessageContainerView.addSubview(cameraButton)
    }
    
    private func setupNavigationBarViewConstraints() {
        NSLayoutConstraint.activate([
            navigationBarView.topAnchor.constraint(equalTo: topAnchor),
            navigationBarView.leadingAnchor.constraint(equalTo: leadingAnchor),
            navigationBarView.trailingAnchor.constraint(equalTo: trailingAnchor),
            navigationBarView.heightAnchor.constraint(equalToConstant: UIDevice.hasTopNotch ? 116 : 96)
        ])
    }
    
    private func setupBackButtonConstraints() {
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: navigationBarView.leadingAnchor, constant: 0),
            backButton.centerYAnchor.constraint(equalTo: navigationBarView.centerYAnchor, constant: UIDevice.hasTopNotch ? 20 : 0),
            backButton.heightAnchor.constraint(equalToConstant: 40),
            backButton.widthAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupCompanyImageViewConstraints() {
        NSLayoutConstraint.activate([
            userImageView.topAnchor.constraint(equalTo: navigationBarView.topAnchor, constant: UIDevice.hasTopNotch ? 45 : 25),
            userImageView.centerXAnchor.constraint(equalTo: navigationBarView.centerXAnchor),
            userImageView.heightAnchor.constraint(equalToConstant: 40),
            userImageView.widthAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupCompanyNameLabelConstraints() {
        NSLayoutConstraint.activate([
            usernameLabel.topAnchor.constraint(equalTo: userImageView.bottomAnchor, constant: 3),
            usernameLabel.centerXAnchor.constraint(equalTo: navigationBarView.centerXAnchor)
        ])
    }
    
    private func setupRateViewConstraints() {
        NSLayoutConstraint.activate([
            rateView.topAnchor.constraint(equalTo: navigationBarView.bottomAnchor),
            rateView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rateView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rateView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupRateConversationLabelConstraints() {
        NSLayoutConstraint.activate([
            rateConversationLabel.leadingAnchor.constraint(equalTo: rateView.leadingAnchor, constant: 8),
            rateConversationLabel.centerYAnchor.constraint(equalTo: rateView.centerYAnchor),
            rateConversationLabel.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupStarsViewConstraints() {
        NSLayoutConstraint.activate([
            starsView.topAnchor.constraint(equalTo: starsContainerView.topAnchor),
            starsView.leadingAnchor.constraint(equalTo: starsContainerView.leadingAnchor),
            starsView.trailingAnchor.constraint(equalTo: starsContainerView.trailingAnchor),
            starsView.bottomAnchor.constraint(equalTo: starsContainerView.bottomAnchor),
        ])
    }
    
    private func setupStarsContainerViewConstraints() {
        NSLayoutConstraint.activate([
            starsContainerView.centerXAnchor.constraint(equalTo: rateView.centerXAnchor),
            starsContainerView.centerYAnchor.constraint(equalTo: rateView.centerYAnchor),
            starsContainerView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func setupDoneButtonConstraints() {
        NSLayoutConstraint.activate([
            rateButton.trailingAnchor.constraint(equalTo: rateView.trailingAnchor, constant: -8),
            rateButton.centerYAnchor.constraint(equalTo: rateView.centerYAnchor),
            rateButton.widthAnchor.constraint(equalToConstant: 90),
            rateButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupChatCollectionViewConstraints() {
        if UserDefaultsHelper.userType == .client {
           chatCollectionView.topAnchor.constraint(equalTo: rateView.bottomAnchor).isActive = true
        } else {
            chatCollectionView.topAnchor.constraint(equalTo: navigationBarView.bottomAnchor).isActive = true
        }
        NSLayoutConstraint.activate([
            chatCollectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chatCollectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
        ])
    }
    
    private func setupHorizontalLineViewConstraints() {
        NSLayoutConstraint.activate([
            horizontalLineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            horizontalLineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            horizontalLineView.bottomAnchor.constraint(equalTo: bottomContainerView.topAnchor),
            horizontalLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupBottomContainerViewConstraints() {
        bottomContainerViewBottomConstraint = bottomContainerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        NSLayoutConstraint.activate([
            bottomContainerViewBottomConstraint,
            bottomContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomContainerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomContainerView.topAnchor.constraint(equalTo: chatCollectionView.bottomAnchor)
        ])
    }
    
    private func setupSendButtonConstraints() {
        NSLayoutConstraint.activate([
            sendButton.trailingAnchor.constraint(equalTo: bottomContainerView.trailingAnchor, constant: -19),
            sendButton.widthAnchor.constraint(equalToConstant: 32),
            sendButton.heightAnchor.constraint(equalToConstant: 24),
            sendButton.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 25)
        ])
    }
    
    private func setupSendMessageContainerViewConstraints() {
        NSLayoutConstraint.activate([
            sendMessageContainerView.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 19),
            sendMessageContainerView.trailingAnchor.constraint(equalTo: sendButton.leadingAnchor, constant: -10),
            sendMessageContainerView.bottomAnchor.constraint(equalTo: bottomContainerView.bottomAnchor, constant: UIDevice.hasTopNotch ? -49 : -15),
            sendMessageContainerView.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 15)
        ])
    }
    
    private func setupeSendMessageTextViewConstraints() {
        NSLayoutConstraint.activate([
            sendMessageTextView.leadingAnchor.constraint(equalTo: sendMessageContainerView.leadingAnchor, constant: 8),
            sendMessageTextView.trailingAnchor.constraint(equalTo: sendButton.leadingAnchor, constant: -50),
            sendMessageTextView.topAnchor.constraint(equalTo: sendMessageContainerView.topAnchor),
            sendMessageTextView.bottomAnchor.constraint(equalTo: sendMessageContainerView.bottomAnchor),
        ])
    }
    
    private func setupCameraButtonConstraints() {
        NSLayoutConstraint.activate([
            cameraButton.trailingAnchor.constraint(equalTo: sendMessageContainerView.trailingAnchor, constant: -12),
            cameraButton.topAnchor.constraint(equalTo: sendMessageContainerView.topAnchor, constant: 10.5),
            cameraButton.widthAnchor.constraint(equalToConstant: 32),
            cameraButton.heightAnchor.constraint(equalToConstant: 24)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupNavigationBarViewConstraints()
        setupBackButtonConstraints()
        setupCompanyImageViewConstraints()
        setupCompanyNameLabelConstraints()
        setupRateViewConstraints()
        setupRateConversationLabelConstraints()
        setupStarsViewConstraints()
        setupStarsContainerViewConstraints()
        setupDoneButtonConstraints()
        setupChatCollectionViewConstraints()
        setupHorizontalLineViewConstraints()
        setupBottomContainerViewConstraints()
        setupSendButtonConstraints()
        setupSendMessageContainerViewConstraints()
        setupeSendMessageTextViewConstraints()
        setupCameraButtonConstraints()
    }
    
    func configure(userImage: URL?, username: String) {
        userImageView.load(url: userImage)
        usernameLabel.text = username
    }
    
}
