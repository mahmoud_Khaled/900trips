//
//  ChatRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChatRouter: ChatRouterProtocol {
    
    weak var viewController: UIViewController?
    
    private lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = (viewController as! ChatViewController)
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = .coral
        imagePicker.navigationBar.tintColor = .white
        imagePicker.navigationBar.barStyle = .black
        return imagePicker
    }()
    
    static func createModule(with userId: Int, userImage: URL?, username: String, conversationId: Int?, rate: String) -> UIViewController {
        let view = ChatViewController(userId: userId, userImage: userImage, username: username)
        let interactor = ChatInteractor()
        let router = ChatRouter()
        let presenter = ChatPresenter(view: view, interactor: interactor, router: router, userId: userId, conversationId: conversationId, userImage: userImage, userName: username, rate: rate)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }

    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentImagePicker() {
        viewController?.present(imagePicker, animated: true, completion: nil)
    }
    
    func presentImagePreviewer(with url: URL?) {
        viewController?.present(ImagePreviewerViewController(imageUrl: url), animated: true, completion: nil)
    }
}
