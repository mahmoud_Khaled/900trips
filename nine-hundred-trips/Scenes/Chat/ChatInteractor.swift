//
//  ChatInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChatInteractor: ChatInteractorInputProtocol {
    
    weak var presenter: ChatInteractorOutputProtocol?
    private let conversationsWorker = ConversationsWorker()
    
    func getMessages(with userId: Int, page: Int) {
        conversationsWorker.getMessages(with: userId, page: page) { [weak self] result, totalPages in
            guard let self = self else { return }
            self.presenter?.didFetchMessages(with: result, totalPages: totalPages)
        }
    }
    
    func sendMessage(to userId: Int, content: String, messageIdentifier: String) {
        conversationsWorker.sendMessage(to: userId, content: content) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didSendMessage(with: result, messageIdentifier: messageIdentifier)
        }
    }
    
    func sendImage(to userId: Int, file: File, messageIdentifier: String) {
        conversationsWorker.sendImage(to: userId, file: file) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didSendMessage(with: result, messageIdentifier: messageIdentifier)
        }
    }
    
    func rate(userId: Int, rate: Double) {
        conversationsWorker.rate(userId: userId, rate: rate) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRateUser(with: result)
        }
    }
}
