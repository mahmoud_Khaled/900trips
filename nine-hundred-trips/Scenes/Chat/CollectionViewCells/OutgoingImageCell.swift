//
//  OutgoingImageCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/29/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OutgoingImageCell: UICollectionViewCell, ConfigurableCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.coral.cgColor
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "offerimage"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [timeLabel, checkMarkImageView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 4
        return stackView
    }()
    
    private lazy var blackView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 0, alpha: 0.29)
        return view
    }()
    
    private lazy var checkMarkImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_check")?.withRenderingMode(.alwaysTemplate))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.heightAnchor.constraint(equalToConstant: 8).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        return imageView
    }()
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 10)
        label.text = "12:14"
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(imageView)
        containerView.addSubview(blackView)
        containerView.addSubview(stackView)
    }
    
    func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            containerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 34),
        ])
    }
    
    private func setupBlackViewConstraints() {
        NSLayoutConstraint.activate([
            blackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            blackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            blackView.heightAnchor.constraint(equalToConstant: 24),
            blackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor)
        ])
    }
    
    private func setupImageViewConstraints() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            imageView.heightAnchor.constraint(equalToConstant: 250),
            imageView.widthAnchor.constraint(equalToConstant: 250)
        ])
    }
    
    private func setupStackViewConstraints() {
        NSLayoutConstraint.activate([
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -6),
            stackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -6),
            stackView.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupBlackViewConstraints()
        setupImageViewConstraints()
        setupStackViewConstraints()
    }
    
    func configure(model: MessageViewModel) {
        containerView.transform = CGAffineTransform(scaleX: 1, y: -1)
        imageView.load(url: URL(string: model.content))
        timeLabel.text = model.time
        if model.isSent {
            checkMarkImageView.isHidden = false
        } else {
            checkMarkImageView.isHidden = true
        }
    }
}
