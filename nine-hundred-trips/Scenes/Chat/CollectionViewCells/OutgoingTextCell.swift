//
//  ChatCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OutgoingTextCell: UICollectionViewCell, ConfigurableCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.numberOfLines = 0
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [timeLabel, checkMarkImageView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 4
        return stackView
    }()
    
    private lazy var checkMarkImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_check")?.withRenderingMode(.alwaysTemplate))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.heightAnchor.constraint(equalToConstant: 8).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        return imageView
    }()
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 10)
        label.text = "12:14"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(contentLabel)
        containerView.addSubview(stackView)
    }
    
    func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            containerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
            containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 64)
        ])
    }
    
    private func setupContentLabelConstraints() {
        NSLayoutConstraint.activate([
            contentLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 4),
            contentLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            contentLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
        ])
    }
    
    private func setupStackViewConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentLabel.bottomAnchor, constant: 0),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -6),
            stackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -4),
            stackView.leadingAnchor.constraint(greaterThanOrEqualTo: containerView.leadingAnchor, constant: 6),
            stackView.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupContentLabelConstraints()
        setupStackViewConstraints()
    }
    
    func configure(model: MessageViewModel) {
        containerView.transform = CGAffineTransform(scaleX: 1, y: -1)
        contentLabel.text = model.content
        timeLabel.text = model.time
        checkMarkImageView.isHidden = !model.isSent
    }
}
