//
//  IncomingImageCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/29/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class IncomingImageCell: OutgoingImageCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        containerView.layer.borderColor = UIColor.gray.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            containerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -64)
        ])
    }
    
}
