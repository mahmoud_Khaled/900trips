//
//  IncomingTextCell.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/28/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class IncomingTextCell: OutgoingTextCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        containerView.backgroundColor = .babyGray
        contentLabel.textColor = .textColor
        timeLabel.textColor = .textColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            containerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 34),
            containerView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -64)
        ])
    }
    
}
