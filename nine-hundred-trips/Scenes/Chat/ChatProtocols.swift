//
//  ChatProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ChatRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
    func presentImagePicker()
    func presentImagePreviewer(with url: URL?)
}

protocol ChatPresenterProtocol: class {
    var view: ChatViewProtocol? { get set }
    var numberOfRows: Int { get }
    func viewDidLoad()
    func configure(cell: AnyConfigurableCell<MessageViewModel>, at indexPath: IndexPath)
    func loadNextPage()
    func contentForMessage(at indexPath: IndexPath) -> String
    func uuidForMessage(at indexPath: IndexPath) -> String
    func sendMessage(message: String)
    func sendImage(path: URL, file: File)
    func backButtonTapped()
    func cameraButtonTapped()
    func messageType(at indexPath: IndexPath) -> ChatMessageType
    func isSender(at indexPath: IndexPath) -> Bool
    func imageSelected(at indexPath: IndexPath)
    func rateButtonTapped(rate: Double)
}

protocol ChatInteractorInputProtocol: class {
    var presenter: ChatInteractorOutputProtocol? { get set }
    func getMessages(with userId: Int, page: Int)
    func sendMessage(to userId: Int, content: String, messageIdentifier: String)
    func sendImage(to userId: Int, file: File, messageIdentifier: String)
    func rate(userId: Int, rate: Double)
}

protocol ChatInteractorOutputProtocol: class {
    func didFetchMessages(with result: Result<[Message]>, totalPages: Int)
    func didSendMessage(with result: Result<Data>, messageIdentifier: String)
    func didRateUser(with result: Result<Data>)
}

protocol ChatViewProtocol: class {
    var presenter: ChatPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func reloadData()
    func setEmptyTextView()
    func scrollToFirstItem()
    func toggleIsPaginating()
}
