//
//  ContactUsView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ContactUsView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: IQPreviousNextView = {
        let view = IQPreviousNextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "name".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "email".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.keyboardType = .emailAddress
        return textField
    }()
    
    
    lazy var messageTypeTextField: PickerViewTextField<MessageTypeViewModel> = {
        let textField = PickerViewTextField<MessageTypeViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "messageType".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var messageTextViewContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 24
        view.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var messageTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .clear
        textView.font = DinNextFont.regular.getFont(ofSize: 20)
        textView.textColor = .textColor
        textView.placeholder = "messageContent".localized()
        textView.maxHeight = 132
        textView.minHeight = 132
        return textView
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("send".localized(), for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(nameTextField)
        containerView.addSubview(emailTextField)
        containerView.addSubview(messageTypeTextField)
        containerView.addSubview(messageTextViewContainerView)
        messageTextViewContainerView.addSubview(messageTextView)
        containerView.addSubview(sendButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupNameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            nameTextField.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 35),
            nameTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            nameTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            nameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupEmailTextFieldConstraints() {
        NSLayoutConstraint.activate([
            emailTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 13),
            emailTextField.leadingAnchor.constraint(equalTo: nameTextField.leadingAnchor, constant: 0),
            emailTextField.trailingAnchor.constraint(equalTo: nameTextField.trailingAnchor, constant: 0),
            emailTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupMessageTypeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            messageTypeTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 13),
            messageTypeTextField.leadingAnchor.constraint(equalTo: nameTextField.leadingAnchor, constant: 0),
            messageTypeTextField.trailingAnchor.constraint(equalTo: nameTextField.trailingAnchor, constant: 0),
            messageTypeTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupMessageTextViewContainerViewConstraints() {
        NSLayoutConstraint.activate([
            messageTextViewContainerView.topAnchor.constraint(equalTo: messageTypeTextField.bottomAnchor, constant: 13),
            messageTextViewContainerView.leadingAnchor.constraint(equalTo: nameTextField.leadingAnchor, constant: 0),
            messageTextViewContainerView.trailingAnchor.constraint(equalTo: nameTextField.trailingAnchor, constant: 0),
            messageTextViewContainerView.heightAnchor.constraint(equalToConstant: 140)
        ])
    }
    
    private func setupMessageTextViewConstraints() {
        NSLayoutConstraint.activate([
            messageTextView.topAnchor.constraint(equalTo: messageTextViewContainerView.topAnchor, constant: 8),
            messageTextView.leadingAnchor.constraint(equalTo: messageTextViewContainerView.leadingAnchor, constant: 8),
            messageTextView.trailingAnchor.constraint(equalTo: messageTextViewContainerView.trailingAnchor, constant: -8),
        ])
    }
    
    private func setupSendButtonConstraints() {
        NSLayoutConstraint.activate([
            sendButton.topAnchor.constraint(equalTo: messageTextViewContainerView.bottomAnchor, constant: 35),
            sendButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            sendButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            sendButton.heightAnchor.constraint(equalToConstant: 50),
            sendButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupNameTextFieldConstraints()
        setupEmailTextFieldConstraints()
        setupMessageTypeTextFieldConstraints()
        setupMessageTextViewContainerViewConstraints()
        setupMessageTextViewConstraints()
        setupSendButtonConstraints()
    }
    
}
