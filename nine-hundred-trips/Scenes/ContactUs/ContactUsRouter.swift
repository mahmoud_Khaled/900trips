//
//  ContactUsRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ContactUsRouter: ContactUsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ContactUsViewController()
        let interactor = ContactUsInteractor()
        let router = ContactUsRouter()
        let presenter = ContactUsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
}
