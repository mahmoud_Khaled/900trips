//
//  ContactUsPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ContactUsPresenter: ContactUsPresenterProtocol, ContactUsInteractorOutputProtocol {
    
    weak var view: ContactUsViewProtocol?
    private let interactor: ContactUsInteractorInputProtocol
    private let router: ContactUsRouterProtocol
    
    init(view: ContactUsViewProtocol, interactor: ContactUsInteractorInputProtocol, router: ContactUsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getMessageTypes() {
        view?.showActivityIndicator()
        interactor.getMessageTypes()
    }
    
    func didFetchMessageTypes(with result: Result<[MessageType]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let messageTypes):
            let messageTypeViewModels = messageTypes.map(MessageTypeViewModel.init)
            view?.didFetch(messageTypes: messageTypeViewModels)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func contactUs(name: String, email: String, messageTypeId: Int, content: String) {
        if name.isEmpty {
            router.showAlert(with: "emptyName".localized())
        } else if email.isEmpty {
            router.showAlert(with: "emptyEmail".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: email) {
            router.showAlert(with: "incorrectEmail".localized())
        } else if messageTypeId == 0 {
            router.showAlert(with: "selectMessageType".localized())
        } else if content.isEmpty {
            router.showAlert(with: "emptyMessageContent".localized())
        } else {
            view?.showActivityIndicator()
            interactor.contactUs(name: name, email: email, messageTypeId: messageTypeId, content: content)
        }
    }
    
    func didContactUs(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
