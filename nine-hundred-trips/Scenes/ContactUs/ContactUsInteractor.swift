//
//  ContactUsInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ContactUsInteractor: ContactUsInteractorInputProtocol {
    
    weak var presenter: ContactUsInteractorOutputProtocol?
    
    private let applicationDataWorker = ApplicationDataWorker()
    private let userWorker = UserWorker()
    
    func getMessageTypes() {
        applicationDataWorker.getMessageTypes { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchMessageTypes(with: result)
        }
    }
    
    func contactUs(name: String, email: String, messageTypeId: Int, content: String) {
        userWorker.contactUs(name: name, email: email, messageTypeId: messageTypeId, content: content) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didContactUs(with: result)
        }
    }
}
