//
//  ContactUsProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ContactUsRouterProtocol: class {
    func showAlert(with message: String)
}

protocol ContactUsPresenterProtocol: class {
    var view: ContactUsViewProtocol? { get set }
    func getMessageTypes()
    func contactUs(name: String, email: String, messageTypeId: Int, content: String)
}

protocol ContactUsInteractorInputProtocol: class {
    var presenter: ContactUsInteractorOutputProtocol? { get set }
    func getMessageTypes()
    func contactUs(name: String, email: String, messageTypeId: Int, content: String)
}

protocol ContactUsInteractorOutputProtocol: class {
    func didFetchMessageTypes(with result: Result<[MessageType]>)
    func didContactUs(with result: Result<Data>)
}

protocol ContactUsViewProtocol: class {
    var presenter: ContactUsPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetch(messageTypes: [MessageTypeViewModel])
    func showSuccessView()
}
