//
//  ContactUsViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ContactUsViewController: BaseViewController, ContactUsViewProtocol {
    
    private let mainView = ContactUsView()
    var presenter: ContactUsPresenterProtocol!
    private var messageTypeId: Int = 0
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "contactUs".localized()
        presenter.getMessageTypes()
        addMessageTypeTextFieldAction()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func addMessageTypeTextFieldAction() {
        mainView.messageTypeTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.messageTypeId = item.id
        }
    }
    
    private func addTargets() {
        mainView.sendButton.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
    }
    
    @objc private func sendButtonTapped() {
        presenter.contactUs(name: mainView.nameTextField.text!, email: mainView.emailTextField.text!, messageTypeId: messageTypeId, content: mainView.messageTextView.text)
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func didFetch(messageTypes: [MessageTypeViewModel]) {
        mainView.messageTypeTextField.items = messageTypes
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "messageSentSuccessfully".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.mainView.nameTextField.text = ""
            self.mainView.emailTextField.text = ""
            self.messageTypeId = 0
            self.mainView.messageTypeTextField.text = ""
            self.mainView.messageTextView.text = ""
        }
    }
}
