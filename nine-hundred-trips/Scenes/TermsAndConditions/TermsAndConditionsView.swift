//
//  TermsAndConditionsView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TermsAndConditionsView: UIView {
    
    lazy var termsAndConditionsTextView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textColor = .darkGray
        textView.textAlignment = .center
        textView.font = DinNextFont.regular.getFont(ofSize: 17)
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
       addSubview(termsAndConditionsTextView)
    }
    
    
    private func setupTermsAndConditionsTextViewConstraints() {
        NSLayoutConstraint.activate([
         termsAndConditionsTextView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
         termsAndConditionsTextView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
         termsAndConditionsTextView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
         termsAndConditionsTextView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -70)
            
        ])
    }
    

    private func layoutUI() {
        addSubviews()
        setupTermsAndConditionsTextViewConstraints()
    }
    
    func configure(termsAndConditions: String) {
        termsAndConditionsTextView.text = termsAndConditions
    }
}
