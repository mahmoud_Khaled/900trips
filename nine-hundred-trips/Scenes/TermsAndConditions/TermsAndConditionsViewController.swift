//
//  TermsAndConditionsViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: BaseViewController, TermsAndConditionsViewProtocol {
  
  
    private let mainView = TermsAndConditionsView()
    var presenter: TermsAndConditionsPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        presenter.getTermsAndConditions()
    }
 
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        title = "termsAndConditions".localized()
    }
    
    func setupNavigationBarButtons() {
        let closeButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_close")?.with(size: .init(width: 25, height: 25)))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(closeButtonTapped))
            .build()
            navigationItem.leftBarButtonItem = closeButton
    }
    
    @objc private func closeButtonTapped() {
        presenter.closeButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func didFetchTermsAndConditions(string: String) {
        mainView.configure(termsAndConditions: string)
    }
    
}
