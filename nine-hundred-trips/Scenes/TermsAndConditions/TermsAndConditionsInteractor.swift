//
//  TermsAndConditionsInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TermsAndConditionsInteractor: TermsAndConditionsInteractorInputProtocol {
    
    weak var presenter: TermsAndConditionsInteractorOutputProtocol?
    
    private let applicationDataWorker = ApplicationDataWorker()
    
    func getTermsAndConditions() {
        applicationDataWorker.getTermsAndConditions { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchTermsAndConditions(with: result)
        }
    }
}
