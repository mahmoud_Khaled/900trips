//
//  TermsAndConditionsProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol TermsAndConditionsRouterProtocol: class {
    func showAlert(with message: String)
     func dismiss()
}

protocol TermsAndConditionsPresenterProtocol: class {
    var view: TermsAndConditionsViewProtocol? { get set }
    func getTermsAndConditions()
    func closeButtonTapped()
}

protocol TermsAndConditionsInteractorInputProtocol: class {
    var presenter: TermsAndConditionsInteractorOutputProtocol? { get set }
    func getTermsAndConditions()
}

protocol TermsAndConditionsInteractorOutputProtocol: class {
    func didFetchTermsAndConditions(with result: Result<Page>)
}

protocol TermsAndConditionsViewProtocol: class {
    var presenter: TermsAndConditionsPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetchTermsAndConditions(string: String)
}
