//
//  TermsAndConditionsRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TermsAndConditionsRouter: TermsAndConditionsRouterProtocol {
   
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = TermsAndConditionsViewController()
        let interactor = TermsAndConditionsInteractor()
        let router = TermsAndConditionsRouter()
        let presenter = TermsAndConditionsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
