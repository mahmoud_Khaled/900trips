//
//  TermsAndConditionsPresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TermsAndConditionsPresenter: TermsAndConditionsPresenterProtocol, TermsAndConditionsInteractorOutputProtocol {
    
    weak var view: TermsAndConditionsViewProtocol?
    private let interactor: TermsAndConditionsInteractorInputProtocol
    private let router: TermsAndConditionsRouterProtocol
    
    init(view: TermsAndConditionsViewProtocol, interactor: TermsAndConditionsInteractorInputProtocol, router: TermsAndConditionsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func closeButtonTapped() {
        router.dismiss()
    }
    
    func didFetchTermsAndConditions(with result: Result<Page>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let page):
            view?.didFetchTermsAndConditions(string: page.content)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func getTermsAndConditions() {
        view?.showActivityIndicator()
        interactor.getTermsAndConditions()
    }
    
    
    
}
