//
//  RequestQuotationPresenter.swift
//  nine-hundred-trips
//
//  Created by Ibtdi.com on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RequestQuotationPresenter: RequestQuotationPresenterProtocol, RequestQuotationInteractorOutputProtocol {
    
    weak var view: RequestQuotationViewProtocol?
    private let interactor: RequestQuotationInteractorInputProtocol
    private let router: RequestQuotationRouterProtocol
    
    var priceQuoteViewModel: RequestQuotationPriceViewModel!
    
    init(view: RequestQuotationViewProtocol, interactor: RequestQuotationInteractorInputProtocol, router: RequestQuotationRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func okButtonTapped() {
        guard priceQuoteViewModel != nil else { return }
        router.navigateToRequestQuotationCompletion(with: priceQuoteViewModel.price)
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        interactor.getRequestQuotationPrice()
    }
    
    func didFetchRequestQuotationPrice(with result: Result<RequestQuotationPrice>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            priceQuoteViewModel = RequestQuotationPriceViewModel(model: value)
            view?.setPrice(priceQuoteViewModel.description)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
