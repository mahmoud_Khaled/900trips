//
//  RequestQuotationView.swift
//  nine-hundred-trips
//
//  Created by Ibtdi.com on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationView: UIView {
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_request_quotation")?.imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    
    private lazy var servicePriceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.07), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var servicePriceTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "servicePrice".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
   lazy var servicePriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "------"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 22)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var okButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ok".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.cornerRadius = 25
        button.backgroundColor = .coral
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(logoImageView)
        addSubview(servicePriceView)
        servicePriceView.addSubview(servicePriceTitle)
        servicePriceView.addSubview(servicePriceLabel)
        addSubview(okButton)
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 50),
            logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoImageView.widthAnchor.constraint(equalToConstant: 110),
            logoImageView.heightAnchor.constraint(equalToConstant: 110)
        ])
    }
    
    private func setupServicePriceViewConstraints() {
        NSLayoutConstraint.activate([
            servicePriceView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 20),
            servicePriceView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            servicePriceView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            servicePriceView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }

    private func setupServicePriceTitleConstraints() {
        NSLayoutConstraint.activate([
            servicePriceTitle.leadingAnchor.constraint(equalTo: servicePriceView.leadingAnchor, constant: 10),
            servicePriceTitle.centerYAnchor.constraint(equalTo: servicePriceView.centerYAnchor)
        ])
    }
    
    private func setupServicePriceLabelConstraints() {
        NSLayoutConstraint.activate([
            servicePriceLabel.trailingAnchor.constraint(equalTo: servicePriceView.trailingAnchor, constant: -10),
            servicePriceLabel.centerYAnchor.constraint(equalTo: servicePriceView.centerYAnchor)
        ])
    }
    
    private func setupOkButtonConstraints() {
        NSLayoutConstraint.activate([
            okButton.topAnchor.constraint(equalTo: servicePriceLabel.bottomAnchor, constant: 25),
            okButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            okButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            okButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupLogoImageViewConstraints()
        setupServicePriceViewConstraints()
        setupServicePriceTitleConstraints()
        setupServicePriceLabelConstraints()
        setupOkButtonConstraints()
    }
    
}
