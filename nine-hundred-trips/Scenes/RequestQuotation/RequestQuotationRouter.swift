//
//  RequestQuotationRouter.swift
//  nine-hundred-trips
//
//  Created by Ibtdi.com on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationRouter: RequestQuotationRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = RequestQuotationViewController()
        let interactor = RequestQuotationInteractor()
        let router = RequestQuotationRouter()
        let presenter = RequestQuotationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToRequestQuotationCompletion(with price: Double) {
        viewController?.navigationController?.pushViewController(RequestQuotationCompletionRouter.createModule(with: price), animated: true)
    }
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}
