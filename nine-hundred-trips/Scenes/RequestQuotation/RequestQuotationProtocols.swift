//
//  RequestQuotationProtocols.swift
//  nine-hundred-trips
//
//  Created by Ibtdi.com on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol RequestQuotationRouterProtocol: class {
    func navigateToRequestQuotationCompletion(with price: Double)
    func showAlert(with message: String)
}

protocol RequestQuotationPresenterProtocol: class {
    var view: RequestQuotationViewProtocol? { get set }
    func okButtonTapped()
    func viewDidLoad()
}

protocol RequestQuotationInteractorInputProtocol: class {
    var presenter: RequestQuotationInteractorOutputProtocol? { get set }
    func getRequestQuotationPrice()
}

protocol RequestQuotationInteractorOutputProtocol: class {
    func didFetchRequestQuotationPrice(with result: Result<RequestQuotationPrice>)
}

protocol RequestQuotationViewProtocol: class {
    var presenter: RequestQuotationPresenterProtocol! { get set }
    func setPrice(_ price: String)
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
}
