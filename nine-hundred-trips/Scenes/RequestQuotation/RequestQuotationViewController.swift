//
//  RequestQuotationViewController.swift
//  nine-hundred-trips
//
//  Created by Ibtdi.com on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationViewController: BaseViewController, RequestQuotationViewProtocol {
    
    private let mainView = RequestQuotationView()
    var presenter: RequestQuotationPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        presenter.viewDidLoad()
        title = "requestQuotation".localized()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func addTargets() {
        mainView.okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
    }
    
    @objc private func okButtonTapped() {
        presenter.okButtonTapped()
    }
    
    func setPrice(_ price: String) {
        mainView.servicePriceLabel.text = price
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
}
