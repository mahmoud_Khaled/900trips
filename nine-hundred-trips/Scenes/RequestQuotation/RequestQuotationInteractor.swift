//
//  RequestQuotationInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RequestQuotationInteractor: RequestQuotationInteractorInputProtocol {

    var presenter: RequestQuotationInteractorOutputProtocol?
    private let orderWorker = OrderWorker()
    
    func getRequestQuotationPrice() {
        orderWorker.getRequestQuotationPrice { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchRequestQuotationPrice(with: result)
        }
    }
    
}
