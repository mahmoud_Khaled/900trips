//
//  CompanyDetailsViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: BaseViewController, CompanyDetailsViewProtocol {
    
    private let mainView = CompanyDetailsView()
    var presenter: CompanyDetailsPresenterProtocol!
    
    private var isPaginating: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var previousStatusBarHidden = false
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return shouldHideStatusBar
    }
    
    private var shouldHideStatusBar: Bool {
        let frame = mainView.backButton.convert(mainView.backButton.bounds, to: nil)
        return frame.minY < view.safeAreaInsets.top
    }
    
    private var viewModel: UserViewModel
    
    init(viewModel: UserViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.configure(viewModel: viewModel)
        mainView.scrollView.delegate = self
        setupTableView()
        presenter.getCompanyDetails(id: viewModel.id)
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    private func addTargets() {
        mainView.backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        mainView.callButton.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        mainView.messageButton.addTarget(self, action: #selector(messageButtonTapped), for: .touchUpInside)
        mainView.facebookButton.addTarget(self, action: #selector(facebookButtonTapped), for: .touchUpInside)
        mainView.twitterButton.addTarget(self, action: #selector(twitterButtonTapped), for: .touchUpInside)
        mainView.instagramButton.addTarget(self, action: #selector(instagramButtonTapped), for: .touchUpInside)
        mainView.tapGesture.addTarget(self, action: #selector(websiteLabelTapped))
    }
    
    @objc private func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    @objc private func callButtonTapped() {
        presenter.callButtonTapped()
    }
    
    @objc private func messageButtonTapped() {
        presenter.messageButtonTapped()
    }
    
    @objc private func facebookButtonTapped() {
        if let facebookLink = viewModel.facebook {
            guard let facebookLink = URL(string: facebookLink) else { return }
            UIApplication.shared.open(facebookLink)
        }
    }
    
    @objc private func twitterButtonTapped() {
        if let twitterLink = viewModel.twitter {
            guard let twitterLink = URL(string: twitterLink) else { return }
            UIApplication.shared.open(twitterLink)
        }
    }
    
    @objc private func instagramButtonTapped() {
        if let instagramLink = viewModel.instagram {
            guard let instagramLink = URL(string: instagramLink) else { return }
            UIApplication.shared.open(instagramLink)
        }
    }
    
    @objc private func websiteLabelTapped() {
        guard let websiteLink = URL(string: viewModel.domain) else { return }
        UIApplication.shared.open(websiteLink)
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func setIsPaginating(isPaginating: Bool) {
        self.isPaginating = isPaginating
    }
    
    func reloadData() {
        mainView.setTableViewHeight(CGFloat(presenter.numberOfRows * 190))
        mainView.tableView.reloadData()
    }
    
    func configure(viewModel: UserViewModel) {
        self.viewModel = viewModel
        mainView.configure(viewModel: viewModel)
    }
    
    func setEmptyMessage(_ message: String) {
        mainView.tableView.emptyMessage(message: message)
        mainView.setTableViewHeight(190)
    }
}

extension CompanyDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        guard scrollView == mainView.scrollView else { return }
        if previousStatusBarHidden != shouldHideStatusBar {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            })
            
            previousStatusBarHidden = shouldHideStatusBar
        }
        
        if offsetY > contentHeight - scrollView.frame.size.height && !isPaginating {
            isPaginating = true
            presenter.loadNextPage()
        }
    }
}

extension CompanyDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.className, for: indexPath) as! OfferCell
        cell.favouriteButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.toggleFavourite(indexPath: indexPath)
        }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
}
