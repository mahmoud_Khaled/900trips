//
//  CompanyDetailsView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyDetailsView: UIView {
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerImageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profilebg")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profilephoto")
        imageView.layer.cornerRadius = 43
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-------"
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 30)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------"
        label.textColor = .gray
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryFlagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 11
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var websiteView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.backgroundColor = .deepDarkOrange
        return view
    }()
    
    lazy var tapGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer()
    }()
    
    private lazy var websiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-------"
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.textAlignment = .center
        label.addGestureRecognizer(tapGesture)
        label.isUserInteractionEnabled = true
        return label
    }()
    
    private lazy var websiteImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_website")
        return imageView
    }()
    
    private lazy var firstSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var secondSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var verticalSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 1, alpha: 0.6)
        return view
    }()
    
    private lazy var likesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "numberOfLikes".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    private lazy var numberOfLikesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "----"
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.textColor = .white
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var offersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "numberOfOffers".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    private lazy var numberOfOffersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "----"
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.textColor = .white
        label.textAlignment = .center
        return label
        
    }()
    
    private lazy var offersStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [numberOfOffersLabel, offersLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()
    
    private lazy var likesStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [numberOfLikesLabel, likesLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()
    
    lazy var facebookButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_facebook"), for: .normal)
        return button
    }()
    
    lazy var instagramButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_instagram"), for: .normal)
        return button
    }()
    
    lazy var twitterButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_twitter"), for: .normal)
        return button
    }()
    
    private lazy var socialMediaView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    private lazy var socialMediaStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [facebookButton, instagramButton, twitterButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        return stackView
    }()
    
    private lazy var bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var infoIconImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_about_company"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var bioLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.medium.getFont(ofSize: 20)
        label.text = "bio".localized()
        return label
    }()
    
    private lazy var bioContentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.text = "bioNotAvailable".localized()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        return view
    }()
    
    private lazy var companyOffersIconImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_company_offers"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var offersTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.medium.getFont(ofSize: 20)
        label.text = "offers".localized()
        return label
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(OfferCell.self, forCellReuseIdentifier: OfferCell.className)
        tableView.estimatedRowHeight = 0
        tableView.rowHeight = 190
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    private lazy var contactsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var seperatorVerticallView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var callButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_call")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    lazy var messageButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_message")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    private lazy var callLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "call".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var messageabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "message".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private var bottomContainerViewTopConstraint: NSLayoutConstraint!
    private var tableViewHeightConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(headerImageContainerView)
        containerView.addSubview(headerImageView)
        containerView.addSubview(backButton)
        containerView.addSubview(profileImageView)
        containerView.addSubview(companyNameLabel)
        containerView.addSubview(countryView)
        countryView.addSubview(countryNameLabel)
        countryView.addSubview(countryFlagImageView)
        containerView.addSubview(websiteView)
        websiteView.addSubview(websiteLabel)
        websiteView.addSubview(websiteImageView)
        containerView.addSubview(firstSeparatorLine)
        containerView.addSubview(verticalSeparatorLine)
        containerView.addSubview(secondSeparatorLine)
        containerView.addSubview(offersStackView)
        containerView.addSubview(likesStackView)
        containerView.addSubview(socialMediaView)
        socialMediaView.addSubview(socialMediaStackView)
        containerView.addSubview(bottomContainerView)
        bottomContainerView.addSubview(infoIconImageView)
        bottomContainerView.addSubview(bioLabel)
        bottomContainerView.addSubview(bioContentLabel)
        bottomContainerView.addSubview(lineView)
        bottomContainerView.addSubview(companyOffersIconImageView)
        bottomContainerView.addSubview(offersTextLabel)
        bottomContainerView.addSubview(tableView)
        addSubview(contactsView)
        contactsView.addSubview(seperatorVerticallView)
        contactsView.addSubview(callButton)
        contactsView.addSubview(messageButton)
        contactsView.addSubview(callLabel)
        contactsView.addSubview(messageabel)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -85)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupHeaderImageContainerViewConstraints() {
        let headerImageContainerViewHeight = headerImageContainerView.heightAnchor.constraint(equalToConstant: 375)
        NSLayoutConstraint.activate([
            headerImageContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            headerImageContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerImageContainerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerImageContainerViewHeight
        ])
    }
    
    private func setupHeaderImageViewConstraints() {
        let headerImageViewTopAnchor = headerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0)
        headerImageViewTopAnchor.priority = .defaultHigh
        
        let heightAnchor = headerImageView.heightAnchor.constraint(greaterThanOrEqualTo: headerImageContainerView.heightAnchor, multiplier: 0)
        heightAnchor.priority = .required
        
        NSLayoutConstraint.activate([
            headerImageView.leadingAnchor.constraint(equalTo: headerImageContainerView.leadingAnchor),
            headerImageView.trailingAnchor.constraint(equalTo: headerImageContainerView.trailingAnchor),
            headerImageViewTopAnchor,
            heightAnchor,
            headerImageView.bottomAnchor.constraint(equalTo: headerImageContainerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupBackButtonConstraints() {
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: UIDevice.hasTopNotch ? 48 : 32),
            backButton.trailingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 32),
            backButton.heightAnchor.constraint(equalToConstant: 23.5),
            backButton.widthAnchor.constraint(equalToConstant: 26)
        ])
    }
    
    private func setupProfileImageViewConstraints() {
        NSLayoutConstraint.activate([
            profileImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 50),
            profileImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            profileImageView.heightAnchor.constraint(equalToConstant: 86),
            profileImageView.widthAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupCompanyNamelabelConstraints() {
        NSLayoutConstraint.activate([
            companyNameLabel.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 5),
            companyNameLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            companyNameLabel.heightAnchor.constraint(equalToConstant: 40),
            companyNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            companyNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8)
        ])
    }
    private func setupCountryViewConstraints() {
        NSLayoutConstraint.activate([
            countryView.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 0),
            countryView.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
            countryView.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    private func setupCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            countryNameLabel.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 5),
            countryNameLabel.trailingAnchor.constraint(equalTo: countryView.trailingAnchor, constant: -24),
            countryNameLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupCountryFlagImageView() {
        NSLayoutConstraint.activate([
            countryFlagImageView.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 10),
            countryFlagImageView.leadingAnchor.constraint(equalTo: countryView.leadingAnchor, constant: 24),
            countryFlagImageView.trailingAnchor.constraint(equalTo: countryNameLabel.leadingAnchor, constant: -8),
            countryFlagImageView.heightAnchor.constraint(equalToConstant: 22),
            countryFlagImageView.widthAnchor.constraint(equalToConstant: 22)
        ])
    }
    
    private func setupWebsiteViewConstraints() {
        NSLayoutConstraint.activate([
            websiteView.topAnchor.constraint(equalTo: countryView.bottomAnchor, constant: 0),
            websiteView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            websiteView.heightAnchor.constraint(equalToConstant: 30),
        ])
    }
    
    private func setupWebsiteLabelConstraints() {
        NSLayoutConstraint.activate([
            websiteLabel.centerYAnchor.constraint(equalTo: websiteImageView.centerYAnchor, constant: -3.5),
            websiteLabel.trailingAnchor.constraint(equalTo: websiteView.trailingAnchor, constant: -24),
        ])
    }
    
    private func setupWebsiteImageView() {
        NSLayoutConstraint.activate([
            websiteImageView.topAnchor.constraint(equalTo: websiteView.topAnchor, constant: 5),
            websiteImageView.trailingAnchor.constraint(equalTo: websiteLabel.leadingAnchor, constant: -8),
            websiteImageView.heightAnchor.constraint(equalToConstant: 18),
            websiteImageView.widthAnchor.constraint(equalToConstant: 18),
            websiteImageView.leadingAnchor.constraint(equalTo: websiteView.leadingAnchor, constant: 24)
        ])
    }
    
    private func setupFirstSeparatorLineConstrains() {
        NSLayoutConstraint.activate([
            firstSeparatorLine.topAnchor.constraint(equalTo: websiteView.bottomAnchor, constant: 5),
            firstSeparatorLine.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            firstSeparatorLine.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            firstSeparatorLine.heightAnchor.constraint(equalToConstant: 0.5),
        ])
    }
    
    
    private func setupSecondSeparatorLineConstrains() {
        NSLayoutConstraint.activate([
            secondSeparatorLine.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 78),
            secondSeparatorLine.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            secondSeparatorLine.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            secondSeparatorLine.heightAnchor.constraint(equalToConstant: 0.5),
        ])
    }
    
    private func setupVerticalSeparatorLineConstraints() {
        NSLayoutConstraint.activate([
            verticalSeparatorLine.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 0),
            verticalSeparatorLine.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor, constant: 0),
            verticalSeparatorLine.bottomAnchor.constraint(equalTo: secondSeparatorLine.topAnchor, constant: 0),
            verticalSeparatorLine.widthAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupOffersStackViewConstraints() {
        NSLayoutConstraint.activate([
            offersStackView.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 3),
            offersStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            offersStackView.trailingAnchor.constraint(equalTo: verticalSeparatorLine.leadingAnchor, constant: -8)
        ])
    }
    
    private func setupLikesStackViewConstraints() {
        NSLayoutConstraint.activate([
            likesStackView.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 3),
            likesStackView.leadingAnchor.constraint(equalTo: verticalSeparatorLine.trailingAnchor, constant: 8),
            likesStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupSocialMediaViewConstraints() {
        NSLayoutConstraint.activate([
            socialMediaView.topAnchor.constraint(equalTo: secondSeparatorLine.bottomAnchor, constant: 8),
            socialMediaView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            socialMediaView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            socialMediaView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupSocialMediaStackViewConstraints() {
        NSLayoutConstraint.activate([
            socialMediaStackView.topAnchor.constraint(equalTo: socialMediaView.topAnchor, constant: 8),
            socialMediaStackView.centerXAnchor.constraint(equalTo: socialMediaView.centerXAnchor, constant: 0)
        ])
    }
    
    private func setupBottomContainerViewConstraints() {
        bottomContainerViewTopConstraint = bottomContainerView.topAnchor.constraint(equalTo: socialMediaView.bottomAnchor, constant: 24)
        NSLayoutConstraint.activate([
            bottomContainerViewTopConstraint,
            bottomContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            bottomContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            bottomContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupInfoIconImageViewConstraints() {
        NSLayoutConstraint.activate([
            infoIconImageView.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 4),
            infoIconImageView.heightAnchor.constraint(equalToConstant: 32),
            infoIconImageView.widthAnchor.constraint(equalToConstant: 32),
            infoIconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 18)
        ])
    }
    
    private func setupBioLabelConstraints() {
        NSLayoutConstraint.activate([
            bioLabel.centerYAnchor.constraint(equalTo: infoIconImageView.centerYAnchor, constant: -2),
            bioLabel.leadingAnchor.constraint(equalTo: infoIconImageView.trailingAnchor, constant: 8)
        ])
    }
    
    private func setupBioContentLabelConstraints() {
        NSLayoutConstraint.activate([
            bioContentLabel.topAnchor.constraint(equalTo: infoIconImageView.bottomAnchor, constant: 9),
            bioContentLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
            bioContentLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: bioContentLabel.bottomAnchor, constant: 20),
            lineView.heightAnchor.constraint(equalToConstant: 1),
            lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
            lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16)
        ])
    }
    
    private func setupCompanyOffersIconImageViewConstraints() {
        NSLayoutConstraint.activate([
            companyOffersIconImageView.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 8),
            companyOffersIconImageView.heightAnchor.constraint(equalToConstant: 32),
            companyOffersIconImageView.widthAnchor.constraint(equalToConstant: 32),
            companyOffersIconImageView.leadingAnchor.constraint(equalTo: infoIconImageView.leadingAnchor)
        ])
    }
    
    private func setupOffersTextLabelConstraints() {
        NSLayoutConstraint.activate([
            offersTextLabel.centerYAnchor.constraint(equalTo: companyOffersIconImageView.centerYAnchor, constant: -2),
            offersTextLabel.leadingAnchor.constraint(equalTo: companyOffersIconImageView.trailingAnchor, constant: 8)
        ])
    }
    
    private func setupTableViewConstraints() {
        tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: companyOffersIconImageView.bottomAnchor, constant: 24),
            tableView.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 16),
            tableView.trailingAnchor.constraint(equalTo: bottomContainerView.trailingAnchor, constant: -16),
            tableViewHeightConstraint,
            tableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContactsViewConstraints() {
        NSLayoutConstraint.activate([
            contactsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            contactsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            contactsView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0),
            contactsView.heightAnchor.constraint(equalToConstant: 85)
        ])
    }
    
    private func setupSperatorVertivalViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorVerticallView.topAnchor.constraint(equalTo: contactsView.topAnchor, constant: 0),
            seperatorVerticallView.centerXAnchor.constraint(equalTo: contactsView.centerXAnchor),
            seperatorVerticallView.bottomAnchor.constraint(equalTo: contactsView.bottomAnchor, constant: 0),
            seperatorVerticallView.widthAnchor.constraint(equalToConstant: 1),
        ])
    }
    
    
    private func setupCallButtonConstraints() {
        NSLayoutConstraint.activate([
            callButton.centerYAnchor.constraint(equalTo: contactsView.centerYAnchor, constant: -15),
            callButton.trailingAnchor.constraint(equalTo: seperatorVerticallView.leadingAnchor, constant: -60),
            callButton.widthAnchor.constraint(equalToConstant: 40),
            callButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupMessageButtonConstraints() {
        NSLayoutConstraint.activate([
            messageButton.centerYAnchor.constraint(equalTo: contactsView.centerYAnchor, constant: -15),
            messageButton.leadingAnchor.constraint(equalTo: seperatorVerticallView.trailingAnchor, constant: 60),
            messageButton.widthAnchor.constraint(equalToConstant: 40),
            messageButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    
    private func setupCallLabelConstraints() {
        NSLayoutConstraint.activate([
            callLabel.centerXAnchor.constraint(equalTo: callButton.centerXAnchor),
            callLabel.topAnchor.constraint(equalTo: callButton.bottomAnchor, constant: 5),
        ])
    }
    
    private func setupMessageabelConstraints() {
        NSLayoutConstraint.activate([
            messageabel.centerXAnchor.constraint(equalTo: messageButton.centerXAnchor),
            messageabel.topAnchor.constraint(equalTo: messageButton.bottomAnchor, constant: 5),
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupHeaderImageContainerViewConstraints()
        setupHeaderImageViewConstraints()
        setupBackButtonConstraints()
        setupProfileImageViewConstraints()
        setupCompanyNamelabelConstraints()
        setupCountryViewConstraints()
        setupCountryNameLabelConstraints()
        setupCountryFlagImageView()
        setupWebsiteViewConstraints()
        setupWebsiteLabelConstraints()
        setupWebsiteImageView()
        setupFirstSeparatorLineConstrains()
        setupVerticalSeparatorLineConstraints()
        setupSecondSeparatorLineConstrains()
        setupOffersStackViewConstraints()
        setupLikesStackViewConstraints()
        setupSocialMediaViewConstraints()
        setupSocialMediaStackViewConstraints()
        setupBottomContainerViewConstraints()
        setupInfoIconImageViewConstraints()
        setupBioLabelConstraints()
        setupBioContentLabelConstraints()
        setupLineViewConstraints()
        setupCompanyOffersIconImageViewConstraints()
        setupOffersTextLabelConstraints()
        setupTableViewConstraints()
        setupContactsViewConstraints()
        setupSperatorVertivalViewConstraints()
        setupCallButtonConstraints()
        setupMessageButtonConstraints()
        setupCallLabelConstraints()
        setupMessageabelConstraints()
    }
    
    func configure(viewModel: UserViewModel) {
        companyNameLabel.text = viewModel.username
        countryNameLabel.text = viewModel.countryName
        websiteLabel.attributedText = NSMutableAttributedString(string: viewModel.domain, attributes: [
            .font: DinNextFont.regular.getFont(ofSize: 18),
            .underlineStyle: viewModel.domain == "N/A" ? 0 : 1
        ])
        countryFlagImageView.load(url: viewModel.countryFlagUrl)
        profileImageView.load(url: viewModel.imageUrl, placeholder: UIImage(named: "profile_photo"))
        numberOfOffersLabel.text = "\(viewModel.offerCount)"
        numberOfLikesLabel.text = "\(viewModel.likesCount)"
        facebookButton.isHidden = viewModel.isFacebookButtonHidden
        instagramButton.isHidden = viewModel.isInstagramButtonHidden
        twitterButton.isHidden = viewModel.isTwitterButtonHidden
        socialMediaView.isHidden = viewModel.isSocialMediaViewHidden
        if viewModel.isSocialMediaViewHidden {
            bottomContainerViewTopConstraint.isActive = false
            bottomContainerViewTopConstraint = bottomContainerView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: 16)
            bottomContainerViewTopConstraint.isActive = true
        }
    }
    
    func setTableViewHeight(_ height: CGFloat) {
        tableViewHeightConstraint.constant = height
    }
}

