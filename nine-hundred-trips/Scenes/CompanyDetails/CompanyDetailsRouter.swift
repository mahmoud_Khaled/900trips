//
//  CompanyDetailsRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum CompanyDetailsDestination {
    case offerDetails(indexPath: IndexPath, id: Int, delegate: UpdateOfferProtocol)
    case chat(userId: Int, userImage: URL?, username: String, rate: String)
}

class CompanyDetailsRouter: CompanyDetailsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(with viewModel: UserViewModel) -> UIViewController {
        let view = CompanyDetailsViewController(viewModel: viewModel)
        let interactor = CompanyDetailsInteractor()
        let router = CompanyDetailsRouter()
        let presenter = CompanyDetailsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showActionSheet(number1: String, number2: String) {
        
        let actionSheet = AlertBuilder(title: "numbers".localized(), message: nil, preferredStyle: .actionSheet)
        
        if !number1.isEmpty {
            _ =  actionSheet.addAction(title: number1, style: .default) { [weak self] in
                guard let self = self else { return }
                self.callNumber(number: number1)
            }
        }
        
        if !number2.isEmpty {
            _ = actionSheet.addAction(title: number2, style: .default) { [weak self] in
                guard let self = self else { return }
                self.callNumber(number: number2)
            }
        }
        
        if number1.isEmpty && number2.isEmpty {
            showAlert(with: "noPhoneNumbers".localized())
            return
        }
        
        actionSheet.addAction(title: "cancel".localized(), style: .cancel)
            .build()
            .show()
    }
    
    private func callNumber(number: String) {
        let url: URL = URL(string: ("tel://" + number))!
        UIApplication.shared.open(url)
    }
    
    func navigate(to destination: CompanyDetailsDestination) {
        switch destination {
        case .offerDetails(let indexPath, let id, let delegate):
            viewController?.navigationController?.pushViewController(OfferDetailsRouter.createModule(isMyOffer: false, indexPath: indexPath, offerId: id, delegate: delegate), animated: true)
        case .chat(let userId, let userImage, let username, let rate):
            viewController?.tabBarController?.navigationController?.pushViewController(ChatRouter.createModule(with: userId, userImage: userImage, username: username, conversationId: nil, rate: rate), animated: true)
        }
    }
}
