//
//  CompanyDetailsProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol CompanyDetailsRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
    func showActionSheet(number1: String, number2: String)
    func navigate(to destination: CompanyDetailsDestination)
}

protocol CompanyDetailsPresenterProtocol: class {
    var view: CompanyDetailsViewProtocol? { get set }
    var numberOfRows: Int { get }
    func getCompanyDetails(id: Int)
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath)
    func backButtonTapped()
    func toggleFavourite(indexPath: IndexPath)
    func callButtonTapped()
    func didSelectRow(at indexPath: IndexPath)
    func loadNextPage()
    func messageButtonTapped()
}

protocol CompanyDetailsInteractorInputProtocol: class {
    var presenter: CompanyDetailsInteractorOutputProtocol? { get set }
    func getCompanyDetails(id: Int, page: Int)
    func addFavourite(offerId: Int, index: Int)
    func removeFavourite(offerId: Int, index: Int)
}

protocol CompanyDetailsInteractorOutputProtocol: class {
    func didFetchCompanyDetails(with result: Result<CompanyDetails>, totalPages: Int)
    func didAddFavourite(with result: Result<Data>, index: Int)
    func didRemoveFavourite(with result: Result<Data>, index: Int)
}

protocol CompanyDetailsViewProtocol: class {
    var presenter: CompanyDetailsPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func setIsPaginating(isPaginating: Bool)
    func reloadData()
    func configure(viewModel: UserViewModel)
    func setEmptyMessage(_ message: String)
}
