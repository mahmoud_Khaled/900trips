//
//  CompanyDetailsPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompanyDetailsPresenter: CompanyDetailsPresenterProtocol, CompanyDetailsInteractorOutputProtocol, UpdateOfferProtocol {
    
    weak var view: CompanyDetailsViewProtocol?
    private let interactor: CompanyDetailsInteractorInputProtocol
    private let router: CompanyDetailsRouterProtocol
    
    var offersViewModels = [OfferViewModel]()
    var viewModel: UserViewModel?
    var companyId: Int?
    
    var numberOfRows: Int {
        return offersViewModels.count
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    private var totalPages = 1
    private var currentPage = 1
    
    init(view: CompanyDetailsViewProtocol, interactor: CompanyDetailsInteractorInputProtocol, router: CompanyDetailsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getCompanyDetails(id: Int) {
        companyId = id
        view?.showActivityIndicator()
        interactor.getCompanyDetails(id: id, page: currentPage)
    }
    
    func didFetchCompanyDetails(with result: Result<CompanyDetails>, totalPages: Int) {
        view?.hideActivityIndicator()
        self.totalPages = totalPages
        switch result {
        case .success(let companyDetails):
            view?.setIsPaginating(isPaginating: false)
            offersViewModels.append(contentsOf: companyDetails.offers.map {
                OfferViewModel(
                    offer: $0,
                    dateFormatter: self.dateFormatter,
                    stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}"
                )
            })
            viewModel = UserViewModel(user: companyDetails.company)
            view?.configure(viewModel: UserViewModel(user: companyDetails.company))
            view?.reloadData()
            if offersViewModels.count == 0 {
                view?.setEmptyMessage("noOffers".localized())
            }
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath) {
        cell.configure(model: offersViewModels[indexPath.row])
    }
    
    func toggleFavourite(indexPath: IndexPath) {
        guard UserDefaultsHelper.isLoggedIn else {
            router.showAlert(with: "loginFirst".localized())
            return
        }
        view?.showActivityIndicator()
        if offersViewModels[indexPath.row].isLiked {
            interactor.removeFavourite(offerId: offersViewModels[indexPath.row].id, index: indexPath.row)
        } else {
            interactor.addFavourite(offerId: offersViewModels[indexPath.row].id, index: indexPath.row)
        }
    }
    
    func didAddFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            offersViewModels[index].isLiked = true
            view?.reloadData()
            NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
                "offerId": offersViewModels[index].id,
                "likeStatus": offersViewModels[index].isLiked
            ])
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didRemoveFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            offersViewModels[index].isLiked = false
            view?.reloadData()
            NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
                "offerId": offersViewModels[index].id,
                "likeStatus": offersViewModels[index].isLiked
            ])
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func backButtonTapped() {
        router.dismiss()
    }
    
    func callButtonTapped() {
        guard viewModel != nil else { return }
        router.showActionSheet(number1: viewModel!.phone, number2: viewModel!.phone1)
    }
    
    func messageButtonTapped() {
        guard viewModel != nil else { return }
        if UserDefaultsHelper.isLoggedIn {
            router.navigate(to: .chat(userId: viewModel!.id, userImage: viewModel!.imageUrl, username: viewModel!.username, rate: viewModel!.rate))
        } else {
            router.showAlert(with: "loginFirst".localized())
        }
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        router.navigate(to: .offerDetails(indexPath: indexPath, id: offersViewModels[indexPath.row].id, delegate: self))
    }
    
    func updateOffer(indexPath: IndexPath, isLiked: Bool) {
        offersViewModels[indexPath.row].isLiked = isLiked
        NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
            "offerId": offersViewModels[indexPath.row].id,
            "likeStatus": offersViewModels[indexPath.row].isLiked
        ])
        view?.reloadData()
    }
    
    func loadNextPage() {
        if(currentPage < totalPages) {
            view?.showActivityIndicator()
            currentPage += 1
            interactor.getCompanyDetails(id: companyId ?? 0, page: currentPage)
        }
    }
}
