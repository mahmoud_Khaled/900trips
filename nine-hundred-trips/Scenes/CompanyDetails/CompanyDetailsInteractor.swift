//
//  CompanyDetailsInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyDetailsInteractor: CompanyDetailsInteractorInputProtocol {
    
    weak var presenter: CompanyDetailsInteractorOutputProtocol?
    
    private let userWorker = UserWorker()
    private let offersWorker = OffersWorker()
    
    func getCompanyDetails(id: Int, page: Int) {
        userWorker.getCompanyDetails(id: id, page: page) { [weak self] result, totalPages in
            guard let self = self else { return }
            self.presenter?.didFetchCompanyDetails(with: result, totalPages: totalPages)
        }
    }
    
    func addFavourite(offerId: Int, index: Int) {
        offersWorker.addFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddFavourite(with: result, index: index)
        }
    }
    
    func removeFavourite(offerId: Int, index: Int) {
        offersWorker.removeFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRemoveFavourite(with: result, index: index)
        }
    }
}
