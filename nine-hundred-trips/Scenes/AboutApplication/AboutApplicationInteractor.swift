//
//  AboutApplicationInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AboutApplicationInteractor: AboutApplicationInteractorInputProtocol {
    
    weak var presenter: AboutApplicationInteractorOutputProtocol?
    
    private let applicationDataWorker = ApplicationDataWorker()
    
    func getAboutApplication() {
        applicationDataWorker.getAboutApplication { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchAboutApplication(with: result)
        }
    }
}
