//
//  AboutApplicationPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AboutApplicationPresenter: AboutApplicationPresenterProtocol, AboutApplicationInteractorOutputProtocol {
    
    weak var view: AboutApplicationViewProtocol?
    private let interactor: AboutApplicationInteractorInputProtocol
    private let router: AboutApplicationRouterProtocol
    
    init(view: AboutApplicationViewProtocol, interactor: AboutApplicationInteractorInputProtocol, router: AboutApplicationRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getAboutApplication() {
        view?.showActivityIndicator()
        interactor.getAboutApplication()
    }
    
    func didFetchAboutApplication(with result: Result<Page>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let page):
            view?.didFetchAboutApplication(string: page.content)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
