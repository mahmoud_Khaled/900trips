//
//  AboutApplicationView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AboutApplicationView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        return view
    }()
    
    lazy var aboutApplicationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .darkGray
        label.textAlignment = .center
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        return label
    }()
    
    private lazy var designedByContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.12), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var designedByLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.text = "designedAndProgrammed".localized()
        return label
    }()
    
    private lazy var ibtdiLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .mediumTurquoise
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.text = "ibtdi".localized()
        return label
    }()
    
    private lazy var designedByLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 0.10)
        return view
    }()

    private lazy var ibtdiLogoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ibtdi_logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var ibtdiWebsiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .mediumTurquoise
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "www.ibtdi.com"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(logoImageView)
        containerView.addSubview(lineView)
        containerView.addSubview(aboutApplicationLabel)
        containerView.addSubview(designedByContainerView)
        designedByContainerView.addSubview(designedByLabel)
        designedByContainerView.addSubview(ibtdiLabel)
        designedByContainerView.addSubview(designedByLineView)
        designedByContainerView.addSubview(ibtdiLogoImageView)
        designedByContainerView.addSubview(ibtdiWebsiteLabel)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 27),
            logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageView.heightAnchor.constraint(equalToConstant: 209),
            logoImageView.widthAnchor.constraint(equalToConstant: 180)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 18),
            lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupAboutApplicationLabelConstraints() {
        NSLayoutConstraint.activate([
            aboutApplicationLabel.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 21.5),
            aboutApplicationLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
            aboutApplicationLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16)
        ])
    }
    
    private func setupDesignedByContainerViewConstraints() {
        NSLayoutConstraint.activate([
            designedByContainerView.topAnchor.constraint(equalTo: aboutApplicationLabel.bottomAnchor, constant: 27),
            designedByContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            designedByContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            designedByContainerView.heightAnchor.constraint(equalToConstant: 110),
            designedByContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 44)
        ])
    }
    
    private func setupDesignedByLabelConstraints() {
        NSLayoutConstraint.activate([
            designedByLabel.topAnchor.constraint(equalTo: designedByContainerView.topAnchor, constant: 7),
            designedByLabel.leadingAnchor.constraint(equalTo: designedByContainerView.leadingAnchor, constant: 10)
        ])
    }
    
    private func setupIbtdiLabelConstraints() {
        NSLayoutConstraint.activate([
            ibtdiLabel.topAnchor.constraint(equalTo: designedByLabel.topAnchor, constant: 0),
            ibtdiLabel.trailingAnchor.constraint(equalTo: designedByContainerView.trailingAnchor, constant: -16)
        ])
    }
    
    private func setupDesignedByLineViewConstraints() {
        NSLayoutConstraint.activate([
            designedByLineView.topAnchor.constraint(equalTo: ibtdiLabel.bottomAnchor, constant: 10.5),
            designedByLineView.leadingAnchor.constraint(equalTo: designedByContainerView.leadingAnchor, constant: 0),
            designedByLineView.trailingAnchor.constraint(equalTo: designedByContainerView.trailingAnchor, constant: 0),
            designedByLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupIbtdiLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            ibtdiLogoImageView.widthAnchor.constraint(equalToConstant: 64),
            ibtdiLogoImageView.heightAnchor.constraint(equalToConstant: 36),
            ibtdiLogoImageView.topAnchor.constraint(equalTo: designedByLineView.bottomAnchor, constant: 4.5),
            ibtdiLogoImageView.centerXAnchor.constraint(equalTo: designedByContainerView.centerXAnchor)
        ])
    }
    
    private func setupIbtdiWebsiteLabelConstraints() {
        NSLayoutConstraint.activate([
            ibtdiWebsiteLabel.topAnchor.constraint(equalTo: ibtdiLogoImageView.bottomAnchor, constant: -4),
            ibtdiWebsiteLabel.centerXAnchor.constraint(equalTo: designedByContainerView.centerXAnchor)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupLineViewConstraints()
        setupAboutApplicationLabelConstraints()
        setupDesignedByContainerViewConstraints()
        setupDesignedByLabelConstraints()
        setupIbtdiLabelConstraints()
        setupDesignedByLineViewConstraints()
        setupIbtdiLabelConstraints()
        setupIbtdiLogoImageViewConstraints()
        setupIbtdiWebsiteLabelConstraints()
    }
    
    func configure(about: String) {
        aboutApplicationLabel.text = about
    }
}
