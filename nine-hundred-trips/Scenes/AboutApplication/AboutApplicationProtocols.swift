//
//  AboutApplicationProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol AboutApplicationRouterProtocol: class {
    func showAlert(with message: String)
}

protocol AboutApplicationPresenterProtocol: class {
    var view: AboutApplicationViewProtocol? { get set }
    func getAboutApplication()
}

protocol AboutApplicationInteractorInputProtocol: class {
    var presenter: AboutApplicationInteractorOutputProtocol? { get set }
    func getAboutApplication()
}

protocol AboutApplicationInteractorOutputProtocol: class {
    func didFetchAboutApplication(with result: Result<Page>)
}

protocol AboutApplicationViewProtocol: class {
    var presenter: AboutApplicationPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetchAboutApplication(string: String)
}
