//
//  AboutApplicationRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AboutApplicationRouter: AboutApplicationRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = AboutApplicationViewController()
        let interactor = AboutApplicationInteractor()
        let router = AboutApplicationRouter()
        let presenter = AboutApplicationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}
