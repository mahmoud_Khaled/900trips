//
//  UpdateSocialMedialLinksPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class EditSocialMedialLinksPresenter: EditSocialMedialLinksPresenterProtocol, EditSocialMedialLinksInteractorOutputProtocol {
 
    weak var view: EditSocialMedialLinksViewProtocol?
    private let interactor: EditSocialMedialLinksInteractorInputProtocol
    private let router: EditSocialMedialLinksRouterProtocol
    
    init(view: EditSocialMedialLinksViewProtocol, interactor: EditSocialMedialLinksInteractorInputProtocol, router: EditSocialMedialLinksRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func editButtonTapped(facebook: String, twitter: String, instagrma: String, website: String) {
        
        if website != "" {
            guard NSPredicate(format: "SELF MATCHES %@",  Constants.domainRegex).evaluate(with: website) else  {
                router.showAlert(with: "incorrectWebsiteLink".localized())
                return
            }
        }
        
        if facebook != "" {
            guard NSPredicate(format: "SELF MATCHES %@",  Constants.domainRegex).evaluate(with: facebook) else  {
                router.showAlert(with: "incorrectFacebookLink".localized())
                return
            }
        }
        
        if twitter != "" {
            guard NSPredicate(format: "SELF MATCHES %@",  Constants.domainRegex).evaluate(with: twitter) else  {
                router.showAlert(with: "incorrectTwitterLink".localized())
                return
            }
        }
        
        if instagrma != "" {
            guard NSPredicate(format: "SELF MATCHES %@", Constants.domainRegex).evaluate(with: instagrma) else  {
                router.showAlert(with: "incorrectInstagramLink".localized())
                return
            }
        }
        
        view?.showActivityIndicator()
        interactor.updateSocialLinks(facebook: facebook, twitter: twitter, instagram: instagrma, website: website)
    }
    
    func didUpdateSocialLinks(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
            NotificationCenter.default.post(name: .updateProfile, object: nil, userInfo: nil)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
  
    func dismiss() {
        router.dismiss()
    }
    
}
