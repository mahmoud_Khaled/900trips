//
//  UpdateSocialMedialLinksProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol EditSocialMedialLinksRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
}

protocol EditSocialMedialLinksPresenterProtocol: class {
    var view: EditSocialMedialLinksViewProtocol? { get set }
    func editButtonTapped(facebook: String, twitter: String, instagrma: String, website: String)
    func dismiss()
}

protocol EditSocialMedialLinksInteractorInputProtocol: class {
    var presenter: EditSocialMedialLinksInteractorOutputProtocol? { get set }
    func updateSocialLinks(facebook: String, twitter: String, instagram: String, website: String)
}

protocol EditSocialMedialLinksInteractorOutputProtocol: class {
    func didUpdateSocialLinks(with result: Result<Data>)
    
}

protocol EditSocialMedialLinksViewProtocol: class {
    var presenter: EditSocialMedialLinksPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
