//
//  UpdateSocialMedialLinksViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditSocialMedialLinksViewController: BaseViewController, EditSocialMedialLinksViewProtocol {
 
    private let  mainView = EditSocialMedialLinksView()
    var presenter: EditSocialMedialLinksPresenterProtocol!
    
     private let viewModel: UserViewModel
    
    init(viewModel: UserViewModel) {
        self.viewModel = viewModel
        mainView.configure(viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        addTarget()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
        title = "socialMedia".localized()
    }
    
    private func addTarget() {
        mainView.editButton.addTarget(self, action: #selector(editButtonTapped), for: .touchUpInside)
    }
    
    @objc func editButtonTapped() {
        presenter.editButtonTapped(facebook: mainView.facebookTextField.text!, twitter:  mainView.twitterTextField.text!, instagrma:  mainView.instagramTextField.text!, website: mainView.websiteTextField.text!)
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "yourInformationisUpdateSuccessfully".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.presenter?.dismiss()
         }
    }
    
    
}
