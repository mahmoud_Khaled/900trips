//
//  UpdateSocialMedialLinksView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditSocialMedialLinksView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
 
    lazy var websiteTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        textField.textColor = .darkGray
        textField.setIconWithoutPadding(direction: .left, image: UIImage(named: "ic_website"), width: 25, height: 25, tintColor: .lightGray)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.placeholder = "website".localized()
        return textField
    }()
    
    private lazy var firstLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .veryLightGray
        return view
    }()
    
 
    lazy var facebookTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        textField.textColor = .darkGray
        textField.setIconWithoutPadding(direction: .left, image: UIImage(named: "ic_facebook"), width: 25, height: 25)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.placeholder = "facebook".localized()
        return textField
    }()
    
    private lazy var secondLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .veryLightGray
        return view
    }()
    
    lazy var twitterTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        textField.textColor = .darkGray
        textField.setIconWithoutPadding(direction: .left, image: UIImage(named: "ic_twitter"), width: 25, height: 25)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.placeholder = "twitter".localized()
        return textField
    }()
    
    private lazy var thirdLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .veryLightGray
        return view
    }()
    
    lazy var instagramTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        textField.textColor = .darkGray
        textField.setIconWithoutPadding(direction: .left, image: UIImage(named: "ic_instagram"), width: 25, height: 25)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.placeholder = "instagram".localized()
        return textField
    }()
    
    private lazy var inputsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [websiteTextField, firstLineView ,facebookTextField, secondLineView, twitterTextField,thirdLineView, instagramTextField ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.distribution = .equalSpacing
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(top: 0, left: 0, bottom: 0, right: -15)
        return stackView
    }()
    
    
    lazy var editButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 25
        button.backgroundColor = .coral
        button.setTitle("update".localized(), for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 20)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(inputsStackView)
        containerView.addSubview(editButton)
    }
    
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupInputsStackViewConstraints() {
        NSLayoutConstraint.activate([
            inputsStackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 25),
            inputsStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            inputsStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            inputsStackView.heightAnchor.constraint(equalToConstant: 140)
        ])
    }
    
    private func setupFirstLineViewConstraints() {
        NSLayoutConstraint.activate([
            firstLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSecondLineViewConstraints() {
        NSLayoutConstraint.activate([
            secondLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupThirdLineViewConstraints() {
        NSLayoutConstraint.activate([
            thirdLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupUpdateButtonConstraints() {
        NSLayoutConstraint.activate([
            editButton.topAnchor.constraint(equalTo: inputsStackView.bottomAnchor, constant: 35),
            editButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            editButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            editButton.heightAnchor.constraint(equalToConstant: 50),
            editButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupInputsStackViewConstraints()
        setupThirdLineViewConstraints()
        setupFirstLineViewConstraints()
        setupSecondLineViewConstraints()
        setupUpdateButtonConstraints()
    }
    
    func configure(viewModel: UserViewModel) {
        facebookTextField.text = viewModel.facebook
        twitterTextField.text = viewModel.twitter
        instagramTextField.text = viewModel.instagram
    }
}
