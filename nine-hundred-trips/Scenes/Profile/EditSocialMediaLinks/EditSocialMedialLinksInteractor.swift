//
//  UpdateSocialMedialLinksInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditSocialMedialLinksInteractor: EditSocialMedialLinksInteractorInputProtocol {
    
    weak var presenter: EditSocialMedialLinksInteractorOutputProtocol?
    private let userWorker = UserWorker()
    
    func updateSocialLinks(facebook: String, twitter: String, instagram: String, website: String) {
        userWorker.updateSocialLinks(facebook: facebook, twitter: twitter, instagram: instagram, website: website) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUpdateSocialLinks(with: result)
        }
    }
}
