//
//  EditCompanyProfileView.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditCompanyProfileView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var uploadProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_upload_photo")
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .gray
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.isUserInteractionEnabled = false
        imageView.layer.cornerRadius = 38
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var updateProfileImageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "updateProfileImage".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var userNameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = .darkGray
        textField.font = DinNextFont.regular.getFont(ofSize: 18)
        textField.attributedPlaceholder = NSAttributedString(string: "companyName".localized(),
                                                             attributes: [.foregroundColor: UIColor.darkGray])
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = .darkGray
        textField.font = DinNextFont.regular.getFont(ofSize: 18)
        textField.attributedPlaceholder = NSAttributedString(string: "email".localized(),
                                                             attributes: [.foregroundColor: UIColor.darkGray])
        
        textField.layer.cornerRadius = 30.5
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var companyTypeTextField: PickerViewTextField<CompanyTypeViewModel> = {
        let textField = PickerViewTextField<CompanyTypeViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "companyType".localized()
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var countryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var currencyTextField: PickerViewTextField<CurrencyViewModel> = {
        let textField = PickerViewTextField<CurrencyViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "currency".localized()
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 25
        button.setTitle("save".localized(), for: .normal)
        button.backgroundColor = .coral
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 23)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(uploadProfileButton)
        uploadProfileButton.addSubview(userImageView)
        uploadProfileButton.addSubview(updateProfileImageLabel)
        containerView.addSubview(userNameTextField)
        containerView.addSubview(emailTextField)
        containerView.addSubview(companyTypeTextField)
        containerView.addSubview(countryTextField)
        containerView.addSubview(currencyTextField)
        containerView.addSubview(saveButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupUploadImageButtonConstraints() {
        NSLayoutConstraint.activate([
            uploadProfileButton.topAnchor.constraint(equalTo:  containerView.topAnchor, constant: 20),
            uploadProfileButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0),
            uploadProfileButton.heightAnchor.constraint(equalToConstant: 130),
            uploadProfileButton.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
    
    private func setupEditProfileImageViewConstraints() {
        NSLayoutConstraint.activate([
            userImageView.topAnchor.constraint(equalTo: uploadProfileButton.topAnchor, constant: 5),
            userImageView.centerXAnchor.constraint(equalTo: uploadProfileButton.centerXAnchor, constant: 0),
            userImageView.heightAnchor.constraint(equalToConstant: 76),
            userImageView.widthAnchor.constraint(equalToConstant: 76)
        ])
    }
    private func setupUploadProfileImageLabelConstraints() {
        NSLayoutConstraint.activate([
            updateProfileImageLabel.topAnchor.constraint(equalTo: userImageView.bottomAnchor, constant: 5),
            updateProfileImageLabel.centerXAnchor.constraint(equalTo: uploadProfileButton.centerXAnchor, constant: 0),
            updateProfileImageLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupUserNameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            userNameTextField.topAnchor.constraint(equalTo: uploadProfileButton.bottomAnchor, constant: 15),
            userNameTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            userNameTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            userNameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupEmailTextFieldConstraints() {
        NSLayoutConstraint.activate([
            emailTextField.topAnchor.constraint(equalTo: userNameTextField.bottomAnchor, constant: 15),
            emailTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            emailTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            emailTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupCompanyTypeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            companyTypeTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 15),
            companyTypeTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            companyTypeTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            companyTypeTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            countryTextField.topAnchor.constraint(equalTo: companyTypeTextField.bottomAnchor, constant: 15),
            countryTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            countryTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            countryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupCurrencyTextFieldConstraints() {
        NSLayoutConstraint.activate([
            currencyTextField.topAnchor.constraint(equalTo: countryTextField.bottomAnchor, constant: 15),
            currencyTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            currencyTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            currencyTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupSaveButtonConstraints() {
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: currencyTextField.bottomAnchor, constant: 15),
            saveButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            saveButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            saveButton.heightAnchor.constraint(equalToConstant: 50),
            saveButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupUploadImageButtonConstraints()
        setupEditProfileImageViewConstraints()
        setupUploadProfileImageLabelConstraints()
        setupUserNameTextFieldConstraints()
        setupEmailTextFieldConstraints()
        setupCompanyTypeTextFieldConstraints()
        setupCountryTextFieldConstraints()
        setupCurrencyTextFieldConstraints()
        setupSaveButtonConstraints()
    }
    
    func configure(viewModel: UserViewModel) {
        userNameTextField.text = viewModel.username
        emailTextField.text = viewModel.email
        companyTypeTextField.setSelectedItem(viewModel.companyType)
        countryTextField.setSelectedItem(viewModel.countryName)
        currencyTextField.setSelectedItem(viewModel.currency)
        userImageView.load(url: viewModel.imageUrl, placeholder: UIImage(named: "ic_upload_photo"))
    }
}
