//
//  EditCompanyProfileInteractor.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation


class EditCompanyProfileInteractor: EditCompanyProfileInteractorInputProtocol {
 
    weak var presenter: EditCompanyProfileInteractorOutputProtocol?
    
    private var userWorker = UserWorker()
    private let applicationDataWorker = ApplicationDataWorker()
    
    func updateCompanyProfile(companyName: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: [File]?) {
        userWorker.updateUserProfile(userName: companyName, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: photo) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUpdateUserProfile(with: result)
        }
    }
    
    func getCountriesAndCurrencies() {
        applicationDataWorker.getCountriesAndCurrenceies { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountriesAndCurrencies(with: result)
        }

    }
    
    func getCompanyType() {
        applicationDataWorker.getCompanyTypes { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCompanyTypes(with: result)
        }
    }
}
