//
//  EditCompanyProfileRouter.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit


class EditCompanyProfileRouter: EditCompanyProfileRouterProtocol {
  
    weak var viewController: UIViewController?
    
    private lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = (viewController as! EditCompanyProfileViewController) 
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = .coral
        imagePicker.navigationBar.tintColor = .white
        imagePicker.navigationBar.barStyle = .black
        return imagePicker
    }()
    

    
    static func createModule(with viewModel: UserViewModel) -> UIViewController {
        let view = EditCompanyProfileViewController(viewModel: viewModel)
        let interactor = EditCompanyProfileInteractor()
        let router = EditCompanyProfileRouter()
        let presenter = EditCompanyProfilePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showPermissionAlert(with title: String, message: String) {
        AlertBuilder(title: title, message: message, preferredStyle: .alert)
            .addAction(title: "Cancel", style: .cancel)
            .addAction(title: "Settings", style: .default) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
            .build()
            .show()
        

    }
    
    func presentImagePicker() {
        viewController?.present(imagePicker, animated: true, completion: nil)
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    
    
}
