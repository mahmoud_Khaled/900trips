//
//  EditCompanyProfileViewController.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit


class EditCompanyProfileViewController: BaseViewController, EditCompanyProfileViewProtocol {

    private let mainView = EditCompanyProfileView()
    var presenter: EditCompanyProfilePresenterProtocol!
    private var countryId: Int?
    private var currencyId: Int?
    private var companyTypeId: Int?
    private let viewModel: UserViewModel
    
    private var hasSelectedImage = false
    
    init(viewModel: UserViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupNavigationBarStyle()
        pickerViewDidSelectItemAction()
        addTargets()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
        title = "editCompanyInformation".localized()
    }
    
    private func addTargets() {
        mainView.saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        mainView.uploadProfileButton.addTarget(self, action: #selector(uploadImagesButtonTapped), for: .touchUpInside)
    }
    
    @objc func saveButtonTapped() {
        let imageToUpload = (name: "photo.jpg", key: "photo", mimeType: MimeType.jpg, data: mainView.userImageView.image?.jpegData(compressionQuality: 0.5) ?? Data())
        presenter.updateUserProfile(username: mainView.userNameTextField.text!, email: mainView.emailTextField.text!, countryId: countryId ?? 0, currencyId: currencyId ?? 0, companyType: companyTypeId ?? 0, photo: hasSelectedImage ? imageToUpload : nil)
    }
    
    
   
    func setCurrenciesAndCountries(countries: [CountryViewModel], currencies: [CurrencyViewModel]) {
        mainView.countryTextField.items = countries
        mainView.currencyTextField.items = currencies
        mainView.configure(viewModel: viewModel)
    }
    
    func setCompanyTypes(companyType: [CompanyTypeViewModel]) {
        mainView.companyTypeTextField.items = companyType
        mainView.configure(viewModel: viewModel)
    }
    
    private func pickerViewDidSelectItemAction() {
        mainView.currencyTextField.didSelectItem = { [weak self] item in
            guard let self = self  else { return }
            self.currencyId =  item.id
        }
        
        mainView.countryTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.countryId =  item.id
        }
        
        mainView.companyTypeTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.companyTypeId =  item.id
        }
    }
    
    @objc private func uploadImagesButtonTapped() {
        presenter.presentImagePicker()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "yourInformationisUpdateSuccessfully".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.presenter.dismiss()
        }
    }
    
}

extension EditCompanyProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.mainView.userImageView.image = image
                self.hasSelectedImage = true
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
