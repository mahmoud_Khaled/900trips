//
//  File.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol EditCompanyProfileRouterProtocol: class {
    func showAlert(with message: String)
    func showPermissionAlert(with title: String, message: String)
    func presentImagePicker()
    func dismiss()
}

protocol EditCompanyProfilePresenterProtocol: class {
    func updateUserProfile(username: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: File?)
    func viewDidLoad()
    func showPermissionAlert(with title: String, message: String)
    func presentImagePicker()
    func dismiss()

}

protocol EditCompanyProfileInteractorInputProtocol: class {
    func updateCompanyProfile(companyName: String, email: String, countryId: Int, currencyId: Int, companyType:Int, photo: [File]?)
    func getCountriesAndCurrencies()
    func getCompanyType()

}

protocol EditCompanyProfileInteractorOutputProtocol: class {
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>)
    func didUpdateUserProfile(with result: Result<Data>)
    func didFetchCompanyTypes(with result: Result<[CompanyType]> )

}

protocol EditCompanyProfileViewProtocol: class {
    func setCurrenciesAndCountries(countries: [CountryViewModel], currencies: [CurrencyViewModel])
    func setCompanyTypes(companyType: [CompanyTypeViewModel])
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()

}
