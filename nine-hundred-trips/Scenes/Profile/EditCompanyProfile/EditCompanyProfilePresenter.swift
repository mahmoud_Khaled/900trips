//
//  EditCompanyProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by Raghad Ali on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit


class EditCompanyProfilePresenter: EditCompanyProfilePresenterProtocol, EditCompanyProfileInteractorOutputProtocol {
   
    weak var view: EditCompanyProfileViewProtocol?
    private let interactor: EditCompanyProfileInteractorInputProtocol
    private let router: EditCompanyProfileRouterProtocol
    
    private var viewModel: UserViewModel!
    
    private var countryId = 0
    private var currencyId = 0
    private var companyId = 0
    
    init(view: EditCompanyProfileViewProtocol, interactor: EditCompanyProfileInteractorInputProtocol, router: EditCompanyProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getCountriesAndCurrencies()
        interactor.getCompanyType()
    }
    
    func updateUserProfile(username: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: File?) {
        view?.showActivityIndicator()
        self.countryId = countryId
        self.currencyId = currencyId
        self.companyId = companyType
        if let photo = photo {
            interactor.updateCompanyProfile(companyName: username, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: [photo])
        } else {
            interactor.updateCompanyProfile(companyName: username, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: nil)
        }
        
    }
    
 
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            view?.setCurrenciesAndCountries(countries: value.countries.map(CountryViewModel.init), currencies: value.currencies.map(CurrencyViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCompanyTypes(with result: Result<[CompanyType]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            view?.setCompanyTypes(companyType: value.map(CompanyTypeViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didUpdateUserProfile(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
            UserDefaultsHelper.countryId = countryId
            UserDefaultsHelper.currencyId = currencyId
            UserDefaultsHelper.companyId = companyId
            NotificationCenter.default.post(name: .updateProfile, object: nil, userInfo: nil)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func showPermissionAlert(with title: String, message: String) {
        router.showPermissionAlert(with: title, message: message)
    }
    
    func presentImagePicker() {
        router.presentImagePicker()
    }
    
    func dismiss() {
         router.dismiss()
    }
    
}
