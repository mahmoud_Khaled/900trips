//
//  UpdateCompanyPhoneNumberInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditPhoneNumberInteractor: EditPhoneNumberInteractorInputProtocol {
   
    weak var presenter: EditPhoneNumberInteractorOutputProtocol?
     private var userWorker = UserWorker()
    
    func updatePhoneNumbers(phone: String, secondPhoneNumber: String) {
        userWorker.updateCompanyPhoneNumbers(phone: phone, secondPhoneNumber: secondPhoneNumber) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUpdatePhoneNumber(with: result)
        }
    }
    
}
