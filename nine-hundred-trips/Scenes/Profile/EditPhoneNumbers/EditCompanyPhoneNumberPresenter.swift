//
//  UpdateCompanyPhoneNumberPresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class EditPhoneNumberPresenter: EditPhoneNumberPresenterProtocol, EditPhoneNumberInteractorOutputProtocol {
  
    weak var view: EditPhoneNumberViewProtocol?
    private let interactor: EditPhoneNumberInteractorInputProtocol
    private let router: EditPhoneNumberRouterProtocol
    
    init(view: EditPhoneNumberViewProtocol, interactor: EditPhoneNumberInteractorInputProtocol, router: EditPhoneNumberRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func saveButtonTapped(phone: String, secondPhoneNumber: String) {
         view?.showActivityIndicator()
        interactor.updatePhoneNumbers(phone: phone, secondPhoneNumber: secondPhoneNumber)
    }

    func didUpdatePhoneNumber(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func dismiss() {
        router.dismiss()
    }
    
}
