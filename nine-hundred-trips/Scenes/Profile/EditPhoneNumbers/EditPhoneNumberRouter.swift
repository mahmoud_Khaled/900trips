//
//  UpdateCompanyPhoneNumberRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditPhoneNumberRouter: EditPhoneNumberRouterProtocol {
  
    weak var viewController: UIViewController?
    
    static func createModule(with viewModel: UserViewModel) -> UIViewController {
        let view = EditPhoneNumberViewController(viewModel: viewModel)
        let interactor = EditPhoneNumberInteractor()
        let router = EditPhoneNumberRouter()
        let presenter = EditPhoneNumberPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    
    
}
