//
//  UpdateCompanyPhoneNumberView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditPhoneNumberView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var phoneNumberTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = .textColor
        textField.font = DinNextFont.regular.getFont(ofSize: 18)
        textField.textAlignment = .center
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var secondPhoneNumberTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = .textColor
        textField.font = DinNextFont.regular.getFont(ofSize: 18)
        textField.textAlignment = .center
        textField.layer.cornerRadius = 30.5
        textField.layer.applySketchShadow(color: UIColor.textColor.withAlphaComponent(0.20), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    private lazy var phoneNumberStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [phoneNumberTextField, secondPhoneNumberTextField])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 13
        return stackView
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 25
        button.setTitle("save".localized(), for: .normal)
        button.backgroundColor = .coral
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 23)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(phoneNumberStackView)
        containerView.addSubview(saveButton)
    }
    
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupPhoneNumberStackViewConstraints() {
        NSLayoutConstraint.activate([
            phoneNumberStackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 25),
            phoneNumberStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 25),
            phoneNumberStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -25),
            phoneNumberStackView.heightAnchor.constraint(equalToConstant: 140)
        ])
    }
    
    private func setupSaveButtonConstraints() {
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: phoneNumberStackView.bottomAnchor, constant: 30),
            saveButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 25),
            saveButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -25),
            saveButton.heightAnchor.constraint(equalToConstant: 50),
            saveButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupPhoneNumberStackViewConstraints()
        setupSaveButtonConstraints()
    }
    
    func configure(viewModel: UserViewModel) {
        phoneNumberTextField.text = viewModel.phone
        secondPhoneNumberTextField.text = viewModel.phone1
    }
}
