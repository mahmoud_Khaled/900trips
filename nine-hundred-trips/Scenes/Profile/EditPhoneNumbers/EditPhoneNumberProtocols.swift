//
//  UpdateCompanyPhoneNumberProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/27/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol EditPhoneNumberRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
}

protocol EditPhoneNumberPresenterProtocol: class {
    var view: EditPhoneNumberViewProtocol? { get set }
    func saveButtonTapped(phone: String, secondPhoneNumber: String)
    func dismiss()
}

protocol EditPhoneNumberInteractorInputProtocol: class {
    var presenter: EditPhoneNumberInteractorOutputProtocol? { get set }
    func updatePhoneNumbers(phone: String, secondPhoneNumber: String)
}

protocol EditPhoneNumberInteractorOutputProtocol: class {
    func didUpdatePhoneNumber(with result: Result<Data>)
}

protocol EditPhoneNumberViewProtocol: class {
    var presenter: EditPhoneNumberPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
