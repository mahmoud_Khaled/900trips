//
//  EditUserProfileInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditUserProfileInteractor: EditUserProfileInteractorInputProtocol {
  
    weak var presenter: EditUserProfileInteractorOutputProtocol?
    
    
    private let applicationDataWorker = ApplicationDataWorker()
    private let userWorker = UserWorker()
    
    func updateUserProfile(userName: String, email: String, countryId: Int, currencyId: Int, companyType: Int,  photo: [File]?) {
        userWorker.updateUserProfile(userName: userName, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: photo) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUpdateUserProfile(with: result)
        }
    }
    
    func getCountriesAndCurrencies() {
        applicationDataWorker.getCountriesAndCurrenceies { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountriesAndCurrencies(with: result)
        }
    }
}
