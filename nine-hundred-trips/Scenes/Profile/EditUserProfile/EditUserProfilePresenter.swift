//
//  EditUserProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class EditUserProfilePresenter: EditUserProfilePresenterProtocol, EditUserProfileInteractorOutputProtocol {
 
    weak var view: EditUserProfileViewProtocol?
    private let interactor: EditUserProfileInteractorInputProtocol
    private let router: EditUserProfileRouterProtocol
    
    private var countries: [Country] = []
    private var currencies: [Currency] = []

    private var countryId = 0
    private var currencyId = 0
    
    init(view: EditUserProfileViewProtocol, interactor: EditUserProfileInteractorInputProtocol, router: EditUserProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getCountriesAndCurrencies()
    }
    
    func updateUserProfile(username: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: File?) {
        view?.showActivityIndicator()
        self.countryId = countryId
        self.currencyId = currencyId
        if let photo = photo {
            interactor.updateUserProfile(userName: username, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: [photo])
        } else {
            interactor.updateUserProfile(userName: username, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType, photo: nil)
        }
        
    }
    
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            countries = value.countries
            currencies = value.currencies
            view?.setCurrenciesAndCountries(countries: countries.map(CountryViewModel.init), currencies: currencies.map(CurrencyViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didUpdateUserProfile(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
            UserDefaultsHelper.countryId = countryId
            UserDefaultsHelper.currencyId = currencyId
            NotificationCenter.default.post(name: .updateProfile, object: nil, userInfo: nil)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func showPermissionAlert(with title: String, message: String) {
        router.showPermissionAlert(with: title, message: message)
    }
    
    func presentImagePicker() {
        router.presentImagePicker()
    }
    
    func dismiss() {
        router.dismiss()
    }
}
