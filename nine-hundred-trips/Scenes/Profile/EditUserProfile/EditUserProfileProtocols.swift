//
//  EditUserProfileProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol EditUserProfileRouterProtocol: class {
    func showAlert(with message: String)
    func showPermissionAlert(with title: String, message: String)
    func presentImagePicker()
    func dismiss()
}

protocol EditUserProfilePresenterProtocol: class {
    var view: EditUserProfileViewProtocol? { get set }
    func updateUserProfile(username: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: File?)
    func viewDidLoad()
    func showPermissionAlert(with title: String, message: String)
    func presentImagePicker()
    func dismiss()
}

protocol EditUserProfileInteractorInputProtocol: class {
    var presenter: EditUserProfileInteractorOutputProtocol? { get set }
    func updateUserProfile(userName: String, email: String, countryId: Int, currencyId: Int, companyType:Int, photo: [File]?)
    func getCountriesAndCurrencies()
}

protocol EditUserProfileInteractorOutputProtocol: class {
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>)
    func didUpdateUserProfile(with result: Result<Data>)
}

protocol EditUserProfileViewProtocol: class {
    var presenter: EditUserProfilePresenterProtocol! { get set }
    func setCurrenciesAndCountries(countries: [CountryViewModel], currencies: [CurrencyViewModel])
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
