//
//  UpdateCompanyProfileRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum UpdateProfileCompanyDestination {
    case editProfile(viewModel: UserViewModel)
    case editPassword
    case phoneNumbers(viewModel: UserViewModel)
    case socialMedia(viewModel: UserViewModel)
}


class CompanyInformationRouter: CompanyInformationRouterProtocol {

    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = CompanyInformationViewController()
        let interactor = CompanyInformationInteractor()
        let router = CompanyInformationRouter()
        let presenter = CompanyInformationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigate(destination: UpdateProfileCompanyDestination) {
        switch destination {
        case .editProfile(let viewModel):
            viewController?.navigationController?.pushViewController(EditCompanyProfileRouter.createModule(with: viewModel), animated: true)
        case .editPassword:
            viewController?.navigationController?.pushViewController(EditUserPasswordRouter.createModule(), animated: true)
        case .phoneNumbers(let viewModel):
             viewController?.navigationController?.pushViewController(EditPhoneNumberRouter.createModule(with: viewModel), animated: true)
        case .socialMedia(let viewModel):
            viewController?.navigationController?.pushViewController(EditSocialMedialLinksRouter.createModule(with: viewModel), animated: true)
       }
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }

}
