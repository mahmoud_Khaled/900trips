//
//  UpdateCompanyProfileProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol CompanyInformationRouterProtocol: class {
    func navigate(destination: UpdateProfileCompanyDestination)
    func showAlert(with message: String)

}

protocol CompanyInformationPresenterProtocol: class {
    var view: CompanyInformationProfileViewProtocol? { get set }
    func editProfileButtonTapped()
    func editPasswordButtonTapped()
    func phoneNumbersButtonTapped()
    func socialMediaButtonTapped()
    func getUserData()

}

protocol CompanyInformationInteractorInputProtocol: class {
    var presenter: CompanyInformationInteractorOutputProtocol? { get set }
    func getUserData()
}

protocol CompanyInformationInteractorOutputProtocol: class {
     func didFetchUserData(with result: Result<User>)
    
}

protocol CompanyInformationProfileViewProtocol: class {
    var presenter: CompanyInformationPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func setUserData(viewModel: UserViewModel)

}
