//
//  UpdateCompanyProfileView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyInformationView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        return scrolView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var mainView: UpdateProfileCommonView = {
        let mainView = UpdateProfileCommonView(userType: UserDefaultsHelper.userType)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        return mainView
    }()
    
    private lazy var separatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .veryLightGray
        return view
    }()
    
    lazy var phoneNumbersButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("phoneNumbers".localized(), for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 18)
        button.contentHorizontalAlignment = .left
        button.tag = -1
        button.titleEdgeInsets.left = 10
        return button
    }()
    
    lazy var socialMediaButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("socialMedia".localized(), for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 18)
        button.contentHorizontalAlignment = .left
        button.tag = -1
        button.titleEdgeInsets.left = 10
        return button
    }()
    
    private lazy var bottomSeparatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .veryLightGray
        return view
    }()
    
    private lazy var bottomButtonView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.borderColor = UIColor.veryLightGray.cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        backgroundColor = .white
    }
    
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(mainView)
        containerView.addSubview(separatorLineView)
        containerView.addSubview(bottomButtonView)
        containerView.addSubview(phoneNumbersButton)
        containerView.addSubview(bottomSeparatorLineView)
        containerView.addSubview(socialMediaButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupMainViewConstraints() {
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            mainView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            mainView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
        ])
    }
    
    private func setupSeparatorLineViewConstrains() {
        NSLayoutConstraint.activate([
            separatorLineView.topAnchor.constraint(equalTo: mainView.bottomAnchor, constant: 0),
            separatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 17),
            separatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -17),
            separatorLineView.heightAnchor.constraint(equalToConstant: 0.5),
        ])
    }
    
    private func setupBottomButtonsViewConstraints() {
        NSLayoutConstraint.activate([
            bottomButtonView.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 30),
            bottomButtonView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            bottomButtonView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            bottomButtonView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupPhoneNumbersConstraints() {
        NSLayoutConstraint.activate([
            phoneNumbersButton.topAnchor.constraint(equalTo: bottomButtonView.topAnchor, constant: 5),
            phoneNumbersButton.leadingAnchor.constraint(equalTo: bottomButtonView.leadingAnchor, constant: 5),
            phoneNumbersButton.trailingAnchor.constraint(equalTo: bottomButtonView.trailingAnchor, constant: -5),
        ])
    }
    
    private func setupBottomSeparatorLineViewConstraints() {
        NSLayoutConstraint.activate([
            bottomSeparatorLineView.topAnchor.constraint(equalTo: phoneNumbersButton.bottomAnchor, constant: 5),
            bottomSeparatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            bottomSeparatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            bottomSeparatorLineView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
    private func setupSocialMediaConstraints() {
        NSLayoutConstraint.activate([
            socialMediaButton.topAnchor.constraint(equalTo: bottomSeparatorLineView.bottomAnchor, constant: 5),
            socialMediaButton.leadingAnchor.constraint(equalTo: bottomButtonView.leadingAnchor, constant: 5),
            socialMediaButton.trailingAnchor.constraint(equalTo: bottomButtonView.trailingAnchor, constant: -5),
            socialMediaButton.bottomAnchor.constraint(equalTo: bottomButtonView.bottomAnchor, constant: -5)
        ])
    }

    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupMainViewConstraints()
        setupSeparatorLineViewConstrains()
        setupBottomButtonsViewConstraints()
        setupPhoneNumbersConstraints()
        setupBottomSeparatorLineViewConstraints()
        setupSocialMediaConstraints()
    }
    
    
    func configure(viewModel: UserViewModel) {
        mainView.dynamicUsernameLabel.text = viewModel.username
        mainView.dynamicEmailLabel.text = viewModel.email
        mainView.dynamicCompanyTypeLabel.text = viewModel.companyType
        mainView.dynamicCountryLabel.text = viewModel.countryName
        mainView.dynamicCurrencyLabel.text = viewModel.currency
        mainView.updateProfileImage.load(url: viewModel.imageUrl, placeholder: UIImage(named: "profile_photo"))
    }
    
}
