//
//  UpdateCompanyProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompanyInformationPresenter: CompanyInformationPresenterProtocol, CompanyInformationInteractorOutputProtocol {
  
    weak var view: CompanyInformationProfileViewProtocol?
    private let interactor: CompanyInformationInteractorInputProtocol
    private let router: CompanyInformationRouterProtocol
    
    private var viewModel: UserViewModel!
    

    
    init(view: CompanyInformationProfileViewProtocol, interactor: CompanyInformationInteractorInputProtocol, router: CompanyInformationRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func didFetchUserData(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let user):
            viewModel = UserViewModel(user: user)
            view?.setUserData(viewModel: viewModel)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func getUserData() {
        view?.showActivityIndicator()
        interactor.getUserData()
    }
    
    func editProfileButtonTapped() {
        guard viewModel != nil else { return }
        router.navigate(destination: .editProfile(viewModel: viewModel))
    }
    
    func editPasswordButtonTapped() {
        guard viewModel != nil else { return }
        router.navigate(destination: .editPassword)
    }
    
    func phoneNumbersButtonTapped() {
        guard viewModel != nil else { return }
        router.navigate(destination: .phoneNumbers(viewModel: viewModel))
    }
    
    func socialMediaButtonTapped() {
        guard viewModel != nil else { return }
        router.navigate(destination: .socialMedia(viewModel: viewModel))
    }
    
}
