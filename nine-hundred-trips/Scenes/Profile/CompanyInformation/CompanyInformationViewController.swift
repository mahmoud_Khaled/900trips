//
//  UpdateCompanyProfileViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyInformationViewController: BaseViewController, CompanyInformationProfileViewProtocol {
    
   
    private let mainView = CompanyInformationView()
    var presenter: CompanyInformationPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserData()
        addTargets()
        navigationController?.isNavigationBarHidden = false
    }
    

    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        title = "companyInformation".localized()
    }
    
    private func setupNavigationBarButtons() {
        let editButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_editinformation"))
            .height(20)
            .width(20)
            .action(target: self, selector: #selector(editButtonTapped))
            .build()
        navigationItem.rightBarButtonItems = [editButton]
        
    }
    
    private func addTargets() {
        mainView.mainView.editPasswordButton.addTarget(self, action: #selector(editPasswordButtonTapped), for: .touchUpInside)
        mainView.phoneNumbersButton.addTarget(self, action: #selector(phoneNumbersButtonTapped), for: .touchUpInside)
        mainView.socialMediaButton.addTarget(self, action: #selector(socialMediaButtonTapped), for: .touchUpInside)
    }
    
    @objc private func editButtonTapped() {
        presenter?.editProfileButtonTapped()
    }
    
    @objc func editPasswordButtonTapped() {
        presenter.editPasswordButtonTapped()
    }
    
    @objc func phoneNumbersButtonTapped() {
        presenter.phoneNumbersButtonTapped()
    }
    
    @objc private func socialMediaButtonTapped() {
        presenter.socialMediaButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    private func getUserData() {
        presenter.getUserData()
    }

    func setUserData(viewModel: UserViewModel) {
        mainView.configure(viewModel: viewModel)
    }
    
}
