//
//  EditUserPasswordInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditUserPasswordInteractor: EditUserPasswordInteractorInputProtocol {
   
    weak var presenter: EditUserPasswordInteractorOutputProtocol?
    private let userWorker = UserWorker()
    
    func editPassword(oldPassword: String, newPassword: String, confirmPassword: String) {
        userWorker.editPassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword) { [ weak self ] result in
             guard let self = self else { return }
            self.presenter?.didEditPassword(with: result)
        }
    }
}
