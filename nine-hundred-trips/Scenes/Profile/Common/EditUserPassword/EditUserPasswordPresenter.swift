//
//  EditUserPasswordPresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class EditUserPasswordPresenter: EditUserPasswordPresenterProtocol, EditUserPasswordInteractorOutputProtocol {
  
    weak var view: EditUserPasswordViewProtocol?
    private let interactor: EditUserPasswordInteractorInputProtocol
    private let router: EditUserPasswordRouterProtocol
    
    init(view: EditUserPasswordViewProtocol, interactor: EditUserPasswordInteractorInputProtocol, router: EditUserPasswordRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func saveButtonTapped(oldPassword: String, newPassword: String, confirmPassword: String) {
        if oldPassword.isEmpty || newPassword.isEmpty || confirmPassword.isEmpty {
            router.showAlert(with: "requiredFields".localized())
        } else if oldPassword == newPassword {
            router.showAlert(with: "newPasswordAndOldPasswordIsTheSame".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$").evaluate(with: newPassword) {
              router.showAlert(with: "incorrectPassword".localized())
        } else if newPassword != confirmPassword {
            router.showAlert(with: "newPasswordAndCOnfirmPasswordNotTheSame".localized())
        } else {
            view?.showActivityIndicator()
            interactor.editPassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)
        }
    }
    
    func didEditPassword(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    
    func dismiss() {
        router.dismiss()
    }
    
}
