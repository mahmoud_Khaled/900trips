//
//  EditUserPasswordProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol EditUserPasswordRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
}

protocol EditUserPasswordPresenterProtocol: class {
    var view: EditUserPasswordViewProtocol? { get set }
    func saveButtonTapped(oldPassword: String, newPassword: String, confirmPassword: String)
    func dismiss()
}

protocol EditUserPasswordInteractorInputProtocol: class {
    var presenter: EditUserPasswordInteractorOutputProtocol? { get set }
    func editPassword(oldPassword: String, newPassword: String, confirmPassword: String)
}

protocol EditUserPasswordInteractorOutputProtocol: class {
     func didEditPassword(with result: Result<Data>)
    
}

protocol EditUserPasswordViewProtocol: class {
    var presenter: EditUserPasswordPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
