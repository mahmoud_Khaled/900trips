//
//  EditUserPasswordView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//
import UIKit

class EditUserPasswordView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var oldPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false  
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.attributedPlaceholder = NSAttributedString(string: "oldPassword".localized(),
                                                             attributes: [.foregroundColor: UIColor.darkGray])
        textField.layer.applySketchShadow(color: .shadow, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var newPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.attributedPlaceholder = NSAttributedString(string:  "newPassword".localized(),
                                                             attributes: [.foregroundColor: UIColor.darkGray])
        textField.layer.applySketchShadow(color: .shadow, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var confirmNewPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.attributedPlaceholder = NSAttributedString(string: "repeatNewPassword".localized(),
                                                             attributes: [.foregroundColor: UIColor.darkGray])
        textField.layer.applySketchShadow(color: .shadow, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 25
        button.setTitle("save".localized(), for: .normal)
        button.backgroundColor = .coral
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(oldPasswordTextField)
        containerView.addSubview(newPasswordTextField)
        containerView.addSubview(confirmNewPasswordTextField)
        containerView.addSubview(saveButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupOldPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            oldPasswordTextField.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 35),
            oldPasswordTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            oldPasswordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            oldPasswordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    private func setupNewPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            newPasswordTextField.topAnchor.constraint(equalTo: oldPasswordTextField.bottomAnchor, constant: 15),
            newPasswordTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            newPasswordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            newPasswordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    private func setupConfirmNewPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            confirmNewPasswordTextField.topAnchor.constraint(equalTo: newPasswordTextField.bottomAnchor, constant: 15),
            confirmNewPasswordTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            confirmNewPasswordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            confirmNewPasswordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupSaveButtonConstraints() {
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: confirmNewPasswordTextField.bottomAnchor, constant: 15),
            saveButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            saveButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            saveButton.heightAnchor.constraint(equalToConstant: 50),
            saveButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupOldPasswordTextFieldConstraints()
        setupNewPasswordTextFieldConstraints()
        setupConfirmNewPasswordTextFieldConstraints()
        setupSaveButtonConstraints()
    }
    
}
