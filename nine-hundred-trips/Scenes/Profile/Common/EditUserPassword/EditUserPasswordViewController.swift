//
//  EditUserPasswordViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditUserPasswordViewController: BaseViewController, EditUserPasswordViewProtocol {
   
    private let mainView = EditUserPasswordView()
    var presenter: EditUserPasswordPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        addTargets()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
        title = "editPassword".localized()
    }
    
    private func addTargets() {
        mainView.saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
    }
    
    @objc func saveButtonTapped() {
        presenter.saveButtonTapped(oldPassword: mainView.oldPasswordTextField.text!, newPassword: mainView.newPasswordTextField.text!, confirmPassword: mainView.confirmNewPasswordTextField.text!)
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "yourPasswordIsUpdated".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.presenter.dismiss()
        }
    }
}
