//
//  EditUserPasswordRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class EditUserPasswordRouter: EditUserPasswordRouterProtocol {
  
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = EditUserPasswordViewController()
        let interactor = EditUserPasswordInteractor()
        let router = EditUserPasswordRouter()
        let presenter = EditUserPasswordPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
       viewController?.navigationController?.popViewController(animated: true)
    }
    
}
