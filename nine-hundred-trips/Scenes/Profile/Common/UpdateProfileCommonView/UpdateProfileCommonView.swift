//
//  UpdateProfileView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/20/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ subviews: [UIView]) {
        subviews.forEach(addArrangedSubview(_:))
    }
}

class UpdateProfileCommonView: UIView {
    
    lazy var updateProfileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .gray
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.isUserInteractionEnabled = false
        imageView.layer.cornerRadius = 38
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var informationView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var staticUsernameLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "username".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var staticEmailLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "email".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var staticCompanyTypeLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "companyType".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var statisCountryLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "country".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var statisCurrencyLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "currency".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var staticPasswordLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "password".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textColor = .darkGray
        label.backgroundColor = UIColor.babyGray.withAlphaComponent(0.42)
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        return label
    }()
    
    private lazy var staticPersonalInformationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var dynamicUsernameLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dynamicEmailLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dynamicCompanyTypeLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dynamicCountryLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dynamicCurrencyLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    lazy var dynamicPasswordLabel: PaddingLabel = {
        let label = PaddingLabel()
        label.leftInset = 12
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "*************".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .cyan
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.babyGray.cgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        return label
    }()
    
    
    private lazy var dynamicPersonalInformationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var editPasswordButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_editpass"), for: .normal)
        button.setTitle("editPassword".localized(), for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 16)
        button.tag = -1
        button.titleEdgeInsets.left = 16
        return button
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(userType: UserType) {
        super.init(frame: .zero)
        layoutUI()
        if userType == .client {
            staticPersonalInformationStackView.addArrangedSubviews([
                staticUsernameLabel,
                staticEmailLabel,
                statisCountryLabel,
                statisCurrencyLabel,
                staticPasswordLabel
            ])
            
            dynamicPersonalInformationStackView.addArrangedSubviews([
                dynamicUsernameLabel,
                dynamicEmailLabel,
                dynamicCountryLabel,
                dynamicCurrencyLabel,
                dynamicPasswordLabel
            ])
            
        } else {
            staticPersonalInformationStackView.addArrangedSubviews([
                staticUsernameLabel,
                staticEmailLabel,
                staticCompanyTypeLabel,
                statisCountryLabel,
                statisCurrencyLabel,
                staticPasswordLabel
            ])
            
            dynamicPersonalInformationStackView.addArrangedSubviews([
                dynamicUsernameLabel,
                dynamicEmailLabel,
                dynamicCompanyTypeLabel,
                dynamicCountryLabel,
                dynamicCurrencyLabel,
                dynamicPasswordLabel
            ])
        }
}
    
    private func addSubviews() {
        addSubview(updateProfileImage)
        addSubview(staticPersonalInformationStackView)
        addSubview(dynamicPersonalInformationStackView)
        addSubview(editPasswordButton)
    }
    
    private func setupEditProfileImageViewConstraints() {
        NSLayoutConstraint.activate([
            updateProfileImage.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            updateProfileImage.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0),
            updateProfileImage.heightAnchor.constraint(equalToConstant: 76),
            updateProfileImage.widthAnchor.constraint(equalToConstant: 76)
        ])
    }
    
    private func setupStaticPersonalInformationStackViewIConstraints() {
        NSLayoutConstraint.activate([
            staticPersonalInformationStackView.topAnchor.constraint(equalTo: updateProfileImage.bottomAnchor, constant: 15),
            staticPersonalInformationStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 3),
            staticPersonalInformationStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.39)
        ])
    }
    
    private func setupDynamicPersonalInformationStackViewIConstraints() {
        NSLayoutConstraint.activate([
            dynamicPersonalInformationStackView.topAnchor.constraint(equalTo: updateProfileImage.bottomAnchor, constant: 15),
            dynamicPersonalInformationStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 3),
            dynamicPersonalInformationStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.610),
            dynamicPersonalInformationStackView.heightAnchor.constraint(equalTo: staticPersonalInformationStackView.heightAnchor, multiplier: 1.0)
        ])
    }
    
    private func setupEditPaswordButtonConstraints() {
        NSLayoutConstraint.activate([
            editPasswordButton.topAnchor.constraint(equalTo: dynamicPersonalInformationStackView.bottomAnchor, constant: 25),
            editPasswordButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            editPasswordButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            editPasswordButton.heightAnchor.constraint(equalToConstant: 34),
            editPasswordButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
        ])
        
    }
    private func layoutUI() {
        addSubviews()
        setupEditProfileImageViewConstraints()
        setupStaticPersonalInformationStackViewIConstraints()
        setupDynamicPersonalInformationStackViewIConstraints()
        setupEditPaswordButtonConstraints()
    }
    
}

