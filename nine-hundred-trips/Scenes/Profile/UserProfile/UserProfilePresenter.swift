//
//  UserProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class UserProfilePresenter: UserProfilePresenterProtocol, UserProfileInteractorOutputProtocol {
   

    weak var view: UserProfileViewProtocol?
    private let interactor: UserProfileInteractorInputProtocol
    private let router: UserProfileRouterProtocol
    
    private var tableViewItems = [ProfileItem]()
    
    var numberOfRows: Int {
        return tableViewItems.count
    }
    
    init(view: UserProfileViewProtocol, interactor: UserProfileInteractorInputProtocol, router: UserProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        
        if UserDefaultsHelper.isLoggedIn {
            tableViewItems.append(contentsOf: [
                ProfileItem(
                    name: "personalInformation".localized(),
                    image: "ic_edit",
                    editImage: "ic_more_colored",
                    action: { [weak self] in
                        guard let self = self else { return }
                        self.router.navigate(to: .editProfile)
                    }
                ),
                ProfileItem(
                    name: "reservationRequests".localized(),
                    image: "ic_note",
                    editImage: "",
                    action: { [weak self] in
                        guard let self = self else { return }
                        self.router.navigate(to: .reservationRequests)
                    }
                ),
                ProfileItem(
                    name: "favourites".localized(),
                    image: "ic_favourites",
                    editImage: "",
                    action: { [weak self] in
                        guard let self = self else { return }
                        self.router.navigate(to: .favourites)
                    }
                ),
                ProfileItem(
                    name: "requestQuotation".localized(),
                    image: "ic_tag",
                    editImage: "",
                    action: { [weak self] in
                        guard let self = self else { return }
                        self.router.navigate(to: .requestQuotation)
                    }
                ),
            ])
        }
        
        if !UserDefaultsHelper.isLoggedIn {
            tableViewItems.append(
                ProfileItem(
                    name: "countryAndcurrency".localized(),
                    image: "ic_globe_unfilled",
                    editImage: "",
                    action: { [weak self] in
                        guard let self = self else { return }
                        self.router.navigate(to: .countryAndCurrency)
                    }
                )
            )
        }
        
        tableViewItems.append(contentsOf: [
            ProfileItem(
                name: "language".localized(),
                image: "ic_translate",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .applicationLanguage)
                }
            ),
            ProfileItem(
                name: "contactUs".localized(),
                image: "ic_contactus",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .contactUs)
                }
            ),
            ProfileItem(
                name: "aboutApplication".localized(),
                image: "ic_info",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .aboutApplication)
                }
            ),
            ProfileItem(
                name: "termsAndConditions".localized(),
                image: "ic_terms_and_conditions",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .termsAndConditions)
                }
            )
        ])
        
        if UserDefaultsHelper.isLoggedIn {
            tableViewItems.append(ProfileItem(
                name: "budgetAndAccounts".localized(),
                image: "ic_excel",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.view?.showActivityIndicator()
                    self.interactor.downloadBudgetAndAccountsFile()
                }
            ))
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateProfile), name: .updateProfile, object: nil)
    }
    
    
    func configure(cell: AnyConfigurableCell<ProfileItem>, for indexPath: IndexPath) {
        cell.configure(model: tableViewItems[indexPath.row])
    }
    
    func logoutButtonTapped() {
        view?.showActivityIndicator()
        interactor.logout()
    }
    
    func getUserData() {
        if UserDefaultsHelper.isLoggedIn {
            view?.showActivityIndicator()
            interactor.getUserData()
        } else {
            view?.prepareProfileForVisitor()
        }
    }

    func didLogout(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            removeSavedData()
            router.navigate(to: .logout)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchUserData(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let user):
            let userViewModel = UserViewModel(user: user)
            view?.didFetchUserData(viewModel: userViewModel)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.tableViewItems[indexPath.row].action()
        }
    }
    
    func signInViewTapped() {
        router.navigate(to: .login)
    }
    
    private func removeSavedData() {
        UserDefaultsHelper.id = 0
        UserDefaultsHelper.token = ""
        UserDefaultsHelper.isLoggedIn = false
        UserDefaults.standard.synchronize()
    }
    
    @objc private func didUpdateProfile() {
        getUserData()
    }
    
    func didDownloadBudgetAndAccountsFile(with result: Result<URL?>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let url):
            view?.showFile(at: url)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
}
