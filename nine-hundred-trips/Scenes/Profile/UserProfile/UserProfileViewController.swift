//
//  UserProfileViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class UserProfileViewController: BaseViewController, UserProfileViewProtocol {
  
    
    private let  mainView = UserProfileView()
    var presenter: UserProfilePresenterProtocol!
    
    private var previousStatusBarHidden = false

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return shouldHideStatusBar
    }
    
    private var shouldHideStatusBar: Bool {
        let frame = mainView.logoutButton.convert(mainView.logoutButton.bounds, to: nil)
        return frame.minY < view.safeAreaInsets.top
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaultsHelper.isLoggedIn {
           setupNavigationBarButton()
        }
        mainView.scrollView.delegate = self
        addTargets()
        setupTableView()
        getUserData()
        setupNavigationBarStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupNavigationBarButton() {
        let logoutButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .height(25)
            .width(25)
            .image(UIImage(named: "ic_logout")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection())
            .action(target: self, selector: #selector(logoutButtonTapped))
            .build()
        navigationItem.rightBarButtonItem = logoutButton
    }
    
    private func addTargets() {
        mainView.signInViewTapGesture.addTarget(self, action: #selector(signInViewTapped))
        mainView.logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate  = self
    }
    
    @objc private func logoutButtonTapped() {
       presenter.logoutButtonTapped()
    }
    
    @objc private func signInViewTapped() {
        presenter.signInViewTapped()
    }
    
    private func getUserData() {
        presenter.getUserData()
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func didFetchUserData(viewModel: UserViewModel) {
        mainView.configure(viewModel: viewModel)
        mainView.setTableViewHeight(CGFloat(presenter.numberOfRows * 50))
    }
    
    func prepareProfileForVisitor() {
        mainView.prepareForVisitor()
        mainView.setTableViewHeight(CGFloat(presenter.numberOfRows * 50))
    }
    
    func showFile(at path: URL?) {
        let viewer = UIDocumentInteractionController()
        viewer.url = path
        viewer.delegate = self
        viewer.presentPreview(animated: true)
    }
}


extension UserProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.className) as! ProfileCell
        presenter.configure(cell: AnyConfigurableCell(cell), for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
}

extension UserProfileViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.navigationController ?? self
    }
}

extension UserProfileViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if previousStatusBarHidden != shouldHideStatusBar {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            })
            
            previousStatusBarHidden = shouldHideStatusBar
        }
    }
}
