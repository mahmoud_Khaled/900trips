//
//  UserProfileRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum UserProfileDestination {
    case login
    case countryAndCurrency
    case applicationLanguage
    case contactUs
    case aboutApplication
    case logout
    case editProfile
    case favourites
    case requestQuotation
    case termsAndConditions
    case reservationRequests
}

class UserProfileRouter: UserProfileRouterProtocol {
   
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UserProfileViewController()
        view.tabBarItem = UITabBarItem(title: "myAccount".localized(), image: UIImage(named: "ic_account"), selectedImage: UIImage(named: "ic_account"))
        view.tabBarItem.setTitleTextAttributes([
            .font: DinNextFont.regular.getFont(ofSize: 13)
        ], for: .normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = UserProfileInteractor()
        let router = UserProfileRouter()
        let presenter = UserProfilePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigate(to destination: UserProfileDestination) {
        switch destination {
        case .login:
            viewController?.tabBarController?.navigationController?.pushViewController(LoginRouter.createModule(), animated: true)
        case .countryAndCurrency:
            viewController?.present(CountryAndCurrencyRouter.createModule(), animated: true, completion: nil)
        case .applicationLanguage:
            viewController?.navigationController?.pushViewController(ApplicationLanguageRouter.createModule(), animated: true)
        case .contactUs:
            viewController?.navigationController?.pushViewController(ContactUsRouter.createModule(), animated: true)
        case .aboutApplication:
            viewController?.navigationController?.pushViewController(AboutApplicationRouter.createModule(), animated: true)
        case .logout:
            AppDelegate.shared.setRootViewController(AppDelegate.shared.createTabBarController(), animated: true)
        case .editProfile:
            viewController?.navigationController?.pushViewController(PersonalInformationRouter.createModule(), animated: true)
        case .favourites:
            viewController?.navigationController?.pushViewController(FavouritesRouter.createModule(), animated: true)
        case .requestQuotation:
            viewController?.navigationController?.pushViewController(RequestQuotationRouter.createModule(), animated: true)
        case .termsAndConditions:
            viewController?.present(TermsAndConditionsRouter.createModule(), animated: true)
        case .reservationRequests:
            viewController?.navigationController?.pushViewController(ReservationRequestsRouter.createModule(), animated: true)
        }
    }
}
