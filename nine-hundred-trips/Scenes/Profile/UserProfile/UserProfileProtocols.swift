//
//  UserProfileProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol UserProfileRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(to destination: UserProfileDestination)
}

protocol UserProfilePresenterProtocol: class {
    var view: UserProfileViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<ProfileItem>, for indexPath: IndexPath)
    func logoutButtonTapped()
    func getUserData()
    func didSelectRow(at indexPath: IndexPath)
    func signInViewTapped()
}

protocol UserProfileInteractorInputProtocol: class {
    var presenter: UserProfileInteractorOutputProtocol? { get set }
    func logout()
    func getUserData()
    func downloadBudgetAndAccountsFile()
}

protocol UserProfileInteractorOutputProtocol: class {
    func didFetchUserData(with result: Result<User>)
    func didLogout(with result: Result<Data>)
    func didDownloadBudgetAndAccountsFile(with result: Result<URL?>)
}

protocol UserProfileViewProtocol: class {
    var presenter: UserProfilePresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetchUserData(viewModel: UserViewModel)
    func prepareProfileForVisitor()
    func showFile(at path: URL?)
}
