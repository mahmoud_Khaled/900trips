//
//  UserProfileInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class UserProfileInteractor: UserProfileInteractorInputProtocol {
    
    weak var presenter: UserProfileInteractorOutputProtocol?
    private var userWorker = UserWorker()
    
    func logout() {
        userWorker.logout { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didLogout(with: result)
        }
    }
    
    func getUserData() {
        userWorker.getUserData { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchUserData(with: result)
        }
    }
    
    func downloadBudgetAndAccountsFile() {
        userWorker.downloadBudgetAndAccountsFile { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didDownloadBudgetAndAccountsFile(with: result)
        }
    }
}
