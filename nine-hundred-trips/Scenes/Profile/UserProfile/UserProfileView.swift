//
//  UserProfileView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class UserProfileView: UIView {
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerImageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profile_background")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var logoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_logout")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    private lazy var logoImageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 60
        view.clipsToBounds = true
        view.isHidden = true
        return view
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profile_photo")
        imageView.layer.cornerRadius = 43
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "--------"
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 30)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------"
        label.textColor = .gray
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryFlagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 12.5
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var emailView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 17.5
        view.layer.masksToBounds = true
        view.backgroundColor = .deepDarkOrange
        return view
    }()

    private lazy var emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "--------"
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var emailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_mail")
        return imageView
    }()
    
    private lazy var signInStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [signInView])
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var signInViewTapGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer()
        return tapGesture
    }()
    
    private lazy var signInView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.isHidden = true
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(signInViewTapGesture)
        return view
    }()
    
    private lazy var signInImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "profile_photo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var signInLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.text = "signInNow".localized()
        return label
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ProfileCell.self, forCellReuseIdentifier: ProfileCell.className)
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 50
        return tableView
    }()
    
    private var tableViewHeightAnchor: NSLayoutConstraint!
    private var headerImageContainerViewHeight: NSLayoutConstraint!
    private var signInStackViewTopConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(headerImageContainerView)
        containerView.addSubview(headerImageView)
        containerView.addSubview(logoutButton)
        headerImageView.addSubview(logoImageContainerView)
        logoImageContainerView.addSubview(logoImageView)
        containerView.addSubview(profileImageView)
        containerView.addSubview(userNameLabel)
        containerView.addSubview(countryView)
        countryView.addSubview(countryNameLabel)
        countryView.addSubview(countryFlagImageView)
        containerView.addSubview(emailView)
        emailView.addSubview(emailLabel)
        emailView.addSubview(emailImageView)
        containerView.addSubview(signInStackView)
        signInView.addSubview(signInImageView)
        signInView.addSubview(signInLabel)
        containerView.addSubview(tableView)
    }
  
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    
    private func setupHeaderImageContainerViewConstraints() {
        headerImageContainerViewHeight = headerImageContainerView.heightAnchor.constraint(equalToConstant: 375)
        NSLayoutConstraint.activate([
            headerImageContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            headerImageContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerImageContainerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerImageContainerViewHeight
        ])
    }
    
    private func setupHeaderImageViewConstraints() {
        let headerImageViewTopAnchor = headerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0)
        headerImageViewTopAnchor.priority = .defaultHigh
        
        let heightAnchor = headerImageView.heightAnchor.constraint(greaterThanOrEqualTo: headerImageContainerView.heightAnchor, multiplier: 0)
        heightAnchor.priority = .required
        
        NSLayoutConstraint.activate([
            headerImageView.leadingAnchor.constraint(equalTo: headerImageContainerView.leadingAnchor),
            headerImageView.trailingAnchor.constraint(equalTo: headerImageContainerView.trailingAnchor),
            headerImageViewTopAnchor,
            heightAnchor,
            headerImageView.bottomAnchor.constraint(equalTo: headerImageContainerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupLogoutButtonConstraints() {
        NSLayoutConstraint.activate([
            logoutButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: UIDevice.hasTopNotch ? 48 : 32),
            logoutButton.trailingAnchor.constraint(equalTo: headerImageView.trailingAnchor, constant: -15),
            logoutButton.heightAnchor.constraint(equalToConstant: 33),
            logoutButton.widthAnchor.constraint(equalToConstant: 33)
        ])
    }
    
    private func setupLogoImageContainerViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageContainerView.topAnchor.constraint(equalTo: headerImageView.topAnchor, constant: 50),
            logoImageContainerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageContainerView.widthAnchor.constraint(equalToConstant: 120),
            logoImageContainerView.heightAnchor.constraint(equalToConstant: 120)
        ])
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: logoImageContainerView.topAnchor, constant: 8),
            logoImageView.leadingAnchor.constraint(equalTo: logoImageContainerView.leadingAnchor, constant: 8),
            logoImageView.trailingAnchor.constraint(equalTo: logoImageContainerView.trailingAnchor, constant: -8),
            logoImageView.bottomAnchor.constraint(equalTo: logoImageContainerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupProfileImageViewConstraints() {
        NSLayoutConstraint.activate([
           profileImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 70),
           profileImageView.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
           profileImageView.heightAnchor.constraint(equalToConstant: 86),
           profileImageView.widthAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupCompanyNamelabelConstraints() {
        NSLayoutConstraint.activate([
            userNameLabel.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 0),
            userNameLabel.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
            userNameLabel.heightAnchor.constraint(equalToConstant: 40),
            userNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            userNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8)
            
       ])
    }
    private func setupCountryViewConstraints() {
        NSLayoutConstraint.activate([
            countryView.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor, constant: 0),
            countryView.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
            countryView.heightAnchor.constraint(equalToConstant: 45),
        ])
    }
    
    private func setupCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            countryNameLabel.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 5),
            countryNameLabel.trailingAnchor.constraint(equalTo: countryView.trailingAnchor, constant: -24),
            countryNameLabel.heightAnchor.constraint(equalToConstant: 40),
       ])
    }
    
    private func setupCountryFlagImageView() {
        NSLayoutConstraint.activate([
            countryFlagImageView.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 15),
            countryFlagImageView.leadingAnchor.constraint(equalTo: countryView.leadingAnchor, constant: 24),
            countryFlagImageView.trailingAnchor.constraint(equalTo: countryNameLabel.leadingAnchor, constant: -8),
            countryFlagImageView.heightAnchor.constraint(equalToConstant: 25),
            countryFlagImageView.widthAnchor.constraint(equalToConstant: 25)
       ])
    }
    
    private func setupEmailViewConstraints() {
        NSLayoutConstraint.activate([
            emailView.topAnchor.constraint(equalTo: countryView.bottomAnchor, constant: 0),
            emailView.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
            emailView.heightAnchor.constraint(equalToConstant: 35),
        ])
    }
    
    private func setupEmailLabelConstraints() {
        NSLayoutConstraint.activate([
           emailLabel.topAnchor.constraint(equalTo: emailView.topAnchor, constant: 5),
           emailLabel.trailingAnchor.constraint(equalTo: emailView.trailingAnchor, constant: -24),
           emailLabel.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
    private func setupEmailImageView() {
        NSLayoutConstraint.activate([
            emailImageView.topAnchor.constraint(equalTo: emailView.topAnchor, constant: 5),
            emailImageView.trailingAnchor.constraint(equalTo: emailLabel.leadingAnchor, constant: -8),
            emailImageView.heightAnchor.constraint(equalToConstant: 20),
            emailImageView.widthAnchor.constraint(equalToConstant: 20),
            emailImageView.leadingAnchor.constraint(equalTo: emailView.leadingAnchor, constant: 24)
        ])
    }
    
    private func setupSignInViewConstraints() {
        NSLayoutConstraint.activate([
            signInView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupSignInStackViewConstraints() {
        signInStackViewTopConstraint = signInStackView.topAnchor.constraint(equalTo: emailView.bottomAnchor, constant: 8)
        NSLayoutConstraint.activate([
            signInStackViewTopConstraint,
            signInStackView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 8),
            signInStackView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: -8),
        ])
    }
    
    private func setupSignInImageViewConstraints() {
        NSLayoutConstraint.activate([
            signInImageView.centerYAnchor.constraint(equalTo: signInView.centerYAnchor),
            signInImageView.heightAnchor.constraint(equalToConstant: 50),
            signInImageView.widthAnchor.constraint(equalToConstant: 50),
            signInImageView.leadingAnchor.constraint(equalTo: signInView.leadingAnchor, constant: 8)
        ])
    }
    
    private func setupSignInLabelConstraints() {
        NSLayoutConstraint.activate([
            signInLabel.centerYAnchor.constraint(equalTo: signInImageView.centerYAnchor),
            signInLabel.leadingAnchor.constraint(equalTo: signInImageView.trailingAnchor, constant: 8)
        ])
    }
  
    private func setupTableViewConstraints() {
        tableViewHeightAnchor = tableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: signInStackView.bottomAnchor, constant: 4),
            tableView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            tableViewHeightAnchor
        ])
    }
    
 
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupHeaderImageContainerViewConstraints()
        setupHeaderImageViewConstraints()
        setupLogoutButtonConstraints()
        setupLogoImageContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupProfileImageViewConstraints()
        setupCompanyNamelabelConstraints()
        setupCountryViewConstraints()
        setupCountryNameLabelConstraints()
        setupCountryFlagImageView()
        setupEmailViewConstraints()
        setupEmailLabelConstraints()
        setupEmailImageView()
        setupSignInViewConstraints()
        setupSignInStackViewConstraints()
        setupSignInImageViewConstraints()
        setupSignInLabelConstraints()
        setupTableViewConstraints()
        
    }
    
    func configure(viewModel: UserViewModel) {
        emailLabel.text = viewModel.email
        userNameLabel.text = viewModel.username
        countryNameLabel.text = viewModel.countryName
        countryFlagImageView.load(url: viewModel.countryFlagUrl)
        profileImageView.load(url: viewModel.imageUrl, placeholder: UIImage(named: "profile_photo"))
    }
    
    func setTableViewHeight(_ height: CGFloat) {
        tableViewHeightAnchor.constant = height
    }
    
    func prepareForVisitor() {
        profileImageView.isHidden = true
        userNameLabel.isHidden = true
        countryView.isHidden = true
        emailView.isHidden = true
        headerImageContainerViewHeight.isActive = false
        headerImageContainerViewHeight = headerImageContainerView.heightAnchor.constraint(equalTo: headerImageContainerView.widthAnchor, multiplier: 0.65)
        headerImageContainerViewHeight.isActive = true
        signInStackViewTopConstraint.isActive = false
        signInStackViewTopConstraint = signInStackView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: 8)
        signInStackViewTopConstraint.isActive = true
        logoImageContainerView.isHidden = false
        signInView.isHidden = false
        signInStackView.isHidden = false
        logoutButton.isHidden = true
    }
}
