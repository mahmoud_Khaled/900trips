//
//  CompanyProfileViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyProfileViewController: BaseViewController, CompanyProfileViewProtocol {
    
    private let mainView = CompanyProfileView()
    var presenter: CompanyProfilePresenterProtocol!
    var viewModel: UserViewModel?
    
    private var previousStatusBarHidden = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return shouldHideStatusBar
    }
    
    private var shouldHideStatusBar: Bool {
        let frame = mainView.logoutButton.convert(mainView.logoutButton.bounds, to: nil)
        return frame.minY < view.safeAreaInsets.top
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addTargets()
        presenter.getUserData()
        mainView.scrollView.delegate = self
        setupNavigationBarStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource  = self
        mainView.tableView.delegate = self
    }
    
    private func addTargets() {
        mainView.logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
        mainView.facebookButton.addTarget(self, action: #selector(facebookButtonTapped), for: .touchUpInside)
        mainView.twitterButton.addTarget(self, action: #selector(twitterButtonTapped), for: .touchUpInside)
        mainView.instagramButton.addTarget(self, action: #selector(instagramButtonTapped), for: .touchUpInside)
        mainView.tapGesture.addTarget(self, action: #selector(websiteLabelTapped))
    }
    
    @objc private func logoutButtonTapped() {
        presenter?.logoutButtonTapped()
    }
    
    @objc private func facebookButtonTapped() {
        if let facebookLink = viewModel?.facebook {
            guard let facebookLink = URL(string: facebookLink) else { return }
            UIApplication.shared.open(facebookLink)
        }
    }
    
    @objc private func twitterButtonTapped() {
        if let twitterLink = viewModel?.twitter {
            guard let twitterLink = URL(string: twitterLink) else { return }
            UIApplication.shared.open(twitterLink)
        }
    }
    
    @objc private func instagramButtonTapped() {
        if let instagramLink = viewModel?.instagram {
            guard let instagramLink = URL(string: instagramLink) else { return }
            UIApplication.shared.open(instagramLink)
        }
    }
    
    @objc private func websiteLabelTapped() {
        if let websiteLink = viewModel?.domain {
            guard let websiteLink = URL(string: websiteLink) else { return }
            UIApplication.shared.open(websiteLink)
        }
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func didFetchUserData(viewModel: UserViewModel) {
        self.viewModel = viewModel
        mainView.configure(viewModel: viewModel)
    }
}


extension CompanyProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.className) as! ProfileCell
        presenter.configure(cell: AnyConfigurableCell(cell), for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
}

extension CompanyProfileViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if previousStatusBarHidden != shouldHideStatusBar {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            })
            
            previousStatusBarHidden = shouldHideStatusBar
        }
    }
    
}
