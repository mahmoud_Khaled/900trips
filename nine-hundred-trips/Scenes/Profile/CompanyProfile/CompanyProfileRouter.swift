//
//  CompanyProfileRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum CompanyProfileDestination {
    case logout
    case editProfile
    case balance
    case packagesAndSubscriptions
    case addOffer
    case myOffers
    case applicationLanguage
    case contactUs
    case aboutApplication
    case termsAndConditions
    case reservationRequests
}

class CompanyProfileRouter: CompanyProfileRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = CompanyProfileViewController()
        view.tabBarItem = UITabBarItem(title: "myAccount".localized(), image: UIImage(named: "ic_account"), selectedImage: UIImage(named: "ic_account"))
        view.tabBarItem.setTitleTextAttributes([
            .font: DinNextFont.regular.getFont(ofSize: 13)
        ], for: .normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = CompanyProfileInteractor()
        let router = CompanyProfileRouter()
        let presenter = CompanyProfilePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigate(to destination: CompanyProfileDestination) {
        switch destination {
        case .logout:
            AppDelegate.shared.setRootViewController(AppDelegate.shared.createTabBarController(), animated: true)
        case .editProfile:
            viewController?.navigationController?.pushViewController(CompanyInformationRouter.createModule(), animated: true)
        case .balance:
            viewController?.navigationController?.pushViewController(BalanceRouter.createModule(), animated: true)
        case .packagesAndSubscriptions:
            viewController?.navigationController?.pushViewController(PackagesAndSubscriptionsRouter.createModule(), animated: true)
        case .addOffer:
            viewController?.navigationController?.pushViewController(AddOfferRouter.createModule(), animated: true)
        case .myOffers:
            viewController?.navigationController?.pushViewController(MyOffersRouter.createModule(), animated: true)
        case .applicationLanguage:
            viewController?.navigationController?.pushViewController(ApplicationLanguageRouter.createModule(), animated: true)
        case .contactUs:
            viewController?.navigationController?.pushViewController(ContactUsRouter.createModule(), animated: true)
        case .aboutApplication:
            viewController?.navigationController?.pushViewController(AboutApplicationRouter.createModule(), animated: true)
        case .termsAndConditions:
            viewController?.present(TermsAndConditionsRouter.createModule(), animated: true)
        case .reservationRequests:
            viewController?.navigationController?.pushViewController(ReservationRequestsRouter.createModule(), animated: true)
        }
    }
    
}
