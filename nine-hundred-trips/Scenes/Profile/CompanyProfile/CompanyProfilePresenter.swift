//
//  CompanyProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompanyProfilePresenter: CompanyProfilePresenterProtocol, CompanyProfileInteractorOutputProtocol {
    
    weak var view: CompanyProfileViewProtocol?
    private let interactor: CompanyProfileInteractorInputProtocol
    private let router: CompanyProfileRouterProtocol
        
    init(view: CompanyProfileViewProtocol, interactor: CompanyProfileInteractorInputProtocol, router: CompanyProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateProfile), name: .updateProfile, object: nil)
    }
    
    lazy var tableViewItems: [ProfileItem] = {
        return [
            ProfileItem(
                name: "companyInformation".localized(),
                image: "ic_edit",
                editImage: "ic_more_colored",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .editProfile)
                }
            ),
            ProfileItem(
                name: "reservationRequests".localized(),
                image: "ic_note",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .reservationRequests)
                }
            ),
            ProfileItem(
                name: "balance".localized(),
                image: "ic_coins",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .balance)
                }
            ),
            ProfileItem(
                name: "packagesAndSubscriptions".localized(),
                image: "ic_wallet",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .packagesAndSubscriptions)
                }
            ),
            ProfileItem(
                name: "addOffer".localized(),
                image: "ic_addoffer",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .addOffer)
                }
            ),
            ProfileItem(
                name: "myOffers".localized(),
                image: "ic_price_tag",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .myOffers)
                }
            ),
            ProfileItem(
                name: "language".localized(),
                image: "ic_translate",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .applicationLanguage)
                }
            ),
            ProfileItem(
                name: "contactUs".localized(),
                image: "ic_contact",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .contactUs)
                }
            ),
            ProfileItem(
                name: "aboutApplication".localized(),
                image: "ic_info",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .aboutApplication)
                }
            ),
            ProfileItem(
                name: "termsAndConditions".localized(),
                image: "ic_terms_and_conditions",
                editImage: "",
                action: { [weak self] in
                    guard let self = self else { return }
                    self.router.navigate(to: .termsAndConditions)
                }
            )
        ]
    }()
    
    var numberOfRows: Int {
        return tableViewItems.count
    }
    
    func configure(cell: AnyConfigurableCell<ProfileItem>, for indexPath: IndexPath) {
        cell.configure(model: tableViewItems[indexPath.row])
    }
    
    func logoutButtonTapped() {
        view?.showActivityIndicator()
        interactor.logout()
    }
    
    func getUserData() {
        view?.showActivityIndicator()
        interactor.getCompanyData()
    }
    
    func didLogout(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            removeSavedData()
            router.navigate(to: .logout)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchUserData(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let user):
            let userViewModel = UserViewModel(user: user)
            view?.didFetchUserData(viewModel: userViewModel)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.tableViewItems[indexPath.row].action()
        }
    }
    
    private func removeSavedData() {
        UserDefaultsHelper.id = 0
        UserDefaultsHelper.token = ""
        UserDefaultsHelper.isLoggedIn = false
        UserDefaultsHelper.userType = .client
        UserDefaults.standard.synchronize()
    }
    
    @objc private func didUpdateProfile() {
        getUserData()
    }
    
}
