//
//  CompanyProfileView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyProfileView: UIView {
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerImageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profilebg")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var logoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_logout")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()
    
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profilephoto")
        imageView.layer.cornerRadius = 43
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-------"
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 30)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----------"
        label.textColor = .gray
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryFlagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 11
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.masksToBounds = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var websiteView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.backgroundColor = .deepDarkOrange
        return view
    }()
    
    lazy var tapGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer()
    }()
    
     lazy var websiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-------"
        label.textColor = .white
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tapGesture)
        return label
    }()
    
    private lazy var websiteImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_website")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var firstSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var secondSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var verticalSeparatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 1, alpha: 0.6)
        return view
    }()
    private lazy var likesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "numberOfLikes".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    private lazy var numberOfLikesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "----"
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.textColor = .white
        label.textAlignment = .center

        return label
    }()
    
    private lazy var offersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "numberOfOffers".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 18)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    private lazy var numberOfOffersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "----"
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.textColor = .white
        label.textAlignment = .center
        return label

    }()

    private lazy var offersStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [numberOfOffersLabel, offersLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()
    
    private lazy var likesStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [numberOfLikesLabel, likesLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()
    
    lazy var facebookButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_facebook"), for: .normal)
        return button
    }()
    
    lazy var instagramButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
         button.setImage(UIImage(named: "ic_instagram"), for: .normal)
        return button
    }()
    
    lazy var twitterButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
         button.setImage(UIImage(named: "ic_twitter"), for: .normal)
        return button
    }()
    
     private lazy var socialMediaView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    private lazy var socialMediaStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [facebookButton, instagramButton, twitterButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        return stackView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ProfileCell.self, forCellReuseIdentifier: ProfileCell.className)
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .black
        tableView.rowHeight = 50
        tableView.isHidden = true
        tableView.bounces = false
        return tableView
    }()
    
    private var tableViewTopConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(headerImageContainerView)
        containerView.addSubview(headerImageView)
        containerView.addSubview(logoutButton)
        containerView.addSubview(profileImageView)
        containerView.addSubview(companyNameLabel)
        containerView.addSubview(countryView)
        countryView.addSubview(countryNameLabel)
        countryView.addSubview(countryFlagImageView)
        containerView.addSubview(websiteView)
        websiteView.addSubview(websiteLabel)
        websiteView.addSubview(websiteImageView)
        containerView.addSubview(firstSeparatorLine)
        containerView.addSubview(verticalSeparatorLine)
        containerView.addSubview(secondSeparatorLine)
        containerView.addSubview(offersStackView)
        containerView.addSubview(likesStackView)
        containerView.addSubview(socialMediaView)
        socialMediaView.addSubview(socialMediaStackView)
        containerView.addSubview(tableView)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupHeaderImageContainerViewConstraints() {
        let headerImageContainerViewHeight = headerImageContainerView.heightAnchor.constraint(equalToConstant: 375)
        NSLayoutConstraint.activate([
            headerImageContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            headerImageContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerImageContainerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerImageContainerViewHeight
        ])
    }
    
    private func setupHeaderImageViewConstraints() {
        let headerImageViewTopAnchor = headerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0)
        headerImageViewTopAnchor.priority = .defaultHigh
        
        let heightAnchor = headerImageView.heightAnchor.constraint(greaterThanOrEqualTo: headerImageContainerView.heightAnchor, multiplier: 0)
        heightAnchor.priority = .required
        
        NSLayoutConstraint.activate([
            headerImageView.leadingAnchor.constraint(equalTo: headerImageContainerView.leadingAnchor),
            headerImageView.trailingAnchor.constraint(equalTo: headerImageContainerView.trailingAnchor),
            headerImageViewTopAnchor,
            heightAnchor,
            headerImageView.bottomAnchor.constraint(equalTo: headerImageContainerView.bottomAnchor, constant: 0)
        ])
    }

    private func setupLogoutButtonConstraints() {
        NSLayoutConstraint.activate([
            logoutButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: UIDevice.hasTopNotch ? 48 : 32),
            logoutButton.trailingAnchor.constraint(equalTo: headerImageView.trailingAnchor, constant: -15),
            logoutButton.heightAnchor.constraint(equalToConstant: 33),
            logoutButton.widthAnchor.constraint(equalToConstant: 33)
        ])
    }

    private func setupProfileImageViewConstraints() {
        NSLayoutConstraint.activate([
            profileImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 50),
            profileImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            profileImageView.heightAnchor.constraint(equalToConstant: 86),
            profileImageView.widthAnchor.constraint(equalToConstant: 86)
        ])
    }

    private func setupCompanyNamelabelConstraints() {
        NSLayoutConstraint.activate([
            companyNameLabel.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 5),
            companyNameLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            companyNameLabel.heightAnchor.constraint(equalToConstant: 40),
            companyNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            companyNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8)

        ])
    }
    private func setupCountryViewConstraints() {
        NSLayoutConstraint.activate([
            countryView.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 0),
            countryView.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor),
            countryView.heightAnchor.constraint(equalToConstant: 40),
        ])
    }

    private func setupCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            countryNameLabel.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 5),
            countryNameLabel.trailingAnchor.constraint(equalTo: countryView.trailingAnchor, constant: -24),
            countryNameLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }

    private func setupCountryFlagImageView() {
        NSLayoutConstraint.activate([
            countryFlagImageView.topAnchor.constraint(equalTo: countryView.topAnchor, constant: 10),
            countryFlagImageView.leadingAnchor.constraint(equalTo: countryView.leadingAnchor, constant: 24),
            countryFlagImageView.trailingAnchor.constraint(equalTo: countryNameLabel.leadingAnchor, constant: -8),
            countryFlagImageView.heightAnchor.constraint(equalToConstant: 22),
            countryFlagImageView.widthAnchor.constraint(equalToConstant: 22)
        ])
    }

    private func setupWebsiteViewConstraints() {
        NSLayoutConstraint.activate([
            websiteView.topAnchor.constraint(equalTo: countryView.bottomAnchor, constant: 0),
            websiteView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            websiteView.heightAnchor.constraint(equalToConstant: 30),
        ])
    }

    private func setupWebsiteLabelConstraints() {
        NSLayoutConstraint.activate([
            websiteLabel.centerYAnchor.constraint(equalTo: websiteImageView.centerYAnchor, constant: -3.5),
            websiteLabel.trailingAnchor.constraint(equalTo: websiteView.trailingAnchor, constant: -24),
        ])
    }

    private func setupWebsiteImageView() {
        NSLayoutConstraint.activate([
            websiteImageView.topAnchor.constraint(equalTo: websiteView.topAnchor, constant: 5),
            websiteImageView.trailingAnchor.constraint(equalTo: websiteLabel.leadingAnchor, constant: -8),
            websiteImageView.heightAnchor.constraint(equalToConstant: 18),
            websiteImageView.widthAnchor.constraint(equalToConstant: 18),
            websiteImageView.leadingAnchor.constraint(equalTo: websiteView.leadingAnchor, constant: 24)
        ])
    }

    private func setupFirstSeparatorLineConstrains() {
        NSLayoutConstraint.activate([
            firstSeparatorLine.topAnchor.constraint(equalTo: websiteView.bottomAnchor, constant: 5),
            firstSeparatorLine.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            firstSeparatorLine.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            firstSeparatorLine.heightAnchor.constraint(equalToConstant: 0.5),
        ])
    }
    
   
    private func setupSecondSeparatorLineConstrains() {
        NSLayoutConstraint.activate([
            secondSeparatorLine.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 78),
            secondSeparatorLine.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            secondSeparatorLine.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            secondSeparatorLine.heightAnchor.constraint(equalToConstant: 0.5),
        ])
    }
    
    private func setupVerticalSeparatorLineConstraints() {
        NSLayoutConstraint.activate([
            verticalSeparatorLine.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 0),
            verticalSeparatorLine.centerXAnchor.constraint(equalTo: headerImageView.centerXAnchor, constant: 0),
            verticalSeparatorLine.bottomAnchor.constraint(equalTo: secondSeparatorLine.topAnchor, constant: 0),
            verticalSeparatorLine.widthAnchor.constraint(equalToConstant: 1)
            
        ])
    }
  
    private func setupOffersStackViewConstraints() {
        NSLayoutConstraint.activate([
            offersStackView.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 3),
            offersStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            offersStackView.trailingAnchor.constraint(equalTo: verticalSeparatorLine.leadingAnchor, constant: -8)
        ])
    }
    
    private func setupLikesStackViewConstraints() {
        NSLayoutConstraint.activate([
            likesStackView.topAnchor.constraint(equalTo: firstSeparatorLine.bottomAnchor, constant: 3),
            likesStackView.leadingAnchor.constraint(equalTo: verticalSeparatorLine.trailingAnchor, constant: 8),
            likesStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupSocialMediaViewConstraints() {
        NSLayoutConstraint.activate([
            socialMediaView.topAnchor.constraint(equalTo: secondSeparatorLine.bottomAnchor, constant: 8),
            socialMediaView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            socialMediaView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            socialMediaView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }

    private func setupSocialMediaStackViewConstraints() {
        NSLayoutConstraint.activate([
            socialMediaStackView.topAnchor.constraint(equalTo: socialMediaView.topAnchor, constant: 8),
            socialMediaStackView.centerXAnchor.constraint(equalTo: socialMediaView.centerXAnchor, constant: 0)
        ])
    }
    
    private func setupTableViewConstraints() {
        tableViewTopConstraint = tableView.topAnchor.constraint(equalTo: socialMediaView.bottomAnchor, constant: 8)
        
        NSLayoutConstraint.activate([
            tableViewTopConstraint,
            tableView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 5),
            tableView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -5),
            tableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -5),
            tableView.heightAnchor.constraint(equalToConstant: 500)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupHeaderImageContainerViewConstraints()
        setupHeaderImageViewConstraints()
        setupLogoutButtonConstraints()
        setupProfileImageViewConstraints()
        setupCompanyNamelabelConstraints()
        setupCountryViewConstraints()
        setupCountryNameLabelConstraints()
        setupCountryFlagImageView()
        setupWebsiteViewConstraints()
        setupWebsiteLabelConstraints()
        setupWebsiteImageView()
        setupFirstSeparatorLineConstrains()
        setupVerticalSeparatorLineConstraints()
        setupSecondSeparatorLineConstrains()
        setupOffersStackViewConstraints()
        setupLikesStackViewConstraints()
        setupSocialMediaViewConstraints()
        setupSocialMediaStackViewConstraints()
        setupTableViewConstraints()
    }
    
    func configure(viewModel: UserViewModel) {
        companyNameLabel.text = viewModel.username
        countryNameLabel.text = viewModel.countryName
        websiteLabel.attributedText = NSMutableAttributedString(string: viewModel.domain, attributes: [
            .font: DinNextFont.regular.getFont(ofSize: 18),
            .underlineStyle: viewModel.domain == "N/A" ? 0 : 1
        ])
        countryFlagImageView.load(url: viewModel.countryFlagUrl)
        profileImageView.load(url: viewModel.imageUrl, placeholder: UIImage(named: "profile_photo"))
        numberOfOffersLabel.text = "\(viewModel.offerCount)"
        numberOfLikesLabel.text = "\(viewModel.likesCount)"
        facebookButton.isHidden = viewModel.isFacebookButtonHidden
        instagramButton.isHidden = viewModel.isInstagramButtonHidden
        twitterButton.isHidden = viewModel.isTwitterButtonHidden
        socialMediaView.isHidden = viewModel.isSocialMediaViewHidden
        if viewModel.isSocialMediaViewHidden {
            tableViewTopConstraint.isActive = false
            tableViewTopConstraint = tableView.topAnchor.constraint(equalTo: secondSeparatorLine.bottomAnchor, constant: 8)
            tableViewTopConstraint.isActive = true
        }
        tableView.isHidden = false
    }


}
