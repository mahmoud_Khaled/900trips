//
//  CompanyProfileProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol CompanyProfileRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(to destination: CompanyProfileDestination)
}

protocol CompanyProfilePresenterProtocol: class {
    var view: CompanyProfileViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<ProfileItem>, for indexPath: IndexPath)
    func logoutButtonTapped()
    func getUserData()
    func didSelectRow(at indexPath: IndexPath)
}

protocol CompanyProfileInteractorInputProtocol: class {
    var presenter: CompanyProfileInteractorOutputProtocol? { get set }
    func logout()
    func getCompanyData()
    
}

protocol CompanyProfileInteractorOutputProtocol: class {
    func didFetchUserData(with result: Result<User>)
    func didLogout(with result: Result<Data>)
    
}

protocol CompanyProfileViewProtocol: class {
    var presenter: CompanyProfilePresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetchUserData(viewModel: UserViewModel)
    
}
