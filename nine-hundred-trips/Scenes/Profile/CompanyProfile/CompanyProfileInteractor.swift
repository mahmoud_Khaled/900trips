//
//  CompanyProfileInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyProfileInteractor: CompanyProfileInteractorInputProtocol {
  
    weak var presenter: CompanyProfileInteractorOutputProtocol?
    private var userWorker = UserWorker()
    
    func logout() {
        userWorker.logout { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didLogout(with: result)
        }
    }
    
    func getCompanyData() {
        userWorker.getUserData { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchUserData(with: result)
        }
    }
}
