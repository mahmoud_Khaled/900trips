//
//  updateUserProfilePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class PersonalInformationPresenter: PersonalInformationPresenterProtocol, PersonalInformationsInteractorOutputProtocol {
   
    weak var view: PersonalInformationViewProtocol?
    private let interactor:  PersonalInformationInteractorInputProtocol?
    private let router: PersonalInformationRouterProtocol?
    private var viewModel: UserViewModel!
    
    init(view: PersonalInformationViewProtocol, interactor: PersonalInformationInteractorInputProtocol, router: PersonalInformationRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func editProfileButtonTapped() {
        guard viewModel != nil else { return }
        router?.navigate(destination: .editProfile(viewModel:viewModel))
    }
    
    func editPasswordButtonTapped() {
        router?.navigate(destination: .editPassword)
    }
    
    func getUserData() {
        view?.showActivityIndicator()
        interactor?.getUserData()
    }
    
    func didFetchUserData(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let user):
            viewModel = UserViewModel(user: user)
            view?.setUserData(viewModel: viewModel)
        case .failure(let error):
            router?.showAlert(with: error.localizedDescription)
        }

    }
}
