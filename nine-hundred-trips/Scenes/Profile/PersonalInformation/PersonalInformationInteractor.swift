//
//  updateUserProfileInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PersonalInformationInteractor: PersonalInformationInteractorInputProtocol {
    
    weak var presenter: PersonalInformationsInteractorOutputProtocol?
    private var userWorker = UserWorker()
    
    func getUserData() {
        userWorker.getUserData { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchUserData(with: result)
        }
        
    }
}
