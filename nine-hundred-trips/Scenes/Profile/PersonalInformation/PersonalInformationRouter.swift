//
//  updateUserProfileRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum UpdateProfileDestination {
    case editProfile(viewModel: UserViewModel)
    case editPassword
 }

class PersonalInformationRouter: PersonalInformationRouterProtocol {
  
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = PersonalInformationViewController()
        let interactor = PersonalInformationInteractor()
        let router = PersonalInformationRouter()
        let presenter = PersonalInformationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigate(destination: UpdateProfileDestination) {
        switch destination {
        case .editProfile(let viewModel):
            viewController?.navigationController?.pushViewController(EditUserProfileRouter.createModule(with: viewModel), animated: true)
        case .editPassword:
             viewController?.navigationController?.pushViewController(EditUserPasswordRouter.createModule(), animated: true)
        }
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}
