//
//  updateUserProfileProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol PersonalInformationRouterProtocol: class {
    func navigate(destination: UpdateProfileDestination)
    func showAlert(with message: String)
}

protocol PersonalInformationPresenterProtocol: class {
    var view: PersonalInformationViewProtocol? { get set }
    func editProfileButtonTapped()
    func editPasswordButtonTapped()
    func getUserData()

}

protocol PersonalInformationInteractorInputProtocol: class {
    var presenter: PersonalInformationsInteractorOutputProtocol? { get set }
    func getUserData()
}

protocol PersonalInformationsInteractorOutputProtocol: class {
    func didFetchUserData(with result: Result<User>)
}

protocol PersonalInformationViewProtocol: class {
    var presenter: PersonalInformationPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func setUserData(viewModel: UserViewModel)

}
