//
//  updateUserProfileViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/13/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PersonalInformationViewController: BaseViewController, PersonalInformationViewProtocol {
    
    private let mainView = PersonalInformationView()
    var presenter: PersonalInformationPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserData()
        navigationController?.isNavigationBarHidden = false
    }

    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
        title = "personalInformation".localized()
    }
    
    private func setupNavigationBarButtons() {
        let editButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_edit_information"))
            .height(20)
            .width(20)
            .action(target: self, selector: #selector(editButtonTapped))
            .build()
        navigationItem.rightBarButtonItems = [editButton]
        
    }
    
 
    @objc private func editButtonTapped() {
        presenter.editProfileButtonTapped()
    }
    
    private func addTargets() {
        mainView.mainView.editPasswordButton.addTarget(self, action: #selector(editPasswordButtonTapped), for: .touchUpInside)
    }
    
    @objc func editPasswordButtonTapped() {
        presenter.editPasswordButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func setUserData(viewModel: UserViewModel) {
        mainView.configure(viewModel: viewModel)
    }
    
    private func getUserData() {
        presenter.getUserData()
    }
}
