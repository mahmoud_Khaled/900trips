//
//  ConversationsPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ConversationsPresenter: ConversationsPresenterProtocol, ConversationsInteractorOutputProtocol {
    
    weak var view: ConversationsViewProtocol?
    private let interactor: ConversationsInteractorInputProtocol
    private let router: ConversationsRouterProtocol
    private var conversations = [ConversationViewModel]()
    
    private var totalPages: Int = 1
    private var currentPage: Int = 1
    private var  isPullToRefresh = false
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    var numberOfRows: Int {
        return conversations.count
    }
    
    init(view: ConversationsViewProtocol, interactor: ConversationsInteractorInputProtocol, router: ConversationsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateConversations), name: .updateConversations, object: nil)
    }
    
    func setEmptyArray() {
        conversations.removeAll()
    }
    
    func viewWillAppear() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getAllConversations(page: currentPage)
    }
    
    func didFetchAllConversations(with result: Result<[Conversation]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        self.totalPages = totalPages
        switch result {
        case .success(let result):
            
            if isPullToRefresh {
                conversations = []
                isPullToRefresh = false
            }
            conversations.append(contentsOf: result.map {
                ConversationViewModel(conversation: $0, dateFormatter: dateFormatter)
            })
            
            if conversations.count == 0 {
                view?.setEmptyText(message: "noConversations".localized())
            } else {
                view?.reloadTableView()
            }
            
        case .failure(let error):
            isPullToRefresh = false
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            view?.showActivityIndicator(isUserInteractionEnabled: true)
            interactor.getAllConversations(page: currentPage)
        }
    }
    
    func pullToRefresh() {
        totalPages = 1
        currentPage = 1
        isPullToRefresh = true
        interactor.getAllConversations(page: currentPage)
    }
    
    func deleteConverstation(at indexPath: IndexPath) {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.deleteConversation(conversationId: conversations[indexPath.row].id, index: indexPath.row)
    }
    
    func didDeleteConversation(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            conversations.remove(at: index)
            if conversations.count == 0 {
                view?.setEmptyText(message: "noConversations".localized())
            }
            view?.reloadTableView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    
    func configure(cell: AnyConfigurableCell<ConversationViewModel>, at indextPath: IndexPath) {
        cell.configure(model: conversations[indextPath.row])
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let conversation = conversations[indexPath.row]
        router.navigateToChat(with: conversation.otherUserId, userImage: conversation.otherUserImage, username: conversation.otherUserName, conversationId: conversation.id, rate: String(conversation.rate))
    }
    
    @objc private func handleUpdateConversations(_ notification: NSNotification) {
        guard
            let userId = notification.userInfo?["userId"] as? Int,
            let content = notification.userInfo?["content"] as? String,
            let time = notification.userInfo?["time"] as? Int
        else { return }
        if let conversation = conversations.first(where: { $0.otherUserId == userId }), let index = conversations.firstIndex(where: { $0.otherUserId == conversation.otherUserId }) {
            conversation.lastMessage = content
            conversation.time = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(time)))
            if index == 0 {
                view?.reloadTableView()
            } else {
                conversations.remove(at: index)
                conversations.insert(conversation, at: 0)
                view?.moveIndexToFirst(index)
            }
        } else {
            guard
                let conversationId = notification.userInfo?["conversationId"] as? Int,
                let userName = notification.userInfo?["userName"] as? String,
                let userImage = notification.userInfo?["userImage"] as? String,
                let time = notification.userInfo?["time"] as? Int,
                let rate = notification.userInfo?["rate"] as? String
            else { return }
            let newConversation = Conversation(id: conversationId, otherUserId: userId, otherUserName: userName, otherUserImage: userImage, time: time, lastMessage: content, rate: rate)
            conversations.insert(ConversationViewModel(conversation: newConversation, dateFormatter: dateFormatter), at: 0)
            view?.setEmptyText(message: "")
            view?.reloadTableView()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
