//
//  ConversationsRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ConversationsRouter: ConversationsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ConversationsViewController()
        view.tabBarItem = UITabBarItem(title: "conversations".localized(), image: UIImage(named: "ic_conversations"), selectedImage: UIImage(named: "ic_conversations"))
        view.tabBarItem.setTitleTextAttributes([ NSAttributedString.Key.font: DinNextFont.regular.getFont(ofSize: 13)], for:UIControl.State.normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = ConversationsInteractor()
        let router = ConversationsRouter()
        let presenter = ConversationsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        let navigationController = UINavigationController(rootViewController: view)
        return navigationController
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToChat(with userId: Int, userImage: URL?, username: String, conversationId: Int, rate: String) {
        viewController?.tabBarController?.navigationController?.pushViewController(ChatRouter.createModule(with: userId, userImage: userImage, username: username, conversationId: conversationId, rate: rate), animated: true)
    }
    
}
