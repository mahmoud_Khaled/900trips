//
//  ConversationsInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ConversationsInteractor: ConversationsInteractorInputProtocol {
    
    weak var presenter: ConversationsInteractorOutputProtocol?
    private let conversationsWorker = ConversationsWorker()
    
    func getAllConversations(page: Int) {
        conversationsWorker.getAllConvesations(page: page) { [weak self ] result,totalPags  in
            guard let self = self else { return }
            self.presenter?.didFetchAllConversations(with: result, totalPages: totalPags)
        }
    }
    
    func deleteConversation(conversationId: Int, index: Int) {
        conversationsWorker.delteConvesation(conversationId: conversationId) { [weak self ] result in
            guard let self = self else { return }
            self.presenter?.didDeleteConversation(with: result, index: index) 
        }
    }
}
