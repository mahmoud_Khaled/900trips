//
//  ConversationsViewController.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ConversationsViewController: BaseViewController, ConversationsViewProtocol {
    
    private let mainView = ConversationsView()
    var presenter: ConversationsPresenterProtocol!
    var firstLoad = true
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "conversations".localized()
        setupNavigationBarStyle()
        setupTableView()
        addTargets() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if firstLoad {
            presenter.viewWillAppear()
            presenter.setEmptyArray()
            firstLoad = false
        }
    }
    
    private func addTargets() {
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
    }
    
    
    func setEmptyText(message: String) {
        mainView.tableView.emptyMessage(message: message)
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func moveIndexToFirst(_ index: Int) {
        mainView.tableView.beginUpdates()
        mainView.tableView.moveRow(at: IndexPath(row: index, section: 0), to: IndexPath(row: 0, section: 0))
        mainView.tableView.endUpdates()
        UIView.performWithoutAnimation {
            mainView.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
    }
}

extension ConversationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConversationCell.className, for: indexPath) as? ConversationCell else { return UITableViewCell() }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: nil) { [weak self] _, _, completion in
            guard let self = self else { return }
            self.presenter.deleteConverstation(at: indexPath)
            completion(true)
        }
        deleteAction.image = UIImage(named: "ic_delete_all")?.with(size: CGSize(width: 20, height: 25))
        deleteAction.backgroundColor = .red
        let config =  UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row <= presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
}

