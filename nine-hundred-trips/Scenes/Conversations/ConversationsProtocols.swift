//
//  ConversationsProtocols.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ConversationsRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToChat(with userId: Int, userImage: URL?, username: String, conversationId: Int, rate: String)
}

protocol ConversationsPresenterProtocol: class {
    var view: ConversationsViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<ConversationViewModel>, at indextPath: IndexPath)
    func viewWillAppear()
    func didSelectRow(at indexPath: IndexPath)
    func setEmptyArray()
    func pullToRefresh()
    func loadNextPage()
    func deleteConverstation(at indexPath: IndexPath)
}

protocol ConversationsInteractorInputProtocol: class {
    var presenter: ConversationsInteractorOutputProtocol? { get set }
    func getAllConversations(page: Int)
    func deleteConversation(conversationId: Int, index: Int)
}

protocol ConversationsInteractorOutputProtocol: class {
    func didFetchAllConversations(with result: Result<[Conversation]>, totalPages: Int)
    func didDeleteConversation(with result: Result<Data>, index: Int)
}

protocol ConversationsViewProtocol: class {
    var presenter: ConversationsPresenterProtocol! { get set }
    func reloadTableView()
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func setEmptyText(message: String)
    func moveIndexToFirst(_ index: Int)
    func endRefreshing()
}
