//
//  ConversationCell.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class ConversationCell: UITableViewCell, ConfigurableCell {
    
    lazy var userImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "offerimage"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 23
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.text = "Dams for tourism "
        label.numberOfLines = 1
        return label
    }()
    
    lazy var companyMessageDetails: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "welcom to dams for tourism"
        label.numberOfLines = 1
        return label
    }()
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "23:59"
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var moreImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_skip")?.withRenderingMode(.alwaysTemplate).imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .coral
        return imageView
    }()
    
    private lazy var cosmosView: CosmosView = {
        let cosmosView = CosmosView()
        cosmosView.translatesAutoresizingMaskIntoConstraints = false
        cosmosView.settings.starSize = 20.0
        cosmosView.settings.starMargin = 6.14
        cosmosView.settings.totalStars = 5
        cosmosView.rating = 0.0
        cosmosView.settings.textColor = .coral
        cosmosView.settings.filledColor = .coral
        cosmosView.settings.filledBorderColor = .coral
        cosmosView.settings.emptyColor = UIColor.coral.withAlphaComponent(0.30)
        cosmosView.settings.emptyBorderColor = UIColor.coral.withAlphaComponent(0.30)
        cosmosView.settings.emptyColor = UIColor(red: 251/255, green: 144/255, blue: 95/255, alpha: 0.3)
        cosmosView.settings.fillMode = .precise
        cosmosView.isUserInteractionEnabled = false
        return cosmosView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        if UserDefaultsHelper.userType == .company {
            cosmosView.isHidden = true
        }
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(userImageView)
        addSubview(usernameLabel)
        addSubview(companyMessageDetails)
        addSubview(timeLabel)
        addSubview(moreImageView)
        addSubview(cosmosView)
    }
    
    private func setupCompanyImageViewConstraints() {
        NSLayoutConstraint.activate([
            userImageView.heightAnchor.constraint(equalToConstant: 46),
            userImageView.widthAnchor.constraint(equalToConstant: 46),
            userImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            userImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20)
        ])
    }
    
    private func setupCompanyNameLabelConstraints() {
        NSLayoutConstraint.activate([
            usernameLabel.centerYAnchor.constraint(equalTo: userImageView.centerYAnchor, constant: -10),
            usernameLabel.leadingAnchor.constraint(equalTo: userImageView.trailingAnchor, constant: 8),
            usernameLabel.trailingAnchor.constraint(equalTo: moreImageView.leadingAnchor, constant: -50)
        ])
    }
    
    private func setupCompanyMessageDetailsConstraints() {
        NSLayoutConstraint.activate([
            companyMessageDetails.centerYAnchor.constraint(equalTo: userImageView.centerYAnchor,constant: 10),
            companyMessageDetails.leadingAnchor.constraint(equalTo: userImageView.trailingAnchor, constant: 8),
            companyMessageDetails.trailingAnchor.constraint(equalTo: moreImageView.leadingAnchor, constant: -8)
        ])
    }
    
    private func setupTimeLabelConstraints() {
        NSLayoutConstraint.activate([
            timeLabel.topAnchor.constraint(equalTo: topAnchor,constant: 10),
            timeLabel.trailingAnchor.constraint(equalTo: moreImageView.leadingAnchor, constant: -8)
        ])
    }
    
    private func setupMoreImageViewConstraints() {
        NSLayoutConstraint.activate([
            moreImageView.heightAnchor.constraint(equalToConstant: 19),
            moreImageView.widthAnchor.constraint(equalToConstant: 19),
            moreImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            moreImageView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private func setupCosmosViewConstraints() {
        NSLayoutConstraint.activate([
            cosmosView.topAnchor.constraint(equalTo: companyMessageDetails.bottomAnchor, constant: 10),
            cosmosView.leadingAnchor.constraint(equalTo: userImageView.trailingAnchor, constant: 8),
        ])
    }

    private func layoutUI() {
        addSubviews()
        setupCompanyImageViewConstraints()
        setupCompanyNameLabelConstraints()
        setupCompanyMessageDetailsConstraints()
        setupMoreImageViewConstraints()
        setupTimeLabelConstraints()
        setupCosmosViewConstraints()
    }
    
    func configure(model: ConversationViewModel) {
        userImageView.load(url: model.otherUserImage)
        usernameLabel.text = model.otherUserName
        companyMessageDetails.text = model.lastMessage
        timeLabel.text = model.time
        cosmosView.rating = model.rate
        cosmosView.text = String(model.rate)
    }
}
