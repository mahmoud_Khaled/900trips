//
//  ConversationsView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ConversationsView: UIView {
    
    let refreshControl = UIRefreshControl()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.register(ConversationCell.self, forCellReuseIdentifier: ConversationCell.className)
        tableView.refreshControl = refreshControl
        tableView.estimatedRowHeight = 0
        if UserDefaultsHelper.userType == .company {
            tableView.rowHeight = 85
        } else {
            tableView.rowHeight = 105
        }
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(tableView)
    }
    
    private func setupTableViewConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupTableViewConstraints()
    }
    
}
