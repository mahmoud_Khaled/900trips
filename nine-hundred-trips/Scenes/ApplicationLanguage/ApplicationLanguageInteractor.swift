//
//  ApplicationLanguageInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ApplicationLanguageInteractor: ApplicationLanguageInteractorInputProtocol {
    
    weak var presenter: ApplicationLanguageInteractorOutputProtocol?
    
    private let userWorker = UserWorker()
    
    func changeLanguage(code: String) {
        userWorker.changeLanguage(code: code) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didChangeLanguage(with: result)
        }
    }
}
