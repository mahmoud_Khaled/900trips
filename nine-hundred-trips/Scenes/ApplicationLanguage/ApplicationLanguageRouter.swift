//
//  ApplicationLanguageRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ApplicationLanguageRouter: ApplicationLanguageRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ApplicationLanguageViewController()
        let interactor = ApplicationLanguageInteractor()
        let router = ApplicationLanguageRouter()
        let presenter = ApplicationLanguagePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToHome(language: Language) {
        if language == .arabic {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        AppDelegate.shared.setRootViewController(
            AppDelegate.shared.createTabBarController(),
            animated: true
        )
    }
    
}
