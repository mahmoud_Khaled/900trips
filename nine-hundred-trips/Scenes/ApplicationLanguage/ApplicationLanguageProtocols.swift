//
//  ApplicationLanguageProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ApplicationLanguageRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToHome(language: Language)
}

protocol ApplicationLanguagePresenterProtocol: class {
    var view: ApplicationLanguageViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<LanguageModel>, for indexPath: IndexPath)
    func didSelectRow(at indexPath: IndexPath)
    func doneButtonTapped()
}

protocol ApplicationLanguageInteractorInputProtocol: class {
    var presenter: ApplicationLanguageInteractorOutputProtocol? { get set }
    func changeLanguage(code: String)
}

protocol ApplicationLanguageInteractorOutputProtocol: class {
    func didChangeLanguage(with result: Result<Data>)
}

protocol ApplicationLanguageViewProtocol: class {
    var presenter: ApplicationLanguagePresenterProtocol! { get set }
    func reloadData()
    func showActivityIndicator()
    func hideActivityIndicator()
}
