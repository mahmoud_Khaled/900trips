//
//  ApplicationLanguageCell.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ApplicationLanguageCell: UITableViewCell, ConfigurableCell {
    
    private lazy var languageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_arabic")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var languageNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "الإمارات"
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        return label
    }()
    
    private lazy var selectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_success")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.07), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(languageImageView)
        addSubview(lineView)
        addSubview(languageNameLabel)
        addSubview(selectedImageView)
    }
    
    private func setupCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            languageImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            languageImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            languageImageView.widthAnchor.constraint(equalToConstant: 25),
            languageImageView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func setupCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            languageNameLabel.leadingAnchor.constraint(equalTo: languageImageView.trailingAnchor, constant: 10),
            languageNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSelectedImageConstraints() {
        NSLayoutConstraint.activate([
            selectedImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            selectedImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            selectedImageView.widthAnchor.constraint(equalToConstant: 25),
            selectedImageView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupCountryImageViewConstraints()
        setupCountryLabelConstraints()
        setupLineViewConstraints()
        setupSelectedImageConstraints()
    }
    
    func configure(model: LanguageModel) {
        languageNameLabel.text = model.name
        selectedImageView.isHidden = !model.isSelected
        if model.code == "ar" {
            languageImageView.image = UIImage(named: "ic_arabic")
        } else {
            languageImageView.image = UIImage(named: "ic_english")
        }
    }
}
