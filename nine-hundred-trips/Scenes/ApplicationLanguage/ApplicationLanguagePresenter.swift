//
//  ApplicationLanguagePresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ApplicationLanguagePresenter: ApplicationLanguagePresenterProtocol, ApplicationLanguageInteractorOutputProtocol {
    
    weak var view: ApplicationLanguageViewProtocol?
    private let interactor: ApplicationLanguageInteractorInputProtocol
    private let router: ApplicationLanguageRouterProtocol
    private var languages = [
        LanguageModel(code: "ar", name: "العربية", isSelected: L102Language.currentLanguage == "ar" ? true : false),
        LanguageModel(code: "en", name: "English", isSelected: L102Language.currentLanguage != "ar" ? true : false)
    ]
    
    var numberOfRows: Int {
        return languages.count
    }
    
    init(view: ApplicationLanguageViewProtocol, interactor: ApplicationLanguageInteractorInputProtocol, router: ApplicationLanguageRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func configure(cell: AnyConfigurableCell<LanguageModel>, for indexPath: IndexPath) {
        cell.configure(model: languages[indexPath.row])
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        for (index, _) in languages.enumerated() {
            languages[index].isSelected = index == indexPath.row
        }
        view?.reloadData()
    }
    
    func doneButtonTapped() {
        view?.showActivityIndicator()
        if UserDefaultsHelper.isLoggedIn {
            interactor.changeLanguage(code: languages.first(where: { $0.isSelected == true })!.code)
        } else {
            let languageCode = languages.first(where: { $0.isSelected == true })!.code
            L102Language.setLanguage(code: languageCode)
            router.navigateToHome(language: Language(rawValue: languageCode) ?? .arabic)
        }
    }
    
    func didChangeLanguage(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            let languageCode = languages.first(where: { $0.isSelected == true })!.code
            L102Language.setLanguage(code: languageCode)
            router.navigateToHome(language: Language(rawValue: languageCode) ?? .arabic)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
