//
//  ApplicationLanguageViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ApplicationLanguageViewController: BaseViewController, ApplicationLanguageViewProtocol {
    
    private let mainView = ApplicationLanguageView()
    var presenter: ApplicationLanguagePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "applicationLanguage".localized()
        setupTableView()
        setupNavigationBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupTableView() {
        mainView.languageTableView.dataSource = self
        mainView.languageTableView.delegate = self
    }
    
    private func setupNavigationBarButton() {
        let doneButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_done"))
            .width(22)
            .height(22)
            .action(target: self, selector: #selector(doneButtonTapped))
            .build()
        
        navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc private func doneButtonTapped() {
        presenter.doneButtonTapped()
    }
    
    func reloadData() {
        mainView.languageTableView.reloadData()
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
}

extension ApplicationLanguageViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ApplicationLanguageCell.className, for: indexPath) as! ApplicationLanguageCell
        presenter.configure(cell: AnyConfigurableCell(cell), for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
}
