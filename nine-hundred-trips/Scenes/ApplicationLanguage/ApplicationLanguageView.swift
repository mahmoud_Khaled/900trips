//
//  ApplicationLanguageView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ApplicationLanguageView: UIView {
    
    lazy var languageContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var languageTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.isScrollEnabled = false
        tableView.register(ApplicationLanguageCell.self, forCellReuseIdentifier: ApplicationLanguageCell.className)
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(languageContainerView)
        languageContainerView.addSubview(languageTableView)
    }
    
    private func setupLanguageContainerViewConstraints() {
        NSLayoutConstraint.activate([
            languageContainerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 26),
            languageContainerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            languageContainerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
        ])
    }
    
    private func setupLanguageTableViewConstraints() {
        NSLayoutConstraint.activate([
            languageTableView.topAnchor.constraint(equalTo: languageContainerView.topAnchor, constant: 4),
            languageTableView.leadingAnchor.constraint(equalTo: languageContainerView.leadingAnchor, constant: 4),
            languageTableView.trailingAnchor.constraint(equalTo: languageContainerView.trailingAnchor, constant: -4),
            languageTableView.bottomAnchor.constraint(equalTo: languageContainerView.bottomAnchor, constant: -4),
            languageTableView.heightAnchor.constraint(equalToConstant: 84)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupLanguageContainerViewConstraints()
        setupLanguageTableViewConstraints()
    }
    
}

