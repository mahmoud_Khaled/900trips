//
//  RegisterConfirmationViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterConfirmationViewController: BaseViewController, RegisterConfirmationViewProtocol {
    
    private let mainView = RegisterConfirmationView()
    var presenter: RegisterConfirmationPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setupNavigationBarStyle()
        title = "verifyAccount".localized()
    }
    
   private func addTargets() {
        mainView.confirmButton.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        mainView.resendCodeButton.addTarget(self, action: #selector(resendCodeButtonTapped), for: .touchUpInside)
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    @objc private func confirmButtonTapped() {
        presenter.confirmButtonTapped(verificationCode: mainView.verificationCodeTextField.text ?? "")
    }
    @objc private func resendCodeButtonTapped() {
        presenter.resendCodeButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "resendSuccessful".localized(), on: navigationController?.view, for: 2) {
            
        }
    }
}
