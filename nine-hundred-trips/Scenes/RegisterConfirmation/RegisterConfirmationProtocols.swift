//
//  RegisterConfirmationProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol RegisterConfirmationRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToHome()
}

protocol RegisterConfirmationPresenterProtocol: class {
    var view: RegisterConfirmationViewProtocol? { get set }
    func confirmButtonTapped(verificationCode: String)
    func resendCodeButtonTapped()
}

protocol RegisterConfirmationInteractorInputProtocol: class {
    var presenter: RegisterConfirmationInteractorOutputProtocol? { get set }
    func verify(email: String, verificationCode: String, deviceToken: String)
    func resendCode(email: String)
}

protocol RegisterConfirmationInteractorOutputProtocol: class {
    func didVerify(with result: Result<User>)
    func didResendCode(with result: Result<Data>)
}

protocol RegisterConfirmationViewProtocol: class {
    var presenter: RegisterConfirmationPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
