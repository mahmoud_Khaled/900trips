//
//  RegisterConfirmationRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterConfirmationRouter: RegisterConfirmationRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(email: String) -> UIViewController {
        let view = RegisterConfirmationViewController()
        let interactor = RegisterConfirmationInteractor()
        let router = RegisterConfirmationRouter()
        let presenter = RegisterConfirmationPresenter(view: view, interactor: interactor, router: router, email: email)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToHome() {
        AppDelegate.shared.setRootViewController(
            AppDelegate.shared.createTabBarController(),
            animated: false
        )
    }
}
