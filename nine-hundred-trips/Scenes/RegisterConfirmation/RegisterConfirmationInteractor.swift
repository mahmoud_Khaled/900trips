//
//  RegisterConfirmationInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterConfirmationInteractor: RegisterConfirmationInteractorInputProtocol {
    
    weak var presenter: RegisterConfirmationInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func verify(email: String, verificationCode: String, deviceToken: String) {
        authenticationWorker.verify(email: email, verificationCode: verificationCode, deviceToken: deviceToken) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didVerify(with: result)
        }
    }
    
    func resendCode(email: String) {
        authenticationWorker.resetPassword(email: email) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didResendCode(with: result)
        }
    }
    
}
