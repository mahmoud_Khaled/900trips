//
//  RegisterConfirmationPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RegisterConfirmationPresenter: RegisterConfirmationPresenterProtocol, RegisterConfirmationInteractorOutputProtocol {
    
    weak var view: RegisterConfirmationViewProtocol?
    private let interactor: RegisterConfirmationInteractorInputProtocol
    private let router: RegisterConfirmationRouterProtocol
    private var email: String
    
    init(view: RegisterConfirmationViewProtocol, interactor: RegisterConfirmationInteractorInputProtocol, router: RegisterConfirmationRouterProtocol, email: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.email = email
    }
    
    func confirmButtonTapped(verificationCode: String) {
        
        guard !verificationCode.isEmpty else {
            router.showAlert(with: "enterVerificationCode".localized())
            return
        }
        
        view?.showActivityIndicator()
        interactor.verify(email: email, verificationCode: verificationCode, deviceToken: UserDefaultsHelper.firebaseToken)
        
    }
    
    func resendCodeButtonTapped() {
        view?.showActivityIndicator()
        interactor.resendCode(email: email)
    }
    
    func didVerify(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            setUserDefaults(user: value)
            router.navigateToHome()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didResendCode(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    private func setUserDefaults(user: User) {
        UserDefaultsHelper.id = user.id ?? 0
        UserDefaultsHelper.token = user.token ?? ""
        UserDefaultsHelper.isLoggedIn = true
        UserDefaultsHelper.userType = user.userType ?? .client
        UserDefaultsHelper.countryId = Int(user.country?.id ?? 0)
        UserDefaultsHelper.currencyId = Int(user.currency?.id ?? 0)
    }
    
}
