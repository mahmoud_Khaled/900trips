//
//  RegisterConfirmationView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterConfirmationView: UIView {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var verifivationCodeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "enterRegisterVerificationCode".localized()
        label.textColor = .textColor
        label.font = DinNextFont.medium.getFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var verificationCodeTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "enterCode".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 5))
        textField.rightViewMode = .always
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("confirm".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 30
        return button
    }()
    
    lazy var resendCodeButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("resendCode".localized(), for: .normal)
        button.setTitleColor(.textColor, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .clear
        return button
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(verifivationCodeLabel)
        containerView.addSubview(verificationCodeTextField)
        containerView.addSubview(confirmButton)
        containerView.addSubview(resendCodeButton)
    }
    
    
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func setupVerificationCodeLabelConstraints() {
        NSLayoutConstraint.activate([
            verifivationCodeLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 100),
            verifivationCodeLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            verifivationCodeLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
        ])
    }
    
    private func setupVerificationCodeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            verificationCodeTextField.topAnchor.constraint(equalTo: verifivationCodeLabel.bottomAnchor, constant: 20),
            verificationCodeTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            verificationCodeTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            verificationCodeTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupResendCodeButtonConstraints() {
        NSLayoutConstraint.activate([
            resendCodeButton.topAnchor.constraint(equalTo: verificationCodeTextField.bottomAnchor, constant: 20),
            resendCodeButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            resendCodeButton.widthAnchor.constraint(equalToConstant: 150),
            resendCodeButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupConfirmButtonConstraints() {
        NSLayoutConstraint.activate([
            confirmButton.topAnchor.constraint(equalTo: resendCodeButton.bottomAnchor, constant: 20),
            confirmButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            confirmButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            confirmButton.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
   
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupVerificationCodeLabelConstraints()
        setupVerificationCodeTextFieldConstraints()
        setupResendCodeButtonConstraints()
        setupConfirmButtonConstraints()
    }
    
}
