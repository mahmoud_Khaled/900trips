//
//  SuggestTourismPlaceRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import YangMingShan

class SuggestTourismPlaceRouter: SuggestTourismPlaceRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(tourismCategories: [PlaceCategoryViewModel]) -> UIViewController {
        let view = SuggestTourismPlaceViewController(assetToImageConverter: AssetToImageConverter())
        let interactor = SuggestTourismPlaceInteractor()
        let router = SuggestTourismPlaceRouter()
        let presenter = SuggestTourismPlacePresenter(view: view, interactor: interactor, router: router, tourismCategories: tourismCategories)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToTourismPlaceLocation(delgate: GetTourismPlaceLocationProtocol) {
        viewController?.navigationController?.pushViewController(TourismPlaceLocationRouter.createModule(delgate: delgate), animated: true)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showPermissionAlert(with title: String, message: String) {
        AlertBuilder(title: title, message: message, preferredStyle: .alert)
            .addAction(title: "Cancel", style: .cancel)
            .addAction(title: "Settings", style: .default) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
            .build()
            .show()
    }
    
    func presenteImagePicker() {
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        let customColor = UIColor.init(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0)

        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        viewController?.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: (viewController as! SuggestTourismPlaceViewController))
    }
}
