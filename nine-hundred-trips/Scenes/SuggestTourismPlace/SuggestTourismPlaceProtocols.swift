//
//  SuggestTourismPlaceProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol SuggestTourismPlaceRouterProtocol: class {
    func navigateToTourismPlaceLocation(delgate: GetTourismPlaceLocationProtocol)
    func showAlert(with message: String)
    func showPermissionAlert(with title: String, message: String)
    func presenteImagePicker()
}

protocol SuggestTourismPlacePresenterProtocol: class {
    var view: SuggestTourismPlaceViewProtocol? { get set }
    func navigateToTourismPlaceLocation()
    func getCities(countryId: Int)
    func viewDidLoad()
    func addTourismPlace(titleAr: String, titleEn: String, descriptionAr: String, descriptionEn: String, categoryId: Int, countryId: Int, cityId: Int, photos: [File])
    func resetValues()
    func showPermissionAlert(with title: String, message: String)
    func presentImagePicker()
}

protocol SuggestTourismPlaceInteractorInputProtocol: class {
    var presenter: SuggestTourismPlaceInteractorOutputProtocol? { get set }
    func getCountries()
    func getCities(countryId: Int)
    func uploadImages(images: [File])
    func addTourismPlace(titleAr: String, titleEn: String, descriptionAr: String, descriptionEn: String, categoryId: Int, countryId: Int, cityId: Int, longitude: Double, latitude: Double, photos: [String])
}

protocol SuggestTourismPlaceInteractorOutputProtocol: class {
    func didFetchCountries(with result: Result<[Country]>)
    func didFetchCities(with result: Result<[City]>)
    func didUploadImages(with result: Result<[String]>)
    func didFetchTourismPlace(with result: Result<Data>)
}

protocol SuggestTourismPlaceViewProtocol: class {
    var presenter: SuggestTourismPlacePresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func setLocationName(title: String)
    func setTourismCategories(categories: [PlaceCategoryViewModel])
    func setCountries(countries: [CountryViewModel])
    func setCities(cities: [CityViewModel])
    func showSuccessView()
}

protocol GetTourismPlaceLocationProtocol: class {
    func getLocation(latitude: Double, longitude: Double, address: String)
}
