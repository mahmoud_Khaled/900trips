//
//  SuggestTourismPlaceViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import YangMingShan

class SuggestTourismPlaceViewController: BaseViewController, SuggestTourismPlaceViewProtocol {
    
    private let mainView = SuggestTourismPlaceView()
    var presenter: SuggestTourismPlacePresenterProtocol!
    
    private var categoryId: Int?
    private var countryId: Int?
    private var cityId: Int?
    private var images = [UIImage]()
    
    private let assetToImageConverter: AssetToImageConverterProtocol
    
    init(assetToImageConverter: AssetToImageConverterProtocol) {
        self.assetToImageConverter = assetToImageConverter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "suggestTourismPlace".localized()
        setupNavigationBarStyle()
        setupCollectionsView()
        pickerViewDidSelectItemAction()
        addTargets()
        presenter.viewDidLoad()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func setupCollectionsView() {
        mainView.imagesCollectionView.delegate = self
        mainView.imagesCollectionView.dataSource = self
    }
    
    private func addTargets() {
        mainView.addImagesButton.addTarget(self, action: #selector(uploadImagesButtonTapped), for: .touchUpInside)
        mainView.chooseLocationButton.addTarget(self, action: #selector(navigateToTourismPlaceLocation), for: .touchUpInside)
        mainView.addButton.addTarget(self, action: #selector(addPlaceButtonTapped), for: .touchUpInside)
    }
    
    func setLocationName(title: String) {
        guard !title.isEmpty else {
            return
        }
        mainView.chooseLocationButton.setTitle(title, for: .normal)
    }
    
    func setCountries(countries: [CountryViewModel]) {
        mainView.countryTextField.items = countries
    }
    
    func setTourismCategories(categories: [PlaceCategoryViewModel]) {
        mainView.tourismCategoryTextField.items = categories
    }
    
    func setCities(cities: [CityViewModel]) {
        mainView.cityTextField.items = cities
    }
    
    private func pickerViewDidSelectItemAction() {
        
        mainView.tourismCategoryTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.categoryId =  item.id
        }
        
        mainView.countryTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.countryId =  item.id
            self.presenter.getCities(countryId: self.countryId!)
        }
        
        mainView.cityTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.cityId = item.id
        }
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "placeSuggestedSuccessfully".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.mainView.resetInputsValues()
            self.presenter.resetValues()
            self.images = []
        }
    }
    
    func deleteImage(indexPath: IndexPath) {
        images.remove(at: indexPath.row)
        mainView.imagesCollectionView.reloadData()
        if images.count == 0 {
            mainView.imagesCollectionView.isHidden = true
        }
    }
    
    @objc private func navigateToTourismPlaceLocation() {
        presenter.navigateToTourismPlaceLocation()
    }
    
    @objc private func uploadImagesButtonTapped() {
        presenter.presentImagePicker()
    }
    
    @objc private func addPlaceButtonTapped() {
        view.endEditing(true)
        var files: [File] = []
        if images.count > 0 {
            files = images.map {
                (name: "photo.png", key: "photos[]", mimeType: MimeType.jpg, data: $0.jpegData(compressionQuality: 0.5) ?? Data())
            }
        }
        presenter.addTourismPlace(titleAr: mainView.arabicTourismPlaceTextField.text!, titleEn: mainView.englishTourismPlaceTextField.text!, descriptionAr: mainView.arabicTourismPlaceTextField.text!, descriptionEn: mainView.englishDescriptionTextView.text!, categoryId: categoryId ?? 0, countryId: countryId ?? 0, cityId: cityId ?? 0, photos: files)
    }
}

extension SuggestTourismPlaceViewController: YMSPhotoPickerViewControllerDelegate {
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        presenter.showPermissionAlert(with: "allowPhotoAlbumAccess".localized(), message: "photoAccessDenied".localized())
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        presenter.showPermissionAlert(with: "allowCameraAccess".localized(), message: "photoAccessDenied".localized())
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            self.images.append(contentsOf: self.assetToImageConverter.convert(assets: photoAssets, targetSize: CGSize(width: 200, height: 400)))
            DispatchQueue.main.async {
                self.mainView.imagesCollectionView.isHidden = false
                self.mainView.imagesCollectionView.reloadData()
            }
        }
    }
}

extension SuggestTourismPlaceViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UploadImageCell.className, for: indexPath) as? UploadImageCell  else { return UICollectionViewCell() }
        cell.configure(model: images[indexPath.item])
        cell.deleteButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.deleteImage(indexPath: indexPath)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
}
