//
//  SuggestTourismPlacePresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class SuggestTourismPlacePresenter: SuggestTourismPlacePresenterProtocol, SuggestTourismPlaceInteractorOutputProtocol, GetTourismPlaceLocationProtocol {
    
    weak var view: SuggestTourismPlaceViewProtocol?
    private let interactor: SuggestTourismPlaceInteractorInputProtocol
    private let router: SuggestTourismPlaceRouterProtocol
    
    private var countries: [CountryViewModel] = []
    private var cities: [CityViewModel] = []
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var  titleArabic = String()
    private var titleEnglish = String()
    private var descriptionArabic = String()
    private var descriptionEnglish = String()
    private var categoryId = Int()
    private var countryId = Int()
    private var cityId = Int()
    
    init(view: SuggestTourismPlaceViewProtocol, interactor: SuggestTourismPlaceInteractorInputProtocol, router: SuggestTourismPlaceRouterProtocol, tourismCategories: [PlaceCategoryViewModel]) {
        self.view = view
        self.interactor = interactor
        self.router = router
        view.setTourismCategories(categories: tourismCategories)
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getCountries()
    }
    
    func getCities(countryId: Int) {
        view?.showActivityIndicator()
        interactor.getCities(countryId: countryId)
    }
    
    func didFetchCountries(with result: Result<[Country]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let result):
            countries = result.map{ CountryViewModel(country: $0) }
            view?.setCountries(countries: countries)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCities(with result: Result<[City]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let result):
            cities = result.map{ CityViewModel(city: $0) }
            view?.setCities(cities: cities)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didUploadImages(with result: Result<[String]>) {
        switch result {
        case .success(let result):
            interactor.addTourismPlace(titleAr: titleArabic, titleEn: titleEnglish, descriptionAr: descriptionArabic, descriptionEn: descriptionEnglish, categoryId: categoryId, countryId: countryId, cityId: cityId, longitude: longitude, latitude: latitude, photos: result)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchTourismPlace(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func navigateToTourismPlaceLocation() {
        router.navigateToTourismPlaceLocation(delgate: self)
    }
        
    func getLocation(latitude: Double, longitude: Double, address: String) {
        self.latitude = latitude
        self.longitude = longitude
        view?.setLocationName(title: address)
    }
    
    func addTourismPlace(titleAr: String, titleEn: String, descriptionAr: String, descriptionEn: String, categoryId: Int, countryId: Int, cityId: Int, photos: [File]) {
        guard photos.count > 0 else {
            router.showAlert(with: "noImagesAlert".localized())
            return
        }
        guard !titleEn.isEmpty else {
            router.showAlert(with: "noEnglishName".localized())
            return
        }
        guard !titleAr.isEmpty else {
            router.showAlert(with: "noArabicName".localized())
            return
        }
        guard categoryId != 0 else {
            router.showAlert(with: "enterCategory".localized())
            return
        }
        guard !descriptionEn.isEmpty else {
            router.showAlert(with: "noEnglishDescription".localized())
            return
        }
        guard !descriptionAr.isEmpty else {
            router.showAlert(with: "noArabicDescription".localized())
            return
        }
        guard countryId != 0 else {
            router.showAlert(with: "enterCountry".localized())
            return
        }
        guard cityId != 0 else {
            router.showAlert(with: "enterCity".localized())
            return
        }
        guard latitude != 0.0, longitude != 0.0 else {
            router.showAlert(with: "enterLocation".localized())
            return
        }
        
        titleArabic = titleAr
        titleEnglish = titleEn
        descriptionArabic = descriptionAr
        descriptionEnglish = descriptionEn
        self.categoryId = categoryId
        self.countryId = countryId
        self.cityId = cityId
        view?.showActivityIndicator()
        interactor.uploadImages(images: photos)
    }
    
    func resetValues() {
        longitude = 0.0
        latitude = 0.0
    }
    
    func showPermissionAlert(with title: String, message: String) {
        router.showPermissionAlert(with: title, message: message)
    }
    
    func presentImagePicker() {
        router.presenteImagePicker()
    }
}
