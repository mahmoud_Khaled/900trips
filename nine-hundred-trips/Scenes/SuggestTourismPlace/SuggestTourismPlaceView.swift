//
//  SuggestTourismPlaceView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SuggestTourismPlaceView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        scrolView.showsHorizontalScrollIndicator = false
        scrolView.showsVerticalScrollIndicator = false
        return scrolView
    }()
    
    private lazy var containerView: IQPreviousNextView = {
        let view = IQPreviousNextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var addImagesContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    lazy var addImagesButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_addphotos"), for: .normal)
        return button
    }()
    
    private lazy var addImagesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "addImages".localized()
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        return label
    }()
    
    private lazy var inputsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(imagesCollectionView)
        stackView.addArrangedSubview(englishTourismPlaceTextField)
        stackView.addArrangedSubview(arabicTourismPlaceTextField)
        stackView.addArrangedSubview(tourismCategoryTextField)
        stackView.addArrangedSubview(englishDescriptionContainerView)
        stackView.addArrangedSubview(arabicDescriptionContainerView)
        stackView.addArrangedSubview(countryTextField)
        stackView.addArrangedSubview(cityTextField)
        stackView.addArrangedSubview(chooseLocationButton)
        stackView.addArrangedSubview(addButton)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    lazy var imagesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(UploadImageCell.self, forCellWithReuseIdentifier: UploadImageCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isHidden = true
        return collectionView
    }()
    
    lazy var englishTourismPlaceTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "nameInEnglish".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var arabicTourismPlaceTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "nameInArabic".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var tourismCategoryTextField: PickerViewTextField<PlaceCategoryViewModel> = {
        let textField = PickerViewTextField<PlaceCategoryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "categoty".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var englishDescriptionContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 30.5
        return view
    }()
    
    lazy var arabicDescriptionContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 30.5
        return view
    }()
    
    lazy var englishDescriptionTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .white
        textView.font = DinNextFont.regular.getFont(ofSize: 20)
        textView.textColor = .textColor
        textView.placeholder = "englishDescription".localized()
        textView.minHeight = 132
        textView.maxHeight = 132
        return textView
    }()
    
    lazy var arabicDescriptionTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .white
        textView.font = DinNextFont.regular.getFont(ofSize: 20)
        textView.textColor = .textColor
        textView.placeholder = "arabicDescription".localized()
        textView.minHeight = 132
        textView.maxHeight = 132
        return textView
    }()
    
    lazy var countryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var cityTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "city".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var chooseLocationButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.mediumTurquoise, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 17)
        button.setImage(UIImage(named: "ic_selectlocation"), for: .normal)
        button.setTitle("choosePlaceLocationOnMap".localized(), for: .normal)
        button.tag = -1
        button.titleEdgeInsets.left = 8
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    lazy var addButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 17)
        button.setTitle("add".localized(), for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(addImagesContainerView)
        containerView.addSubview(addImagesButton)
        containerView.addSubview(addImagesLabel)
        containerView.addSubview(inputsStackView)
        englishDescriptionContainerView.addSubview(englishDescriptionTextView)
        arabicDescriptionContainerView.addSubview(arabicDescriptionTextView)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupAddImagesContainerViewConstraints() {
        NSLayoutConstraint.activate([
            addImagesContainerView.topAnchor.constraint(equalTo: containerView.topAnchor),
            addImagesContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            addImagesContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            addImagesContainerView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func setupAddImagesButtonConstraints() {
        NSLayoutConstraint.activate([
            addImagesButton.heightAnchor.constraint(equalToConstant: 50),
            addImagesButton.widthAnchor.constraint(equalToConstant: 50),
            addImagesButton.topAnchor.constraint(equalTo: addImagesContainerView.topAnchor, constant: 36.5),
            addImagesButton.centerXAnchor.constraint(equalTo: addImagesContainerView.centerXAnchor)
        ])
    }
    
    private func setupAddImagesLabelConstraints() {
        NSLayoutConstraint.activate([
            addImagesLabel.heightAnchor.constraint(equalToConstant: 25),
            addImagesLabel.centerXAnchor.constraint(equalTo: addImagesContainerView.centerXAnchor),
            addImagesLabel.topAnchor.constraint(equalTo: addImagesButton.bottomAnchor, constant: 5)
        ])
    }
    
    private func setupInputsStackViewConstraints() {
        NSLayoutConstraint.activate([
            inputsStackView.topAnchor.constraint(equalTo: addImagesContainerView.bottomAnchor, constant: 15),
            inputsStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            inputsStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            inputsStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15)
        ])
    }
    
    private func setupImagesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            imagesCollectionView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func setupEnglishTourismPlaceTextFeildConstraints() {
        NSLayoutConstraint.activate([
            englishTourismPlaceTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupArabicTourismPlaceTextFeildConstrints() {
        NSLayoutConstraint.activate([
            arabicTourismPlaceTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupPlaceCategoryTextFeildConstraints() {
        NSLayoutConstraint.activate([
            tourismCategoryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupEnglishDescriptionTextViewConstraints() {
        NSLayoutConstraint.activate([
            englishDescriptionTextView.topAnchor.constraint(equalTo: englishDescriptionContainerView.topAnchor, constant: 8),
            englishDescriptionTextView.bottomAnchor.constraint(equalTo: englishDescriptionContainerView.bottomAnchor, constant: -8),
            englishDescriptionTextView.leadingAnchor.constraint(equalTo: englishDescriptionContainerView.leadingAnchor, constant: 8),
            englishDescriptionTextView.trailingAnchor.constraint(equalTo: englishDescriptionContainerView.trailingAnchor, constant: -8)
        ])
    }
    
    private func setupArabicDescriptionTextViewConstraints() {
        NSLayoutConstraint.activate([
            arabicDescriptionTextView.topAnchor.constraint(equalTo: arabicDescriptionContainerView.topAnchor, constant: 8),
            arabicDescriptionTextView.bottomAnchor.constraint(equalTo: arabicDescriptionContainerView.bottomAnchor, constant: -8),
            arabicDescriptionTextView.leadingAnchor.constraint(equalTo: arabicDescriptionContainerView.leadingAnchor, constant: 8),
            arabicDescriptionTextView.trailingAnchor.constraint(equalTo: arabicDescriptionContainerView.trailingAnchor, constant: -8)
        ])
    }
    
    private func setupCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            countryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupCityTextFeildConstraints() {
        NSLayoutConstraint.activate([
            cityTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupAddButtonConatraints() {
        NSLayoutConstraint.activate([
            addButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupAddImagesContainerViewConstraints()
        setupAddImagesButtonConstraints()
        setupAddImagesLabelConstraints()
        setupInputsStackViewConstraints()
        setupImagesCollectionViewConstraints()
        setupEnglishTourismPlaceTextFeildConstraints()
        setupArabicTourismPlaceTextFeildConstrints()
        setupPlaceCategoryTextFeildConstraints()
        setupEnglishDescriptionTextViewConstraints()
        setupArabicDescriptionTextViewConstraints()
        setupCountryTextFieldConstraints()
        setupCityTextFeildConstraints()
        setupAddButtonConatraints()
    }
    
    func resetInputsValues() {
        imagesCollectionView.isHidden = true
        arabicTourismPlaceTextField.text = ""
        englishTourismPlaceTextField.text = ""
        arabicDescriptionTextView.text = ""
        englishDescriptionTextView.text = ""
        tourismCategoryTextField.text = ""
        countryTextField.text = ""
        cityTextField.text = ""
        chooseLocationButton.setTitle("choosePlaceLocationOnMap".localized(), for: .normal)
    }
    
}
