//
//  SuggestTourismPlaceInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SuggestTourismPlaceInteractor: SuggestTourismPlaceInteractorInputProtocol {
    
    weak var presenter: SuggestTourismPlaceInteractorOutputProtocol?
    private let applicationDataWorker = ApplicationDataWorker()
    private let placeGuideWorker = TourismGuideWorker()
    
    func getCountries() {
        applicationDataWorker.getCountries { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountries(with: result)
        }
    }
    
    func getCities(countryId: Int) {
        applicationDataWorker.getCities(countryId: countryId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCities(with: result)
        }
    }
    
    func uploadImages(images: [File]) {
        placeGuideWorker.uploadTourismImages(files: images) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUploadImages(with: result)
        }
    }
    
    func addTourismPlace(titleAr: String, titleEn: String, descriptionAr: String, descriptionEn: String, categoryId: Int, countryId: Int, cityId: Int, longitude: Double, latitude: Double, photos: [String]) {
        let parameters: [String: Any] = [
            "title": titleAr,
            "title_en": titleEn,
            "description": descriptionAr,
            "description_en": descriptionEn,
            "category_id": categoryId,
            "longitude": longitude,
            "latitude": latitude,
            "country_id": countryId,
            "city_id": cityId,
            "photos": photos
        ]
        placeGuideWorker.addTourismPlace(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchTourismPlace(with: result)
        }
    }

}
