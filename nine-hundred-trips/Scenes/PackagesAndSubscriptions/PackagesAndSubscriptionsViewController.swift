//
//  PackagesAndSubscriptionsViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PackagesAndSubscriptionsViewController: BaseViewController, PackagesAndSubscriptionsViewProtocol {
    
    private let  mainView = PackagesAndSubscriptionsView()
    var presenter: PackagesAndSubscriptionsPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "packagesAndSubscriptions".localized()
        setupNavigationBarStyle()
        setupTableView()
        presenter.viewDidLoad()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func setDataOfCurrentSubscriptionPackage(with package: SubscriptionPackageViewModel) {
        mainView.setCurrentSubscriptionPackage(package: package)
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
        mainView.setTableViewHeight(height: CGFloat(presenter.numberOfRows * 120))
    }
    
    private func addTargets() {
        mainView.balanceButton.addTarget(self, action: #selector(balanceButtonTaped), for: .touchUpInside)
    }
    
    @objc private  func balanceButtonTaped() {
        presenter.balacneButtonTapped()
    }
    
}

extension PackagesAndSubscriptionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PackageCell.className, for: indexPath) as? PackageCell else { return UITableViewCell() }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        cell.subscriptionViewTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.subscribeButtonTapped(at: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "subscriptionsType".localized()
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        header.textLabel?.font = DinNextFont.regular.getFont(ofSize: 20)
        header.textLabel?.textColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        header.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}
