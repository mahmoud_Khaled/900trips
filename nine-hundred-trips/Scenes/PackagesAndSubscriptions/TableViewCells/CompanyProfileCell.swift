//
//  CompanyProfileTableViewCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyProfileCell: UITableViewCell, ConfigurableCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: CompanyProfileItem) {
        textLabel?.text = model.name
        textLabel?.textColor = UIColor.darkGray
        textLabel?.font = DinNextFont.regular.getFont(ofSize: 17)
        imageView?.image = UIImage(named: model.image)
    }
}
