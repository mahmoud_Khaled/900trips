//
//  PackageCell.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PackageCell: UITableViewCell, ConfigurableCell {
    
    var subscriptionViewTapped: ()->() = { }
    
    private let subscriptionViewGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer()
        return gesture
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var packageNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Monthly subscription"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var subscribeView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 15
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(subscriptionViewGesture)
        return view
    }()

    private lazy var subscribeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "subscribe".localized()
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.numberOfLines = 1
        return label
    }()

    lazy var subscribeImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_button_plus"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    
    private lazy var firstRoundedView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 5
        return view
    }()
    
    private lazy var firstDetailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "offer period during month"
        label.textColor = UIColor(red: 206/255, green: 206/255, blue: 206/255, alpha: 1)
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var secondRoundedView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 5
        return view
    }()
    
    private lazy var secondDetailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "specialized for one week"
        label.textColor = UIColor(red: 206/255, green: 206/255, blue: 206/255, alpha: 1)
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 1
        return label
    }()

    
    private lazy var packagePriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "1223 AED"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        layoutUI()
        addTarget()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
       addSubview(containerView)
        containerView.addSubview(packageNameLabel)
        containerView.addSubview(subscribeView)
        subscribeView.addSubview(subscribeLabel)
        subscribeView.addSubview(subscribeImageView)
        containerView.addSubview(firstRoundedView)
        containerView.addSubview(firstDetailLabel)
        containerView.addSubview(secondRoundedView)
        containerView.addSubview(secondDetailLabel)
        containerView.addSubview(packagePriceLabel)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)
        ])
    }
    
    private func setupPackageNameLabelConstraints() {
        NSLayoutConstraint.activate([
            packageNameLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            packageNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            packageNameLabel.trailingAnchor.constraint(lessThanOrEqualTo: subscribeView.leadingAnchor, constant: -2),
        ])
    }
    
    private func setupSubscribeViewConstraints() {
        NSLayoutConstraint.activate([
            subscribeView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            subscribeView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            subscribeView.heightAnchor.constraint(equalToConstant: 30),
            subscribeView.widthAnchor.constraint(equalToConstant: 95)
        ])
    }
    
    private func setupSubscribeImageViewConstraints() {
        NSLayoutConstraint.activate([
            subscribeImageView.centerYAnchor.constraint(equalTo: subscribeView.centerYAnchor, constant: 0),
            subscribeImageView.leadingAnchor.constraint(equalTo: subscribeView.leadingAnchor, constant: 8)
        ])
    }
    
    private func setupSubscribeLabelConstraints() {
        NSLayoutConstraint.activate([
            subscribeLabel.centerYAnchor.constraint(equalTo: subscribeView.centerYAnchor, constant: -2),
            subscribeLabel.leadingAnchor.constraint(equalTo: subscribeImageView.trailingAnchor, constant: 3),
            subscribeLabel.trailingAnchor.constraint(equalTo: subscribeView.trailingAnchor, constant: -12)
        ])
    }
    
    private func setupFirstRoundedViewConstraints() {
        NSLayoutConstraint.activate([
            firstRoundedView.topAnchor.constraint(equalTo: packageNameLabel.bottomAnchor, constant: 10),
            firstRoundedView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            firstRoundedView.widthAnchor.constraint(equalToConstant: 10),
            firstRoundedView.heightAnchor.constraint(equalToConstant: 10)
        ])
    }
    
    private func setupFirstDetailLabelConstraints() {
        NSLayoutConstraint.activate([
            firstDetailLabel.centerYAnchor.constraint(equalTo: firstRoundedView.centerYAnchor, constant: -3),
            firstDetailLabel.leadingAnchor.constraint(equalTo: firstRoundedView.trailingAnchor, constant: 4),
            firstDetailLabel.trailingAnchor.constraint(lessThanOrEqualTo: packagePriceLabel.leadingAnchor, constant: -1)
        ])
    }
    
    private func setupSecondRoundedViewConstraints() {
        NSLayoutConstraint.activate([
            secondRoundedView.topAnchor.constraint(equalTo: firstRoundedView.bottomAnchor, constant: 10),
            secondRoundedView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            secondRoundedView.widthAnchor.constraint(equalToConstant: 10),
            secondRoundedView.heightAnchor.constraint(equalToConstant: 10)
        ])
    }
    
    private func setupSecondDetailLabelConstraints() {
        NSLayoutConstraint.activate([
            secondDetailLabel.centerYAnchor.constraint(equalTo: secondRoundedView.centerYAnchor, constant: -3),
            secondDetailLabel.leadingAnchor.constraint(equalTo: secondRoundedView.trailingAnchor, constant: 4),
            secondDetailLabel.trailingAnchor.constraint(lessThanOrEqualTo: packagePriceLabel.leadingAnchor, constant: -1)
        ])
    }
    
    private func setupPackagePriceLabelConstraints() {
        NSLayoutConstraint.activate([
            packagePriceLabel.topAnchor.constraint(equalTo: subscribeView.bottomAnchor, constant: 2),
            packagePriceLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupPackageNameLabelConstraints()
        setupSubscribeViewConstraints()
        setupSubscribeLabelConstraints()
        setupSubscribeImageViewConstraints()
        setupFirstRoundedViewConstraints()
        setupFirstDetailLabelConstraints()
        setupSecondRoundedViewConstraints()
        setupSecondDetailLabelConstraints()
        setupPackagePriceLabelConstraints()
    }
    
    func configure(model: PackageViewModel) {
        packageNameLabel.text = model.name
        packagePriceLabel.text = model.price
        firstDetailLabel.text = model.offers
        secondDetailLabel.text = model.specialOffers
    }
    
    private func addTarget() {
        subscriptionViewGesture.addTarget(self, action: #selector(addSubscriptionViewTapped))
    }
    
    @objc private func addSubscriptionViewTapped() {
        subscriptionViewTapped()
    }
}
