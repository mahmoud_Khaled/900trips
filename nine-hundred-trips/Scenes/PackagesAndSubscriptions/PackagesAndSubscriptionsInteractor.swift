//
//  PackagesAndSubscriptionsInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PackagesAndSubscriptionsInteractor: PackagesAndSubscriptionsInteractorInputProtocol {
    
    weak var presenter: PackagesAndSubscriptionsInteractorOutputProtocol?
    
    private let packagesWorker = PackagesWorker()
    
    func getPackagesAndSubscriptions() {
        packagesWorker.getPackagesAndSubscritions {[weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchPackagesAndSubscriptions(with: result)
        }
    }
    
}
