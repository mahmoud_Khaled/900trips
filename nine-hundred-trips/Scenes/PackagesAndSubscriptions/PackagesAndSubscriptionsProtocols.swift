//
//  PackagesAndSubscriptionsProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol PackagesAndSubscriptionsRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(to destination: PackagesAndSubscriptionsDestination)
}

protocol PackagesAndSubscriptionsPresenterProtocol: class {
    var view: PackagesAndSubscriptionsViewProtocol? { get set }
    var numberOfRows: Int { get }
    func viewDidLoad()
    func configure(cell: AnyConfigurableCell<PackageViewModel>, at indexPath: IndexPath)
    func subscribeButtonTapped(at indexPath: IndexPath)
    func balacneButtonTapped()
    
}

protocol PackagesAndSubscriptionsInteractorInputProtocol: class {
    var presenter: PackagesAndSubscriptionsInteractorOutputProtocol? { get set }
    func getPackagesAndSubscriptions()
}

protocol PackagesAndSubscriptionsInteractorOutputProtocol: class {
    func didFetchPackagesAndSubscriptions(with result: Result<PackageAndSubscription>)
}

protocol PackagesAndSubscriptionsViewProtocol: class {
    var presenter: PackagesAndSubscriptionsPresenterProtocol! { get set }
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func reloadTableView()
    func setDataOfCurrentSubscriptionPackage(with package: SubscriptionPackageViewModel)
}
