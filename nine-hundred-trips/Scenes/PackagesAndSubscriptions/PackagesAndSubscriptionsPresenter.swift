//
//  PackagesAndSubscriptionsPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class PackagesAndSubscriptionsPresenter: PackagesAndSubscriptionsPresenterProtocol, PackagesAndSubscriptionsInteractorOutputProtocol {
    
    weak var view: PackagesAndSubscriptionsViewProtocol?
    private let interactor: PackagesAndSubscriptionsInteractorInputProtocol
    private let router: PackagesAndSubscriptionsRouterProtocol
    
    private var packages: [PackageViewModel] = []
    private var currentPackage: SubscriptionPackageViewModel!
    
    var numberOfRows: Int {
        return packages.count
    }
    
    init(view: PackagesAndSubscriptionsViewProtocol, interactor: PackagesAndSubscriptionsInteractorInputProtocol, router: PackagesAndSubscriptionsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getPackagesAndSubscriptions()
    }
    
    func didFetchPackagesAndSubscriptions(with result: Result<PackageAndSubscription>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            packages =  value.packages.map({ PackageViewModel(package: $0) })
            currentPackage = SubscriptionPackageViewModel(subscriptionPackage: value.current)
            view?.setDataOfCurrentSubscriptionPackage(with: currentPackage)
            view?.reloadTableView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<PackageViewModel>, at indexPath: IndexPath) {
        cell.configure(model: packages[indexPath.row])
    }
    
    func subscribeButtonTapped(at indexPath: IndexPath) {
        
    }
    
    func balacneButtonTapped() {
        router.navigate(to: .balance)
    }
    
}
