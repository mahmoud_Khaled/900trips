//
//  PackagesAndSubscriptionsView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PackagesAndSubscriptionsView: UIView {
    
    private var tableViewHeightConstraint: NSLayoutConstraint!
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var subscriptionsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var currentSubscriptionsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "currentSubscription".localized()
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var seperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        return view
    }()
    
    private lazy var subscriptionsTypeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "---"
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 21)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var informationImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_info")?.withRenderingMode(.alwaysTemplate))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.tintColor = .white
        return imageView
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----"
        label.textColor = UIColor(red: 196/255, green: 196/255, blue: 196/255, alpha: 1)
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var subscriptionDaysView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 58
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var progressView: ProgressView = {
        let view = ProgressView(radius: 40, strokeColor: .coral, lineWidth: 6, frame: CGRect(x: 0, y: 0, width: 102, height: 102))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.strokeEnd = 0
        return view
    }()
    
    private lazy var remainingDaysLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.heavy.getFont(ofSize: 28)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var dayTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "day".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var remainingDaysTilte: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "remainingUntilPayment".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var balanceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("yourBalance".localized(), for: .normal)
        button.backgroundColor = .gray
        button.layer.cornerRadius = 18
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 15)
        button.setTitleColor(.darkGray, for: .normal)
        return button
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(PackageCell.self, forCellReuseIdentifier: PackageCell.className)
        tableView.estimatedRowHeight = 0
        tableView.rowHeight = 110
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(subscriptionsView)
        subscriptionsView.addSubview(currentSubscriptionsLabel)
        subscriptionsView.addSubview(seperatorView)
        subscriptionsView.addSubview(subscriptionsTypeLabel)
        subscriptionsView.addSubview(informationImageView)
        subscriptionsView.addSubview(priceLabel)
        
        containerView.addSubview(subscriptionDaysView)
        subscriptionDaysView.addSubview(progressView)
        subscriptionDaysView.addSubview(remainingDaysLabel)
        subscriptionDaysView.addSubview(dayTitleLabel)
        
        containerView.addSubview(remainingDaysTilte)
        containerView.addSubview(balanceButton)
        containerView.addSubview(tableView)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupSubscriptionsViewConstraints() {
        NSLayoutConstraint.activate([
            subscriptionsView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            subscriptionsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            subscriptionsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            subscriptionsView.heightAnchor.constraint(equalToConstant: 173)
        ])
    }
    
    private func setupCurrentSubscriptionsLabelConstraints() {
        NSLayoutConstraint.activate([
            currentSubscriptionsLabel.centerXAnchor.constraint(equalTo: subscriptionsView.centerXAnchor),
            currentSubscriptionsLabel.topAnchor.constraint(equalTo: subscriptionsView.topAnchor, constant: 10)
        ])
    }
    
    private func setupSeperatorViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorView.topAnchor.constraint(equalTo: currentSubscriptionsLabel.bottomAnchor, constant: 5),
            seperatorView.leadingAnchor.constraint(equalTo: subscriptionsView.leadingAnchor, constant: 0),
            seperatorView.trailingAnchor.constraint(equalTo: subscriptionsView.trailingAnchor, constant: 0),
            seperatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSubscriptionsTypeLabelConstraints() {
        NSLayoutConstraint.activate([
            subscriptionsTypeLabel.topAnchor.constraint(equalTo: seperatorView.bottomAnchor, constant: 5),
            subscriptionsTypeLabel.leadingAnchor.constraint(equalTo: subscriptionsView.leadingAnchor, constant: 10),
            subscriptionsTypeLabel.trailingAnchor.constraint(equalTo: informationImageView.leadingAnchor, constant: -4)
        ])
    }
    
    private func setupInformationImageViewConstraints() {
        NSLayoutConstraint.activate([
            informationImageView.centerYAnchor.constraint(equalTo: subscriptionsTypeLabel.centerYAnchor, constant: 5),
            informationImageView.trailingAnchor.constraint(equalTo: subscriptionsView.trailingAnchor, constant: -8),
            informationImageView.heightAnchor.constraint(equalToConstant: 27),
            informationImageView.widthAnchor.constraint(equalToConstant: 27)
        ])
    }
    
    private func setupPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            priceLabel.topAnchor.constraint(equalTo: subscriptionsTypeLabel.bottomAnchor, constant: 1),
            priceLabel.trailingAnchor.constraint(equalTo: subscriptionsView.trailingAnchor, constant: -4),
            priceLabel.leadingAnchor.constraint(equalTo: subscriptionsView.leadingAnchor, constant: 10)
        ])
    }
    
    private func setupSubscriptionDaysViewConstraints() {
        NSLayoutConstraint.activate([
            subscriptionDaysView.topAnchor.constraint(equalTo: subscriptionsView.bottomAnchor, constant: -50),
            subscriptionDaysView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            subscriptionDaysView.heightAnchor.constraint(equalToConstant: 116),
            subscriptionDaysView.widthAnchor.constraint(equalToConstant: 116),
        ])
    }
    
    private func setupProgressViewConstraints() {
        NSLayoutConstraint.activate([
            progressView.topAnchor.constraint(equalTo: subscriptionDaysView.topAnchor, constant: 5),
            progressView.centerXAnchor.constraint(equalTo: subscriptionDaysView.centerXAnchor),
            progressView.widthAnchor.constraint(equalToConstant: 102),
            progressView.heightAnchor.constraint(equalToConstant: 102)
        ])
        progressView.layoutIfNeeded()
    }
    
    private func setupRemainingDaysLabelConstraints() {
        NSLayoutConstraint.activate([
            remainingDaysLabel.centerXAnchor.constraint(equalTo: subscriptionDaysView.centerXAnchor),
            remainingDaysLabel.centerYAnchor.constraint(equalTo: subscriptionDaysView.centerYAnchor, constant: -15)
        ])
    }
    
    private func setupDayTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            dayTitleLabel.centerXAnchor.constraint(equalTo: subscriptionDaysView.centerXAnchor),
            dayTitleLabel.centerYAnchor.constraint(equalTo: subscriptionDaysView.centerYAnchor, constant: 5)
        ])
    }
    
    private func setupRemainingDaysTilteConstraints() {
        NSLayoutConstraint.activate([
            remainingDaysTilte.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            remainingDaysTilte.topAnchor.constraint(equalTo: subscriptionDaysView.bottomAnchor, constant: -10)
        ])
    }
    
    private func setupBalanceButtonConstraints() {
        NSLayoutConstraint.activate([
            balanceButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            balanceButton.topAnchor.constraint(equalTo: remainingDaysTilte.bottomAnchor, constant: 10),
            balanceButton.heightAnchor.constraint(equalToConstant: 36),
            balanceButton.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
    
    private func setupTableViewConstraints() {
        tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            tableViewHeightConstraint,
            tableView.topAnchor.constraint(equalTo: balanceButton.bottomAnchor, constant: 5),
            tableView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupSubscriptionsViewConstraints()
        setupCurrentSubscriptionsLabelConstraints()
        setupSeperatorViewConstraints()
        setupSubscriptionsTypeLabelConstraints()
        setupInformationImageViewConstraints()
        setupPriceLabelConstraints()
        setupSubscriptionDaysViewConstraints()
        setupProgressViewConstraints()
        setupRemainingDaysLabelConstraints()
        setupDayTitleLabelConstraints()
        setupRemainingDaysTilteConstraints()
        setupBalanceButtonConstraints()
        setupTableViewConstraints()
    }
    
    func setTableViewHeight(height: CGFloat) {
        tableViewHeightConstraint.constant = height
    }
    
    func setCurrentSubscriptionPackage(package: SubscriptionPackageViewModel) {
        subscriptionsTypeLabel.text = package.name
        priceLabel.text = package.price
        progressView.strokeEnd = package.strokeEnd
        remainingDaysLabel.text = package.remainingDaysText
    }
    
}
