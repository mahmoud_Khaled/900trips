//
//  PackagesAndSubscriptionsRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum PackagesAndSubscriptionsDestination {
    case balance
}

class PackagesAndSubscriptionsRouter: PackagesAndSubscriptionsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = PackagesAndSubscriptionsViewController()
        let interactor = PackagesAndSubscriptionsInteractor()
        let router = PackagesAndSubscriptionsRouter()
        let presenter = PackagesAndSubscriptionsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigate(to destination: PackagesAndSubscriptionsDestination) {
        switch destination {
        case .balance: viewController?.navigationController?.pushViewController(BalanceRouter.createModule(), animated: true)
        }
    }
}
