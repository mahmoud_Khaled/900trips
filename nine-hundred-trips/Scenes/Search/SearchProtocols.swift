//
//  SearchProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol SearchRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToSearchResults(with results: [OfferViewModel], totalPages: Int, fromCityId: Int, toCityId: Int)
}

protocol SearchPresenterProtocol: class {
    var view: SearchViewProtocol? { get set }
    func getCountries()
    func getCities(countryId: Int, for fetchType: FetchType)
    func searchOffers(fromCityId: Int, toCityId: Int)
}

protocol SearchInteractorInputProtocol: class {
    var presenter: SearchInteractorOutputProtocol? { get set }
    func getCountries()
    func getCities(countryId: Int)
    func searchOffers(fromCityId: Int, toCityId: Int, page: Int)
}

protocol SearchInteractorOutputProtocol: class {
    func didFetchCountries(with result: Result<[Country]>)
    func didFetchCities(with result: Result<[City]>)
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int)
}

protocol SearchViewProtocol: class {
    var presenter: SearchPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetch(countries: [CountryViewModel])
    func didFetch(cities: [CityViewModel], for fetchType: FetchType)
}
