//
//  SearchViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum FetchType {
    case departure, arrival
}

class SearchViewController: BaseViewController, SearchViewProtocol {
    
    private let mainView = SearchView()
    var presenter: SearchPresenterProtocol!
    
    private var departureCountryId = 0
    private var departureCityId = 0
    
    private var arrivalCountryId = 0
    private var arrivalCityId = 0
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "search".localized()
        presenter.getCountries()
        addTargets()
        
    }
    
    private func addTargets() {
        
        mainView.departureCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCountryId = item.id
            self.mainView.departureCityTextField.text = ""
            self.departureCityId = 0
            self.presenter.getCities(countryId: self.departureCountryId, for: .departure)
        }
        
        mainView.departureCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCityId = item.id
        }
        
        mainView.arrivalCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCountryId = item.id
            self.mainView.arrivalCityTextField.text = ""
            self.arrivalCityId = 0
            self.presenter.getCities(countryId: self.arrivalCountryId, for: .arrival)
        }
        
        mainView.arrivalCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCityId = item.id
        }
        
        mainView.searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        
    }
    
    @objc private func searchButtonTapped() {
        presenter.searchOffers(fromCityId: departureCityId, toCityId: arrivalCityId)
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func didFetch(countries: [CountryViewModel]) {
        mainView.departureCountryTextField.items = countries
        mainView.arrivalCountryTextField.items = countries
    }

    
    func didFetch(cities: [CityViewModel], for fetchType: FetchType) {
        switch fetchType {
        case .departure:
            mainView.departureCityTextField.items = cities
        case .arrival:
            mainView.arrivalCityTextField.items = cities
        }
    }
    
}
