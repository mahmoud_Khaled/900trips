//
//  SearchInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SearchInteractor: SearchInteractorInputProtocol {
    
    weak var presenter: SearchInteractorOutputProtocol?
    
    private let applicationDataWorker = ApplicationDataWorker()
    private let offersWorker = OffersWorker()
    
    func getCountries() {
        applicationDataWorker.getCountries { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountries(with: result)
        }
    }
    
    func getCities(countryId: Int) {
        applicationDataWorker.getCities(countryId: countryId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCities(with: result)
        }
    }
    
    func searchOffers(fromCityId: Int, toCityId: Int, page: Int) {
        offersWorker.searchOffers(fromCityId: fromCityId, toCityId: toCityId, page: page) { [weak self] result, pages  in
            guard let self = self else { return }
            self.presenter?.didFetchOffers(with: result, totalPages: pages)
        }
    }
}
