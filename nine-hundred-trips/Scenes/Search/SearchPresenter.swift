//
//  SearchPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class SearchPresenter: SearchPresenterProtocol, SearchInteractorOutputProtocol {
    
    weak var view: SearchViewProtocol?
    private let interactor: SearchInteractorInputProtocol
    private let router: SearchRouterProtocol
    private var fetchType: FetchType!
    private var totalPages = 1
    private var fromCityId: Int?
    private var toCityId: Int?
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        return dateFormatter
    }()
    
    init(view: SearchViewProtocol, interactor: SearchInteractorInputProtocol, router: SearchRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getCountries() {
        view?.showActivityIndicator()
        interactor.getCountries()
    }
    
    func getCities(countryId: Int, for fetchType: FetchType) {
        view?.showActivityIndicator()
        self.fetchType = fetchType
        interactor.getCities(countryId: countryId)
    }
    
    func searchOffers(fromCityId: Int, toCityId: Int) {
        if fromCityId == 0 {
            router.showAlert(with: "emptyDepartureCity".localized())
        } else if toCityId == 0 {
            router.showAlert(with: "emptyArrivalCity".localized())
        } else {
            self.fromCityId = fromCityId
            self.toCityId = toCityId
            view?.showActivityIndicator()
            interactor.searchOffers(fromCityId: fromCityId, toCityId: toCityId, page: 1)
        }
    }
    
    func didFetchCountries(with result: Result<[Country]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let countries):
            view?.didFetch(countries: countries.map(CountryViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCities(with result: Result<[City]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let cities):
            view?.didFetch(cities: cities.map(CityViewModel.init), for: fetchType)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            self.totalPages = totalPages
            let searchResults = value.map { OfferViewModel(offer: $0, dateFormatter: self.dateFormatter, stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}") }
            
            if searchResults.isEmpty {
                router.showAlert(with: "noResultsFound".localized())
            } else {
                router.navigateToSearchResults(with: searchResults, totalPages: totalPages, fromCityId: fromCityId ?? 0, toCityId: toCityId ?? 0)
            }
            
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
