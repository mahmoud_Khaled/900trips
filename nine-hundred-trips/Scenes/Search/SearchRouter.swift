//
//  SearchRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SearchRouter: SearchRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = SearchViewController()
        let interactor = SearchInteractor()
        let router = SearchRouter()
        let presenter = SearchPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToSearchResults(with results: [OfferViewModel], totalPages: Int, fromCityId: Int, toCityId: Int) {
        viewController?.navigationController?.pushViewController(SearchResultsRouter.createModule(with: results, totalPages: totalPages, fromCityId: fromCityId, toCityId: toCityId), animated: true)
    }
    
}
