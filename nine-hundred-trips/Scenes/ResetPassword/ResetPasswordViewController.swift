//
//  ResetPasswordViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ResetPasswordViewController: BaseViewController, ResetPasswordViewProtocol {
    
    private let mainView = ResetPasswordView()
    var presenter: ResetPasswordPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "resetPassword".localized()
        addTargets()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupNavigationBarButtons() {
        let dismissButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_close"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(dismissButtonTapped))
            .build()
        
        navigationItem.leftBarButtonItem = dismissButton
    }
    
    @objc private func dismissButtonTapped() {
        presenter.dismissButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "resetPasswordCodeSent".localized(), on: navigationController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.presenter.navigateToNewPassword()
        }
    }
    
    private func addTargets() {
        mainView.sendButton.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
    }
    
    @objc private func sendButtonTapped() {
        presenter.resetPassword(email: mainView.emailTextField.text!)
    }
}
