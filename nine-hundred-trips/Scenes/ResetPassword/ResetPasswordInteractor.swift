//
//  ResetPasswordInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ResetPasswordInteractor: ResetPasswordInteractorInputProtocol {
    
    weak var presenter: ResetPasswordInteractorOutputProtocol?
    
    private let authenticationWorker = AuthenticationWorker()
    
    func resetPassword(email: String) {
        authenticationWorker.resetPassword(email: email) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didResetPassword(with: result)
        }
    }
}
