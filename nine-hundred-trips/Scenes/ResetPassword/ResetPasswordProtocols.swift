//
//  ResetPasswordProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ResetPasswordRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToNewPassword(with email: String)
    func dismiss()
}

protocol ResetPasswordPresenterProtocol: class {
    var view: ResetPasswordViewProtocol? { get set }
    func resetPassword(email: String)
    func navigateToNewPassword()
    func dismissButtonTapped()
}

protocol ResetPasswordInteractorInputProtocol: class {
    var presenter: ResetPasswordInteractorOutputProtocol? { get set }
    func resetPassword(email: String)
}

protocol ResetPasswordInteractorOutputProtocol: class {
    func didResetPassword(with result: Result<Data>)
}

protocol ResetPasswordViewProtocol: class {
    var presenter: ResetPasswordPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
