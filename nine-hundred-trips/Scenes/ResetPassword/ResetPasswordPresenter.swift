//
//  ResetPasswordPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ResetPasswordPresenter: ResetPasswordPresenterProtocol, ResetPasswordInteractorOutputProtocol {
    
    weak var view: ResetPasswordViewProtocol?
    private let interactor: ResetPasswordInteractorInputProtocol
    private let router: ResetPasswordRouterProtocol
    private var email: String = ""
    
    init(view: ResetPasswordViewProtocol, interactor: ResetPasswordInteractorInputProtocol, router: ResetPasswordRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func resetPassword(email: String) {
        if email.isEmpty {
            router.showAlert(with: "emptyEmail".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: email) {
            router.showAlert(with: "incorrectEmail".localized())
        } else {
            view?.showActivityIndicator()
            interactor.resetPassword(email: email)
            self.email = email
        }
    }
    
    func didResetPassword(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func navigateToNewPassword() {
        router.navigateToNewPassword(with: email)
    }
    
    func dismissButtonTapped() {
        router.dismiss()
    }
    
}
