//
//  ResetPasswordRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ResetPasswordRouter: ResetPasswordRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ResetPasswordViewController()
        let interactor = ResetPasswordInteractor()
        let router = ResetPasswordRouter()
        let presenter = ResetPasswordPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToNewPassword(with email: String) {
        viewController?.navigationController?.pushViewController(NewPasswordRouter.createModule(with: email), animated: true)
    }
    
    func dismiss() {
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
