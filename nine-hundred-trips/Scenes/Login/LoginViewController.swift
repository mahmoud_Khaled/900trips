//
//  loginViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, LoginViewProtocol {
   
    
    private let mainView = LoginView()
    var presenter: LoginPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setNavigationBarTransparent(true)
        setupNavigationBarStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.tintColor = .coral
    }
    
    private func addTargets() {
        mainView.loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        mainView.forgetPaswordButton.addTarget(self, action:  #selector(forgetPasswordButtonTapped), for: .touchUpInside)
        mainView.registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
    }
    
    
    @objc private func loginButtonTapped () {
        presenter.loginButtonTapped(email: mainView.emailTextField.text!, password: mainView.passwordTextField.text!)
    }
    
    @objc private func forgetPasswordButtonTapped() {
       presenter.forgetPasswordButtonTapped()
    }
    
    @objc private func registerButtonTapped() {
        presenter.registerButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
}
