//
//  loginRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum LoginDestination {
    case home(userType: UserType), forgetPassword, verifyAccount(email: String), register
}

class LoginRouter: LoginRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = LoginViewController()
        let interactor = LoginInteractor()
        let router = LoginRouter()
        let presenter = LoginPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigate(to destination: LoginDestination) {
        switch destination {
        case .home:
            AppDelegate.shared.setRootViewController(
                AppDelegate.shared.createTabBarController(),
                animated: false
            )
        case .forgetPassword:
            viewController?.present(ResetPasswordRouter.createModule(), animated: true, completion: nil)
        case .register:
            viewController?.navigationController?.pushViewController(RegisterRouter.createModule(), animated: true)
        case .verifyAccount(let email):
            viewController?.present(RegisterConfirmationRouter.createModule(email: email), animated: true)
        }
    }
    
    func showAlert(with message: String) {
         viewController?.showAlert(with: message)
    }
    
    func dismiss() {
       viewController?.navigationController?.popViewController(animated: true)
    }
    
    
}
