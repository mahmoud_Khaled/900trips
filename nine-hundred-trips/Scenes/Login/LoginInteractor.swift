//
//  loginInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginInteractor: LoginInteractorInputProtocol {
    
    weak var presenter: LoginInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func login(email: String, password: String, deviceToken: String) {
        authenticationWorker.login(email: email, password: password, deviceToken: deviceToken) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didLogin(with: result)
        }
    }

}
