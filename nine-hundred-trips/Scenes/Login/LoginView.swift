//
//  loginView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    private lazy var backgroundImageView: UIImageView =  {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "background")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var separatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        return view
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.attributedPlaceholder = NSAttributedString(string: "email".localized(),
                                                             attributes: [.foregroundColor: UIColor.coral])
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_mail"), width: 20, height: 20)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.keyboardType = .emailAddress
        return textField
    }()

     lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.attributedPlaceholder = NSAttributedString(string: "password".localized(),
                                                             attributes: [.foregroundColor: UIColor.coral])
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        return textField
    }()

     lazy var forgetPaswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("forgetPassword".localized(), for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 20)
        return button
    }()
    
    private lazy var firstsSeparatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .cyan
        return view
    }()
    
    private lazy var secondSeparatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .cyan
        return view
    }()
    
    private lazy var registerByLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "loginBy".localized()
        label.textColor = .cyan
        label.font = DinNextFont.regular.getFont(ofSize: 19)
        return label
    }()
    
     lazy var loginByFacebookButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named:"ic_facebook"), for: .normal)
        return button
    }()
    
     lazy var loginByGoogleButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named:"ic_google"), for: .normal)
        return button
    }()
    
     lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("login".localized(), for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("register".localized(), for: .normal)
        button.backgroundColor = .cyan
        button.layer.cornerRadius = 25
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(logoImageView)
        containerView.addSubview(separatorLineView)
        containerView.addSubview(emailTextField)
        containerView.addSubview(passwordTextField)
        containerView.addSubview(loginButton)
        containerView.addSubview(forgetPaswordButton)
        containerView.addSubview(firstsSeparatorLineView)
        containerView.addSubview(registerByLabel)
        containerView.addSubview(secondSeparatorLineView)
        containerView.addSubview(loginByFacebookButton)
        containerView.addSubview(loginByGoogleButton)
        containerView.addSubview(registerButton)
    }
    
    private func setupBackgroundImageViewConstraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 24),
            logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageView.widthAnchor.constraint(equalToConstant: 230.0),
            logoImageView.heightAnchor.constraint(equalToConstant: 230.0)
            
        ])
    }
    

    private func setupSeparatorLineViewConstraints() {
        NSLayoutConstraint.activate([
            separatorLineView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 15),
            separatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            separatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            separatorLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupEmailTextFieldConstraints() {
        NSLayoutConstraint.activate([
            emailTextField.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 35),
            emailTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            emailTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            emailTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    

    private func setupPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 15),
            passwordTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            passwordTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            passwordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    
    private func setupLoginButtonConstraints() {
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 15),
            loginButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            loginButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            loginButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupForgetPasswordButtonConstraints() {
        NSLayoutConstraint.activate([
            forgetPaswordButton.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 15),
            forgetPaswordButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            forgetPaswordButton.heightAnchor.constraint(equalToConstant: 30)
        ])
     
    }
    private func setupFirstSeparatorLineViewConstraints() {
        NSLayoutConstraint.activate([
            firstsSeparatorLineView.topAnchor.constraint(equalTo: forgetPaswordButton.bottomAnchor, constant: 25),
            firstsSeparatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            firstsSeparatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            firstsSeparatorLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupRegiserByLabelConstraints() {
        NSLayoutConstraint.activate([
            registerByLabel.topAnchor.constraint(equalTo: firstsSeparatorLineView.bottomAnchor, constant: 6),
            registerByLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupSecondSeparatorLineViewConstraints() {
        NSLayoutConstraint.activate([
            secondSeparatorLineView.topAnchor.constraint(equalTo: registerByLabel.bottomAnchor, constant: 6),
            secondSeparatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            secondSeparatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            secondSeparatorLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupLoginByFacebookButtonConstraints() {
        NSLayoutConstraint.activate([
            loginByFacebookButton.topAnchor.constraint(equalTo: secondSeparatorLineView.bottomAnchor, constant: 20),
            loginByFacebookButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -35)
            
        ])
    }
    
    private func setupLoginByGoogleButtonConstraints() {
        NSLayoutConstraint.activate([
            loginByGoogleButton.topAnchor.constraint(equalTo: secondSeparatorLineView.bottomAnchor, constant: 20),
            loginByGoogleButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 35)
        ])
    }
    
   
    
    private func setupRegisterButtonConstraints() {
        NSLayoutConstraint.activate([
            registerButton.topAnchor.constraint(equalTo: loginByGoogleButton.bottomAnchor, constant: 20),
            registerButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            registerButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            registerButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16),
            registerButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupSeparatorLineViewConstraints()
        setupEmailTextFieldConstraints()
        setupPasswordTextFieldConstraints()
        setupLoginButtonConstraints()
        setupForgetPasswordButtonConstraints()
        setupFirstSeparatorLineViewConstraints()
        setupRegiserByLabelConstraints()
        setupSecondSeparatorLineViewConstraints()
        setupLoginByFacebookButtonConstraints()
        setupLoginByGoogleButtonConstraints()
        setupRegisterButtonConstraints()
    }
    
}
