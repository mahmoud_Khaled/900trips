//
//  loginPresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class LoginPresenter: LoginPresenterProtocol, LoginInteractorOutputProtocol {
   
    
    weak var view: LoginViewProtocol?
    private let interactor: LoginInteractorInputProtocol
    private let router: LoginRouterProtocol
    private var email = ""
    
    init(view: LoginViewProtocol, interactor: LoginInteractorInputProtocol, router: LoginRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loginButtonTapped(email: String, password: String) {
        self.email = email
        if email.isEmpty && password.isEmpty {
             router.showAlert(with: "requiredFields".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: email) {
            router.showAlert(with: "incorrectEmail".localized())
        } else if password == "" {
             router.showAlert(with: "emptyPassword".localized())
        } else {
            view?.showActivityIndicator()
            interactor.login(email: email, password: password, deviceToken: UserDefaultsHelper.firebaseToken)
        }
    }
    
    func facebookLoginButtonTapped() {
    
    }
    
    func googleLoginButtonTapped() {
        
    }
    
    func forgetPasswordButtonTapped() {
        router.navigate(to: .forgetPassword)
    }
    
    func registerButtonTapped() {
        router.navigate(to: .register)
    }
    
    func didLogin(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let user):
            setUserDefaults(user: user)
            router.navigate(to: .home(userType: user.userType ?? .client))
        case .failure(let error):
            if let networkError = error as? NetworkError, case NetworkError.unverified = networkError {
                router.navigate(to: .verifyAccount(email: email))
            } else {
                router.showAlert(with: error.localizedDescription)
            }
        }
    }
    
    private func setUserDefaults(user: User) {
        UserDefaultsHelper.id = user.id ?? 0
        UserDefaultsHelper.token = user.token ?? ""
        UserDefaultsHelper.isLoggedIn = true
        UserDefaultsHelper.userType = user.userType ?? .client
        UserDefaultsHelper.countryId = Int(user.country?.id ?? 0)
        UserDefaultsHelper.currencyId = Int(user.currency?.id ?? 0)
    }
    
}
