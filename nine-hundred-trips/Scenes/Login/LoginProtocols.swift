//
//  loginProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol LoginRouterProtocol: class {
    func navigate(to destination: LoginDestination)
    func showAlert(with message: String)
    func dismiss()

}

protocol LoginPresenterProtocol: class {
    var view: LoginViewProtocol? { get set }
    func loginButtonTapped(email: String, password: String)
    func facebookLoginButtonTapped()
    func googleLoginButtonTapped()
    func forgetPasswordButtonTapped()
    func registerButtonTapped()
}

protocol LoginInteractorInputProtocol: class {
    var presenter: LoginInteractorOutputProtocol? { get set }
    func login(email: String, password: String, deviceToken: String)

}

protocol LoginInteractorOutputProtocol: class {
    func didLogin(with result: Result<User>)
}

protocol LoginViewProtocol: class {
    var presenter: LoginPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
}
