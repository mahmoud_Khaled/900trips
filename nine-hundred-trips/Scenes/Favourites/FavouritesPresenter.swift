//
//  FavouritesPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class FavouritesPresenter: FavouritesPresenterProtocol, FavouritesInteractorOutputProtocol, UpdateOfferProtocol {
    
    weak var view: FavouritesViewProtocol?
    private let interactor: FavouritesInteractorInputProtocol
    private let router: FavouritesRouterProtocol
    
    private var favouritesViewModels = [OfferViewModel]()
    private var currentPage = 1
    private var totalPages = 1
    private var isLoading = false
    
    var numberOfRows: Int {
        return favouritesViewModels.count
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    init(view: FavouritesViewProtocol, interactor: FavouritesInteractorInputProtocol, router: FavouritesRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getFavourites(page: currentPage)
    }
    
    func pullToRefresh() {
        isLoading = true
        currentPage = 1
        view?.changeTableViewUserInteraction(isUserInteractionEnabled: false)
        favouritesViewModels = []
        view?.setEmptyTextToTableView(message: "")
        interactor.getFavourites(page: currentPage)
    }

    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath) {
        guard !isLoading else { return }
        let offerViewModel = favouritesViewModels[indexPath.row]
        cell.configure(model: offerViewModel)
    }
    
    func removeFavourite(at indexPath: IndexPath) {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.removeFavourite(offerId: favouritesViewModels[indexPath.row].id, index: indexPath.row)
    }
    
    func didFetchFavourites(with result: Result<[Offer]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        switch result {
        case .success(let result):
            view?.changeTableViewUserInteraction(isUserInteractionEnabled: true)
            self.totalPages = totalPages
            favouritesViewModels.append(contentsOf: result.map {
                OfferViewModel(
                    offer: $0,
                    dateFormatter: self.dateFormatter,
                    stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}"
                )
            })
            isLoading = false
            view?.reloadTableView()
            if(favouritesViewModels.count == 0) {
                view?.setEmptyTextToTableView(message: "noFavourites".localized())
            }
        case .failure(let error):
            isLoading = false
            view?.resetTableViewOffset()
            router.showAlert(with: error.localizedDescription)
        }
        
    }
    
    func didRemoveFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
                "offerId": favouritesViewModels[index].id,
                "likeStatus": favouritesViewModels[index].isLiked
            ])
            favouritesViewModels.remove(at: index)
            view?.reloadTableView()
            if(favouritesViewModels.count == 0) {
                view?.setEmptyTextToTableView(message: "noFavourites".localized())
            }
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            view?.showActivityIndicator(isUserInteractionEnabled: true)
            currentPage += 1
            interactor.getFavourites(page: currentPage)
        }
    }
    
    func updateOffer(indexPath: IndexPath, isLiked: Bool) {
        favouritesViewModels[indexPath.row].isLiked = isLiked
        view?.reloadTableView()
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let offerId = favouritesViewModels[indexPath.row].id
        router.navigateToOfferDetails(indexPath: indexPath, offerId: offerId, delegate: self)
    }
}
