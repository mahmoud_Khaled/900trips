//
//  FavouritesProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol FavouritesRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToOfferDetails(indexPath: IndexPath, offerId: Int, delegate: UpdateOfferProtocol)
}

protocol FavouritesPresenterProtocol: class {
    var view: FavouritesViewProtocol? { get set }
    func viewDidLoad()
    func pullToRefresh()
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath)
    func removeFavourite(at indexPath: IndexPath)
    func loadNextPage()
    func didSelectRow(at indexPath: IndexPath)
}

protocol FavouritesInteractorInputProtocol: class {
    var presenter: FavouritesInteractorOutputProtocol? { get set }
    func getFavourites(page: Int)
    func removeFavourite(offerId: Int, index: Int)
}

protocol FavouritesInteractorOutputProtocol: class {
    func didFetchFavourites(with result: Result<[Offer]>, totalPages: Int)
    func didRemoveFavourite(with result: Result<Data>, index: Int)
}

protocol FavouritesViewProtocol: class {
    var presenter: FavouritesPresenterProtocol! { get set }
    func reloadTableView()
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func endRefreshing()
    func setEmptyTextToTableView(message: String)
    func resetTableViewOffset()
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool)
}
