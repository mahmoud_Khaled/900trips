//
//  FavouritesViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavouritesViewController: BaseViewController, FavouritesViewProtocol {
    
    private let mainView = FavouritesView()
    var presenter: FavouritesPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "favourites".localized()
        presenter.viewDidLoad()
        addTarget()
        setupNavigationBarStyle()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    private func addTarget() {
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    func setEmptyTextToTableView(message: String) {
        mainView.tableView.emptyMessage(message: message)
    }
    
    func resetTableViewOffset() {
        mainView.tableView.setContentOffset(.zero, animated: true)
    }
    
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool) {
        mainView.tableView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
}

extension FavouritesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.className, for: indexPath) as! OfferCell
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        cell.favouriteButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.removeFavourite(at: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
    
}
