//
//  FavouritesRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavouritesRouter: FavouritesRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = FavouritesViewController()
        let interactor = FavouritesInteractor()
        let router = FavouritesRouter()
        let presenter = FavouritesPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToOfferDetails(indexPath: IndexPath, offerId: Int, delegate: UpdateOfferProtocol) {
        viewController?.navigationController?.pushViewController(OfferDetailsRouter.createModule(isMyOffer: false, indexPath: indexPath, offerId: offerId, delegate: delegate), animated: true)
    }
    
}
