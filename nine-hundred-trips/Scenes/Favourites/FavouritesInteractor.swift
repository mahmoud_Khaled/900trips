//
//  FavouritesInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavouritesInteractor: FavouritesInteractorInputProtocol {
    weak var presenter: FavouritesInteractorOutputProtocol?
    
    private let offersWorker = OffersWorker()
    private let userWorker = UserWorker()
    
    func getFavourites(page: Int) {
        userWorker.getFavourites(page: page) { [weak self] (result, totalPages) in
            guard let self = self else { return }
            self.presenter?.didFetchFavourites(with: result, totalPages: totalPages)
        }
    }
        
    func removeFavourite(offerId: Int, index: Int) {
        offersWorker.removeFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRemoveFavourite(with: result, index: index)
        }
    }
}
