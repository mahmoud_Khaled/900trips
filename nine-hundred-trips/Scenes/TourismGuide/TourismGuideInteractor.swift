//
//  TourismGuideInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismGuideInteractor: TourismGuideInteractorInputProtocol {
    
    weak var presenter: TourismGuideInteractorOutputProtocol?
    private let tourismGuideWorker = TourismGuideWorker()
    private let applicationDataWorker = ApplicationDataWorker()
    
    func getPlacesCategories() {
        tourismGuideWorker.getPlacesCategories { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchPlacesCategories(with: result)
        }
    }
    
    func getTourismPlaces(categoryId: Int, cityId: Int, searchText: String, page: Int) {
        tourismGuideWorker.getTourismPlaces(categoryId: categoryId, cityId: cityId, searchText: searchText, page: page) { [weak self] result, pages  in
            guard let self = self else { return }
            self.presenter?.didFetchTourismPlaces(with: result, totalPages: pages)
        }
    }
    
    func getCities(countryId: Int) {
        applicationDataWorker.getCities(countryId: countryId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCities(with: result)
        }
    }
    
    func downloadTourismGuideFile() {
        tourismGuideWorker.downloadTourismGuideFile { [weak self] url in
            guard let self = self else { return }
            self.presenter?.didDownloadTourismGuideFile(with: url)
        }
    }
}
