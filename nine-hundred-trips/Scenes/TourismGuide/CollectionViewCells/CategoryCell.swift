//
//  CategoryCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell, ConfigurableCell {

    private lazy var categoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubViews() {
        addSubview(categoryImageView)
    }
    
    private func setupCategoryImageViewConstraints() {
        NSLayoutConstraint.activate([
            categoryImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            categoryImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            categoryImageView.widthAnchor.constraint(equalToConstant: 50),
            categoryImageView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func layoutUI() {
        addSubViews()
        setupCategoryImageViewConstraints()
    }
    
    func configure(model: PlaceCategoryViewModel) {
        categoryImageView.load(url: model.photo)
    }


}
