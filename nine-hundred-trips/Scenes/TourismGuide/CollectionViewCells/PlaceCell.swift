//
//  PlaceCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PlaceCell: UICollectionViewCell, ConfigurableCell {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.backgroundColor = .babyGray
        return view
    }()
    
    private lazy var placeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 40
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var placeNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.textColor = .coral
        label.text = "مسجد أسد إبن الفرات"
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var locationNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .black
        label.text = "الشارقة"
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubViews() {
        addSubview(containerView)
        containerView.addSubview(placeImageView)
        containerView.addSubview(placeNameLabel)
        containerView.addSubview(locationNameLabel)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    private func setupPlaceImageViewConstraints() {
        NSLayoutConstraint.activate([
            placeImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            placeImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            placeImageView.heightAnchor.constraint(equalToConstant: 80),
            placeImageView.widthAnchor.constraint(equalToConstant: 80)
        ])
    }
    
    private func setupPlaceNameLabelConstraints() {
        NSLayoutConstraint.activate([
            placeNameLabel.topAnchor.constraint(equalTo: placeImageView.bottomAnchor, constant: 5),
            placeNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            placeNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            placeNameLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupLocationNameLabelConstraints() {
        NSLayoutConstraint.activate([
            locationNameLabel.topAnchor.constraint(equalTo: placeNameLabel.bottomAnchor, constant: 5),
            locationNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 8),
            locationNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: -8),
            locationNameLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    private func layoutUI() {
        addSubViews()
        setupContainerViewConstraints()
        setupPlaceImageViewConstraints()
        setupPlaceNameLabelConstraints()
        setupLocationNameLabelConstraints()
    }
    
    func configure(model: TourismPlaceViewModel) {
        placeNameLabel.text = model.title
        locationNameLabel.text = model.cityName
        if model.imageUrls.count > 0 {
            placeImageView.load(url: model.imageUrls[0])
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        placeImageView.image = nil
    }
}
