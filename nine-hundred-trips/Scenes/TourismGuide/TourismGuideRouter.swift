//
//  TourismGuideRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum TourismGuideDestination {
    case details(viewModel: TourismPlaceViewModel)
    case suggestTourismPlace(categories: [PlaceCategoryViewModel])
}

class TourismGuideRouter: TourismGuideRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = TourismGuideViewController()
        view.tabBarItem = UITabBarItem(title: "tourismGuide".localized(), image: UIImage(named: "ic_tourism_guide"), selectedImage: UIImage(named: "ic_tourism_guide"))
        view.tabBarItem.setTitleTextAttributes([ NSAttributedString.Key.font: DinNextFont.regular.getFont(ofSize: 13)], for:UIControl.State.normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = TourismGuideInteractor()
        let router = TourismGuideRouter()
        let presenter = TourismGuidePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        let navigationController = UINavigationController(rootViewController: view)
        return navigationController
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }

    func navigate(to destination: TourismGuideDestination) {
        switch destination {
        case .details(let viewModel):
            viewController?.navigationController?.pushViewController(TourismGuideDetailsRouter.createModule(tourismPlaceViewModel: viewModel), animated: true)
        case .suggestTourismPlace(let categories):
            viewController?.navigationController?.pushViewController(SuggestTourismPlaceRouter.createModule(tourismCategories: categories), animated: true)
        }
    }
}
