//
//  TourismGuidePresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TourismGuidePresenter: TourismGuidePresenterProtocol, TourismGuideInteractorOutputProtocol {
    
    weak var view: TourismGuideViewProtocol?
    private let interactor: TourismGuideInteractorInputProtocol
    private let router: TourismGuideRouterProtocol
    
    private var placesCategoriesViewModels = [PlaceCategoryViewModel]()
    private var tourismPlacesViewModels = [TourismPlaceViewModel]()
    private var cities: [CityViewModel] = []
    var placesCategoriesNumberOfRows: Int {
        return placesCategoriesViewModels.count
    }
    var tourismPlacesNumberOfRows: Int {
        return tourismPlacesViewModels.count
    }
    private var selectedCategoryId = 0
    private var selectedCityId = 0
    private var searchText = ""
    private var totalPages = 1
    private var currentPage = 1
    private var isLoading = false
    
    init(view: TourismGuideViewProtocol, interactor: TourismGuideInteractorInputProtocol, router: TourismGuideRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        view?.setEmptyTextToCollectionView(message: "")
        currentPage = 1
        selectedCategoryId = 0
        selectedCityId = 0
        interactor.getPlacesCategories()
        interactor.getCities(countryId: UserDefaultsHelper.countryId)
        interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
    }
    
    func filterTourismPlaces(cityId: Int) {
        currentPage = 1
        selectedCityId = cityId
        tourismPlacesViewModels.removeAll()
        view?.reloadPlacesCollectionView()
        view?.showActivityIndicator()
        interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
    }
    
    func getSearchResult(searchName: String) {
        currentPage = 1
        view?.showActivityIndicator()
        tourismPlacesViewModels.removeAll()
        view?.reloadPlacesCollectionView()
        searchText = searchName
        interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
    }
   
    func didFetchPlacesCategories(with result: Result<[PlaceCategory]>) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        switch result {
        case .success(let result):
            view?.changeCollectionViewsUserInteraction(isUserInteractionEnabled: true)
            placesCategoriesViewModels = result.map { PlaceCategoryViewModel(placeCategory: $0) }
            view?.reloadCategoriesCollectionView()
            view?.setCategoryNameLabel(with: "-----")
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCities(with result: Result<[City]>) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        switch result {
        case .success(var result):
            let noFilter = City()
            noFilter.id = 0
            noFilter.name = "noFilter".localized()
            result.insert(noFilter, at: 0)
            cities = result.map{ CityViewModel(city: $0) }
            view?.setCities(cities: cities)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchTourismPlaces(with result: Result<[TourismPlace]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        view?.setIsPaginating(isPaginating: false)
        switch result {
        case .success(let result):
            view?.changeCollectionViewsUserInteraction(isUserInteractionEnabled: true)
            self.totalPages = totalPages
            tourismPlacesViewModels.append(contentsOf: result.map { TourismPlaceViewModel(tourismPlace: $0) } )
            isLoading = false
            if result.count > 0 {
                view?.reloadPlacesCollectionView()
            } else {
                view?.setEmptyTextToCollectionView(message: "noTourismPlaces".localized())
            }
        case .failure(let error):
            isLoading = false
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didDownloadTourismGuideFile(with result: Result<URL?>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let url):
            view?.showFile(at: url)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<PlaceCategoryViewModel>, at indexPath: IndexPath) {
        let placeCategoryModel = placesCategoriesViewModels[indexPath.row]
        cell.configure(model: placeCategoryModel)
    }
    
    func configure(cell: AnyConfigurableCell<TourismPlaceViewModel>, at indexPath: IndexPath) {
        guard !isLoading else { return }
        cell.configure(model: tourismPlacesViewModels[indexPath.row])
    }
    
    func categoriesCollectionViewDidSelectItem(at indexPath: IndexPath) {
        view?.showActivityIndicator()
        currentPage = 1
        view?.setEmptyTextToCollectionView(message: "")
        tourismPlacesViewModels.removeAll()
        view?.reloadPlacesCollectionView()
        view?.setCategoryNameLabel(with: placesCategoriesViewModels[indexPath.row].name)
        selectedCategoryId = placesCategoriesViewModels[indexPath.row].id
        interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
    }
    
    func placesCollectionViewDidSelectItem(at indexPath: IndexPath) {
        router.navigate(to: .details(viewModel: tourismPlacesViewModels[indexPath.row]))
    }
    
    func navigateToSuggestTourismPlace() {
        guard UserDefaultsHelper.isLoggedIn else {
            router.showAlert(with: "loginFirst".localized())
            return
        }
        router.navigate(to: .suggestTourismPlace(categories: placesCategoriesViewModels))
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            view?.showActivityIndicator()
            interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
        }
    }
    
    func downloadTourismGuideFile() {
        view?.showActivityIndicator()
        interactor.downloadTourismGuideFile()
    }
    
    func pullToRefresh() {
        isLoading = true
        currentPage = 1
        view?.changeCollectionViewsUserInteraction(isUserInteractionEnabled: false)
        tourismPlacesViewModels = []
        view?.setEmptyTextToCollectionView(message: "")
        interactor.getTourismPlaces(categoryId: selectedCategoryId, cityId: selectedCityId, searchText: searchText, page: currentPage)
    }
}
