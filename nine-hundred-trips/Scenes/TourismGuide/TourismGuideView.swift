//
//  TourismGuideView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismGuideView: UIView {
    
    let refreshControl = UIRefreshControl()
    private var placesCollectionViewHeightAnchor: NSLayoutConstraint!
    
    private lazy var containerSearchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        return view
    }()
    
    private lazy var searchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 243/255, green: 126/255, blue: 72/255, alpha: 1)
        view.layer.cornerRadius = 21.5
        return view
    }()
    
    lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_filter"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    lazy var filterTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor.clear
        return textField
    }()
    
    lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = 21.5
        textField.attributedPlaceholder = NSAttributedString(string: "searchAboutPlace".localized(), attributes: [
            .font: DinNextFont.regular.getFont(ofSize: 15),
            .foregroundColor: UIColor.white.withAlphaComponent(0.45)
        ])
        textField.returnKeyType = .search
        textField.textColor = .white
        return textField
    }()
    
    lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        scrolView.showsHorizontalScrollIndicator = false
        scrolView.showsVerticalScrollIndicator = false
        scrolView.refreshControl = refreshControl
        return scrolView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var topContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    lazy var suggestTouristPlaceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("suggestTourismPlace".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .mediumTurquoise
        button.layer.cornerRadius = 25
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 15)
        return button
    }()
    
    private lazy var downloadGuideView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var downloadButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "pdf")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitle("downloadTourismGuide".localized(), for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 15)
        button.tag = -1
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets.left = 8
        return button
    }()
    
    lazy var categoriesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(CategoryCell.self, forCellWithReuseIdentifier: CategoryCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    
    private lazy var firstHorizontalLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    lazy var categoryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textColor = .textColor
        label.text = "-----"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var secondHorizontalLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    lazy var placesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(PlaceCell.self, forCellWithReuseIdentifier: PlaceCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = false
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerSearchView)
        containerSearchView.addSubview(searchView)
        searchView.addSubview(filterButton)
        searchView.addSubview(filterTextField)
        searchView.addSubview(searchTextField)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(downloadGuideView)
        downloadGuideView.addSubview(downloadButton)
        containerView.addSubview(topContainerView)
        topContainerView.addSubview(suggestTouristPlaceButton)
        containerView.addSubview(categoriesCollectionView)
        containerView.addSubview(firstHorizontalLineView)
        containerView.addSubview(categoryNameLabel)
        containerView.addSubview(secondHorizontalLineView)
        containerView.addSubview(placesCollectionView)
    }
    
    private func setupContainerSearchViewConstraints() {
        NSLayoutConstraint.activate([
            containerSearchView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            containerSearchView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerSearchView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    private func setupSearchViewConstraints() {
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalTo: containerSearchView.topAnchor),
            searchView.leadingAnchor.constraint(equalTo: containerSearchView.leadingAnchor, constant: 16),
            searchView.trailingAnchor.constraint(equalTo: containerSearchView.trailingAnchor, constant: -16),
            searchView.bottomAnchor.constraint(equalTo: containerSearchView.bottomAnchor, constant: -12)
        ])
    }
    
    private func setupSearchLabelConstraints() {
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: searchView.topAnchor),
            searchTextField.bottomAnchor.constraint(equalTo: searchView.bottomAnchor),
            searchTextField.trailingAnchor.constraint(equalTo: filterButton.leadingAnchor, constant: 0),
            searchTextField.leadingAnchor.constraint(equalTo: searchView.leadingAnchor, constant: 18),
            searchTextField.heightAnchor.constraint(equalToConstant: 43)
        ])
    }
    
    private func setupFilterButtonConstraints() {
        NSLayoutConstraint.activate([
            filterButton.trailingAnchor.constraint(equalTo: searchView.trailingAnchor),
            filterButton.topAnchor.constraint(equalTo: searchView.topAnchor),
            filterButton.bottomAnchor.constraint(equalTo: searchView.bottomAnchor),
            filterButton.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupFilterTextFeildConstraints() {
        NSLayoutConstraint.activate([
            filterTextField.trailingAnchor.constraint(equalTo: searchView.trailingAnchor),
            filterTextField.topAnchor.constraint(equalTo: searchView.topAnchor),
            filterTextField.bottomAnchor.constraint(equalTo: searchView.bottomAnchor),
            filterTextField.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: containerSearchView.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupTopContainerViewConstraints() {
        NSLayoutConstraint.activate([
            topContainerView.topAnchor.constraint(equalTo: containerView.topAnchor),
            topContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            topContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            topContainerView.heightAnchor.constraint(equalToConstant: 70)
        ])
    }
    
    private func setupSuggestTouristPlaceButtonConstraints() {
        NSLayoutConstraint.activate([
            suggestTouristPlaceButton.topAnchor.constraint(equalTo: topContainerView.topAnchor, constant: 10),
            suggestTouristPlaceButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            suggestTouristPlaceButton.widthAnchor.constraint(equalToConstant: 192),
            suggestTouristPlaceButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupDownloadGuideViewConstraints() {
        NSLayoutConstraint.activate([
            downloadGuideView.topAnchor.constraint(equalTo: topContainerView.bottomAnchor, constant: 15),
            downloadGuideView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            downloadGuideView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            downloadGuideView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupDownloadButtonViewConstraints() {
        NSLayoutConstraint.activate([
            downloadButton.leadingAnchor.constraint(equalTo: downloadGuideView.leadingAnchor, constant: 15),
            downloadButton.trailingAnchor.constraint(equalTo: downloadGuideView.trailingAnchor, constant: 0),
            downloadButton.centerYAnchor.constraint(equalTo: downloadGuideView.centerYAnchor)
        ])
    }
    
    private func setupCategoriesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            categoriesCollectionView.topAnchor.constraint(equalTo: downloadGuideView.bottomAnchor, constant: 10),
            categoriesCollectionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 5),
            categoriesCollectionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            categoriesCollectionView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupFirstHorizontalLineViewConstraints() {
        NSLayoutConstraint.activate([
            firstHorizontalLineView.topAnchor.constraint(equalTo: categoriesCollectionView.bottomAnchor, constant: 20),
            firstHorizontalLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            firstHorizontalLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            firstHorizontalLineView.heightAnchor.constraint(equalToConstant: 2)
        ])
    }
    
    private func setupCategoryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            categoryNameLabel.topAnchor.constraint(equalTo: firstHorizontalLineView.bottomAnchor, constant: 5),
            categoryNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            categoryNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupSecondHorizontalLineViewConstraints() {
        NSLayoutConstraint.activate([
            secondHorizontalLineView.topAnchor.constraint(equalTo: categoryNameLabel.bottomAnchor, constant: 10),
            secondHorizontalLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            secondHorizontalLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            secondHorizontalLineView.heightAnchor.constraint(equalToConstant: 2)
        ])
    }
    
    private func setupPlaceCollectionViewConstraints() {
        placesCollectionViewHeightAnchor = placesCollectionView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            placesCollectionView.topAnchor.constraint(equalTo: secondHorizontalLineView.bottomAnchor, constant: 15),
            placesCollectionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            placesCollectionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            placesCollectionView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15),
            placesCollectionViewHeightAnchor
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupContainerSearchViewConstraints()
        setupSearchViewConstraints()
        setupFilterButtonConstraints()
        setupFilterTextFeildConstraints()
        setupSearchLabelConstraints()
        setupTopContainerViewConstraints()
        setupSuggestTouristPlaceButtonConstraints()
        setupDownloadGuideViewConstraints()
        setupDownloadButtonViewConstraints()
        setupCategoriesCollectionViewConstraints()
        setupFirstHorizontalLineViewConstraints()
        setupCategoryNameLabelConstraints()
        setupSecondHorizontalLineViewConstraints()
        setupPlaceCollectionViewConstraints()
    }
    
    func setCollectionViewHeight(_ height: CGFloat) {
        placesCollectionViewHeightAnchor.constant = height
    }
    
}
