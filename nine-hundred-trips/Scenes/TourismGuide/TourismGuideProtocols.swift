//
//  TourismGuideProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol TourismGuideRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(to destination: TourismGuideDestination)
}

protocol TourismGuidePresenterProtocol: class {
    var view: TourismGuideViewProtocol? { get set }
    func viewDidLoad()
    var placesCategoriesNumberOfRows: Int { get }
    var tourismPlacesNumberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<PlaceCategoryViewModel>, at indexPath: IndexPath)
    func configure(cell: AnyConfigurableCell<TourismPlaceViewModel>, at indexPath: IndexPath)
    func categoriesCollectionViewDidSelectItem(at indexPath: IndexPath)
    func placesCollectionViewDidSelectItem(at indexPath: IndexPath)
    func navigateToSuggestTourismPlace()
    func filterTourismPlaces(cityId: Int)
    func getSearchResult(searchName: String)
    func loadNextPage()
    func downloadTourismGuideFile()
    func pullToRefresh()
}

protocol TourismGuideInteractorInputProtocol: class {
    var presenter: TourismGuideInteractorOutputProtocol? { get set }
    func getPlacesCategories()
    func getTourismPlaces(categoryId: Int, cityId: Int, searchText: String, page: Int)
    func getCities(countryId: Int)
    func downloadTourismGuideFile()
}

protocol TourismGuideInteractorOutputProtocol: class {
    func didFetchPlacesCategories(with result: Result<[PlaceCategory]>)
    func didFetchTourismPlaces(with result: Result<[TourismPlace]>, totalPages: Int)
    func didFetchCities(with result: Result<[City]>)
    func didDownloadTourismGuideFile(with result: Result<URL?>)
}

protocol TourismGuideViewProtocol: class {
    var presenter: TourismGuidePresenterProtocol! { get set }
    func reloadCategoriesCollectionView()
    func showActivityIndicator()
    func hideActivityIndicator()
    func endRefreshing()
    func setIsPaginating(isPaginating: Bool)
    func changeCollectionViewsUserInteraction(isUserInteractionEnabled: Bool)
    func setEmptyTextToCollectionView(message: String)
    func setCategoryNameLabel(with categoryName: String)
    func setCities(cities: [CityViewModel])
    func reloadPlacesCollectionView()
    func showFile(at path: URL?)
}
