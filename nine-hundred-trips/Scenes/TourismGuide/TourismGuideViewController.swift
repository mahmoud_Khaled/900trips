//
//  TourismGuideViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismGuideViewController: BaseViewController, TourismGuideViewProtocol {
    
    private let mainView = TourismGuideView()
    var presenter: TourismGuidePresenterProtocol!
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    private var isPaginating: Bool = false
    private var cityId = 0
    
    override func loadView() {
        super.loadView()
        view = mainView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        navigationItem.title = "tourismGuide".localized()
        setupNavigationBarStyle()
        presenter?.viewDidLoad()
        mainView.scrollView.delegate = self
        pickerViewDidSelectItemAction()
        addTargets()
    }
    
    private func setupTableView() {
        mainView.categoriesCollectionView.delegate = self
        mainView.categoriesCollectionView.dataSource = self
        mainView.placesCollectionView.delegate = self
        mainView.placesCollectionView.dataSource = self
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func addTargets() {
        mainView.filterTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(filterTextFieldDoneButtonTapped))
        mainView.suggestTouristPlaceButton.addTarget(self, action: #selector(suggestTorismPlaceButtonTapped), for: .touchUpInside)
        mainView.searchTextField.addTarget(self, action: #selector(searchTextFieldDidChange(_:)), for: .editingChanged)
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        mainView.downloadButton.addTarget(self, action: #selector(downloadTourismFileButtonTapped), for: .touchUpInside)
    }
    
    @objc private func suggestTorismPlaceButtonTapped() {
        presenter.navigateToSuggestTourismPlace()
    }
    
    @objc private func searchTextFieldDidChange(_ textField: UITextField) {
        pendingRequestWorkItem?.cancel()
        let requestWorkItem = DispatchWorkItem { [weak self] in
            guard let self = self else { return }
            self.presenter.getSearchResult(searchName: textField.text!)
        }
        
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500),
                                      execute: requestWorkItem)
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }

    @objc private func downloadTourismFileButtonTapped() {
        presenter.downloadTourismGuideFile()
    }
    
    private func pickerViewDidSelectItemAction() {
        mainView.filterTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.cityId = item.id
        }
    }
    
    @objc private func filterTextFieldDoneButtonTapped() {
        presenter.filterTourismPlaces(cityId: cityId)
    }
    
    func setCategoryNameLabel(with categoryName: String) {
        mainView.categoryNameLabel.text = categoryName
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    func reloadCategoriesCollectionView() {
        mainView.categoriesCollectionView.reloadData()
    }
    
    func setEmptyTextToCollectionView(message: String) {
        mainView.placesCollectionView.emptyMessage(message: message)
    }
    
    func setCities(cities: [CityViewModel]) {
        mainView.filterTextField.items = cities
    }
    
    func setIsPaginating(isPaginating: Bool) {
        self.isPaginating = isPaginating
    }
    
    func changeCollectionViewsUserInteraction(isUserInteractionEnabled: Bool) {
        mainView.categoriesCollectionView.isUserInteractionEnabled = isUserInteractionEnabled
        mainView.placesCollectionView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    func reloadPlacesCollectionView() {
        mainView.placesCollectionView.reloadData()
        mainView.setCollectionViewHeight(mainView.placesCollectionView.collectionViewLayout.collectionViewContentSize.height)
    }
    
    func showFile(at path: URL?) {
        let viewer = UIDocumentInteractionController()
        viewer.url = path
        viewer.delegate = self
        viewer.presentPreview(animated: true)
    }
}

extension TourismGuideViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainView.categoriesCollectionView {
            return presenter.placesCategoriesNumberOfRows
        } else {
            return presenter.tourismPlacesNumberOfRows
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == mainView.categoriesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.className, for: indexPath) as? CategoryCell  else { return UICollectionViewCell() }
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceCell.className, for: indexPath) as? PlaceCell  else { return UICollectionViewCell() }
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == mainView.categoriesCollectionView {
            return CGSize(width: 50, height: 50)
        } else {
            let width = (collectionView.frame.width / 2) - 10
            let height = CGFloat(200)
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == mainView.categoriesCollectionView {
            presenter.categoriesCollectionViewDidSelectItem(at: indexPath)
        } else {
            presenter.placesCollectionViewDidSelectItem(at: indexPath)
        }
    }
}

extension TourismGuideViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height && !isPaginating {
            isPaginating = true
            presenter.loadNextPage()
        }
    }
}

extension TourismGuideViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.navigationController ?? self
    }
}

