//
//  NotificationsProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol NotificationsRouterProtocol: class {
    func showAlert(with message: String)
    func showDeleteAlert(okAction: @escaping () -> ())
}

protocol NotificationsPresenterProtocol: class {
    var view: NotificationsViewProtocol? { get set }
    var numberOfRows: Int { get }
    func viewDidLoad()
    func loadNextPage()
    func pullToRefresh()
    func deleteNotificationButtonTapped(indexPath: IndexPath)
    func deleteAllNotificationsButtonTapped()
    func configure(cell: AnyConfigurableCell<NotificationViewModel>, at indexPath: IndexPath)
}

protocol NotificationsInteractorInputProtocol: class {
    var presenter: NotificationsInteractorOutputProtocol? { get set }
    func getNotifications(page: Int)
    func deleteNotification(notificationId: Int, index: Int)
    func deleteAllNotifications()
}

protocol NotificationsInteractorOutputProtocol: class {
    func didFetchNotifications(with result: Result<[Notification]>, totalPages: Int)
    func didDeleteNotification(with result: Result<Data>, index: Int)
    func didDeleteAllNotifications(with result: Result<Data>)
}

protocol NotificationsViewProtocol: class {
    var presenter: NotificationsPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func endRefreshing()
    func reloadTableView()
    func setupNavigationBarButtons(isHidden: Bool)
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool)
    func setEmptyTextToTableView(message: String)
}
