//
//  NotificationsViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, NotificationsViewProtocol {
    
    private var mainView = NotificationsView()
    var presenter: NotificationsPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        addTargets()
        setupTableView()
        presenter.viewDidLoad()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        title = "notification".localized()
    }
    
    func setupNavigationBarButtons(isHidden: Bool) {
        let deleteButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_delete_all")?.with(size: .init(width: 20, height: 25)))
            .height(25)
            .width(20)
            .action(target: self, selector: #selector(deleteAllNotificationsButtonTapped))
            .build()
        if(isHidden) {
            navigationItem.rightBarButtonItems = nil
        } else {
            navigationItem.rightBarButtonItems = [deleteButton]
        }
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    private func addTargets() {
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
    }
    
    func showActivityIndicator() {
         view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool) {
        mainView.tableView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    func setEmptyTextToTableView(message: String) {
        mainView.tableView.emptyMessage(message: message)
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
    @objc private func deleteAllNotificationsButtonTapped() {
        presenter.deleteAllNotificationsButtonTapped()
    }
}


extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationsCell.className) as! NotificationsCell
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: nil) { [weak self] _, _, completion in
            guard let self = self else { return }
            self.presenter.deleteNotificationButtonTapped(indexPath: indexPath)
            completion(true)
        }
        deleteAction.image = UIImage(named: "ic_delete_all")?.with(size: CGSize(width: 20, height: 25))
        deleteAction.backgroundColor = .red
        let config =  UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }
    
}
