//
//  NotificationsCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell, ConfigurableCell {
    
    private lazy var notificationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Notificationfghdfghdfghdfghdfghfghf"
        label.textColor = UIColor.darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private lazy var notificationImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "ic_notification_circle")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private lazy var horizontalLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
  
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
    }
    
    private func addSubviews() {
        addSubview(notificationImage)
        addSubview(notificationLabel)
        addSubview(horizontalLine)
    }
    
    private func setupNotificationImageConstraints() {
        NSLayoutConstraint.activate([
            notificationImage.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            notificationImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            notificationImage.heightAnchor.constraint(equalToConstant: 35),
            notificationImage.widthAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    private func setupNotificationLabelConstraints() {
        NSLayoutConstraint.activate([
            notificationLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            notificationLabel.leadingAnchor.constraint(equalTo: notificationImage.trailingAnchor, constant: 15),
            notificationLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            notificationLabel.bottomAnchor.constraint(equalTo: horizontalLine.topAnchor, constant: -20),
        ])
    }
    
    private func setupHorizontalLineViewConstraints() {
        NSLayoutConstraint.activate([
            horizontalLine.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            horizontalLine.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            horizontalLine.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            horizontalLine.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(model: NotificationViewModel) {
        notificationLabel.text = model.message
    }
    
    private func layoutUI() {
        addSubviews()
        setupNotificationImageConstraints()
        setupNotificationLabelConstraints()
        setupHorizontalLineViewConstraints()
    }
    

   
}
