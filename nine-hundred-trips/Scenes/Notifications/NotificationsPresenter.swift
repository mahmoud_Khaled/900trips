//
//  NotificationsPresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class NotificationsPresenter: NotificationsPresenterProtocol, NotificationsInteractorOutputProtocol {
  
    weak var view: NotificationsViewProtocol?
    private let interactor: NotificationsInteractorInputProtocol
    private let router: NotificationsRouterProtocol
    
    private var notifications = [NotificationViewModel]()
    private var totalPages = 1
    private var currentPage = 1
    private var isLoading = false
    
    var numberOfRows: Int {
        return notifications.count
    }
    
    init(view: NotificationsViewProtocol, interactor: NotificationsInteractorInputProtocol, router: NotificationsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        isLoading = true
        view?.showActivityIndicator()
        interactor.getNotifications(page: currentPage)
    }
    
    func deleteNotificationButtonTapped(indexPath: IndexPath) {
        view?.showActivityIndicator()
        interactor.deleteNotification(notificationId: notifications[indexPath.row].id, index: indexPath.row)
    }
    
    func deleteAllNotificationsButtonTapped() {
        router.showDeleteAlert {
            guard self.notifications.count > 0 else { return }
            self.view?.showActivityIndicator()
            self.interactor.deleteAllNotifications()
        }
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            view?.showActivityIndicator()
            interactor.getNotifications(page: currentPage)
        }
    }
    
    func pullToRefresh() {
        view?.setEmptyTextToTableView(message: "")
        isLoading = true
        currentPage = 1
        notifications = []
        view?.changeTableViewUserInteraction(isUserInteractionEnabled: false)
        interactor.getNotifications(page: currentPage)
    }
    
    func didFetchNotifications(with result: Result<[Notification]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        switch result {
        case .success(let result):
            isLoading = false
            view?.changeTableViewUserInteraction(isUserInteractionEnabled: true)
            self.totalPages = totalPages
            notifications.append(contentsOf: result.map {
                NotificationViewModel(notification: $0)
            })
            view?.reloadTableView()
            if(notifications.count == 0) {
                view?.setEmptyTextToTableView(message: "noNotifications".localized())
            } else {
                view?.setupNavigationBarButtons(isHidden: false)
            }
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didDeleteNotification(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            notifications.remove(at: index)
            view?.reloadTableView()
            if notifications.count == 0 {
                view?.setEmptyTextToTableView(message: "noNotifications".localized())
                view?.setupNavigationBarButtons(isHidden: true)
            }
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didDeleteAllNotifications(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            notifications = []
            view?.reloadTableView()
            view?.setEmptyTextToTableView(message: "noNotifications".localized())
            view?.setupNavigationBarButtons(isHidden: true)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<NotificationViewModel>, at indexPath: IndexPath) {
        guard !isLoading else { return }
        let notificatonViewModel = notifications[indexPath.row]
        cell.configure(model: notificatonViewModel)
    }
}
