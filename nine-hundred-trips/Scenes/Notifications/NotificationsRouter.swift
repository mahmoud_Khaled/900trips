//
//  NotificationsRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NotificationsRouter: NotificationsRouterProtocol {
  
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = NotificationsViewController()
        let interactor = NotificationsInteractor()
        let router = NotificationsRouter()
        let presenter = NotificationsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return  view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showDeleteAlert(okAction: @escaping () -> ()) {
        AlertBuilder(title: "deleteAll".localized(), message: "confirmDeleteAll".localized(), preferredStyle: .alert)
            .addAction(title: "cancel".localized(), style: .cancel)
            .addAction(title: "ok".localized(), style: .default, handler: okAction)
        .build()
        .show()
    }
}
