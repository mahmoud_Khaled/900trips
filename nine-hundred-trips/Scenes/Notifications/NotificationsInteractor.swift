//
//  NotificationsInteractor.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NotificationsInteractor: NotificationsInteractorInputProtocol {
    
    weak var presenter: NotificationsInteractorOutputProtocol?
    private let notificationWorker = NotificationWorker()
    
    func getNotifications(page: Int) {
        notificationWorker.getAllNotifications(page: page) { [weak self] result, pages  in
            guard let self = self else { return }
            self.presenter?.didFetchNotifications(with: result, totalPages: pages)
        }
    }
    
    func deleteNotification(notificationId: Int, index: Int) {
        notificationWorker.deleteNotification(notificationId: notificationId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didDeleteNotification(with: result, index: index)
        }
    }
    
    func deleteAllNotifications() {
        notificationWorker.deleteAllNotifications() { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didDeleteAllNotifications(with: result)
        }
    }
}
