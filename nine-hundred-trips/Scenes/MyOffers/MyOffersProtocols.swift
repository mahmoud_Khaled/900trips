//
//  MyOffersProtocols.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol MyOffersRouterProtocol: class {
    func showAlert(with message: String)
    func showActionSheet(confirmDeleteAction: @escaping () -> (), markOfferAsSpecial: @escaping ()->())
    func navigateTo(offerId: Int)
}

protocol MyOffersPresenterProtocol: class {
    var view: MyOffersViewProtocol? { get set }
    var numberOfRows: Int { get }
    func viewDidLoad()
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath)
    func loadNextPage()
    func pullToRefresh()
    func menuButtonTapped(at indexPath: IndexPath)
    func didSelect(at indexPath: IndexPath)
}

protocol MyOffersInteractorInputProtocol: class {
    var presenter: MyOffersInteractorOutputProtocol? { get set }
    func getMyOffers(page: Int)
    func deleteOffer(offerId: Int, index: Int)
    func markOfferAsSpecial(offerId: Int)
}

protocol MyOffersInteractorOutputProtocol: class {
    func didFetchMyOffers(with result: Result<[Offer]>, totalPages: Int)
    func didDeleteOffer(with result: Result<Data>, index: Int)
    func didMarkOffer(with result: Result<Data>)
}

protocol MyOffersViewProtocol: class {
    var presenter: MyOffersPresenterProtocol! { get set }
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func reloadData()
    func setEmptyTextToTableView(message: String)
    func endRefreshing()
    func showSuccessView()
}
