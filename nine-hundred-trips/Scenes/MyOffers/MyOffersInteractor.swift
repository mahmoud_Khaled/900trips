//
//  MyOffersInteractor.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class MyOffersInteractor: MyOffersInteractorInputProtocol {
    
    weak var presenter: MyOffersInteractorOutputProtocol?
    private let offersWorker = OffersWorker()
    
    func getMyOffers(page: Int) {
        
        offersWorker.getMyOffers(page: page) { [weak self] result, totalPages in
            guard let self = self else { return }
            self.presenter?.didFetchMyOffers(with: result, totalPages: totalPages)
        }
    }
    
    func deleteOffer(offerId: Int, index: Int) {
        offersWorker.deleteOffer(offerId: offerId) { [weak self] result  in
            guard let self = self else { return }
            self.presenter?.didDeleteOffer(with: result, index: index)
        }
    }
    
    func markOfferAsSpecial(offerId: Int) {
        offersWorker.markOfferAsSpecial(offerId: offerId) { [weak self]  result in
            guard let self = self else { return }
            self.presenter?.didMarkOffer(with: result)
        }
    }
    
    
}
