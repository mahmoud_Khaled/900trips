//
//  MyOffersRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class MyOffersRouter: MyOffersRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = MyOffersViewController()
        let interactor = MyOffersInteractor()
        let router = MyOffersRouter()
        let presenter = MyOffersPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showActionSheet(confirmDeleteAction: @escaping () -> (), markOfferAsSpecial: @escaping () -> ()) {
        
        AlertBuilder(title: nil, message: nil, preferredStyle: .actionSheet)
            .addAction(title: "markOfferAsSpecial".localized(), style: .default) {
                markOfferAsSpecial()
            }
            .addAction(title: "delete".localized(), style: .destructive) { [weak self] in
                guard let self = self else { return }
                self.showDeletePopupView(with: confirmDeleteAction)
            }
            .addAction(title: "cancel".localized(), style: .cancel)
            .build()
            .show()
    }
    
    private func showDeletePopupView(with action: @escaping () -> ()) {
        let popupView = DeleteView()
        viewController?.show(popupView: popupView, on: viewController?.tabBarController?.view, confirmAction: {
            popupView.removeFromSuperview()
            action()
        }, cancelAction: {
            popupView.fadeOut(duration: 0.25) {
                popupView.removeFromSuperview()
            }
        })
    }
    
    func navigateTo(offerId: Int) {
       viewController?.navigationController?.pushViewController(OfferDetailsRouter.createModule(isMyOffer: true, indexPath: nil, offerId: offerId, delegate: nil), animated: true)
    }
    
}
