//
//  MyOffersPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class MyOffersPresenter: MyOffersPresenterProtocol, MyOffersInteractorOutputProtocol {
    
    weak var view: MyOffersViewProtocol?
    private let interactor: MyOffersInteractorInputProtocol
    private let router: MyOffersRouterProtocol
    private var offers: [OfferViewModel] = []
    
    var numberOfRows: Int {
        return offers.count
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    private var currentPage = 1
    private var totalPages = 1
    private var  isPullToRefresh = false
    
    init(view: MyOffersViewProtocol, interactor: MyOffersInteractorInputProtocol, router: MyOffersRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getMyOffers(page: currentPage)
    }
    
    func didFetchMyOffers(with result: Result<[Offer]>, totalPages: Int) {
        
        view?.hideActivityIndicator()
        self.totalPages = totalPages
        view?.endRefreshing()
        
        switch result {
        case .success(let value):
            
            if isPullToRefresh {
                offers = []
                isPullToRefresh = false
            }

            
            offers.append(contentsOf: value.map({ OfferViewModel(offer: $0, dateFormatter: dateFormatter, stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}") }))
            
            offers.forEach({ $0.isMyOffer = true })
            
            if offers.count == 0 {
                 view?.setEmptyTextToTableView(message: "noOffers".localized())
            } else {
               view?.reloadData()
            }

        case .failure(let error):
            isPullToRefresh = false
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath) {
        cell.configure(model: offers[indexPath.row])
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            view?.showActivityIndicator(isUserInteractionEnabled: true)
            interactor.getMyOffers(page: currentPage)
        }

    }
    
    func pullToRefresh() {
        totalPages = 1
        currentPage = 1
        isPullToRefresh = true
        interactor.getMyOffers(page: currentPage)
    }
    
    func menuButtonTapped(at indexPath: IndexPath) {
        let id = offers[indexPath.row].id
        
        router.showActionSheet(confirmDeleteAction: { [weak self ] in
            guard let self = self else { return }
            self.deleteAction(offerId: id, at: indexPath)
            
            }, markOfferAsSpecial: { [weak self] in
            guard let self = self else { return }
            self.markOfferAsSpecial(offerId: id)
        })
    }
    
    private func deleteAction(offerId: Int, at indexPath: IndexPath) {
        self.view?.showActivityIndicator(isUserInteractionEnabled: true)
        self.interactor.deleteOffer(offerId: offerId, index: indexPath.row)
    }
    
    func didDeleteOffer(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            offers.remove(at: index)
            if offers.count == 0 {
                view?.setEmptyTextToTableView(message: "noOffers".localized())
            }
            view?.reloadData()
        case.failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    private func markOfferAsSpecial(offerId: Int) {
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        interactor.markOfferAsSpecial(offerId: offerId)
    }
    
    func didMarkOffer(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didSelect(at indexPath: IndexPath) {
        router.navigateTo(offerId: offers[indexPath.row].id)
    }
}
