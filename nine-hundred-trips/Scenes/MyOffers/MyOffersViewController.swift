//
//  MyOffersViewController.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class MyOffersViewController: BaseViewController, MyOffersViewProtocol {
    
    private var mainView = MyOffersView()
    var presenter: MyOffersPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "myOffers".localized()
        setupNavigationBarStyle()
        setupTableView()
        presenter?.viewDidLoad()
        addTarget()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func addTarget() {
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func reloadData() {
        mainView.tableView.reloadData()
    }
    
    func setEmptyTextToTableView(message: String) {
        mainView.tableView.emptyMessage(message: message)
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
    func showSuccessView() {
        
        show(popupView: SuccessView(), model: "offerHasBeenMarkedSpecial".localized(), on: tabBarController?.view, for: 2) {
            
        }
    }
    
}

extension MyOffersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.className, for: indexPath) as? OfferCell else { return UITableViewCell() }
        
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        
        cell.menuButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.menuButtonTapped(at: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelect(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
}
