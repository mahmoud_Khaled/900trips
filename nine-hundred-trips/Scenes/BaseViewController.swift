//
//  BaseViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBackButtonTitle()
    }
    
}
