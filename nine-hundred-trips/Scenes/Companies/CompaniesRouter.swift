//
//  CompaniesRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompaniesRouter: CompaniesRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = CompaniesViewController()
        view.tabBarItem = UITabBarItem(title: "companies".localized(), image: UIImage(named: "ic_companies"), selectedImage: UIImage(named: "ic_companies"))
        view.tabBarItem.setTitleTextAttributes([
            .font: DinNextFont.regular.getFont(ofSize: 13)
        ], for: .normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = CompaniesInteractor()
        let router = CompaniesRouter()
        let presenter = CompaniesPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToCompanyDetails(with viewModel: UserViewModel) {
        viewController?.navigationController?.pushViewController(CompanyDetailsRouter.createModule(with: viewModel), animated: true)
    }
}
