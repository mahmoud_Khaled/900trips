//
//  CompanyCell.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompanyCell: UICollectionViewCell, ConfigurableCell {
    
    private lazy var companyLogoContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.16), alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        view.layer.cornerRadius = 45
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var companyLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 16)
        label.text = "-----"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var countryContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var countryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = UIColor.textColor.withAlphaComponent(0.6)
        label.text = "------"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 248/255, green: 246/255, blue: 246/255, alpha: 1.0)
        layer.cornerRadius = 12
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(companyLogoContainerView)
        companyLogoContainerView.addSubview(companyLogoImageView)
        addSubview(companyNameLabel)
        addSubview(countryContainerView)
        countryContainerView.addSubview(countryImageView)
        countryContainerView.addSubview(countryNameLabel)
    }
    
    private func setupCompanyLogoContainerViewConstraints() {
        NSLayoutConstraint.activate([
            companyLogoContainerView.topAnchor.constraint(equalTo: topAnchor, constant: 13),
            companyLogoContainerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            companyLogoContainerView.heightAnchor.constraint(equalToConstant: 90),
            companyLogoContainerView.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupCompanyLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            companyLogoImageView.topAnchor.constraint(equalTo: companyLogoContainerView.topAnchor, constant: 8),
            companyLogoImageView.leadingAnchor.constraint(equalTo: companyLogoContainerView.leadingAnchor, constant: 8),
            companyLogoImageView.trailingAnchor.constraint(equalTo: companyLogoContainerView.trailingAnchor, constant: -8),
            companyLogoImageView.bottomAnchor.constraint(equalTo: companyLogoContainerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupCompanyNameLabelConstraints() {
        NSLayoutConstraint.activate([
            companyNameLabel.topAnchor.constraint(equalTo: companyLogoContainerView.bottomAnchor, constant: 8),
            companyNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            companyNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
    }
    
    private func setupCountryContainerViewConstraints() {
        NSLayoutConstraint.activate([
            countryContainerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            countryContainerView.heightAnchor.constraint(equalToConstant: 20),
            countryContainerView.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 13),
            countryContainerView.widthAnchor.constraint(lessThanOrEqualToConstant: frame.width - 16)
        ])
    }
    
    private func setupCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            countryImageView.topAnchor.constraint(equalTo: countryContainerView.topAnchor),
            countryImageView.heightAnchor.constraint(equalToConstant: 12),
            countryImageView.widthAnchor.constraint(equalToConstant: 12),
            countryImageView.leadingAnchor.constraint(equalTo: countryContainerView.leadingAnchor, constant: 8)
        ])
    }
    
    private func setupCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            countryNameLabel.leadingAnchor.constraint(equalTo: countryImageView.trailingAnchor, constant: 4),
            countryNameLabel.centerYAnchor.constraint(equalTo: countryImageView.centerYAnchor, constant: -3),
            countryNameLabel.trailingAnchor.constraint(equalTo: countryContainerView.trailingAnchor, constant: -4)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupCompanyLogoContainerViewConstraints()
        setupCompanyLogoImageViewConstraints()
        setupCompanyNameLabelConstraints()
        setupCountryContainerViewConstraints()
        setupCountryImageViewConstraints()
        setupCountryNameLabelConstraints()
    }
    
    func configure(model: UserViewModel) {
        companyLogoImageView.load(url: model.imageUrl)
        companyNameLabel.text = model.username
        countryImageView.load(url: model.countryFlagUrl)
        countryNameLabel.text = model.countryName
    }
}
