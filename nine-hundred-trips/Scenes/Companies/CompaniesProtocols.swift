//
//  CompaniesProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol CompaniesRouterProtocol: class {
    func showAlert(with message: String)
    func navigateToCompanyDetails(with viewModel: UserViewModel)
}

protocol CompaniesPresenterProtocol: class {
    var view: CompaniesViewProtocol? { get set }
    var numberOfRows: Int { get }
    func searchCompanies(query: String)
    func configure(cell: AnyConfigurableCell<UserViewModel>, indexPath: IndexPath)
    func clearSearchResults()
    func loadNextPage(query: String)
    func pullToRefresh(query: String)
    func didSelectItem(at indexPath: IndexPath)
}

protocol CompaniesInteractorInputProtocol: class {
    var presenter: CompaniesInteractorOutputProtocol? { get set }
    func searchCompanies(query: String, page: Int)
}

protocol CompaniesInteractorOutputProtocol: class {
    func didFetchCompanies(with result: Result<[User]>, totalPages: Int)
}

protocol CompaniesViewProtocol: class {
    var presenter: CompaniesPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func reloadData()
    func endRefreshing()
    func resetCollectionViewOffset()
    func setEmptyMessage(message: String)
    func changeCollectionViewUserInteraction(isUserInteractionEnabled: Bool)
}
