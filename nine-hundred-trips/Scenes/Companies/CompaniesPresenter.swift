//
//  CompaniesPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CompaniesPresenter: CompaniesPresenterProtocol, CompaniesInteractorOutputProtocol {
    
    weak var view: CompaniesViewProtocol?
    private let interactor: CompaniesInteractorInputProtocol
    private let router: CompaniesRouterProtocol
    
    private var usersViewModels = [UserViewModel]()
    private var totalPages = 1
    private var currentPage = 1
    private var isLoading = false
    
    var numberOfRows: Int {
        return usersViewModels.count
    }
    
    init(view: CompaniesViewProtocol, interactor: CompaniesInteractorInputProtocol, router: CompaniesRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func searchCompanies(query: String) {
        view?.setEmptyMessage(message: "")
        view?.showActivityIndicator()
        isLoading = true
        interactor.searchCompanies(query: query.lowercased(), page: currentPage)
    }
    
    func loadNextPage(query: String) {
        if currentPage < totalPages {
            currentPage += 1
            view?.showActivityIndicator()
            isLoading = true
            interactor.searchCompanies(query: query.lowercased(), page: currentPage)
        }
    }
    
    func didFetchCompanies(with result: Result<[User]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        self.totalPages = totalPages
        switch result {
        case .success(let users):
            view?.changeCollectionViewUserInteraction(isUserInteractionEnabled: true)
            usersViewModels.append(contentsOf: users.map(UserViewModel.init))
            usersViewModels.removeAll(where: { $0.id == UserDefaultsHelper.id })
            if usersViewModels.count == 0 {
                view?.setEmptyMessage(message: "noResults".localized())
            }
            isLoading = false
            view?.reloadData()
        case .failure(let error):
            isLoading = false
            view?.resetCollectionViewOffset()
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<UserViewModel>, indexPath: IndexPath) {
        guard !isLoading else { return }
        cell.configure(model: usersViewModels[indexPath.row])
    }
    
    func clearSearchResults() {
        totalPages = 1
        currentPage = 1
        usersViewModels.removeAll()
    }
    
    func pullToRefresh(query: String) {
        totalPages = 1
        currentPage = 1
        view?.changeCollectionViewUserInteraction(isUserInteractionEnabled: false)
        usersViewModels.removeAll()
        isLoading = true
        interactor.searchCompanies(query: query.lowercased(), page: currentPage)
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        router.navigateToCompanyDetails(with: usersViewModels[indexPath.item])
    }
}
