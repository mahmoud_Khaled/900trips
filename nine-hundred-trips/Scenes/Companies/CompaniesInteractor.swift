//
//  CompaniesInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompaniesInteractor: CompaniesInteractorInputProtocol {
    
    weak var presenter: CompaniesInteractorOutputProtocol?
    
    private let userWorker = UserWorker()
    
    func searchCompanies(query: String, page: Int) {
        userWorker.searchCompanies(query: query, page: page) { [weak self] result, totalPages in
            guard let self = self else { return }
            self.presenter?.didFetchCompanies(with: result, totalPages: totalPages)
        }
    }
}
