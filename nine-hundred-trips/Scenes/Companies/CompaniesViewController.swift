//
//  CompaniesViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompaniesViewController: BaseViewController, CompaniesViewProtocol {
    
    private let mainView = CompaniesView()
    var presenter: CompaniesPresenterProtocol!
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "companies".localized()
        setupNavigationBarStyle()
        setupCollectionView()
        addTargets()
        presenter.searchCompanies(query: mainView.searchTextField.text!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupCollectionView() {
        mainView.companiesCollectionView.dataSource = self
        mainView.companiesCollectionView.delegate = self
    }
    
    private func addTargets() {
        mainView.searchTextField.addTarget(self, action: #selector(searchTextFieldDidChange(_:)), for: .editingChanged)
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func reloadData() {
        mainView.companiesCollectionView.reloadData()
    }
    
    @objc private func searchTextFieldDidChange(_ textField: UITextField) {
        pendingRequestWorkItem?.cancel()
        let requestWorkItem = DispatchWorkItem { [weak self] in
            guard let self = self else { return }
            self.presenter.clearSearchResults()
            self.presenter.searchCompanies(query: textField.text!)
        }
        
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500),
                                      execute: requestWorkItem)
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh(query: mainView.searchTextField.text!)
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    func resetCollectionViewOffset() {
        mainView.companiesCollectionView.setContentOffset(.zero, animated: true)
    }
    
    func setEmptyMessage(message: String) {
        mainView.companiesCollectionView.emptyMessage(message: message)
    }
    
    func changeCollectionViewUserInteraction(isUserInteractionEnabled: Bool) {
        mainView.companiesCollectionView.isUserInteractionEnabled = isUserInteractionEnabled
    }
}

extension CompaniesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CompanyCell.className, for: indexPath) as! CompanyCell
        presenter.configure(cell: AnyConfigurableCell(cell), indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 24, left: 24, bottom: 24, right: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 60) / 2
        let height: CGFloat = 175
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == collectionView.numberOfItems(inSection: 0) - 1 {
            presenter.loadNextPage(query: mainView.searchTextField.text!.lowercased())
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItem(at: indexPath)
    }
}
