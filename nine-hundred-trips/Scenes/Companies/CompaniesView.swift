//
//  CompaniesView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CompaniesView: UIView {
    
    private lazy var searchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        return view
    }()
    
    lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor(red: 243/255, green: 126/255, blue: 72/255, alpha: 1)
        textField.layer.cornerRadius = 21.5
        textField.attributedPlaceholder = NSAttributedString(string: "searchForACompany".localized(), attributes: [
            .font: DinNextFont.regular.getFont(ofSize: 15),
            .foregroundColor: UIColor.white.withAlphaComponent(0.45)
        ])
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.returnKeyType = .search
        textField.textColor = .white
        return textField
    }()
    
    let refreshControl = UIRefreshControl()
    
    lazy var companiesCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CompanyCell.self, forCellWithReuseIdentifier: CompanyCell.className)
        collectionView.backgroundColor = .white
        collectionView.refreshControl = refreshControl
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(searchView)
        searchView.addSubview(searchTextField)
        addSubview(companiesCollectionView)
    }
    
    private func setupSearchViewConstraints() {
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            searchView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            searchView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
        ])
    }
    
    private func setupSearchTextFieldConstraints() {
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: searchView.topAnchor, constant: 0),
            searchTextField.leadingAnchor.constraint(equalTo: searchView.leadingAnchor, constant: 16),
            searchTextField.trailingAnchor.constraint(equalTo: searchView.trailingAnchor, constant: -16),
            searchTextField.bottomAnchor.constraint(equalTo: searchView.bottomAnchor, constant: -12),
            searchTextField.heightAnchor.constraint(equalToConstant: 43)
        ])
    }
    
    private func setupCompaniesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            companiesCollectionView.topAnchor.constraint(equalTo: searchView.bottomAnchor, constant: 0),
            companiesCollectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            companiesCollectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            companiesCollectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupSearchViewConstraints()
        setupSearchTextFieldConstraints()
        setupCompaniesCollectionViewConstraints()
    }
    
}
