//
//  CountryAndCurrencyInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CountryAndCurrencyInteractor: CountryAndCurrencyInteractorInputProtocol {

    weak var presenter: CountryAndCurrencyInteractorOutputProtocol?
    private let applicationDataWorker = ApplicationDataWorker()
    
    func getCountriesAndCurrencies() {
        applicationDataWorker.getCountriesAndCurrenceies { [weak self] result in
            guard let self = self else { return }
            self.presenter?.countriesAndCurrenciesFetched(with: result)
        }
    }
}
