//
//  CurrencyCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell, ConfigurableCell {

    private lazy var currencyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_arabic")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var currencyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "الإمارات"
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        return label
    }()
    
    private lazy var selectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_success")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.07), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(currencyImageView)
        addSubview(lineView)
        addSubview(currencyNameLabel)
        addSubview(selectedImageView)
    }
    
    private func setupCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            currencyImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            currencyImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            currencyImageView.widthAnchor.constraint(equalToConstant: 25),
            currencyImageView.heightAnchor.constraint(equalToConstant: 15)
        ])
    }
    
    private func setupCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            currencyNameLabel.leadingAnchor.constraint(equalTo: currencyImageView.trailingAnchor, constant: 10),
            currencyNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSelectedImageConstraints() {
        NSLayoutConstraint.activate([
            selectedImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            selectedImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            selectedImageView.widthAnchor.constraint(equalToConstant: 25),
            selectedImageView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupCountryImageViewConstraints()
        setupCountryLabelConstraints()
        setupLineViewConstraints()
        setupSelectedImageConstraints()
    }
    
    func configure(model: CurrencyViewModel) {
        currencyImageView.load(url: model.photoUrl)
        currencyNameLabel.text = model.name
        selectedImageView.isHidden = !model.isSelected
    }

}
