//
//  CountryTableViewCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell, ConfigurableCell {

    private lazy var countryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_arabic")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "الإمارات"
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        return label
    }()
    
    private lazy var selectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_success")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.07), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(countryImageView)
        addSubview(lineView)
        addSubview(countryNameLabel)
        addSubview(selectedImageView)
    }
    
    private func setupCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            countryImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            countryImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            countryImageView.widthAnchor.constraint(equalToConstant: 25),
            countryImageView.heightAnchor.constraint(equalToConstant: 15)
        ])
    }
    
    private func setupCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            countryNameLabel.leadingAnchor.constraint(equalTo: countryImageView.trailingAnchor, constant: 10),
            countryNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSelectedImageConstraints() {
        NSLayoutConstraint.activate([
            selectedImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            selectedImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            selectedImageView.widthAnchor.constraint(equalToConstant: 25),
            selectedImageView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }

    private func layoutUI() {
        addSubviews()
        setupCountryImageViewConstraints()
        setupCountryLabelConstraints()
        setupLineViewConstraints()
        setupSelectedImageConstraints()
    }
    
    func configure(model: CountryViewModel) {
        countryImageView.load(url: model.photoUrl)
        countryNameLabel.text = model.name
        selectedImageView.isHidden = !model.isSelected
    }
}
