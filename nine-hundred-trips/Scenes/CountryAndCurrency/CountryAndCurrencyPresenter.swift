//
//  CountryAndCurrencyPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class CountryAndCurrencyPresenter: CountryAndCurrencyPresenterProtocol, CountryAndCurrencyInteractorOutputProtocol {
    
    weak var view: CountryAndCurrencyViewProtocol?
    private let interactor: CountryAndCurrencyInteractorInputProtocol
    private let router: CountryAndCurrencyRouterProtocol
    
    private var countryViewModels = [CountryViewModel]()
    private var currencyViewModels = [CurrencyViewModel]()
    
    var countriesNumberOfRows: Int {
        return countryViewModels.count
    }
    
    var currenciesNumberOfRows: Int {
        return currencyViewModels.count
    }
    
    init(view: CountryAndCurrencyViewProtocol, interactor: CountryAndCurrencyInteractorInputProtocol, router: CountryAndCurrencyRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getCountriesAndCurrencies()
    }
    
    func countriesDidSelectRow(at indexPath: IndexPath) {
        for (index, _) in countryViewModels.enumerated() {
            countryViewModels[index].isSelected = index == indexPath.row
        }
        view?.reloadCountryTableView()
    }
    
    func currenciesDidSelectRow(at indexPath: IndexPath) {
        for (index, _) in currencyViewModels.enumerated() {
            currencyViewModels[index].isSelected = index == indexPath.row
        }
        view?.reloadCurrencyTableView()
    }
    
    func countriesAndCurrenciesFetched(with result: Result<CountryAndCurrency>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let result):
            countryViewModels = result.countries.compactMap(CountryViewModel.init)
            currencyViewModels = result.currencies.compactMap(CurrencyViewModel.init)
            if UserDefaultsHelper.countryId != 0 {
                countryViewModels.first(where: { $0.id == UserDefaultsHelper.countryId })?.isSelected = true
            }
            
            if UserDefaultsHelper.currencyId != 0 {
                currencyViewModels.first(where: { $0.id == UserDefaultsHelper.currencyId })?.isSelected = true
            }
            view?.reloadCurrencyTableView()
            view?.reloadCountryTableView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<CountryViewModel>, at indexPath: IndexPath) {
        let countryViewModel = countryViewModels[indexPath.row]
        cell.configure(model: countryViewModel)
    }
    
    func configure(cell: AnyConfigurableCell<CurrencyViewModel>, at indexPath: IndexPath) {
        let currencyViewModel = currencyViewModels[indexPath.row]
        cell.configure(model: currencyViewModel)
    }
    
    func dismissButtonTapped() {
        router.dismiss()
    }
    
    func confirmButtonTapped() {
        if countryViewModels.filter({ $0.isSelected }).count == 0 {
            router.showAlert(with: "selectCountry".localized())
        } else if currencyViewModels.filter({ $0.isSelected }).count == 0 {
            router.showAlert(with: "selectCurrency".localized())
        } else {
            guard
                let selectedCountryId = countryViewModels.first(where: { $0.isSelected })?.id,
                let selectedCurrencyId = currencyViewModels.first(where: { $0.isSelected })?.id
            else { return }
            if UserDefaultsHelper.countryId == 0 && UserDefaultsHelper.currencyId == 0 {
                router.navigateToHome()
            } else {
                router.dismiss()
            }
            
            UserDefaultsHelper.countryId = selectedCountryId
            UserDefaultsHelper.currencyId = selectedCurrencyId
            
        }
    }
    
}
