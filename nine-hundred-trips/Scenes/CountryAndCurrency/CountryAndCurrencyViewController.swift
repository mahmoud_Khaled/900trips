//
//  CountryAndCurrencyViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CountryAndCurrencyViewController: BaseViewController, CountryAndCurrencyViewProtocol {
    
    private let mainView = CountryAndCurrencyView()
    var presenter: CountryAndCurrencyPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "countryAndCurrency".localized()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        setupTableView()
        presenter.viewDidLoad()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    private func setupNavigationBarButtons() {
        let dismissButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_close"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(dismissButtonTapped))
            .build()
        
        
        let confirmButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_done"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(confirmButtonTapped))
            .build()
        
        navigationItem.rightBarButtonItem = confirmButton
        navigationItem.leftBarButtonItems = [dismissButton]
    }
    
    @objc private func dismissButtonTapped() {
        presenter.dismissButtonTapped()
    }
    
    @objc private func confirmButtonTapped() {
        presenter.confirmButtonTapped()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func setupTableView() {
        mainView.countryTableView.dataSource = self
        mainView.countryTableView.delegate = self
        mainView.currencyTableView.dataSource = self
        mainView.currencyTableView.delegate = self
    }
    
    func reloadCountryTableView() {
        mainView.countryTableViewHeightConstraint.constant = CGFloat(presenter.countriesNumberOfRows * 44)
        mainView.countryContainerView.isHidden = false
        mainView.countryTableView.reloadData()
    }
    
    func reloadCurrencyTableView() {
        mainView.currencyTableViewHeightConstraint.constant = CGFloat(presenter.currenciesNumberOfRows * 44)
        mainView.currencyContainerView.isHidden = false
        mainView.currencyTableView.reloadData()
    }
    
}

extension CountryAndCurrencyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainView.countryTableView {
            return presenter.countriesNumberOfRows
        } else {
            return presenter.currenciesNumberOfRows
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mainView.countryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: CountryCell.className, for: indexPath) as! CountryCell
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.className, for: indexPath) as! CurrencyCell
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == mainView.countryTableView) {
            presenter.countriesDidSelectRow(at: indexPath)
        } else {
            presenter.currenciesDidSelectRow(at: indexPath)
        }
    }
}
