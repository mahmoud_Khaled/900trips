//
//  CountryAndCurrencyProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol CountryAndCurrencyRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
    func navigateToHome()
}

protocol CountryAndCurrencyPresenterProtocol: class {
    var view: CountryAndCurrencyViewProtocol? { get set }
    var countriesNumberOfRows: Int { get }
    var currenciesNumberOfRows: Int { get }
    func countriesDidSelectRow(at indexPath: IndexPath)
    func currenciesDidSelectRow(at indexPath: IndexPath)
    func viewDidLoad()
    func configure(cell: AnyConfigurableCell<CountryViewModel>, at indexPath: IndexPath)
    func configure(cell: AnyConfigurableCell<CurrencyViewModel>, at indexPath: IndexPath)
    func dismissButtonTapped()
    func confirmButtonTapped()
}

protocol CountryAndCurrencyInteractorInputProtocol: class {
    var presenter: CountryAndCurrencyInteractorOutputProtocol? { get set }
    func getCountriesAndCurrencies()
}

protocol CountryAndCurrencyInteractorOutputProtocol: class {
    func countriesAndCurrenciesFetched(with result: Result<CountryAndCurrency>)
}

protocol CountryAndCurrencyViewProtocol: class {
    var presenter: CountryAndCurrencyPresenterProtocol! { get set }
    func reloadCurrencyTableView()
    func reloadCountryTableView()
    func showActivityIndicator()
    func hideActivityIndicator()
}
