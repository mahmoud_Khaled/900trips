//
//  CountryAndCurrencyView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CountryAndCurrencyView: UIView {
    
    var countryTableViewHeightConstraint: NSLayoutConstraint!
    var currencyTableViewHeightConstraint: NSLayoutConstraint!
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var countryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "country".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        return label
    }()
    
    lazy var countryContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        view.isHidden = true
        return view
    }()
    
    lazy var countryTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.isScrollEnabled = false
        tableView.register(CountryCell.self, forCellReuseIdentifier: CountryCell.className)
        return tableView
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "currency".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        return label
    }()
    
    lazy var currencyContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        view.isHidden = true
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var currencyTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.isScrollEnabled = false
        tableView.register(CurrencyCell.self, forCellReuseIdentifier: CurrencyCell.className)
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(countryLabel)
        containerView.addSubview(countryContainerView)
        countryContainerView.addSubview(countryTableView)
        containerView.addSubview(currencyLabel)
        containerView.addSubview(currencyContainerView)
        currencyContainerView.addSubview(currencyTableView)
    }
    
    private func setupScollViewConstrains(){
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstrains(){
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
        ])
    }
    
    private func setupCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            countryLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            countryLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupCountryTableContainerViewConstraints() {
        NSLayoutConstraint.activate([
            countryContainerView.topAnchor.constraint(equalTo: countryLabel.bottomAnchor, constant: 8),
            countryContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            countryContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupCountryTableViewConstraints() {
        countryTableViewHeightConstraint = countryTableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            countryTableView.topAnchor.constraint(equalTo: countryContainerView.topAnchor, constant: 4),
            countryTableView.leadingAnchor.constraint(equalTo: countryContainerView.leadingAnchor, constant: 4),
            countryTableView.trailingAnchor.constraint(equalTo: countryContainerView.trailingAnchor, constant: -4),
            countryTableView.bottomAnchor.constraint(equalTo: countryContainerView.bottomAnchor, constant: -4),
            countryTableViewHeightConstraint
        ])
    }
    
    private func setupCurrencyLabelConstraints() {
        NSLayoutConstraint.activate([
            currencyLabel.topAnchor.constraint(equalTo: countryContainerView.bottomAnchor, constant: 20),
            currencyLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupCurrencyTableContainerViewConstraints() {
        NSLayoutConstraint.activate([
            currencyContainerView.topAnchor.constraint(equalTo: currencyLabel.bottomAnchor, constant: 8),
            currencyContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            currencyContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupCurrencyTableViewConstraints() {
        currencyTableViewHeightConstraint = currencyTableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            currencyTableView.topAnchor.constraint(equalTo: currencyContainerView.topAnchor, constant: 4),
            currencyTableView.leadingAnchor.constraint(equalTo: currencyContainerView.leadingAnchor, constant: 4),
            currencyTableView.trailingAnchor.constraint(equalTo: currencyContainerView.trailingAnchor, constant: -4),
            currencyTableView.bottomAnchor.constraint(equalTo: currencyContainerView.bottomAnchor, constant: -4),
            currencyTableViewHeightConstraint,
            currencyTableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScollViewConstrains()
        setupContainerViewConstrains()
        setupCountryLabelConstraints()
        setupCountryTableContainerViewConstraints()
        setupCountryTableViewConstraints()
        setupCurrencyLabelConstraints()
        setupCurrencyTableContainerViewConstraints()
        setupCurrencyTableViewConstraints()
    }
    
}
