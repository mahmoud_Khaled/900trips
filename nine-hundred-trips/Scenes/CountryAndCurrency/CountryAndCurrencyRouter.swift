//
//  CountryAndCurrencyRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CountryAndCurrencyRouter: CountryAndCurrencyRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = CountryAndCurrencyViewController()
        let interactor = CountryAndCurrencyInteractor()
        let router = CountryAndCurrencyRouter()
        let presenter = CountryAndCurrencyPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func navigateToHome() {
        AppDelegate.shared.setRootViewController(
            AppDelegate.shared.createTabBarController(),
            animated: false
        )
    }
}
