//
//  ChooseLanguageProtocols.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ChooseLanguageRouterProtocol: class {
    func navigateToOpeningPage(language: Language)
}

protocol ChooseLanguagePresenterProtocol: class {
    var view: ChooseLanguageViewProtocol? { get set }
    func arabicButtonTapped()
    func englishButtonTapped()
}

protocol ChooseLanguageViewProtocol: class {
    var presenter: ChooseLanguagePresenterProtocol! { get set }
}
