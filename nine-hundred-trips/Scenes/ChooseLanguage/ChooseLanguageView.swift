//
//  ChooseLanguageView.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageView: UIView {
    
    private lazy var backgroundImageView: UIImageView =  {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "background")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var separatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        return view
    }()
    

    lazy var arabicButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .gray
        button.setImage(UIImage(named: "ic_arabic"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: -15, left: 3, bottom: 15, right: -3)
        button.layer.cornerRadius = 18
        return button
    }()
    
    private lazy var arabicNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "العربية"
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        return label
    }()
    
     lazy var englishButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .gray
        button.setImage(UIImage(named: "ic_english"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: -15, left: 3, bottom: 15, right: -3)
        button.layer.cornerRadius = 18
        return button
    }()
    
    private lazy var engilshNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "English"
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.textColor = .darkGray
        return label
    }()
    
    private lazy var orLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or".localized()
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 25)
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
       addSubview(backgroundImageView)
       addSubview(scrollView)
       scrollView.addSubview(containerView)
       containerView.addSubview(logoImageView)
       containerView.addSubview(separatorLineView)
       containerView.addSubview(arabicButton)
       arabicButton.addSubview(arabicNameLabel)
       containerView.addSubview(englishButton)
       englishButton.addSubview(engilshNameLabel)
       containerView.addSubview(orLabel)
        
        
    }
    
    private func setupBackgroundImageViewConstraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 70),
            logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageView.widthAnchor.constraint(equalToConstant: 230.0),
            logoImageView.heightAnchor.constraint(equalToConstant: 230.0)
        ])
    }
    
    private func setupSeparatorLineViewConstraints() {
        NSLayoutConstraint.activate([
            separatorLineView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 15),
            separatorLineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            separatorLineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            separatorLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupArabicButtonConstraints() {
        NSLayoutConstraint.activate([
            arabicButton.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 25),
            arabicButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -80),
            arabicButton.widthAnchor.constraint(equalToConstant: 115),
            arabicButton.heightAnchor.constraint(equalToConstant: 145),
            arabicButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    private func setupArabicNameLabelConstraints() {
        NSLayoutConstraint.activate([
           arabicNameLabel.bottomAnchor.constraint(equalTo: arabicButton.bottomAnchor, constant: -10),
           arabicNameLabel.centerXAnchor.constraint(equalTo: arabicButton.centerXAnchor)
        ])
    }
    
    private func setupEngilshButtonConstraints() {
        NSLayoutConstraint.activate([
            englishButton.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 25),
            englishButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 80),
            englishButton.widthAnchor.constraint(equalToConstant: 115),
            englishButton.heightAnchor.constraint(equalToConstant: 145),
            englishButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    private func setupEngilshNameLabelConstraints() {
        NSLayoutConstraint.activate([
            engilshNameLabel.bottomAnchor.constraint(equalTo: englishButton.bottomAnchor, constant: -10),
            engilshNameLabel.centerXAnchor.constraint(equalTo: englishButton.centerXAnchor)
        ])
    }
    
    private func setupOrLabelConstraints() {
        NSLayoutConstraint.activate([
            orLabel.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 80),
            orLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupSeparatorLineViewConstraints()
        setupArabicButtonConstraints()
        setupEngilshButtonConstraints()
        setupArabicNameLabelConstraints()
        setupEngilshNameLabelConstraints()
        setupOrLabelConstraints()
    }
    
}
