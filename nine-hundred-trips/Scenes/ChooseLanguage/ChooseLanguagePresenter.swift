//
//  ChooseLanguagePresenter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum Language: String {
    case english = "en", arabic = "ar"
}

class ChooseLanguagePresenter: ChooseLanguagePresenterProtocol {
    
    weak var view: ChooseLanguageViewProtocol?
    private let router: ChooseLanguageRouterProtocol
    
    init(view: ChooseLanguageViewProtocol, router: ChooseLanguageRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func arabicButtonTapped() {
        changeLanguage(language: .arabic)
    }
    
    func englishButtonTapped() {
        changeLanguage(language: .english)
    }
    
    private func changeLanguage(language: Language) {
        L102Language.setLanguage(code: language.rawValue)
        router.navigateToOpeningPage(language: language)
    }
    
}
