//
//  ChooseLanguageRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageRouter: ChooseLanguageRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ChooseLanguageViewController()
        let router = ChooseLanguageRouter()
        let presenter = ChooseLanguagePresenter(view: view, router: router)
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToOpeningPage(language: Language) {
        if language == .arabic {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        AppDelegate.shared.setRootViewController(
            UINavigationController.create(rootViewController: OpeningPageRouter.createModule()),
            animated: true
        )
    }
}
