//
//  ChooseLanguageViewController.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageViewController: BaseViewController, ChooseLanguageViewProtocol {
    
   private let mainView = ChooseLanguageView()
   var presenter: ChooseLanguagePresenterProtocol!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
    }
    
    private func addTargets() {
        mainView.arabicButton.addTarget(self, action: #selector(arabicButtonTapped), for: .touchUpInside)
        mainView.englishButton.addTarget(self, action: #selector(englishButtonTapped), for: .touchUpInside)
    }
    
    
    @objc private func arabicButtonTapped() {
        presenter.arabicButtonTapped()
    }
    
    @objc private func englishButtonTapped() {
        presenter.englishButtonTapped()
    }
    
}
