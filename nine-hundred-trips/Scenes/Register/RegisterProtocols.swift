//
//  RegisterProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol RegisterRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(destination: RegisterDestination)
}

protocol RegisterPresenterProtocol: class {
    var view: RegisterViewProtocol? { get set }
    func registerButtonTapped(userType: UserType, username: String?, companyName: String?, email: String, password: String, confirmPassword: String, countryId: Int, currencyId: Int, companyType: Int?, agreeTerms: Bool)
    func viewDidLoad()
    func termsAndConditionsButtonTapped()

}

protocol RegisterInteractorInputProtocol: class {
    var presenter: RegisterInteractorOutputProtocol? { get set }
    func register(userType: UserType, username: String, companyName: String, email: String, password: String, confirmPassword: String, countryId: Int, currencyId: Int, companyType: Int)
    func getCountriesAndCurrencies()
    func getCompanyTypes()
}

protocol RegisterInteractorOutputProtocol: class {
    func didRegister(with result: Result<Data>)
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>)
    func didFetchCompanyTypes(with result: Result<[CompanyType]>)
}

protocol RegisterViewProtocol: class {
    var presenter: RegisterPresenterProtocol! { get set }
    func setCurrenciesAndCountries(countries: [CountryViewModel], currencies: [CurrencyViewModel])
    func setCompanyTypes(types: [CompanyTypeViewModel])
    func showActivityIndicator()
    func hideActivityIndicator()
}
