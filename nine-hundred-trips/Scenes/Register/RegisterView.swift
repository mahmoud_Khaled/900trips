//
//  RegisterView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "background"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var companyViewGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer()
        return tapGesture
    }()
    
    lazy var registerAsACompanyView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 55
        if L102Language.currentLanguage == "en" {
            view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        } else {
            view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        }
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(companyViewGesture)
        return view
    }()
    
    lazy var userViewGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer()
        return tapGesture
    }()
    
    lazy var registerAsAUserView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 55
        if L102Language.currentLanguage == "en" {
            view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        } else {
            view.layer.maskedCorners = [.layerMinXMinYCorner , .layerMinXMaxYCorner]
        }
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(userViewGesture)
        return view
    }()
    
    private lazy var companyImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_company"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var registerAsCompanyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "registerAsACompany".localized()
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var registerAsUserLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "registerAsAUser".localized()
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_user_register"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var usernameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "username".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_user"), width: 19, height: 21)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var companyNameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "companyName".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_company_name"), width: 19, height: 21)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isHidden = true
        return textField
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "email".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_mail"), width: 19, height: 22)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.keyboardType = .emailAddress
        return textField
    }()
    
    lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "password".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.isSecureTextEntry = true
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var confirmPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "confirmPassword".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.isSecureTextEntry = true
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var countryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_globe"), width: 18, height: 18)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var currencyTextField: PickerViewTextField<CurrencyViewModel> = {
        let textField = PickerViewTextField<CurrencyViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "currency".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_currency"), width: 18, height: 18)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var companyTypeTextField: PickerViewTextField<CompanyTypeViewModel> = {
        let textField = PickerViewTextField<CompanyTypeViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "companyType".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 3, blur: 6, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_company_type"), width: 18, height: 21)
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        textField.isHidden = true
        return textField
    }()
    
    private lazy var userInformationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(usernameTextField)
        stackView.addArrangedSubview(companyNameTextField)
        stackView.addArrangedSubview(emailTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(confirmPasswordTextField)
        stackView.addArrangedSubview(countryTextField)
        stackView.addArrangedSubview(currencyTextField)
        stackView.addArrangedSubview(companyTypeTextField)
        stackView.axis = .vertical
        stackView.spacing = 10.0
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    lazy var agreeToTermsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(UIImage(named: "ic_unchecked")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()

    lazy var termsLinkButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("agreeTermsAndConditions".localized(), for: .normal)
        button.setTitleColor(.textColor, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 17)
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets.left = 5
        button.tag = -1
        return button
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("register".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 30
        return button
    }()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(logoImageView)
        containerView.addSubview(registerAsACompanyView)
        containerView.addSubview(registerAsAUserView)
        registerAsACompanyView.addSubview(companyImageView)
        registerAsACompanyView.addSubview(registerAsCompanyLabel)
        registerAsAUserView.addSubview(userImageView)
        registerAsAUserView.addSubview(registerAsUserLabel)
        containerView.addSubview(userInformationStackView)
        containerView.addSubview(agreeToTermsButton)
        containerView.addSubview(termsLinkButton)
        containerView.addSubview(registerButton)
    }
    
    private func setupBackgroundImageViewConstraints(){
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0)
        ])
    }

    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 24),
            logoImageView.widthAnchor.constraint(equalToConstant: 128),
            logoImageView.heightAnchor.constraint(equalToConstant: 148)
        ])
    }

    private func setupRegisterAsAUserViewConstraints() {
        NSLayoutConstraint.activate([
            registerAsACompanyView.centerYAnchor.constraint(equalTo: logoImageView.centerYAnchor),
            registerAsACompanyView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0.0),
            registerAsACompanyView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.3),
            registerAsACompanyView.heightAnchor.constraint(equalToConstant: 110)
        ])
    }
    
    private func setupRegisterAsACompanyViewConstraints() {
        NSLayoutConstraint.activate([
            registerAsAUserView.centerYAnchor.constraint(equalTo: logoImageView.centerYAnchor),
            registerAsAUserView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            registerAsAUserView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.3),
            registerAsAUserView.heightAnchor.constraint(equalToConstant: 110)
        ])
    }
    
    private func setupCompanyImageViewConstraints() {
        if L102Language.currentLanguage == "en" {
            companyImageView.centerXAnchor.constraint(equalTo: registerAsACompanyView.centerXAnchor,constant: 8.0).isActive = true
        } else {
            companyImageView.centerXAnchor.constraint(equalTo: registerAsACompanyView.centerXAnchor,constant: -8.0).isActive = true
        }
        NSLayoutConstraint.activate([
            
            companyImageView.topAnchor.constraint(equalTo: registerAsACompanyView.topAnchor, constant: 20.0),
            companyImageView.widthAnchor.constraint(equalToConstant: 52.0),
            companyImageView.heightAnchor.constraint(equalToConstant: 29.0)
        ])
    }
    
    private func setupRegisterAsCompanyLabelConstraints() {
        NSLayoutConstraint.activate([
            registerAsCompanyLabel.centerXAnchor.constraint(equalTo: companyImageView.centerXAnchor),
            registerAsCompanyLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 2.0),
            registerAsCompanyLabel.leadingAnchor.constraint(equalTo: registerAsACompanyView.leadingAnchor, constant: 15),
            registerAsCompanyLabel.trailingAnchor.constraint(equalTo: registerAsACompanyView.trailingAnchor, constant: -10),
        ])
    }
    
    private func setupUserImageViewConstraints() {
        if L102Language.currentLanguage == "en" {
            userImageView.centerXAnchor.constraint(equalTo: registerAsAUserView.centerXAnchor, constant: -8.0).isActive = true
        } else {
            userImageView.centerXAnchor.constraint(equalTo: registerAsAUserView.centerXAnchor, constant: 8.0).isActive = true
        }
        
        NSLayoutConstraint.activate([
            userImageView.centerXAnchor.constraint(equalTo: registerAsAUserView.centerXAnchor, constant: 8.0),
            userImageView.topAnchor.constraint(equalTo: registerAsAUserView.topAnchor, constant: 20.0),
            userImageView.widthAnchor.constraint(equalToConstant: 52.0),
            userImageView.heightAnchor.constraint(equalToConstant: 29.0)
        ])
    }
    
    private func setupRegisterAsUserLabelConstraints() {
        NSLayoutConstraint.activate([
            registerAsUserLabel.centerXAnchor.constraint(equalTo: userImageView.centerXAnchor),
            registerAsUserLabel.topAnchor.constraint(equalTo: userImageView.bottomAnchor, constant: 2.0),
            registerAsUserLabel.leadingAnchor.constraint(equalTo: registerAsAUserView.leadingAnchor, constant: 10),
            registerAsUserLabel.trailingAnchor.constraint(equalTo: registerAsAUserView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupUsernameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            usernameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupCompanyNameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            companyNameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupUserInformationStackViewConstraints() {
        NSLayoutConstraint.activate([
            userInformationStackView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 20),
            userInformationStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            userInformationStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupAgreeToTermsButtonConstraints() {
        NSLayoutConstraint.activate([
            agreeToTermsButton.topAnchor.constraint(equalTo: userInformationStackView.bottomAnchor, constant: 20),
            agreeToTermsButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            agreeToTermsButton.heightAnchor.constraint(equalToConstant: 25),
            agreeToTermsButton.widthAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func setupTermsLinkButtonConstraints() {
        NSLayoutConstraint.activate([
            termsLinkButton.centerYAnchor.constraint(equalTo: agreeToTermsButton.centerYAnchor, constant: -3),
            termsLinkButton.leadingAnchor.constraint(equalTo: agreeToTermsButton.trailingAnchor, constant: 7),
            termsLinkButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            termsLinkButton.heightAnchor.constraint(equalToConstant: 25)

        ])
    }
    
    private func setupRegisterButtonConstraints() {
        NSLayoutConstraint.activate([
            registerButton.topAnchor.constraint(equalTo: agreeToTermsButton.bottomAnchor, constant: 20),
            registerButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            registerButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            registerButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            registerButton.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupRegisterAsAUserViewConstraints()
        setupRegisterAsACompanyViewConstraints()
        setupCompanyImageViewConstraints()
        setupUserImageViewConstraints()
        setupRegisterAsCompanyLabelConstraints()
        setupRegisterAsUserLabelConstraints()
        setupUsernameTextFieldConstraints()
        setupCompanyNameTextFieldConstraints()
        setupUserInformationStackViewConstraints()
        setupAgreeToTermsButtonConstraints()
        setupTermsLinkButtonConstraints()
        setupRegisterButtonConstraints()
    }
    
}
