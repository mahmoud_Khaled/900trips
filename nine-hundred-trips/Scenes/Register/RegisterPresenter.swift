//
//  RegisterPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RegisterPresenter: RegisterPresenterProtocol, RegisterInteractorOutputProtocol {
 
    weak var view: RegisterViewProtocol?
    private let interactor: RegisterInteractorInputProtocol
    private let router: RegisterRouterProtocol
    
    private var countries: [Country] = []
    private var currencies: [Currency] = []
    private var email: String?
    
    init(view: RegisterViewProtocol, interactor: RegisterInteractorInputProtocol, router: RegisterRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator()
        interactor.getCountriesAndCurrencies()
        interactor.getCompanyTypes()
    }
    
    func registerButtonTapped(userType: UserType, username: String?, companyName: String?, email: String, password: String, confirmPassword: String, countryId: Int, currencyId: Int, companyType: Int?, agreeTerms: Bool) {
        
        
        if userType == .client {
            
            guard !username!.isEmpty else {
                router.showAlert(with: "emptyUsername".localized())
                return
            }
            
        } else if userType == .company {
            
            guard !companyName!.isEmpty else {
                router.showAlert(with: "emptyCompanyName".localized())
                return
            }
            
            guard companyType != 0 else {
                router.showAlert(with: "emptyCompanyType".localized())
                return
            }
            
        }
        
        guard !email.isEmpty else {
            router.showAlert(with: "emptyEmail".localized())
            return
        }
        
        self.email = email
        
        guard NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: email) else  {
            router.showAlert(with: "incorrectEmail".localized())
            return
        }
        
        guard !password.isEmpty else {
            router.showAlert(with: "emptyPassword".localized())
            return
        }
        
        guard NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$").evaluate(with: password) else  {
            router.showAlert(with: "incorrectPassword".localized())
            return
        }
        
        guard !confirmPassword.isEmpty else {
            router.showAlert(with: "emptyRepeatPassword".localized())
            return
        }
        
        guard confirmPassword == password else {
            router.showAlert(with: "compatablePassword".localized())
            return
        }

        guard countryId  != 0 else {
            router.showAlert(with: "selectCountry".localized())
            return
        }
        
        guard currencyId  != 0 else {
            router.showAlert(with: "selectCurrency".localized())
            return
        }
        
        guard agreeTerms == true else {
            router.showAlert(with: "termsAndConditionsAgreement".localized())
            return
        }
        
        view?.showActivityIndicator()
        interactor.register(userType: userType, username: username ?? "", companyName: companyName ?? "", email: email, password: password, confirmPassword: confirmPassword, countryId: countryId, currencyId: currencyId, companyType: companyType ?? 0)
    }
    
    func termsAndConditionsButtonTapped() {
        router.navigate(destination: .termsAndConditions)
    }
    
    
    
    func didRegister(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
             router.navigate(destination: .confirmationCode(email: email ?? "" ))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCountriesAndCurrencies(with result: Result<CountryAndCurrency>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            countries = value.countries
            currencies = value.currencies
            view?.setCurrenciesAndCountries(countries: countries.map(CountryViewModel.init), currencies: currencies.map(CurrencyViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCompanyTypes(with result: Result<[CompanyType]>) {
        switch result {
        case .success(let value):
            view?.setCompanyTypes(types: value.map(CompanyTypeViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
}
