//
//  RegisterInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterInteractor: RegisterInteractorInputProtocol {
    
    weak var presenter: RegisterInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    private let applicationDataWorker = ApplicationDataWorker()
    
    func register(userType: UserType, username: String, companyName: String, email: String, password: String, confirmPassword: String, countryId: Int, currencyId: Int, companyType: Int) {
        
        authenticationWorker.register(userType: userType, username: username, companyName: companyName, email: email, password: password, confirmPassword: confirmPassword, countryId: countryId, currencyId: currencyId, companyType: companyType) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRegister(with: result)
        }
    }
    
    func getCountriesAndCurrencies() {
        applicationDataWorker.getCountriesAndCurrenceies { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountriesAndCurrencies(with: result)
        }
    }

    func getCompanyTypes() {
        applicationDataWorker.getCompanyTypes { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCompanyTypes(with: result)
        }
    }
    
}
