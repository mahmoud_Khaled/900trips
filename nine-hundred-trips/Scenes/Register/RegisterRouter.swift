//
//  RegisterRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit


enum RegisterDestination {
    case confirmationCode(email: String)
    case termsAndConditions
}

class RegisterRouter: RegisterRouterProtocol {
  
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = RegisterViewController()
        let interactor = RegisterInteractor()
        let router = RegisterRouter()
        let presenter = RegisterPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }

    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigate(destination: RegisterDestination) {
        switch destination {
        case .confirmationCode(let email):
             viewController?.present(RegisterConfirmationRouter.createModule(email: email), animated: true)
        case .termsAndConditions:
            viewController?.present(TermsAndConditionsRouter.createModule(), animated: true)
        }
    }
    
    
    
}
