//
//  RegisterViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController, RegisterViewProtocol {
    
    private let  mainView = RegisterView()
    var presenter: RegisterPresenterProtocol!
    private var userType: UserType = .client
    private var isChecked: Bool = false
    private var countryId: Int?
    private var currencyId: Int?
    private var companyTypeId: Int?
    
    override func loadView() {
        super.loadView()
        view = mainView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        presenter.viewDidLoad()
        pickerViewDidSelectItemAction()
        setNavigationBarTransparent(true)
        setupNavigationBarStyle()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.tintColor = .coral
    }
    
    private func addTargets() {
        mainView.companyViewGesture.addTarget(self, action: #selector(registerAsACompanyViewTapped))
        mainView.userViewGesture.addTarget(self, action: #selector(registerAsAUserViewTapped))
        mainView.registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        mainView.agreeToTermsButton.addTarget(self, action: #selector(agreeToTermsButtonTapped), for: .touchUpInside)
        mainView.termsLinkButton.addTarget(self, action: #selector(termsAndConditionsButtonTapped), for: .touchUpInside)
        
    }
    
    @objc private func registerAsACompanyViewTapped() {
        mainView.registerAsACompanyView.backgroundColor = .mediumTurquoise
        mainView.registerAsAUserView.backgroundColor = .coral
        mainView.usernameTextField.isHidden = true
        mainView.companyNameTextField.isHidden = false
        mainView.companyTypeTextField.isHidden = false
        userType = .company
    }
    
    @objc private func registerAsAUserViewTapped() {
        mainView.registerAsAUserView.backgroundColor = .mediumTurquoise
        mainView.registerAsACompanyView.backgroundColor = .coral
        mainView.usernameTextField.isHidden = false
        mainView.companyNameTextField.isHidden = true
        mainView.companyTypeTextField.isHidden = true
        userType = .client
    }
    
    @objc private func registerButtonTapped() {
        if userType == .client {
            presenter.registerButtonTapped(userType: userType, username: mainView.usernameTextField.text ?? "", companyName: nil, email: mainView.emailTextField.text ?? "", password: mainView.passwordTextField.text ?? "", confirmPassword: mainView.confirmPasswordTextField.text ?? "", countryId: countryId ?? 0, currencyId: currencyId ?? 0, companyType: nil, agreeTerms: isChecked)
            
        } else if userType == .company {
            
            presenter.registerButtonTapped(userType: userType, username: nil, companyName: mainView.companyNameTextField.text ?? "", email: mainView.emailTextField.text ?? "", password: mainView.passwordTextField.text ?? "", confirmPassword: mainView.confirmPasswordTextField.text ?? "", countryId: countryId ?? 0, currencyId: currencyId ?? 0, companyType: companyTypeId ?? 0, agreeTerms: isChecked)
        }
        
    }
    
    @objc private func agreeToTermsButtonTapped() {
        isChecked = !isChecked
        if isChecked {
            mainView.agreeToTermsButton.setImage(UIImage(named: "ic_check")?.withRenderingMode(.alwaysOriginal), for: .normal)
        } else {
            mainView.agreeToTermsButton.setImage(nil, for: .normal)
        }
    }
    
    @objc private func termsAndConditionsButtonTapped() {
        presenter.termsAndConditionsButtonTapped()
    }
    
    func setCurrenciesAndCountries(countries: [CountryViewModel], currencies: [CurrencyViewModel]) {
        mainView.countryTextField.items = countries
        mainView.currencyTextField.items = currencies
    }
    
    func setCompanyTypes(types: [CompanyTypeViewModel]) {
        mainView.companyTypeTextField.items = types
    }
    

    private func pickerViewDidSelectItemAction() {
        mainView.currencyTextField.didSelectItem = { [weak self] item in
            guard let self = self  else { return }
            self.currencyId =  item.id
        }
        
        mainView.countryTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.countryId =  item.id
        }
        
        mainView.companyTypeTextField.didSelectItem = { [weak self ] item in
            guard let self = self else { return }
            self.companyTypeId = item.id
        }
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
}
