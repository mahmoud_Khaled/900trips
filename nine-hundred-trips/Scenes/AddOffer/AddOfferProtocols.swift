//
//  AddOfferProtocols.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol AddOfferRouterProtocol: class {
    func presenteImagePicker()
    func showPermissionAlert(with title: String, message: String)
    func showAlert(with message: String)
    func navigate(to destination: AddOfferDestination)
}

protocol AddOfferPresenterProtocol: class {
    var view: AddOfferViewProtocol? { get set }
    func uploadImagesButtonTapped()
    func showPermissionAlert(with title: String, message: String)
    var numberOfRows: Int { get }
    func viewDidLoad()
    func addServicesButtonTapped()
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath)
    func deleteServiceButtonTapped(at indexPath: IndexPath)
    func addOfferButtonTapped(offerData: AddOfferModel)
    func resetValues()
    func titleForService(at indexPath: IndexPath) -> String
}

protocol AddOfferInteractorInputProtocol: class {
    var presenter: AddOfferInteractorOutputProtocol? { get set }
    func getAddOfferData()
    func uploadImages(images: [File])
    func addOffer(offerData: AddOfferModel, selectedServices: [Int], photos: [String])
}

protocol AddOfferInteractorOutputProtocol: class {
    func didFetchAddOfferData(with result: Result<AddOfferData>)
    func didUploadImages(with result: Result<[String]>)
    func didAddOffer(with result: Result<Data>)
}

protocol AddOfferViewProtocol: class {
    var presenter: AddOfferPresenterProtocol! { get set }
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    
    func setAllPickerData(services: [ServiceViewModel], countries: [CountryViewModel], currencies: [CurrencyViewModel], flightCompanies: [FlightCompany], flightSeatType: [FlightSeatType], offerTypes: [OfferType])
    
    func reloadServiceCollectionView()
    func toggleCollectionViewHidden(isHidden: Bool)
    func showSuccessView()
}


protocol ChooseServiceDelegate: class {
    func didChooseServices(services: [ServiceViewModel], selectedServices: [ServiceViewModel])
}
