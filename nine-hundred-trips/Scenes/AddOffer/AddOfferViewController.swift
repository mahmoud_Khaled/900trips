//
//  AddOfferViewController.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import YangMingShan

enum Trasnsit: Int {
    case yes = 1
    case no = 0
}

class AddOfferViewController: BaseViewController, AddOfferViewProtocol {

    private let mainView = AddOfferView()
    var presenter: AddOfferPresenterProtocol!
    private var images = [UIImage]()
    private var photos: [File] = []
    
    private var offerData = AddOfferModel()
    
    private let assetToImageConverter: AssetToImageConverterProtocol
    
    private var departureCountryId = 0
    private var departureCityId = 0
    private var arrivalCountryId = 0
    private var arrivalCityId = 0
    private var flightCompanyId = 0
    private var flightSeatTypId = 0
    private var transitCountryId = 0
    private var transitTime = ""
    private var transit: Trasnsit = .yes
    private var currencyId = 0
    private var offerTypeId = -1
    
    init(assetToImageConverter: AssetToImageConverterProtocol) {
        self.assetToImageConverter = assetToImageConverter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "addOffer".localized()
        setupNavigationBarStyle()
        setupCollectionView()
        addTargets()
        presenter.viewDidLoad()
        pickerViewDidSelectItemAction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupCollectionView() {
        mainView.imagesCollectionView.dataSource = self
        mainView.imagesCollectionView.delegate = self
        
        mainView.servicesCollectionView.dataSource = self
        mainView.servicesCollectionView.delegate = self
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func addTargets() {
        mainView.yesButton.addTarget(self, action: #selector(yesButtonTapped), for: .touchUpInside)
        mainView.noButton.addTarget(self, action: #selector(noButtonTapped), for: .touchUpInside)
        mainView.addImagesButton.addTarget(self, action: #selector(uploadImagesButtonTapped), for: .touchUpInside)
        mainView.addOfferButton.addTarget(self, action: #selector(addOfferButtonTapped), for: .touchUpInside)
        mainView.addServicesButton.addTarget(self, action: #selector(addServicesButtonTapped), for: .touchUpInside)
    }
    
    private func deleteImage(at indexPath: IndexPath) {
        images.remove(at: indexPath.row)
        mainView.imagesCollectionView.reloadData()
        if images.count == 0 {
            mainView.imagesCollectionView.isHidden = true
        }
    }
    
    @objc private func yesButtonTapped() {
        mainView.transitView.isHidden = false
        mainView.transitCountryTextField.isEnabled = true
        mainView.transitTimeTextField.isEnabled = true
        transit = .yes
        mainView.setButtonImages(selectedButton: mainView.yesButton, unselectedButton: mainView.noButton)
    }
    
    @objc private func noButtonTapped() {
        mainView.transitView.isHidden = true
        mainView.transitCountryTextField.isEnabled = false
        mainView.transitTimeTextField.isEnabled = false
        transit = .no
        transitCountryId = 0
        transitTime = ""
        mainView.setButtonImages(selectedButton: mainView.noButton, unselectedButton: mainView.yesButton)
    }
    
    @objc private func addServicesButtonTapped() {
        presenter.addServicesButtonTapped()
    }

    @objc private func addOfferButtonTapped() {
        
        if images.count > 0 {
            offerData.photos = images.map {
                (name: "photo.jpg", key: "photos[]", mimeType: MimeType.jpg, data: $0.jpegData(compressionQuality: 0.5) ?? Data())
            }
        } else {
            offerData.photos = []
        }
        
        offerData.title = mainView.offerTitleTextField.text!
        offerData.departureCountryId = departureCountryId
        offerData.departureCityId = departureCityId
        offerData.arrivalCountryId = arrivalCountryId
        offerData.arrivalCityId = arrivalCityId
        offerData.transit = transit
        offerData.transitCountryId = transitCountryId
        transitTime = mainView.transitTimeTextField.text!
        offerData.transitTime = transitTime
        offerData.offerDays = mainView.offerDaysPeriodTextField.text!
        offerData.flightCompanyId = flightCompanyId
        offerData.flightSeatTypId = flightSeatTypId
        offerData.hotelName = mainView.hotelNameTextField.text!
        offerData.hotelRate = mainView.hotelStarsView.rating
        offerData.fromDate = mainView.fromDateTextField.text!
        offerData.toDate = mainView.toDateTextField.text!
        offerData.adultPrice = mainView.adultPriceTextField.text!
        offerData.childPrice = mainView.childPriceTextField.text!
        offerData.currencyId = currencyId
        offerData.offerTypeId = offerTypeId

        
        presenter.addOfferButtonTapped(offerData: offerData)
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "offerAddedSuccessfully".localized(), on: tabBarController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.mainView.resetInputsValues()
            self.resetVariables()   
            self.presenter.resetValues()
        }
    }
    
    func setAllPickerData(services: [ServiceViewModel], countries: [CountryViewModel], currencies: [CurrencyViewModel], flightCompanies: [FlightCompany], flightSeatType: [FlightSeatType], offerTypes: [OfferType]) {
        
        mainView.departureCountryTextField.items = countries
        mainView.arrivalCountryTextField.items = countries
        mainView.flightCompanyNameTextField.items = flightCompanies
        mainView.flightClassTextField.items = flightSeatType
        mainView.transitCountryTextField.items = countries
        mainView.currencyTextField.items = currencies
        mainView.offerTypeTextField.items = offerTypes
        
    }
    
    func reloadServiceCollectionView() {
        mainView.servicesCollectionView.reloadData()
    }
    
    func toggleCollectionViewHidden(isHidden: Bool) {
        mainView.servicesCollectionView.isHidden = isHidden
    }
    
    private func pickerViewDidSelectItemAction() {
        
        mainView.departureCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCountryId = item.id
            self.departureCityId = 0
            self.mainView.departureCityTextField.text = ""
            self.mainView.departureCityTextField.items = item.cities
        }
        
        mainView.arrivalCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCountryId = item.id
            self.arrivalCityId = 0
            self.mainView.arrivalCityTextField.text = ""
            self.mainView.arrivalCityTextField.items = item.cities
        }
        
        mainView.departureCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCityId = item.id
        }
        
        mainView.arrivalCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCityId = item.id
        }
        
        mainView.flightCompanyNameTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.flightCompanyId = item.id ?? 0
        }
        
        mainView.flightClassTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.flightSeatTypId = item.id
        }
        
        if transit == .yes {
            mainView.transitCountryTextField.didSelectItem = { [weak self] item in
                guard let self = self else { return }
                self.transitCountryId = item.id
            }
        }
        
        mainView.currencyTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.currencyId = item.id
        }
        
        mainView.offerTypeTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.offerTypeId = item.id
        }
    }
    
    @objc private func uploadImagesButtonTapped() {
        presenter.uploadImagesButtonTapped()
    }
    
    private func resetVariables() {
        images = []
        photos = []
        mainView.imagesCollectionView.reloadData()
        departureCountryId = 0
        departureCityId = 0
        arrivalCountryId = 0
        arrivalCityId = 0
        flightCompanyId = 0
        flightSeatTypId = 0
        transitCountryId = 0
        transitTime = ""
        transit = .yes
        currencyId = 0
        offerTypeId = -1
    }
}

extension AddOfferViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainView.servicesCollectionView {
            return presenter.numberOfRows
        } else {
            return images.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == mainView.servicesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChosenServiceCell.className, for: indexPath) as? ChosenServiceCell else { return UICollectionViewCell() }
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            cell.deleteButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.presenter.deleteServiceButtonTapped(at: indexPath)
            }
            return cell
            
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UploadImageCell.className, for: indexPath) as? UploadImageCell else { return UICollectionViewCell() }
            cell.configure(model: images[indexPath.item])
            cell.deleteButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.deleteImage(at: indexPath)
            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == mainView.servicesCollectionView {
            return CGSize(width: calculateTextWidth(at: indexPath), height: 50)
        } else {
            return CGSize(width: 150, height: 150)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == mainView.servicesCollectionView {
            return 0
        }
        return 10
    }
    
    private func calculateTextWidth(at indexPath: IndexPath) -> CGFloat {
        let approximateSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: 1000)
        let estimatedWidth = NSString(string: presenter.titleForService(at: indexPath)).boundingRect(with: approximateSize, options: .usesLineFragmentOrigin, attributes: [.font: DinNextFont.regular.getFont(ofSize: 14)], context: nil).width
        return estimatedWidth + 75
    }
}

extension AddOfferViewController: YMSPhotoPickerViewControllerDelegate {
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        presenter.showPermissionAlert(with: "allowPhotoAlbumAccess".localized(), message: "photoAccessDenied".localized())
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        presenter.showPermissionAlert(with: "allowCameraAccess".localized(), message: "photoAccessDenied".localized())
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            self.images.append(contentsOf: self.assetToImageConverter.convert(assets: photoAssets, targetSize: CGSize(width: 200, height: 400)))
            DispatchQueue.main.async {
                self.mainView.imagesCollectionView.isHidden = false
                self.mainView.imagesCollectionView.reloadData()
            }
        }
    }
}
