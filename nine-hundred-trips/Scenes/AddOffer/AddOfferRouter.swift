//
//  AddOfferRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import YangMingShan

enum AddOfferDestination {
    case addServices(services: [ServiceViewModel], selectedServices:[ServiceViewModel], delegate: ChooseServiceDelegate)
}

class AddOfferRouter: AddOfferRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = AddOfferViewController(assetToImageConverter: AssetToImageConverter())
        let interactor = AddOfferInteractor()
        let router = AddOfferRouter()
        let presenter = AddOfferPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func presenteImagePicker() {
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        let customColor = UIColor.init(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        viewController?.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: (viewController as! AddOfferViewController))
    }
    
    func showPermissionAlert(with title: String, message: String) {
        AlertBuilder(title: title, message: message, preferredStyle: .alert)
            .addAction(title: "Cancel", style: .cancel)
            .addAction(title: "Settings", style: .default) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
            .build()
            .show()
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigate(to destination: AddOfferDestination) {
        switch destination {
        case .addServices(let services, let selectedServices, let delegate):
            viewController?.navigationController?.pushViewController(ServicesRouter.createModule(services: services, selectedServices: selectedServices, delegate: delegate), animated: true)
        }
    }
    
}
