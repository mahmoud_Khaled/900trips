//
//  AddOfferView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Cosmos

class AddOfferView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        scrolView.showsHorizontalScrollIndicator = false
        scrolView.showsVerticalScrollIndicator = false
        return scrolView
    }()
    
    private lazy var containerView: IQPreviousNextView = {
        let view = IQPreviousNextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var addImagesContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        return view
    }()
    
    lazy var addImagesButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_addphotos"), for: .normal)
        return button
    }()
    
    private lazy var addImagesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "addImages".localized()
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        return label
    }()
    
    lazy var imagesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(UploadImageCell.self, forCellWithReuseIdentifier: UploadImageCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isHidden = true
        return collectionView
    }()
    
    private lazy var offerTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "offerTitle".localized()
        label.textAlignment = .left
        return label
    }()
    
    lazy var offerTitleTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "offerTitle".localized()
        textField.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3), alpha: 0.4, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var imagesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(imagesCollectionView)
        stackView.addArrangedSubview(offerTitleLabel)
        stackView.addArrangedSubview(offerTitleTextField)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    private lazy var departureContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var departureCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseDepartureCountry".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var departureCountryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    private lazy var departureCityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseDepartureCity".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var departureCityTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "city".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    private lazy var flightImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_flight_colored")?.imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var arrivalContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var arrivalCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseArrivalCountry".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var arrivalCountryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    private lazy var arrivalCityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseArrivalCity".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var arrivalCityTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "city".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
   
    lazy var fromDateTextField: DatePickerTextField = {
        let textField = DatePickerTextField(dateFormat: "yyyy-MM-dd")
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "from".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .right, image: UIImage(named: "ic_calender"), width: 30, height: 30, customLeading: 10)
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    private lazy var offerPeriodLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "offerPeriod".localized()
        return label
    }()
    
    lazy var toDateTextField: DatePickerTextField = {
        let textField = DatePickerTextField(dateFormat: "yyyy-MM-dd")
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "to".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .right, image: UIImage(named: "ic_calender"), width: 30, height: 30, customLeading: 10)
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.leftViewMode = .always
        return textField
    }()
    
    private lazy var dateStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(fromDateTextField)
        stackView.addArrangedSubview(toDateTextField)
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    private lazy var offerDaysPeriodLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "offerDaysPeriod".localized()
        return label
    }()
    
    lazy var offerDaysPeriodTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.keyboardType = .asciiCapableNumberPad
        textField.placeholder = "offerDaysPeriod".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var flightCompanyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "flightCompanyName".localized()
        return label
    }()
    
    lazy var flightCompanyNameTextField: PickerViewTextField<FlightCompany> = {
        let textField = PickerViewTextField<FlightCompany>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "chooseFlightCompany".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    private lazy var flightClassLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "flightClass".localized()
        return label
    }()
    
    lazy var flightClassTextField: PickerViewTextField<FlightSeatType> = {
        let textField = PickerViewTextField<FlightSeatType>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "chooseFlightClass".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    private lazy var transitLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "isThereATransit?".localized()
        return label
    }()
    
    lazy var yesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .white
        button.setTitle("yes".localized(), for: .normal)
        button.setTitleColor(.textColor, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 17)
        button.setImage(UIImage(named: "check_circle")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.imageView!.heightAnchor.constraint(equalToConstant: 20),
            button.imageView!.widthAnchor.constraint(equalToConstant: 20),
            button.imageView!.centerYAnchor.constraint(equalTo: button.centerYAnchor, constant: 4),
            button.imageView!.leadingAnchor.constraint(lessThanOrEqualTo: button.leadingAnchor, constant: 3)
        ])
        button.layer.masksToBounds = true
        return button
    }()

    lazy var noButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .white
        button.setTitle("no".localized(), for: .normal)
        button.setTitleColor(.textColor, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 17)
        button.setImage(UIImage(named: "uncheck_circle")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.imageView!.heightAnchor.constraint(equalToConstant: 20),
            button.imageView!.widthAnchor.constraint(equalToConstant: 20),
            button.imageView!.centerYAnchor.constraint(equalTo: button.centerYAnchor, constant: 3.5),
            button.imageView!.leadingAnchor.constraint(lessThanOrEqualTo: button.leadingAnchor, constant: 3)
            ])
        button.layer.masksToBounds = true
        return button
    }()
    
    lazy var transitView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var transitCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "enterTransitCountry".localized()
        return label
    }()
    
    lazy var transitCountryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "chooseTransitCountry".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    private lazy var transitTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "stopTimeInMinutes".localized()
        return label
    }()
    
    lazy var transitTimeTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "ex:StopTime".localized()
        textField.keyboardType = .asciiCapableNumberPad
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()

    
    private lazy var transitStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(transitView)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    private lazy var hotelNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "hotelName".localized()
        return label
    }()
    
    lazy var hotelNameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "hotelName".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var hotelStarsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "hotelStars".localized()
        return label
    }()
    
    lazy var hotelStarsView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.totalStars = 7
        view.settings.starSize = 30
        view.settings.fillMode = .full
        view.settings.updateOnTouch = true
        view.settings.filledColor = .mediumTurquoise
        view.settings.filledBorderColor = .mediumTurquoise
        view.settings.emptyColor = .gray
        view.settings.emptyBorderColor = .gray
        view.settings.starMargin = 2
        view.rating = 5.0
        return view
    }()
    
    private lazy var serviceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "flightServices".localized()
        return label
    }()
    
    private lazy var chooseServiceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 30.5
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var addServicesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("addServices".localized(), for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 20)
        button.backgroundColor = .clear
        button.setTitleColor(.textColor, for: .normal)
        button.contentHorizontalAlignment = .leading
        button.clipsToBounds = true
        return button
    }()
    
    private lazy var forwardImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_dropdown"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var servicesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(ChosenServiceCell.self, forCellWithReuseIdentifier: ChosenServiceCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isHidden = true
        return collectionView
    }()
    
    private lazy var serviceStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(chooseServiceView)
        stackView.addArrangedSubview(servicesCollectionView)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    private lazy var offerValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "offerValue".localized()
        return label
    }()
    
    lazy var adultView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var adultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "adultPerson".localized()
        return label
    }()
    
    lazy var adultPriceTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.keyboardType = .asciiCapableNumberPad
        textField.placeholder = "adultPrice".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var childView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var childLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "childPerson".localized()
        return label
    }()
    
    lazy var childPriceTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "childPrice".localized()
        textField.keyboardType = .asciiCapableNumberPad
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var offerValueStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(adultView)
        stackView.addArrangedSubview(childView)
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.layer.masksToBounds = true
        return stackView
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "currency".localized()
        return label
    }()
    
    lazy var currencyTextField: PickerViewTextField<CurrencyViewModel> = {
        let textField = PickerViewTextField<CurrencyViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "chooseCurrency".localized()
        textField.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.1), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    lazy var offerTypeView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 12
        return view
    }()
    
    private lazy var offerTypeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "offerType".localized()
        return label
    }()
    
    lazy var offerTypeTextField: PickerViewTextField<OfferType> = {
        let textField = PickerViewTextField<OfferType>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "offerType".localized()
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.setIcon(direction: .right, image: UIImage(named: "ic_dropdown"), width: 20, height: 20)
        return textField
    }()
    
    lazy var addOfferButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("addOffer".localized(), for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(addImagesContainerView)
        containerView.addSubview(addImagesButton)
        containerView.addSubview(addImagesLabel)
        containerView.addSubview(imagesStackView)
        
        containerView.addSubview(departureContainerView)
        departureContainerView.addSubview(departureCountryLabel)
        departureContainerView.addSubview(departureCountryTextField)
        departureContainerView.addSubview(departureCityLabel)
        departureContainerView.addSubview(departureCityTextField)
        containerView.addSubview(flightImageView)
        containerView.addSubview(arrivalContainerView)
        arrivalContainerView.addSubview(arrivalCountryLabel)
        arrivalContainerView.addSubview(arrivalCountryTextField)
        arrivalContainerView.addSubview(arrivalCityLabel)
        arrivalContainerView.addSubview(arrivalCityTextField)
        
        containerView.addSubview(offerPeriodLabel)
        containerView.addSubview(dateStackView)
        
        containerView.addSubview(offerDaysPeriodLabel)
        containerView.addSubview(offerDaysPeriodTextField)
        
        containerView.addSubview(flightCompanyNameLabel)
        containerView.addSubview(flightCompanyNameTextField)
        
        containerView.addSubview(flightClassLabel)
        containerView.addSubview(flightClassTextField)
        
        containerView.addSubview(transitLabel)
        containerView.addSubview(yesButton)
        containerView.addSubview(noButton)
        
        transitView.addSubview(transitCountryLabel)
        transitView.addSubview(transitCountryTextField)
        transitView.addSubview(transitTimeLabel)
        transitView.addSubview(transitTimeTextField)
        containerView.addSubview(transitStackView)
        
        containerView.addSubview(hotelNameLabel)
        containerView.addSubview(hotelNameTextField)
        containerView.addSubview(hotelStarsLabel)
        containerView.addSubview(hotelStarsView)
        
        containerView.addSubview(serviceLabel)
        containerView.addSubview(serviceStackView)
        chooseServiceView.addSubview(addServicesButton)
        chooseServiceView.addSubview(forwardImageView)
        
        containerView.addSubview(offerValueLabel)
        
        adultView.addSubview(adultLabel)
        adultView.addSubview(adultPriceTextField)
        childView.addSubview(childLabel)
        childView.addSubview(childPriceTextField)
        containerView.addSubview(offerValueStackView)
        
        containerView.addSubview(currencyLabel)
        containerView.addSubview(currencyTextField)
        
        containerView.addSubview(offerTypeView)
        offerTypeView.addSubview(offerTypeLabel)
        offerTypeView.addSubview(offerTypeTextField)
        
        containerView.addSubview(addOfferButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0)
        ])
    }
    
    private func setupAddImagesContainerViewConstraints() {
        NSLayoutConstraint.activate([
            addImagesContainerView.topAnchor.constraint(equalTo: containerView.topAnchor),
            addImagesContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            addImagesContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            addImagesContainerView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func setupAddImagesButtonConstraints() {
        NSLayoutConstraint.activate([
            addImagesButton.heightAnchor.constraint(equalToConstant: 50),
            addImagesButton.widthAnchor.constraint(equalToConstant: 50),
            addImagesButton.topAnchor.constraint(equalTo: addImagesContainerView.topAnchor, constant: 36.5),
            addImagesButton.centerXAnchor.constraint(equalTo: addImagesContainerView.centerXAnchor)
        ])
    }
    
    private func setupAddImagesLabelConstraints() {
        NSLayoutConstraint.activate([
            addImagesLabel.centerXAnchor.constraint(equalTo: addImagesContainerView.centerXAnchor),
            addImagesLabel.topAnchor.constraint(equalTo: addImagesButton.bottomAnchor, constant: 5)
        ])
    }
    
    private func setupImagesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            imagesCollectionView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func setupOfferTitleTextFieldTextFeildConstraints() {
        NSLayoutConstraint.activate([
            offerTitleTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupImagesStackViewConstraints() {
        NSLayoutConstraint.activate([
            imagesStackView.topAnchor.constraint(equalTo: addImagesContainerView.bottomAnchor, constant: 15),
            imagesStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            imagesStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupDepartureContainerViewConstraints() {
        NSLayoutConstraint.activate([
            departureContainerView.topAnchor.constraint(equalTo: imagesStackView.bottomAnchor, constant: 15),
            departureContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            departureContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupDepartureCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            departureCountryLabel.topAnchor.constraint(equalTo: departureContainerView.topAnchor, constant: 13),
            departureCountryLabel.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupDepartureCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            departureCountryTextField.topAnchor.constraint(equalTo: departureCountryLabel.bottomAnchor, constant: 13),
            departureCountryTextField.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 14.5),
            departureCountryTextField.trailingAnchor.constraint(equalTo: departureContainerView.trailingAnchor, constant: -8),
            departureCountryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupDepartureCityLabelConstraints() {
        NSLayoutConstraint.activate([
            departureCityLabel.topAnchor.constraint(equalTo: departureCountryTextField.bottomAnchor, constant: 14),
            departureCityLabel.leadingAnchor.constraint(equalTo: departureCountryLabel.leadingAnchor, constant: 0),
        ])
    }
    
    private func setupDepartureCityTextFieldConstraints() {
        NSLayoutConstraint.activate([
            departureCityTextField.topAnchor.constraint(equalTo: departureCityLabel.bottomAnchor, constant: 13),
            departureCityTextField.leadingAnchor.constraint(equalTo: departureCountryTextField.leadingAnchor, constant: 0),
            departureCityTextField.trailingAnchor.constraint(equalTo: departureCountryTextField.trailingAnchor, constant: 0),
            departureCityTextField.heightAnchor.constraint(equalToConstant: 61),
            departureCityTextField.bottomAnchor.constraint(equalTo: departureContainerView.bottomAnchor, constant: -26)
        ])
    }
    
    private func setupFlightImageViewConstraints() {
        NSLayoutConstraint.activate([
            flightImageView.heightAnchor.constraint(equalToConstant: 40),
            flightImageView.widthAnchor.constraint(equalToConstant: 40),
            flightImageView.topAnchor.constraint(equalTo: departureContainerView.bottomAnchor, constant: 19),
            flightImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupArrivalContainerViewConstraints() {
        NSLayoutConstraint.activate([
            arrivalContainerView.topAnchor.constraint(equalTo: flightImageView.bottomAnchor, constant: 19),
            arrivalContainerView.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 0),
            arrivalContainerView.trailingAnchor.constraint(equalTo: departureContainerView.trailingAnchor, constant: 0),
        ])
    }
    
    
    private func setupArrivalCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            arrivalCountryLabel.topAnchor.constraint(equalTo: arrivalContainerView.topAnchor, constant: 13),
            arrivalCountryLabel.leadingAnchor.constraint(equalTo: arrivalContainerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupArrivalCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            arrivalCountryTextField.topAnchor.constraint(equalTo: arrivalCountryLabel.bottomAnchor, constant: 13),
            arrivalCountryTextField.leadingAnchor.constraint(equalTo: arrivalContainerView.leadingAnchor, constant: 14.5),
            arrivalCountryTextField.trailingAnchor.constraint(equalTo: arrivalContainerView.trailingAnchor, constant: -8),
            arrivalCountryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupArrivalCityLabelConstraints() {
        NSLayoutConstraint.activate([
            arrivalCityLabel.topAnchor.constraint(equalTo: arrivalCountryTextField.bottomAnchor, constant: 14),
            arrivalCityLabel.leadingAnchor.constraint(equalTo: arrivalCountryLabel.leadingAnchor, constant: 0),
        ])
    }
    
    private func setupArrivalCityTextFieldConstraints() {
        NSLayoutConstraint.activate([
            arrivalCityTextField.topAnchor.constraint(equalTo: arrivalCityLabel.bottomAnchor, constant: 13),
            arrivalCityTextField.leadingAnchor.constraint(equalTo: arrivalCountryTextField.leadingAnchor, constant: 0),
            arrivalCityTextField.trailingAnchor.constraint(equalTo: arrivalCountryTextField.trailingAnchor, constant: 0),
            arrivalCityTextField.heightAnchor.constraint(equalToConstant: 61),
            arrivalCityTextField.bottomAnchor.constraint(equalTo: arrivalContainerView.bottomAnchor, constant: -26)
        ])
    }
    
    private func setupFromDateTextFieldConstraints() {
        NSLayoutConstraint.activate([
            fromDateTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupdOfferPeriodLabelConstraints() {
        NSLayoutConstraint.activate([
            offerPeriodLabel.topAnchor.constraint(equalTo: arrivalContainerView.bottomAnchor, constant: 12),
            offerPeriodLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupdDateStackViewConstraints() {
        NSLayoutConstraint.activate([
            dateStackView.topAnchor.constraint(equalTo: offerPeriodLabel.bottomAnchor, constant: 15),
            dateStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            dateStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupdOfferDaysPeriodLabelConstraints() {
        NSLayoutConstraint.activate([
            offerDaysPeriodLabel.topAnchor.constraint(equalTo: dateStackView.bottomAnchor, constant: 12),
            offerDaysPeriodLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupOfferDaysPeriodTextFieldConstraints() {
        NSLayoutConstraint.activate([
            offerDaysPeriodTextField.topAnchor.constraint(equalTo: offerDaysPeriodLabel.bottomAnchor, constant: 15),
            offerDaysPeriodTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            offerDaysPeriodTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            offerDaysPeriodTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupdFlightCompanyNameLabelConstraints() {
        NSLayoutConstraint.activate([
            flightCompanyNameLabel.topAnchor.constraint(equalTo: offerDaysPeriodTextField.bottomAnchor, constant: 12),
            flightCompanyNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupFlightCompanyNameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            flightCompanyNameTextField.topAnchor.constraint(equalTo: flightCompanyNameLabel.bottomAnchor, constant: 15),
            flightCompanyNameTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            flightCompanyNameTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            flightCompanyNameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupdFlightClassLabelConstraints() {
        NSLayoutConstraint.activate([
            flightClassLabel.topAnchor.constraint(equalTo: flightCompanyNameTextField.bottomAnchor, constant: 12),
            flightClassLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupFlightClassTextFieldConstraints() {
        NSLayoutConstraint.activate([
            flightClassTextField.topAnchor.constraint(equalTo: flightClassLabel.bottomAnchor, constant: 15),
            flightClassTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            flightClassTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            flightClassTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupTransitLabelConstraints() {
        NSLayoutConstraint.activate([
            transitLabel.topAnchor.constraint(equalTo: flightClassTextField.bottomAnchor, constant: 12),
            transitLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupYesButtonConstraints() {
        NSLayoutConstraint.activate([
            yesButton.topAnchor.constraint(equalTo: transitLabel.bottomAnchor, constant: 8),
            yesButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            yesButton.widthAnchor.constraint(equalToConstant: 55) ,
            yesButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupNoButtonConstraints() {
        NSLayoutConstraint.activate([
            noButton.topAnchor.constraint(equalTo: transitLabel.bottomAnchor, constant: 8),
            noButton.leadingAnchor.constraint(equalTo: yesButton.trailingAnchor, constant: 10),
            noButton.widthAnchor.constraint(equalToConstant: 55) ,
            noButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupdTransitCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            transitCountryLabel.topAnchor.constraint(equalTo: transitView.topAnchor, constant: 5),
            transitCountryLabel.leadingAnchor.constraint(equalTo: transitView.leadingAnchor, constant: 0)
        ])
    }
    
    private func setupTransitCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            transitCountryTextField.topAnchor.constraint(equalTo: transitCountryLabel.bottomAnchor, constant: 15),
            transitCountryTextField.leadingAnchor.constraint(equalTo: transitView.leadingAnchor, constant: 0),
            transitCountryTextField.trailingAnchor.constraint(equalTo: transitView.trailingAnchor, constant: 0),
            transitCountryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupTransitTimeLabelConstraints() {
        NSLayoutConstraint.activate([
            transitTimeLabel.topAnchor.constraint(equalTo: transitCountryTextField.bottomAnchor, constant: 12),
            transitTimeLabel.leadingAnchor.constraint(equalTo: transitView.leadingAnchor, constant: 0)
        ])
    }
    
    private func setupTranistTimeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            transitTimeTextField.topAnchor.constraint(equalTo: transitTimeLabel.bottomAnchor, constant: 15),
            transitTimeTextField.leadingAnchor.constraint(equalTo: transitView.leadingAnchor, constant: 0),
            transitTimeTextField.trailingAnchor.constraint(equalTo: transitView.trailingAnchor, constant: 0),
            transitTimeTextField.heightAnchor.constraint(equalToConstant: 61),
            transitTimeTextField.bottomAnchor.constraint(equalTo: transitView.bottomAnchor, constant: -10)
        ])
    }
    
    private func setupTransitStackViewConstraints() {
        NSLayoutConstraint.activate([
            transitStackView.topAnchor.constraint(equalTo: yesButton.bottomAnchor, constant: 12),
            transitStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            transitStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupHotelNameLabelConstraints() {
        NSLayoutConstraint.activate([
            hotelNameLabel.topAnchor.constraint(equalTo: transitStackView.bottomAnchor, constant: 12),
            hotelNameLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupHotelNameTextFieldConstraints() {
        NSLayoutConstraint.activate([
            hotelNameTextField.topAnchor.constraint(equalTo: hotelNameLabel.bottomAnchor, constant: 15),
            hotelNameTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            hotelNameTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            hotelNameTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupHotelStarsLabelConstraints() {
        NSLayoutConstraint.activate([
            hotelStarsLabel.topAnchor.constraint(equalTo: hotelNameTextField.bottomAnchor, constant: 12),
            hotelStarsLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupHotelStarsViewConstraints() {
        NSLayoutConstraint.activate([
            hotelStarsView.topAnchor.constraint(equalTo: hotelStarsLabel.bottomAnchor, constant: 15),
            hotelStarsView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupServiceCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            servicesCollectionView.heightAnchor.constraint(equalToConstant: 70)
        ])
    }
    
    private func setupChooseServiceViewConstraints() {
        NSLayoutConstraint.activate([
            chooseServiceView.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupAddServicesButtonConstraints() {
        NSLayoutConstraint.activate([
            addServicesButton.topAnchor.constraint(equalTo: chooseServiceView.topAnchor, constant: 0),
            addServicesButton.bottomAnchor.constraint(equalTo: chooseServiceView.bottomAnchor, constant: 0),
            addServicesButton.leadingAnchor.constraint(equalTo: chooseServiceView.leadingAnchor, constant: 20),
            addServicesButton.trailingAnchor.constraint(equalTo: chooseServiceView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupForwardImageViewConstrints() {
        NSLayoutConstraint.activate([
            forwardImageView.centerYAnchor.constraint(equalTo: chooseServiceView.centerYAnchor),
            forwardImageView.trailingAnchor.constraint(equalTo: chooseServiceView.trailingAnchor, constant: -20),
            forwardImageView.widthAnchor.constraint(equalToConstant: 20),
            forwardImageView.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    
    private func setupServiceLabelConstraints() {
        NSLayoutConstraint.activate([
            serviceLabel.topAnchor.constraint(equalTo: hotelStarsView.bottomAnchor, constant: 12),
            serviceLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupServiceStackViewConstraints() {
        NSLayoutConstraint.activate([
            serviceStackView.topAnchor.constraint(equalTo: serviceLabel.bottomAnchor, constant: 15),
            serviceStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            serviceStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupOfferValueLabelConstraints() {
        NSLayoutConstraint.activate([
            offerValueLabel.topAnchor.constraint(equalTo: serviceStackView.bottomAnchor, constant: 12),
            offerValueLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupAdultLabelConstraints() {
        NSLayoutConstraint.activate([
            adultLabel.topAnchor.constraint(equalTo: adultView.topAnchor, constant: 10),
            adultLabel.leadingAnchor.constraint(equalTo: adultView.leadingAnchor, constant: 10)
        ])
    }
    
    private func setupAdultPriceTextFieldConstraints() {
        NSLayoutConstraint.activate([
            adultPriceTextField.topAnchor.constraint(equalTo: adultLabel.bottomAnchor, constant: 10),
            adultPriceTextField.leadingAnchor.constraint(equalTo: adultView.leadingAnchor, constant: 10),
            adultPriceTextField.trailingAnchor.constraint(equalTo: adultView.trailingAnchor, constant: -10),
            adultPriceTextField.heightAnchor.constraint(equalToConstant: 61),
            adultPriceTextField.bottomAnchor.constraint(equalTo: adultView.bottomAnchor, constant: -10)
        ])
    }
    
    private func setupChildLabelConstraints() {
        NSLayoutConstraint.activate([
            childLabel.topAnchor.constraint(equalTo: childView.topAnchor, constant: 10),
            childLabel.leadingAnchor.constraint(equalTo: childView.leadingAnchor, constant: 10)
        ])
    }
    
    private func setupChildPriceTextFieldConstraints() {
        NSLayoutConstraint.activate([
            childPriceTextField.topAnchor.constraint(equalTo: childLabel.bottomAnchor, constant: 10),
            childPriceTextField.leadingAnchor.constraint(equalTo: childView.leadingAnchor, constant: 10),
            childPriceTextField.trailingAnchor.constraint(equalTo: childView.trailingAnchor, constant: -10),
            childPriceTextField.heightAnchor.constraint(equalToConstant: 61),
            childPriceTextField.bottomAnchor.constraint(equalTo: childView.bottomAnchor, constant: -10)
        ])
    }
    
    private func setupOfferValueStackViewConstraints() {
        NSLayoutConstraint.activate([
            offerValueStackView.topAnchor.constraint(equalTo: offerValueLabel.bottomAnchor, constant: 15),
            offerValueStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 6),
            offerValueStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupCurrencyLabelConstraints() {
        NSLayoutConstraint.activate([
            currencyLabel.topAnchor.constraint(equalTo: offerValueStackView.bottomAnchor, constant: 12),
            currencyLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupCurrencyTextFieldConstraints() {
        NSLayoutConstraint.activate([
            currencyTextField.topAnchor.constraint(equalTo: currencyLabel.bottomAnchor, constant: 15),
            currencyTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            currencyTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            currencyTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupOfferTypeViewConstraints() {
        NSLayoutConstraint.activate([
            offerTypeView.topAnchor.constraint(equalTo: currencyTextField.bottomAnchor, constant: 15),
            offerTypeView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            offerTypeView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupOfferTypeLabelConstraints() {
        NSLayoutConstraint.activate([
            offerTypeLabel.topAnchor.constraint(equalTo: offerTypeView.topAnchor, constant: 12),
            offerTypeLabel.leadingAnchor.constraint(equalTo: offerTypeView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupOfferTypeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            offerTypeTextField.topAnchor.constraint(equalTo: offerTypeLabel.bottomAnchor, constant: 15),
            offerTypeTextField.leadingAnchor.constraint(equalTo: offerTypeView.leadingAnchor, constant: 15),
            offerTypeTextField.trailingAnchor.constraint(equalTo: offerTypeView.trailingAnchor, constant: -15),
            offerTypeTextField.heightAnchor.constraint(equalToConstant: 61),
            offerTypeTextField.bottomAnchor.constraint(equalTo: offerTypeView.bottomAnchor, constant: -24)
        ])
    }
    
    private func setupAddOfferButtonConstraints() {
        NSLayoutConstraint.activate([
            addOfferButton.topAnchor.constraint(equalTo: offerTypeView.bottomAnchor, constant: 20),
            addOfferButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            addOfferButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            addOfferButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15),
            addOfferButton.heightAnchor.constraint(equalToConstant: 50)

        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupAddImagesContainerViewConstraints()
        setupAddImagesButtonConstraints()
        setupAddImagesLabelConstraints()
        setupImagesCollectionViewConstraints()
        setupOfferTitleTextFieldTextFeildConstraints()
        setupImagesStackViewConstraints()
        
        setupDepartureContainerViewConstraints()
        setupDepartureCountryLabelConstraints()
        setupDepartureCountryTextFieldConstraints()
        setupDepartureCityLabelConstraints()
        setupDepartureCityTextFieldConstraints()
        setupFlightImageViewConstraints()
        setupArrivalContainerViewConstraints()
        setupArrivalCountryLabelConstraints()
        setupArrivalCountryTextFieldConstraints()
        setupArrivalCityLabelConstraints()
        setupArrivalCityTextFieldConstraints()
        setupFromDateTextFieldConstraints()
        
        setupdOfferPeriodLabelConstraints()
        setupdDateStackViewConstraints()
        
        setupdOfferDaysPeriodLabelConstraints()
        setupOfferDaysPeriodTextFieldConstraints()
        setupdFlightCompanyNameLabelConstraints()
        setupFlightCompanyNameTextFieldConstraints()
        
        setupdFlightClassLabelConstraints()
        setupFlightClassTextFieldConstraints()
        
        setupTransitLabelConstraints()
        setupYesButtonConstraints()
        setupNoButtonConstraints()
        
        setupdTransitCountryLabelConstraints()
        setupTransitCountryTextFieldConstraints()
        setupTransitTimeLabelConstraints()
        setupTranistTimeTextFieldConstraints()
        setupTransitStackViewConstraints()
        
        setupHotelNameLabelConstraints()
        setupHotelNameTextFieldConstraints()
        
        setupHotelStarsLabelConstraints()
        setupHotelStarsViewConstraints()
        
        setupServiceLabelConstraints()
        setupServiceCollectionViewConstraints()
        setupChooseServiceViewConstraints()
        setupServiceStackViewConstraints()
        
        setupOfferValueLabelConstraints()
        setupAdultLabelConstraints()
        setupChildLabelConstraints()
        setupAdultPriceTextFieldConstraints()
        setupChildPriceTextFieldConstraints()
        setupOfferValueStackViewConstraints()
        
        setupCurrencyLabelConstraints()
        setupCurrencyTextFieldConstraints()
        
        setupOfferTypeViewConstraints()
        setupOfferTypeLabelConstraints()
        setupOfferTypeTextFieldConstraints()
        setupAddOfferButtonConstraints()
        
        setupAddServicesButtonConstraints()
        setupForwardImageViewConstrints()
    }
    
    
    func setButtonImages(selectedButton: UIButton, unselectedButton: UIButton) {
        unselectedButton.setImage(UIImage(named: "uncheck_circle")?.withRenderingMode(.alwaysOriginal), for: .normal)
        selectedButton.setImage(UIImage(named: "check_circle")?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    func resetInputsValues() {
        offerTitleTextField.text = ""
        departureCountryTextField.text = ""
        departureCityTextField.text = ""
        arrivalCountryTextField.text = ""
        arrivalCityTextField.text = ""
        fromDateTextField.text = ""
        toDateTextField.text = ""
        offerDaysPeriodTextField.text = ""
        flightCompanyNameTextField.text = ""
        flightClassTextField.text = ""
        transitCountryTextField.text = ""
        transitTimeTextField.text = ""
        hotelNameTextField.text = ""
        hotelStarsView.rating = 5.0
        adultPriceTextField.text = ""
        childPriceTextField.text = ""
        offerTypeTextField.text = ""
        currencyTextField.text = ""
        transitView.isHidden = false
        imagesCollectionView.isHidden = true
        setButtonImages(selectedButton: yesButton, unselectedButton: noButton)
        transitTimeTextField.isEnabled = true
        transitCountryTextField.isEnabled = true
    }
    
}
