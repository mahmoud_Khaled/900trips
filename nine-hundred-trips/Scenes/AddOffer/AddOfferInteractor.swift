//
//  AddOfferInteractor.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AddOfferInteractor: AddOfferInteractorInputProtocol {

    weak var presenter: AddOfferInteractorOutputProtocol?
    
    private let offersWorker = OffersWorker()
    
    func getAddOfferData() {
        offersWorker.getDataOfAddOffer { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchAddOfferData(with: result)
        }
    }
    
    func uploadImages(images: [File]) {
        offersWorker.uploadOfferImages(files: images) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didUploadImages(with: result)
        }
    }
    
    func addOffer(offerData: AddOfferModel, selectedServices: [Int], photos: [String]) {
        
        
        let parameters: [String: Any] = [
            "title": offerData.title,
            "from_city": offerData.departureCityId,
            "to_city": offerData.arrivalCityId,
            "from_date": offerData.fromDate,
            "to_date": offerData.toDate,
            "nights": offerData.offerDays,
            "flight_company": offerData.flightCompanyId,
            "flight_seat_type": offerData.flightSeatTypId,
            "hotel_name": offerData.hotelName,
            "hotel_stars": offerData.hotelRate,
            "adult_price": offerData.adultPrice,
            "child_price": offerData.childPrice,
            "currency_id": offerData.currencyId,
            "transit": offerData.transit.rawValue,
            "transit_country": offerData.transitCountryId,
            "stop_minutes": offerData.transitTime,
            "offer_type": offerData.offerTypeId,
            "services": selectedServices,
            "photos": photos
        ]
        
        offersWorker.addOffer(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddOffer(with: result)
        }
        
    }
}
