//
//  ChoosenService.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/26/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChosenServiceCell: UICollectionViewCell, ConfigurableCell {
    
    var deleteButtonTapped: (() -> ())?
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 9
        view.layer.masksToBounds = true
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var serviceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .black
        return imageView
    }()
    
    private lazy var serviceNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.textColor = .textColor
        label.text = "مسجد أسد إبن الفرات"
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var deleteButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .red
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addTarget()
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(serviceImageView)
        containerView.addSubview(serviceNameLabel)
        containerView.addSubview(deleteButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4)
        ])
    }
    
    private func setupServiceImageViewConstraints() {
        NSLayoutConstraint.activate([
            serviceImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 5),
            serviceImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            serviceImageView.heightAnchor.constraint(equalToConstant: 32),
            serviceImageView.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    private func setupServiceNameLabelConstraints() {
        NSLayoutConstraint.activate([
            serviceNameLabel.leadingAnchor.constraint(equalTo: serviceImageView.trailingAnchor, constant: 5),
            serviceNameLabel.centerYAnchor.constraint(equalTo: serviceImageView.centerYAnchor),
            serviceNameLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -2),
            serviceNameLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupDeleteButtonConstraints() {
        NSLayoutConstraint.activate([
            deleteButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -5),
            deleteButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            deleteButton.heightAnchor.constraint(equalToConstant: 15),
            deleteButton.widthAnchor.constraint(equalToConstant: 15)
        ])
    }
   
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupServiceImageViewConstraints()
        setupServiceNameLabelConstraints()
        setupDeleteButtonConstraints()
    }
    
    func configure(model: ServiceViewModel) {
        serviceImageView.load(url: model.imageUrl)
        serviceNameLabel.text = model.name
    }
    
    func addTarget() {
        deleteButton.addTarget(self, action: #selector(deleteButtonTapped(_:)), for: .touchUpInside)
    }
    
    @objc private func deleteButtonTapped(_ sender: UIButton) {
        deleteButtonTapped?()
    }
    
}
