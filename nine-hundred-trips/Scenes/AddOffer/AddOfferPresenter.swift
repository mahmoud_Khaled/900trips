//
//  AddOfferPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AddOfferPresenter: AddOfferPresenterProtocol, AddOfferInteractorOutputProtocol, ChooseServiceDelegate {
    
    weak var view: AddOfferViewProtocol?
    private let interactor: AddOfferInteractorInputProtocol
    private let router: AddOfferRouterProtocol
    
    private var services: [ServiceViewModel] = []
    private var selectedServices: [ServiceViewModel] = []
    private var selectedServicesIds: [Int] = []
    private var countries: [CountryViewModel] = []
    private var currencies: [CurrencyViewModel] = []
    private var flightCompanies: [FlightCompany] = []
    private var flightSeatType: [FlightSeatType] = []
    private let offerTypes = OfferType.createOfferTypes()
    
    private var offerData = AddOfferModel()
    
    var numberOfRows: Int {
        return selectedServices.count
    }
    
    init(view: AddOfferViewProtocol, interactor: AddOfferInteractorInputProtocol, router: AddOfferRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func uploadImagesButtonTapped() {
        router.presenteImagePicker()
    }
    
    func showPermissionAlert(with title: String, message: String) {
        router.showPermissionAlert(with: title, message: message)
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getAddOfferData()
    }
    
    func didFetchAddOfferData(with result: Result<AddOfferData>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            let addOfferData = AddOfferDataViewModel(data: value)
            services = addOfferData.services
            countries = addOfferData.countries
            currencies = addOfferData.currencies
            flightCompanies = addOfferData.flightCompnaies
            flightSeatType = addOfferData.flightSeatTypes
            
            view?.setAllPickerData(services: services, countries: countries, currencies: currencies, flightCompanies: flightCompanies, flightSeatType: flightSeatType, offerTypes: offerTypes)
            
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath) {
        cell.configure(model: selectedServices[indexPath.row])
    }
    
    func addServicesButtonTapped() {
        router.navigate(to: .addServices(services: services, selectedServices: selectedServices, delegate: self))
    }
    
    func didChooseServices(services: [ServiceViewModel], selectedServices: [ServiceViewModel]) {
        self.services = services
        self.selectedServices = selectedServices
        
        if selectedServices.count == 0 {
            view?.toggleCollectionViewHidden(isHidden: true)
            view?.reloadServiceCollectionView()
        } else {
            view?.reloadServiceCollectionView()
            view?.toggleCollectionViewHidden(isHidden: false)
        }
        
    }
    
    func deleteServiceButtonTapped(at indexPath: IndexPath) {
        let id = selectedServices[indexPath.row].id
        selectedServices = selectedServices.filter({ $0.id != id })
        
        for (index, item) in services.enumerated() where item.id == id {
            services[index].isSelected = false
        }
        
        view?.reloadServiceCollectionView()
        
        if selectedServices.count == 0 {
            view?.toggleCollectionViewHidden(isHidden: true)
        }
    }
    
    func addOfferButtonTapped(offerData: AddOfferModel) {
        
        guard offerData.photos.count > 0 else {
            router.showAlert(with: "noImagesAlert".localized())
            return
        }
        
        guard !offerData.title.isEmpty else {
            router.showAlert(with: "noOfferTitle".localized())
            return
        }
        
        guard offerData.departureCityId != 0 else {
            router.showAlert(with: "emptyDepartureCity".localized())
            return
        }
        
        guard offerData.arrivalCityId != 0 else {
            router.showAlert(with: "emptyArrivalCity".localized())
            return
        }
        
        guard !offerData.fromDate.isEmpty else {
            router.showAlert(with: "noFromDate".localized())
            return
        }
        
        guard !offerData.toDate.isEmpty else {
            router.showAlert(with: "noToDate".localized())
            return
        }
        
        guard offerData.toDate >= offerData.fromDate else {
            router.showAlert(with: "dateError".localized())
            return
        }
        
        guard !offerData.offerDays.isEmpty else {
            router.showAlert(with: "noOfferDays".localized())
            return
        }
        
        guard offerData.flightCompanyId != 0 else {
            router.showAlert(with: "noFlightCompany".localized())
            return
        }
        
        guard offerData.flightSeatTypId != 0 else {
            router.showAlert(with: "noFlightSeatType".localized())
            return
        }
        
        if offerData.transit == .yes {
            
            guard offerData.transitCountryId != 0 else {
                router.showAlert(with: "noTransitCountry".localized())
                return
            }
            
            guard !offerData.transitTime.isEmpty else {
                router.showAlert(with: "noTransitTime".localized())
                return
            }
        }
        
        guard !offerData.hotelName.isEmpty else {
            router.showAlert(with: "noHotelName".localized())
            return
        }
        
        guard offerData.hotelRate != 0 else {
            router.showAlert(with: "noHotelRate" .localized())
            return
        }
        
        guard selectedServices.count != 0 else {
            router.showAlert(with: "noServices".localized())
            return
        }
        
        selectedServicesIds = []
        selectedServicesIds.append(contentsOf: selectedServices.map({ $0.id }))
        
        
        guard  !offerData.adultPrice.isEmpty else {
            router.showAlert(with: "noAdultPrice".localized())
            return
        }
        
        guard  !offerData.childPrice.isEmpty else {
            router.showAlert(with: "noChildPrice".localized())
            return
        }
        
        guard offerData.currencyId != 0 else {
            router.showAlert(with: "noCurrencyId".localized())
            return
        }

        guard offerData.offerTypeId >= 0 else {
            router.showAlert(with: "noOfferType" .localized())
            return
        }
        
        self.offerData = offerData
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        interactor.uploadImages(images: offerData.photos)
        
    }
    
    func didUploadImages(with result: Result<[String]>) {
        switch result {
        case .success(let value):
            interactor.addOffer(offerData: offerData, selectedServices: selectedServicesIds, photos: value)
        case .failure(let error):
            view?.hideActivityIndicator()
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didAddOffer(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case.failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func resetValues() {
        selectedServicesIds = []
        selectedServices = []
        for i in 0...services.count - 1 {
            services[i].isSelected = false
        }
        view?.reloadServiceCollectionView()
        view?.toggleCollectionViewHidden(isHidden: true)
    }
    
    func titleForService(at indexPath: IndexPath) -> String {
        return selectedServices[indexPath.item].name
    }
}

