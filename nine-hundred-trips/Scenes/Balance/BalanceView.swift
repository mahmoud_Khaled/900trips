//
//  BalancView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BalanceView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var subscriptionView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var progressView: ProgressView = {
        let view = ProgressView(radius: 40, strokeColor: .coral, lineWidth: 6, frame: CGRect(x: 0, y: 0, width: 102, height: 102))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.strokeEnd = 0
        return view
    }()
    
    private lazy var daysLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.heavy.getFont(ofSize: 30)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subscriptionTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "totalSubscription".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subscriptionValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 21)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var offersView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var offerSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var doneOffersTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "offersDone".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    private lazy var doneOffersValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var remainigOffersTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "remainingOffers".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var remainigOffersValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    
    private lazy var specialOffersView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var specialOfferSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var specialDoneOffersTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "specialOffersDone".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    private lazy var specialDoneOffersValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var specialRemainigOffersTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "remainingSpecialOffers".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var specialRemainigOffersValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "0"
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(subscriptionView)
        subscriptionView.addSubview(progressView)
        subscriptionView.addSubview(daysLabel)
        subscriptionView.addSubview(subscriptionTitleLabel)
        subscriptionView.addSubview(subscriptionValueLabel)
        
        containerView.addSubview(offersView)
        
        offersView.addSubview(offerSeperatorView)
        offersView.addSubview(doneOffersTitle)
        offersView.addSubview(doneOffersValueLabel)
        offersView.addSubview(remainigOffersTitleLabel)
        offersView.addSubview(remainigOffersValueLabel)
        
        containerView.addSubview(specialOffersView)
        
        specialOffersView.addSubview(specialOfferSeperatorView)
        specialOffersView.addSubview(specialDoneOffersTitle)
        specialOffersView.addSubview(specialDoneOffersValueLabel)
        specialOffersView.addSubview(specialRemainigOffersTitleLabel)
        specialOffersView.addSubview(specialRemainigOffersValueLabel)
        
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0)
        ])
    }
    
    private func setupSubscriptionViewConstraints() {
        NSLayoutConstraint.activate([
            subscriptionView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            subscriptionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            subscriptionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
        ])
    }
    
    private func setupProgressViewConstraints() {
        NSLayoutConstraint.activate([
            progressView.topAnchor.constraint(equalTo: subscriptionView.topAnchor, constant: 10),
            progressView.centerXAnchor.constraint(equalTo: subscriptionView.centerXAnchor),
            progressView.widthAnchor.constraint(equalToConstant: 102),
            progressView.heightAnchor.constraint(equalToConstant: 102)
        ])
        progressView.layoutIfNeeded()
    }
    
    private func setupDaysLabelConstraints() {
        NSLayoutConstraint.activate([
            daysLabel.centerXAnchor.constraint(equalTo: progressView.centerXAnchor),
            daysLabel.centerYAnchor.constraint(equalTo: progressView.centerYAnchor)
        ])
    }
    
    private func setupSubscriptionTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            subscriptionTitleLabel.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 15),
            subscriptionTitleLabel.bottomAnchor.constraint(equalTo: subscriptionView.bottomAnchor, constant: -10),
            subscriptionTitleLabel.leadingAnchor.constraint(equalTo: subscriptionView.leadingAnchor, constant: 10)
        ])
    }
    
    private func setupSubscriptionValueLabelConstraints() {
        NSLayoutConstraint.activate([
            subscriptionValueLabel.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 15),
            subscriptionValueLabel.bottomAnchor.constraint(equalTo: subscriptionView.bottomAnchor, constant: -10),
            subscriptionValueLabel.trailingAnchor.constraint(equalTo: subscriptionView.trailingAnchor, constant: -10)
        ])
    }
    
    private func setupOffersViewConstraints() {
        NSLayoutConstraint.activate([
            offersView.topAnchor.constraint(equalTo: subscriptionView.bottomAnchor, constant: 15),
            offersView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            offersView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            offersView.heightAnchor.constraint(equalToConstant: 101)
        ])
    }
    
    private func setupOfferSeperatorViewConstraints() {
        NSLayoutConstraint.activate([
            offerSeperatorView.centerYAnchor.constraint(equalTo: offersView.centerYAnchor),
            offerSeperatorView.leadingAnchor.constraint(equalTo: offersView.leadingAnchor, constant: 0),
            offerSeperatorView.trailingAnchor.constraint(equalTo: offersView.trailingAnchor, constant: 0),
            offerSeperatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupDoneOffersTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            doneOffersTitle.topAnchor.constraint(equalTo: offersView.topAnchor, constant: 15),
            doneOffersTitle.leadingAnchor.constraint(equalTo: offersView.leadingAnchor, constant: 15),
        ])
    }
    
    private func setupDoneOffersValueLabelConstraints() {
        NSLayoutConstraint.activate([
            doneOffersValueLabel.topAnchor.constraint(equalTo: offersView.topAnchor, constant: 15),
            doneOffersValueLabel.trailingAnchor.constraint(equalTo: offersView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupRemainigOffersTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            remainigOffersTitleLabel.bottomAnchor.constraint(equalTo: offersView.bottomAnchor, constant: -15),
            remainigOffersTitleLabel.leadingAnchor.constraint(equalTo: offersView.leadingAnchor, constant: 15),
        ])
    }
    
    private func setupRemainigOffersValueLabelConstraints() {
        NSLayoutConstraint.activate([
            remainigOffersValueLabel.bottomAnchor.constraint(equalTo: offersView.bottomAnchor, constant: -15),
            remainigOffersValueLabel.trailingAnchor.constraint(equalTo: offersView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupSpecialOffersViewConstraints() {
        NSLayoutConstraint.activate([
            specialOffersView.topAnchor.constraint(equalTo: offersView.bottomAnchor, constant: 15),
            specialOffersView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            specialOffersView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            specialOffersView.heightAnchor.constraint(equalToConstant: 101)
        ])
    }
    
    private func setupSpecialOfferSeperatorViewConstraints() {
        NSLayoutConstraint.activate([
            specialOfferSeperatorView.centerYAnchor.constraint(equalTo: specialOffersView.centerYAnchor),
            specialOfferSeperatorView.leadingAnchor.constraint(equalTo: specialOffersView.leadingAnchor, constant: 0),
            specialOfferSeperatorView.trailingAnchor.constraint(equalTo: specialOffersView.trailingAnchor, constant: 0),
            specialOfferSeperatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupSpecialDoneOffersTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            specialDoneOffersTitle.topAnchor.constraint(equalTo: specialOffersView.topAnchor, constant: 15),
            specialDoneOffersTitle.leadingAnchor.constraint(equalTo: specialOffersView.leadingAnchor, constant: 15),
        ])
    }
    
    private func setupSpecialDoneOffersValueLabelConstraints() {
        NSLayoutConstraint.activate([
            specialDoneOffersValueLabel.topAnchor.constraint(equalTo: specialOffersView.topAnchor, constant: 15),
            specialDoneOffersValueLabel.trailingAnchor.constraint(equalTo: specialOffersView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupSpecialRemainigOffersTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            specialRemainigOffersTitleLabel.bottomAnchor.constraint(equalTo: specialOffersView.bottomAnchor, constant: -15),
            specialRemainigOffersTitleLabel.leadingAnchor.constraint(equalTo: specialOffersView.leadingAnchor, constant: 15),
        ])
    }
    
    private func setupSpecialRemainigOffersValueLabelConstraints() {
        NSLayoutConstraint.activate([
            specialRemainigOffersValueLabel.bottomAnchor.constraint(equalTo: specialOffersView.bottomAnchor, constant: -15),
            specialRemainigOffersValueLabel.trailingAnchor.constraint(equalTo: specialOffersView.trailingAnchor, constant: -15),
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupSubscriptionViewConstraints()
        setupProgressViewConstraints()
        setupDaysLabelConstraints()
        setupSubscriptionTitleLabelConstraints()
        setupSubscriptionValueLabelConstraints()
        setupOffersViewConstraints()
        setupOfferSeperatorViewConstraints()
        setupDoneOffersTitleLabelConstraints()
        setupDoneOffersValueLabelConstraints()
        setupRemainigOffersTitleLabelConstraints()
        setupRemainigOffersValueLabelConstraints()
        
        setupSpecialOffersViewConstraints()
        setupSpecialOfferSeperatorViewConstraints()
        setupSpecialDoneOffersTitleLabelConstraints()
        setupSpecialDoneOffersValueLabelConstraints()
        setupSpecialRemainigOffersTitleLabelConstraints()
        setupSpecialRemainigOffersValueLabelConstraints()
    }
    
    func setCurrentSubscriptionPackage(package: SubscriptionPackageViewModel) {
        daysLabel.text = package.remainingDaysText
        subscriptionValueLabel.text = package.price
        progressView.strokeEnd = package.strokeEnd
        doneOffersValueLabel.text = package.doneOffers
        remainigOffersValueLabel.text = package.remainingOffers
        specialDoneOffersValueLabel.text = package.specialDoneOffers
        specialRemainigOffersValueLabel.text = package.specialRemainingOffers
    }
    
}
