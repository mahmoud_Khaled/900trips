//
//  BalancProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol BalanceRouterProtocol: class {
    func showAlert(with message: String)
}

protocol BalancePresenterProtocol: class {
    var view: BalanceViewProtocol? { get set }
    func viewDidLoad()
}

protocol BalanceInteractorInputProtocol: class {
    var presenter: BalanceInteractorOutputProtocol? { get set }
    func getPackagesAndSubscriptions()
}

protocol BalanceInteractorOutputProtocol: class {
   func  didFetchPackagesAndSubscriptions(with result: Result<PackageAndSubscription>)
    
}

protocol BalanceViewProtocol: class {
    var presenter: BalancePresenterProtocol! { get set }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()

    func setCurrentSubscriptionPackage(package: SubscriptionPackageViewModel)
}
