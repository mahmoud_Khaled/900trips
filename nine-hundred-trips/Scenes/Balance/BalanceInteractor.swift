//
//  BalancInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BalanceInteractor: BalanceInteractorInputProtocol {
    
    weak var presenter: BalanceInteractorOutputProtocol?
    
    private let packagesWorker = PackagesWorker()
    
    func getPackagesAndSubscriptions() {
        packagesWorker.getPackagesAndSubscritions {[weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchPackagesAndSubscriptions(with: result)
        }
    }
}
