//
//  BalancPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class BalancePresenter: BalancePresenterProtocol, BalanceInteractorOutputProtocol {
    
    weak var view: BalanceViewProtocol?
    private let interactor: BalanceInteractorInputProtocol
    private let router: BalanceRouterProtocol
    
    init(view: BalanceViewProtocol, interactor: BalanceInteractorInputProtocol, router: BalanceRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getPackagesAndSubscriptions()
    }
    
    func didFetchPackagesAndSubscriptions(with result: Result<PackageAndSubscription>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            view?.setCurrentSubscriptionPackage(package: SubscriptionPackageViewModel(subscriptionPackage: value.current))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
}
