//
//  NewPasswordViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordViewController: BaseViewController, NewPasswordViewProtocol {
    
    private let mainView = NewPasswordView()
    var presenter: NewPasswordPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "resetPassword".localized()
        addTargets()
    }
    
    func showActivityIndicator() {
        view.showProgressHUD(isUserInteractionEnabled: false)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "resetPasswordSuccessful".localized(), on: navigationController?.view, for: 2) { [weak self] in
            guard let self = self else { return }
            self.presenter.dismiss()
        }
    }
    
    private func addTargets() {
        mainView.saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
    }
    
    @objc private func saveButtonTapped() {
        presenter.setNewPassword(activationCode: mainView.activationCodeTextField.text!, newPassword: mainView.newPasswordTextField.text!, confirmNewPassword: mainView.confirmNewPasswordTextField.text!)
    }
}
