//
//  NewPasswordInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordInteractor: NewPasswordInteractorInputProtocol {
    
    weak var presenter: NewPasswordInteractorOutputProtocol?
    
    private let authenticationWorker = AuthenticationWorker()
    
    func setNewPassword(email: String, activationCode: String, newPassword: String, confirmNewPassword: String) {
        authenticationWorker.setNewPassword(email: email, activationCode: activationCode, newPassword: newPassword, confirmNewPassword: confirmNewPassword) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didSetNewPassword(with: result)
        }
    }
}
