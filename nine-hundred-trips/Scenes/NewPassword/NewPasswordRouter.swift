//
//  NewPasswordRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordRouter: NewPasswordRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(with email: String) -> UIViewController {
        let view = NewPasswordViewController()
        let interactor = NewPasswordInteractor()
        let router = NewPasswordRouter()
        let presenter = NewPasswordPresenter(view: view, interactor: interactor, router: router, email: email)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func dismiss() {
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}
