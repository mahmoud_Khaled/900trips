//
//  NewPasswordPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class NewPasswordPresenter: NewPasswordPresenterProtocol, NewPasswordInteractorOutputProtocol {
    
    weak var view: NewPasswordViewProtocol?
    private let interactor: NewPasswordInteractorInputProtocol
    private let router: NewPasswordRouterProtocol
    private let email: String
    
    init(view: NewPasswordViewProtocol, interactor: NewPasswordInteractorInputProtocol, router: NewPasswordRouterProtocol, email: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.email = email
    }
    
    func setNewPassword(activationCode: String, newPassword: String, confirmNewPassword: String) {
        if activationCode.isEmpty {
            router.showAlert(with: "emptyActivationCode".localized())
        } else if newPassword.isEmpty {
            router.showAlert(with: "emptyPassword".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$").evaluate(with: newPassword) {
            router.showAlert(with: "incorrectPassword".localized())
        } else if newPassword != confirmNewPassword {
            router.showAlert(with: "passwordsMismatch".localized())
        } else {
            view?.showActivityIndicator()
            interactor.setNewPassword(email: email, activationCode: activationCode, newPassword: newPassword, confirmNewPassword: confirmNewPassword)
        }
    }
    
    func didSetNewPassword(with result: Result<Data>) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            view?.showSuccessView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func dismiss() {
        router.dismiss()
    }
}
