//
//  NewPasswordView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordView: UIView {
   
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var codeSentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "enterTheCodeSentToYou".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.textColor = .textColor
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var activationCodeTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "activationCode".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private let lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(0xF8F6F6)
        return view
    }()
    
    private lazy var enterNewPasswordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "entreNewPassword".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 20)
        label.textColor = .textColor
        return label
    }()
    
    lazy var newPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "newPassword".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.isSecureTextEntry = true
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var confirmNewPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "repeatNewPassword".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.setIcon(direction: .left, image: UIImage(named: "ic_password"), width: 20, height: 20)
        textField.isSecureTextEntry = true
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("save".localized(), for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(codeSentLabel)
        containerView.addSubview(enterNewPasswordLabel)
        containerView.addSubview(activationCodeTextField)
        containerView.addSubview(lineView)
        containerView.addSubview(newPasswordTextField)
        containerView.addSubview(confirmNewPasswordTextField)
        containerView.addSubview(saveButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupCodeSentLabelConstraints() {
        NSLayoutConstraint.activate([
            codeSentLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 21),
            codeSentLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            codeSentLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupActivationCodeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            activationCodeTextField.topAnchor.constraint(equalTo: codeSentLabel.bottomAnchor, constant: 23),
            activationCodeTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            activationCodeTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            activationCodeTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: activationCodeTextField.bottomAnchor, constant: 31),
            lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupEnterNewPasswordLabelConstraints() {
        NSLayoutConstraint.activate([
            enterNewPasswordLabel.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 30),
            enterNewPasswordLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
        ])
    }
    
    private func setupNewPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            newPasswordTextField.topAnchor.constraint(equalTo: enterNewPasswordLabel.bottomAnchor, constant: 22),
            newPasswordTextField.leadingAnchor.constraint(equalTo: activationCodeTextField.leadingAnchor, constant: 0),
            newPasswordTextField.trailingAnchor.constraint(equalTo: activationCodeTextField.trailingAnchor, constant: 0),
            newPasswordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupConfirmNewPasswordTextFieldConstraints() {
        NSLayoutConstraint.activate([
            confirmNewPasswordTextField.topAnchor.constraint(equalTo: newPasswordTextField.bottomAnchor, constant: 12),
            confirmNewPasswordTextField.leadingAnchor.constraint(equalTo: newPasswordTextField.leadingAnchor, constant: 0),
            confirmNewPasswordTextField.trailingAnchor.constraint(equalTo: newPasswordTextField.trailingAnchor, constant: 0),
            confirmNewPasswordTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupSaveButtonConstraints() {
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: confirmNewPasswordTextField.bottomAnchor, constant: 32),
            saveButton.leadingAnchor.constraint(equalTo: confirmNewPasswordTextField.leadingAnchor),
            saveButton.trailingAnchor.constraint(equalTo: confirmNewPasswordTextField.trailingAnchor),
            saveButton.heightAnchor.constraint(equalToConstant: 50),
            saveButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupCodeSentLabelConstraints()
        setupActivationCodeTextFieldConstraints()
        setupLineViewConstraints()
        setupEnterNewPasswordLabelConstraints()
        setupNewPasswordTextFieldConstraints()
        setupConfirmNewPasswordTextFieldConstraints()
        setupSaveButtonConstraints()
    }
    
}
