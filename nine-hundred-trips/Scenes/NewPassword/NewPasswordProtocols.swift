//
//  NewPasswordProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol NewPasswordRouterProtocol: class {
    func showAlert(with message: String)
    func dismiss()
}

protocol NewPasswordPresenterProtocol: class {
    var view: NewPasswordViewProtocol? { get set }
    func setNewPassword(activationCode: String, newPassword: String, confirmNewPassword: String)
    func dismiss()
}

protocol NewPasswordInteractorInputProtocol: class {
    var presenter: NewPasswordInteractorOutputProtocol? { get set }
    func setNewPassword(email: String, activationCode: String, newPassword: String, confirmNewPassword: String)
}

protocol NewPasswordInteractorOutputProtocol: class {
    func didSetNewPassword(with result: Result<Data>)
}

protocol NewPasswordViewProtocol: class {
    var presenter: NewPasswordPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func showSuccessView()
}
