//
//  UserProfileTableViewCell.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell, ConfigurableCell {
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var optionTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        return label
    }()
    
    private lazy var editImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        layoutUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(iconImageView)
        addSubview(optionTextLabel)
        addSubview(editImageView)
    }
    
    private func setupIconImageViewConstraints() {
        NSLayoutConstraint.activate([
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            iconImageView.heightAnchor.constraint(equalToConstant: 24),
            iconImageView.widthAnchor.constraint(equalToConstant: 24),
            iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8)
        ])
    }
    
    private func setupOptionTextLabelConstraints() {
        NSLayoutConstraint.activate([
            optionTextLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor, constant: -2),
            optionTextLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 12),
            optionTextLabel.trailingAnchor.constraint(equalTo: editImageView.trailingAnchor, constant: -8)
        ])
    }
    
    private func setupEditImageViewConstraints() {
        NSLayoutConstraint.activate([
            editImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            editImageView.widthAnchor.constraint(equalToConstant: 24),
            editImageView.heightAnchor.constraint(equalToConstant: 24),
            editImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupIconImageViewConstraints()
        setupOptionTextLabelConstraints()
        setupEditImageViewConstraints()
    }
    
    func configure(model: ProfileItem) {
        optionTextLabel.text = model.name
        iconImageView.image = UIImage(named: model.image)
        if model.editImage != "" {
            editImageView.image = UIImage(named: model.editImage)?.withHorizontallyFlippedOrientation().imageFlippedForRightToLeftLayoutDirection()
        }
    }
    
    override func prepareForReuse() {
        editImageView.image = nil
    }
}
