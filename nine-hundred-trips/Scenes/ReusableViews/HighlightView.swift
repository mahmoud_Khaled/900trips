//
//  HighlightView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class HighlightView: UIView {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.92)
        view.layer.cornerRadius = 21
        return view
    }()
    
    private lazy var seosnalView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var seosnalTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 13)
        label.numberOfLines = 0
        label.text = "seasonal".localized()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var highlightImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_star"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var highlightAskLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        label.text = "highlighAsk".localized()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var seperatorHorizontalView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var costTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.medium.getFont(ofSize: 13)
        label.numberOfLines = 0
        label.text = "cost".localized()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var costPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .mediumTurquoise
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.numberOfLines = 0
        label.text = "50 AED"
        label.textAlignment = .center
        return label
    }()
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("confirm".localized(), for: .normal)
        button.setTitleColor(.coral, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.coral.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("cancel".localized(), for: .normal)
        button.setTitleColor(.mediumTurquoise, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.mediumTurquoise.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        addTargets()
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addTargets() {
        confirmButton.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
    }
    
    @objc private func confirmButtonTapped() {
        
    }
    
    @objc private func cancelButtonTapped() {
        
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(seosnalView)
        seosnalView.addSubview(seosnalTitle)
        containerView.addSubview(highlightImageView)
        containerView.addSubview(highlightAskLabel)
        containerView.addSubview(seperatorHorizontalView)
        containerView.addSubview(costTitle)
        containerView.addSubview(costPriceLabel)
        containerView.addSubview(confirmButton)
        containerView.addSubview(cancelButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.75)
        ])
    }
    
    private func setupSeosnalViewConstraints() {
        NSLayoutConstraint.activate([
            seosnalView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            seosnalView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            seosnalView.heightAnchor.constraint(equalToConstant: 25),
            seosnalView.widthAnchor.constraint(equalToConstant: 65)
        ])
    }
    
    private func setupSeosnalTitleConstraints() {
        NSLayoutConstraint.activate([
            seosnalTitle.centerXAnchor.constraint(equalTo: seosnalView.centerXAnchor),
            seosnalTitle.centerYAnchor.constraint(equalTo: seosnalView.centerYAnchor)
        ])
    }
    
    private func setupHighlightImageViewConstraints() {
        NSLayoutConstraint.activate([
            highlightImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            highlightImageView.heightAnchor.constraint(equalToConstant: 60),
            highlightImageView.widthAnchor.constraint(equalToConstant: 60),
            highlightImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupHighlightAskLabelConstraints() {
        NSLayoutConstraint.activate([
            highlightAskLabel.topAnchor.constraint(equalTo: highlightImageView.bottomAnchor, constant: 5),
            highlightAskLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            highlightAskLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            highlightAskLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func setupSeperatorHorizontalViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorHorizontalView.topAnchor.constraint(equalTo: highlightAskLabel.bottomAnchor, constant: 10),
            seperatorHorizontalView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            seperatorHorizontalView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            seperatorHorizontalView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupCostTitleConstraints() {
        NSLayoutConstraint.activate([
            costTitle.topAnchor.constraint(equalTo: seperatorHorizontalView.bottomAnchor, constant: 10),
            costTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            costTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            costTitle.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupCostPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            costPriceLabel.topAnchor.constraint(equalTo: costTitle.bottomAnchor, constant: 2),
            costPriceLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupConfirmButtonConstraints() {
        NSLayoutConstraint.activate([
            confirmButton.topAnchor.constraint(equalTo: costPriceLabel.bottomAnchor, constant: 20),
            confirmButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            confirmButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -50),
            confirmButton.heightAnchor.constraint(equalToConstant: 50),
            confirmButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupCancelButtonConstraints() {
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: costPriceLabel.bottomAnchor, constant: 20),
            cancelButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            cancelButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 50),
            cancelButton.heightAnchor.constraint(equalToConstant: 50),
            cancelButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }

    
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupHighlightImageViewConstraints()
        setupHighlightAskLabelConstraints()
        setupSeperatorHorizontalViewConstraints()
        setupCostTitleConstraints()
        setupCostPriceLabelConstraints()
        setupConfirmButtonConstraints()
        setupCancelButtonConstraints()
        setupSeosnalViewConstraints()
        setupSeosnalTitleConstraints()
    }
}
