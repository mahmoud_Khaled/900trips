//
//  DeleteView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class DeleteView: UIView, PopupViewActionProtocol {
    
    var confirmAction: (() -> ())?
    
    var cancelAction: (() -> ())?
    
    var dismissesAutomtically: Bool {
        return false 
    }
    
    var isConfigurable: Bool {
        return false
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.92)
        view.layer.cornerRadius = 21
        return view
    }()
    
    private lazy var deleteImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_delete"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var deleteAskLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        label.text = "deleteConfirmation".localized()
        label.textAlignment = .center
        return label
    }()
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("confirm".localized(), for: .normal)
        button.setTitleColor(.coral, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.coral.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("cancel".localized(), for: .normal)
        button.setTitleColor(.mediumTurquoise, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.mediumTurquoise.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layoutUI()
        addTargets()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(deleteImageView)
        containerView.addSubview(deleteAskLabel)
        containerView.addSubview(confirmButton)
        containerView.addSubview(cancelButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.75)
        ])
    }
    
    private func setupDeleteImageViewConstraints() {
        NSLayoutConstraint.activate([
            deleteImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            deleteImageView.heightAnchor.constraint(equalToConstant: 60),
            deleteImageView.widthAnchor.constraint(equalToConstant: 60),
            deleteImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupDeleteAskLabelConstraints() {
        NSLayoutConstraint.activate([
            deleteAskLabel.topAnchor.constraint(equalTo: deleteImageView.bottomAnchor, constant: 5),
            deleteAskLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            deleteAskLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            deleteAskLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    
    private func setupConfirmButtonConstraints() {
        NSLayoutConstraint.activate([
            confirmButton.topAnchor.constraint(equalTo: deleteAskLabel.bottomAnchor, constant: 20),
            confirmButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            confirmButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -50),
            confirmButton.heightAnchor.constraint(equalToConstant: 50),
            confirmButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupCancelButtonConstraints() {
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: deleteAskLabel.bottomAnchor, constant: 20),
            cancelButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            cancelButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 50),
            cancelButton.heightAnchor.constraint(equalToConstant: 50),
            cancelButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupDeleteImageViewConstraints()
        setupDeleteAskLabelConstraints()
        setupConfirmButtonConstraints()
        setupCancelButtonConstraints()
    }
    
    private func addTargets() {
        confirmButton.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
    }
    
    
    @objc private func cancelButtonTapped() {
        cancelAction?()
    }
    
    @objc private func confirmButtonTapped() {
        confirmAction?()
    }
    
}
