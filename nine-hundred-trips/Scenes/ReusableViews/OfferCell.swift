//
//  OfferCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OfferCell: UITableViewCell, ConfigurableCell {
    
    var favouriteButtonTapped: () -> () = { }
    var menuButtonTapped: () -> () = { }
    
    private lazy var containerView: UIView = {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        return containerView
    }()
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "offerimage")
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var menuButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(UIImage(named: "ic_menu")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.isHidden = false
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 25)
        label.text = "Paris for one week"
        label.textAlignment = .center
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    private lazy var sinceContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 0, alpha: 0.29)
        view.layer.cornerRadius = 16
        return view
    }()
    
    private lazy var sinceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "from 5 march to 12 march"
        return label
    }()
    
    private lazy var priceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 15
        return view
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "1244 AED"
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 1, alpha: 0.25)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(residenceView)
        stackView.addArrangedSubview(ticketView)
        stackView.axis = .horizontal
        stackView.spacing = 10.0
        stackView.distribution = .fill
        return stackView
        
    }()
    
    private lazy var favouriteButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(UIImage(named: "ic_fav")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    private lazy var residenceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 15
        return view
    }()
    
    private lazy var residenceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_house")
        return imageView
    }()
    
    private lazy var residenceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "residence".localized()
        return label
    }()
    
    private lazy var ticketView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 15
        return view
    }()
    
    private lazy var ticketImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_flight")
        return imageView
    }()
    
    private lazy var ticketLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "flightTicket".localized()
        return label
    }()
    
    private var titleLabelTrailingAnchor: NSLayoutConstraint!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        addTargets()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(backgroundImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(sinceContainerView)
        sinceContainerView.addSubview(sinceLabel)
        containerView.addSubview(priceView)
        priceView.addSubview(priceLabel)
        containerView.addSubview(lineView)
        containerView.addSubview(favouriteButton)
        containerView.addSubview(stackView)
        residenceView.addSubview(residenceImageView)
        residenceView.addSubview(residenceLabel)
        ticketView.addSubview(ticketImageView)
        ticketView.addSubview(ticketLabel)
        
        containerView.addSubview(menuButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
        ])
    }
    
    private func setupBackgroundImageViewConstraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    private func setupTitleLabelConstraints() {
        titleLabelTrailingAnchor = titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            titleLabelTrailingAnchor
        ])
    }
    
    private func setupSinceViewContainer() {
        NSLayoutConstraint.activate([
            sinceContainerView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            sinceContainerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            sinceContainerView.heightAnchor.constraint(equalToConstant: 32),
        ])
    }
    
    private func setupSinceLabelConstraints() {
        NSLayoutConstraint.activate([
            sinceLabel.centerYAnchor.constraint(equalTo: sinceContainerView.centerYAnchor),
            sinceLabel.leadingAnchor.constraint(equalTo: sinceContainerView.leadingAnchor, constant: 10),
            sinceLabel.trailingAnchor.constraint(equalTo: sinceContainerView.trailingAnchor, constant: -10)
        ])
    }
    
    private func setupPriceViewContainerConstraints() {
        NSLayoutConstraint.activate([
            priceView.topAnchor.constraint(equalTo: sinceContainerView.bottomAnchor, constant: 10),
            priceView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            priceView.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            priceLabel.centerYAnchor.constraint(equalTo: priceView.centerYAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: priceView.leadingAnchor, constant: 15),
            priceLabel.trailingAnchor.constraint(equalTo: priceView.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupLineViewConstraints() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: priceView.bottomAnchor, constant: 10),
            lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupFavouriteButtonConstraints() {
        NSLayoutConstraint.activate([
            favouriteButton.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 10),
            favouriteButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            favouriteButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -10),
            favouriteButton.widthAnchor.constraint(equalToConstant: 30),
            favouriteButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupResidenceViewContainerConstraints() {
        NSLayoutConstraint.activate([
            residenceView.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupStackViewContainerConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 10),
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
        ])
    }
    
    private func setupResidenceImageViewConstraints() {
        NSLayoutConstraint.activate([
            residenceImageView.leadingAnchor.constraint(equalTo: residenceView.leadingAnchor, constant: 10),
            residenceImageView.centerYAnchor.constraint(equalTo: residenceView.centerYAnchor),
            residenceImageView.heightAnchor.constraint(equalToConstant: 14),
            residenceImageView.widthAnchor.constraint(equalToConstant: 14)
        ])
    }
    
    private func setupResidenceLabelViewConstraints() {
        NSLayoutConstraint.activate([
            residenceLabel.leadingAnchor.constraint(equalTo: residenceImageView.trailingAnchor, constant: 5),
            residenceLabel.trailingAnchor.constraint(equalTo: residenceView.trailingAnchor, constant: -10),
            residenceLabel.centerYAnchor.constraint(equalTo: residenceImageView.centerYAnchor),
        ])
    }
    
    private func setupTicketViewContainerConstraints() {
        NSLayoutConstraint.activate([
            ticketView.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func setupTicketImageViewConstraints() {
        NSLayoutConstraint.activate([
            ticketImageView.leadingAnchor.constraint(equalTo: ticketView.leadingAnchor, constant: 10),
            ticketImageView.centerYAnchor.constraint(equalTo: ticketView.centerYAnchor),
            ticketImageView.heightAnchor.constraint(equalToConstant: 14),
            ticketImageView.widthAnchor.constraint(equalToConstant: 14)
        ])
    }
    
    private func setupTicketLabelViewConstraints() {
        NSLayoutConstraint.activate([
            ticketLabel.leadingAnchor.constraint(equalTo: ticketImageView.trailingAnchor, constant: 5),
            ticketLabel.trailingAnchor.constraint(equalTo: ticketView.trailingAnchor, constant: -10),
            ticketLabel.centerYAnchor.constraint(equalTo: ticketImageView.centerYAnchor, constant: -1),
        ])
    }
    
    private func setupMenuButtonConstraints() {
        NSLayoutConstraint.activate([
            menuButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            menuButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            menuButton.widthAnchor.constraint(equalToConstant: 30),
            menuButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupBackgroundImageViewConstraints()
        setupTitleLabelConstraints()
        setupSinceViewContainer()
        setupSinceLabelConstraints()
        setupPriceViewContainerConstraints()
        setupLineViewConstraints()
        setupPriceLabelConstraints()
        setupLineViewConstraints()
        setupFavouriteButtonConstraints()
        setupStackViewContainerConstraints()
        setupResidenceViewContainerConstraints()
        setupTicketViewContainerConstraints()
        setupResidenceImageViewConstraints()
        setupResidenceLabelViewConstraints()
        setupTicketImageViewConstraints()
        setupTicketLabelViewConstraints()
        setupMenuButtonConstraints()
    }
    
    func configure(model: OfferViewModel) {
        titleLabel.text = model.title
        sinceLabel.text = model.offerPeriod
        priceLabel.text = model.adultPrice
        residenceView.isHidden = !model.hasResidence
        ticketView.isHidden = !model.hasFlight
        favouriteButton.setBackgroundImage(model.favouriteButtonImage, for: .normal)
        backgroundImageView.load(url: model.imageUrl)
        
        if model.isMyOffer {
            titleLabelTrailingAnchor.isActive = false
            titleLabelTrailingAnchor = titleLabel.trailingAnchor.constraint(equalTo: menuButton.leadingAnchor, constant: 0)
            titleLabelTrailingAnchor.isActive = true
            favouriteButton.isHidden = true
            menuButton.isHidden = false
        } else {
            titleLabelTrailingAnchor.isActive = false
            titleLabelTrailingAnchor = titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
            titleLabelTrailingAnchor.isActive = true
            favouriteButton.isHidden = false
            menuButton.isHidden = true
        }
        
        if UserDefaultsHelper.userType == .company {
            favouriteButton.isHidden = true
        } else {
            favouriteButton.isHidden = false
        }
        
    }
    
    func addTargets() {
        favouriteButton.addTarget(self, action: #selector(addFavouriteButtonTapped), for: .touchUpInside)
        menuButton.addTarget(self, action: #selector(menuButtonClicked), for: .touchUpInside)
    }
    
    @objc private func addFavouriteButtonTapped() {
        favouriteButtonTapped()
    }
    
    @objc private func menuButtonClicked() {
        menuButtonTapped()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        favouriteButton.isHidden = false
    }
}
