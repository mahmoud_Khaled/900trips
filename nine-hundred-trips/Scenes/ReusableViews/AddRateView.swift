//
//  AddRateView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class AddRateView: UIView, PopupViewActionProtocol {
    var dismissesAutomtically: Bool {
        return false
    }
    
    var isConfigurable: Bool {
        return true
    }
    
    var confirmAction: (() -> ())?
    
    var cancelAction: (() -> ())?
    

    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 21
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var rateOfferTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 17)
        label.numberOfLines = 0
        label.text = "rateOfferNow".localized()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var offerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 35.5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var offertitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paris for one week"
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 18)
        label.numberOfLines = 1
        return label
    }()
    
    lazy var rateView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.starSize = 20
        view.settings.fillMode = .half
        view.settings.updateOnTouch = true
        view.settings.filledColor = .coral
        view.settings.filledBorderColor = .coral
        view.settings.emptyColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.emptyBorderColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.starMargin = 0
        return view
    }()
    
    lazy var addRateButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("addRate".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layoutUI()
        addTargets()
        setupGestureRecognizer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(rateOfferTitleLabel)
        containerView.addSubview(offerImageView)
        containerView.addSubview(offertitleLabel)
        containerView.addSubview(rateView)
        containerView.addSubview(addRateButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.90)
        ])
    }
    
    private func setupRateOfferTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            rateOfferTitleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            rateOfferTitleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            rateOfferTitleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            rateOfferTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func setupOfferImageViewConstraints() {
        NSLayoutConstraint.activate([
            offerImageView.topAnchor.constraint(equalTo: rateOfferTitleLabel.bottomAnchor, constant: 20),
            offerImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            
            offerImageView.widthAnchor.constraint(equalToConstant: 71),
            offerImageView.heightAnchor.constraint(equalToConstant: 71)
        ])
    }
    
    private func setupOffertitleLabelConstraints() {
        NSLayoutConstraint.activate([
            offertitleLabel.topAnchor.constraint(equalTo: offerImageView.bottomAnchor, constant: 20),
            offertitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func setupRateViewConstraints() {
        NSLayoutConstraint.activate([
            rateView.topAnchor.constraint(equalTo: offertitleLabel.bottomAnchor, constant: 20),
            rateView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
        ])
    }
    
    private func setupAddRateButtonConstraints() {
        NSLayoutConstraint.activate([
            addRateButton.topAnchor.constraint(equalTo: rateView.bottomAnchor, constant: 20),
            addRateButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            addRateButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            addRateButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            addRateButton.heightAnchor.constraint(equalToConstant: 50),
            addRateButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupRateOfferTitleLabelConstraints()
        setupOffertitleLabelConstraints()
        setupOfferImageViewConstraints()
        setupRateViewConstraints()
        setupAddRateButtonConstraints()
    }
    
    func configure(model: ReservationViewModel) {
        offertitleLabel.text = model.title
        offerImageView.load(url: model.imageUrl)
    }
    
    func addTargets() {
        addRateButton.addTarget(self, action: #selector(addRateButtonTapped), for: .touchUpInside)
    }
    
    func setupGestureRecognizer() {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.delegate = self
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGesture)
        tapGesture.addTarget(self, action: #selector(dismiss))
    }
    
    @objc private func dismiss() {
        fadeOut(duration: 0.25) {
            self.removeFromSuperview()
        }
    }
    
    @objc private func addRateButtonTapped() {
        confirmAction?()
    }
    
}

extension AddRateView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: containerView) == true {
            return false
        }
        return true
    }
}
