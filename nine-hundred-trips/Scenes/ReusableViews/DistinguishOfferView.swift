//
//  DistinguishOfferView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class HighlightView: UIView {

    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.92)
        view.layer.cornerRadius = 21
        return view
    }()
    
    private lazy var successImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_success"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var successLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(successImageView)
        containerView.addSubview(successLabel)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.75),
            ])
    }
    
    private func setupSuccessImageViewConstraints() {
        NSLayoutConstraint.activate([
            successImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 16),
            successImageView.heightAnchor.constraint(equalToConstant: 55),
            successImageView.widthAnchor.constraint(equalToConstant: 55),
            successImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
            ])
    }
    
    private func setupSuccessLabelConstraints() {
        NSLayoutConstraint.activate([
            successLabel.topAnchor.constraint(equalTo: successImageView.bottomAnchor, constant: 14),
            successLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 24),
            successLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24),
            successLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20)
            ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupSuccessImageViewConstraints()
        setupSuccessLabelConstraints()
    }
    
    func configure(text: String) {
        successLabel.text = text
    }

}
