//
//  SubScriptionView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/19/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SubscriptionView: UIView {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.92)
        view.layer.cornerRadius = 21
        return view
    }()
    
    
    private lazy var walletImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_wallet_colored"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var subscriptiontAskLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        label.text = "subscriptionAsk".localized()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var subscriptionView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 22
        return view
    }()
    
    private lazy var subscriptionTypeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .coral
        label.font = DinNextFont.bold.getFont(ofSize: 17)
        label.numberOfLines = 0
        label.text = "Monthly subscription"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var costPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .mediumTurquoise
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.numberOfLines = 0
        label.text = "50 AED"
        label.textAlignment = .center
        return label
    }()
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("confirm".localized(), for: .normal)
        button.setTitleColor(.coral, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.coral.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("cancel".localized(), for: .normal)
        button.setTitleColor(.mediumTurquoise, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.mediumTurquoise.cgColor
        button.layer.cornerRadius = 25
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(walletImageView)
        containerView.addSubview(subscriptiontAskLabel)
        containerView.addSubview(subscriptionView)
        subscriptionView.addSubview(subscriptionTypeLabel)
        containerView.addSubview(costPriceLabel)
        containerView.addSubview(confirmButton)
        containerView.addSubview(cancelButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.75)
        ])
    }
    
    private func setupWalletImageViewConstraints() {
        NSLayoutConstraint.activate([
            walletImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            walletImageView.heightAnchor.constraint(equalToConstant: 60),
            walletImageView.widthAnchor.constraint(equalToConstant: 60),
            walletImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupSubscriptiontAskLabelConstraints() {
        NSLayoutConstraint.activate([
            subscriptiontAskLabel.topAnchor.constraint(equalTo: walletImageView.bottomAnchor, constant: 5),
            subscriptiontAskLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            subscriptiontAskLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            subscriptiontAskLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    

    private func setupSubscriptionViewConstraints() {
        NSLayoutConstraint.activate([
            subscriptionView.topAnchor.constraint(equalTo: subscriptiontAskLabel.bottomAnchor, constant: 10),
            subscriptionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            subscriptionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20)
        ])
    }
    
    private func setupSubscriptionTypeLabelConstraints() {
        NSLayoutConstraint.activate([
            subscriptionTypeLabel.topAnchor.constraint(equalTo: subscriptionView.topAnchor, constant: 10),
            subscriptionTypeLabel.bottomAnchor.constraint(equalTo: subscriptionView.bottomAnchor, constant: -10),
            subscriptionTypeLabel.leadingAnchor.constraint(equalTo: subscriptionView.leadingAnchor, constant: 10),
            subscriptionTypeLabel.trailingAnchor.constraint(equalTo: subscriptionView.trailingAnchor, constant: -10),
        ])
    }

    private func setupCostPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            costPriceLabel.topAnchor.constraint(equalTo: subscriptionView.bottomAnchor, constant: 4),
            costPriceLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupConfirmButtonConstraints() {
        NSLayoutConstraint.activate([
            confirmButton.topAnchor.constraint(equalTo: costPriceLabel.bottomAnchor, constant: 20),
            confirmButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            confirmButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -50),
            confirmButton.heightAnchor.constraint(equalToConstant: 50),
            confirmButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupCancelButtonConstraints() {
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: costPriceLabel.bottomAnchor, constant: 20),
            cancelButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            cancelButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 50),
            cancelButton.heightAnchor.constraint(equalToConstant: 50),
            cancelButton.widthAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupWalletImageViewConstraints()
        setupSubscriptiontAskLabelConstraints()
        setupCostPriceLabelConstraints()
        setupConfirmButtonConstraints()
        setupCancelButtonConstraints()
        setupSubscriptionViewConstraints()
        setupSubscriptionTypeLabelConstraints()
        
    }

}
