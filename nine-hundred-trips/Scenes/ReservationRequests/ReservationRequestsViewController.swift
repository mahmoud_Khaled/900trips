//
//  MyReservationViewController.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ReservationRequestsViewController: BaseViewController, ReservationRequestsViewProtocol {
    
    private let mainView = ReservationRequestsView()
    var presenter: ReservationRequestsPresenterProtocol!
    
    
    private lazy var tableViews: [UITableView] = {
        return [mainView.pendingReservationTableView, mainView.approvedReservastionTableView, mainView.rejectedReservationTableView]
    }()
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addTargets()
        presenter.viewDidLoad()
        toggleTableViewHiddenStatus(current: mainView.pendingReservationTableView)
        title = "reservationRequests".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    func setupTableView() {
        mainView.pendingReservationTableView.dataSource = self
        mainView.pendingReservationTableView.delegate = self
        
        mainView.approvedReservastionTableView.dataSource = self
        mainView.approvedReservastionTableView.delegate = self
        
        mainView.rejectedReservationTableView.dataSource = self
        mainView.rejectedReservationTableView.delegate = self
    }
        
    func reloadTableView(reservationStatus: ReservationStatus) {
        switch reservationStatus {
        case .pending:
            mainView.pendingReservationTableView.reloadData()
        case .approved:
            mainView.approvedReservastionTableView.reloadData()
        case .rejected:
            mainView.rejectedReservationTableView.reloadData()
        }
    }
    
    func reloadRowAt(at indexPath: IndexPath) {
        mainView.approvedReservastionTableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func setEmptyText(text: String, reservationStatus: ReservationStatus) {
        switch reservationStatus {
        case .pending:
            mainView.pendingReservationTableView.emptyMessage(message: text)
        case .approved:
            mainView.approvedReservastionTableView.emptyMessage(message: text)
        case .rejected:
            mainView.rejectedReservationTableView.emptyMessage(message: text)
        }
    }
    
    private func addTargets() {
        mainView.segmentedControl.addTarget(self, action: #selector(swichOffersSegmentedControl), for: .valueChanged)
        mainView.pendingRefreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        mainView.approvedRefreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        mainView.rejectedRefreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    @objc private func swichOffersSegmentedControl(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
       
        switch index {
        case 0:
            toggleTableViewHiddenStatus(current: mainView.pendingReservationTableView)
        case 1:
            toggleTableViewHiddenStatus(current: mainView.approvedReservastionTableView)
        case 2:
            toggleTableViewHiddenStatus(current: mainView.rejectedReservationTableView)
        default:
            break
        }
        
         presenter.swichOffersSegmentedControl(index: index)
    }
    
    private func toggleTableViewHiddenStatus(current: UITableView) {
        tableViews.forEach { $0.isHidden = $0 != current }
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
    func endRefreshing() {
        mainView.pendingRefreshControl.endRefreshing()
        mainView.approvedRefreshControl.endRefreshing()
        mainView.rejectedRefreshControl.endRefreshing()
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func showAddRateView(with reservation: ReservationViewModel, indexPath: IndexPath) {
        let rateView = AddRateView()
        show(popupView: rateView, model: reservation, on: tabBarController?.view, confirmAction: { [weak self] in
            guard let self = self else { return }
            rateView.fadeOut(duration: 0.25) {
                rateView.removeFromSuperview()
            }
            self.presenter.addOfferRateButtonTapped(rate: rateView.rateView.rating, offerId: reservation.offerId, indexPath: indexPath)
        }, cancelAction: {})
    }
    
}

extension ReservationRequestsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainView.pendingReservationTableView {
            return presenter.numberOfPendigRows
        } else if tableView == mainView.approvedReservastionTableView{
            return presenter.numberOfApprovedRows
        } else {
            return presenter.numbersOfRejectedRows
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == mainView.pendingReservationTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: PendingReservationCell.className, for: indexPath) as! PendingReservationCell
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            cell.confirmButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.presenter.approveButtonTapped(at: indexPath)
            }
            
            cell.rejectButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.presenter.rejectButtonTapped(at: indexPath)
            }
            
            cell.editButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.presenter.editButtonTapped(at: indexPath)
            }
            
            return cell
            
        } else if  tableView == mainView.approvedReservastionTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: ApprovedReservationCell.className, for: indexPath) as! ApprovedReservationCell
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            cell.addRateButtonTapped = { [weak self] in
                guard let self = self else { return }
                self.presenter.addRateButtonTapped(at: indexPath)
            }
            return cell
            
        } else {
            
             let cell = tableView.dequeueReusableCell(withIdentifier: RejectedReservationCell.className, for: indexPath) as! RejectedReservationCell
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if tableView == mainView.pendingReservationTableView {
            if indexPath.row == presenter.numberOfPendigRows - 1 {
                presenter.loadNextPage()
            }
        } else if tableView == mainView.approvedReservastionTableView {
            if indexPath.row == presenter.numberOfApprovedRows - 1 {
                presenter.loadNextPage()
            }
        } else {
            if indexPath.row == presenter.numbersOfRejectedRows - 1 {
                presenter.loadNextPage()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDefaultsHelper.userType == .company {
            switch tableView {
            case mainView.pendingReservationTableView:
                return 185
            case mainView.approvedReservastionTableView, mainView.rejectedReservationTableView:
                return 150
            default:
                return 150
            }
        } else {
            return 150
        }
    }
}
