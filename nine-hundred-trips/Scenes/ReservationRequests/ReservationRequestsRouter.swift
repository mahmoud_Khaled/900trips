//
//  MyReservationRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ReservationRequestsRouter: ReservationRequestsProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ReservationRequestsViewController()
        let interactor = ReservationRequestsInteractor()
        let router = ReservationRequestsRouter()
        let presenter = ReservationRequestsPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func navigateToRequestReservation(reservation: ReservationViewModel) {
        
    }
    
}
