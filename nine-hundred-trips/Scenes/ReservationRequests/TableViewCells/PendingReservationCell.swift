//
//  PendingReservation.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class PendingReservationCell: BaseReservationCell {
    
    var confirmButtonTapped: () -> () = { }
    var rejectButtonTapped: () -> () = { }
    var editButtonTapped: () -> () = { }
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("confirm".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 13)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 12.5
        return button
    }()
    
    lazy var rejectButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("reject".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 13)
        button.backgroundColor = .red
        button.layer.cornerRadius = 12.5
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(confirmButton)
        stackView.addArrangedSubview(rejectButton)
        stackView.axis = .horizontal
        stackView.spacing = 5
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    
    lazy var editButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_edit")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buttonsStackView.isHidden = UserDefaultsHelper.userType == .company ? false : true
        editButton.isHidden = UserDefaultsHelper.userType == .company ? true : false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func addSubviews() {
        super.addSubviews()
        containerView.addSubview(buttonsStackView)
        containerView.addSubview(editButton)
    }
    
    private func setupConfirmButtonConstraints() {
        NSLayoutConstraint.activate([
            confirmButton.heightAnchor.constraint(equalToConstant: 25),
            confirmButton.widthAnchor.constraint(equalToConstant: 65)
        ])
    }
    
    private func setupRejectButtonConstraints() {
        NSLayoutConstraint.activate([
            rejectButton.heightAnchor.constraint(equalToConstant: 25),
            rejectButton.widthAnchor.constraint(equalToConstant: 65)
        ])
    }
    
    override func setupOfferTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            offerTitleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            offerTitleLabel.leadingAnchor.constraint(equalTo: offerImageView.trailingAnchor, constant: 15),
            offerTitleLabel.trailingAnchor.constraint(equalTo: editButton.leadingAnchor, constant: -4)
            ])
    }
    
    private func setupEditButtonConstraints() {
        NSLayoutConstraint.activate([
            editButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            editButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            editButton.heightAnchor.constraint(equalToConstant: 23.5),
            editButton.widthAnchor.constraint(equalToConstant: 23.37)
        ])
    }
    
    
    private func setupButtonsStackViewConstraints() {
        NSLayoutConstraint.activate([
            buttonsStackView.topAnchor.constraint(equalTo: offerDetailsStackView.bottomAnchor, constant: 10),
            buttonsStackView.leadingAnchor.constraint(equalTo: offerDetailsStackView.leadingAnchor, constant: 0)
        ])
    }
    
    
    override func layoutUI() {
       super.layoutUI()
        setupConfirmButtonConstraints()
        setupRejectButtonConstraints()
        setupButtonsStackViewConstraints()
        setupEditButtonConstraints()
    }
    
    func addTargets() {
        confirmButton.addTarget(self, action: #selector(confirmButtonTapped(sender:)), for: .touchUpInside)
        rejectButton.addTarget(self, action: #selector(rejecteButtonTapped(sender:)), for: .touchUpInside)
        editButton.addTarget(self, action: #selector(editButtonTapped(sender:)), for: .touchUpInside)
    }

    @objc private func confirmButtonTapped(sender: UIButton) {
        confirmButtonTapped()
    }
    
    @objc private func rejecteButtonTapped(sender: UIButton) {
        rejectButtonTapped()
    }
    
    @objc private func editButtonTapped(sender: UIButton) {
        editButtonTapped()
    }
}
