//
//  BaseCell.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class BaseReservationCell: UITableViewCell, ConfigurableCell {

    
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var offerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 35.5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
     lazy var offerTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paris for one week"
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 18)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var numberOfPersonsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paris for one week"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var offerPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paris for one week"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var companyTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paris for one week"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 1
        return label
    }()
    
    lazy var offerDetailsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(numberOfPersonsLabel)
        stackView.addArrangedSubview(offerPriceLabel)
        stackView.addArrangedSubview(companyTitleLabel)
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(offerImageView)
        containerView.addSubview(offerTitleLabel)
        containerView.addSubview(offerDetailsStackView)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor,constant: 10 ),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        ])
    }
    
    private func setupOfferImageViewConstraints() {
        NSLayoutConstraint.activate([
            offerImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            offerImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            offerImageView.widthAnchor.constraint(equalToConstant: 71),
            offerImageView.heightAnchor.constraint(equalToConstant: 71)
        ])
    }
    
    func setupOfferTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            offerTitleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            offerTitleLabel.leadingAnchor.constraint(equalTo: offerImageView.trailingAnchor, constant: 15)
        ])
    }
    
    private func setupOfferDetailsStackViewConstraints() {
        NSLayoutConstraint.activate([
            offerDetailsStackView.topAnchor.constraint(equalTo: offerTitleLabel.bottomAnchor, constant: 4),
            offerDetailsStackView.leadingAnchor.constraint(equalTo: offerImageView.trailingAnchor, constant: 15),
            offerDetailsStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
        ])
    }
    
    func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupOfferImageViewConstraints()
        setupOfferDetailsStackViewConstraints()
        setupOfferTitleLabelConstraints()
    }
    
    func configure(model: ReservationViewModel) {
        offerImageView.load(url: model.imageUrl)
        offerTitleLabel.text = model.title
        offerPriceLabel.text = "\("totalPrice".localized()) \(model.price)"
        numberOfPersonsLabel.text = "\("numberOfPersons".localized()): \(model.persons)"
        if UserDefaultsHelper.userType == .company {
            companyTitleLabel.text = "\("phoneNumber".localized()): \(model.phone)"
        } else {
            companyTitleLabel.text = "\("company".localized()): \(model.companyName)"
        }
    }
}
