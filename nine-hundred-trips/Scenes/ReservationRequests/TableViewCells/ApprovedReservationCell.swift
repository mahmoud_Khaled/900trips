//
//  ReservationCell.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class ApprovedReservationCell: BaseReservationCell {

    var addRateButtonTapped: () -> () = { }
    
    lazy var addRateButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("addRate".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.regular.getFont(ofSize: 12)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 11.5
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addRateButton.isHidden = UserDefaultsHelper.userType == .company ? true : false
        addTargets()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func addSubviews() {
        super.addSubviews()
        containerView.addSubview(addRateButton)
    }
    
    
    override func setupOfferTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            offerTitleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            offerTitleLabel.leadingAnchor.constraint(equalTo: offerImageView.trailingAnchor, constant: 15),
            offerTitleLabel.trailingAnchor.constraint(equalTo: addRateButton.leadingAnchor, constant: -4)
        ])
    }
    
    private func setupAddRateButtonConstraints() {
        NSLayoutConstraint.activate([
            addRateButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            addRateButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -4),
            addRateButton.heightAnchor.constraint(equalToConstant: 23),
            addRateButton.widthAnchor.constraint(equalToConstant: 65)
        ])
    }
        
    override func layoutUI() {
        super.layoutUI()
        setupAddRateButtonConstraints()
    }
    
    override func configure(model: ReservationViewModel) {
        super.configure(model: model)
        
        if UserDefaultsHelper.userType != .company {
            if model.isRated {
                addRateButton.isHidden = true
            } else {
                addRateButton.isHidden = false
            }
        }
    }
    
    func addTargets() {
        addRateButton.addTarget(self, action: #selector(addRateButtonTapped(sender:)), for: .touchUpInside)
    }

    @objc private func addRateButtonTapped(sender: UIButton) {
        addRateButtonTapped()
    }
    
}
