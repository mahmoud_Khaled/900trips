//
//  MyReservationInteractor.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ReservationRequestsInteractor: ReservationRequestsInteractorInputProtocol {

    weak var presenter: ReservationRequestsInteractorOutputProtocol?
    
    private let reservationWorker = ReservationWorker()
    private let offersWorker = OffersWorker()
    
    func getReservations(reversationStatus: ReservationStatus, page: Int) {
        reservationWorker.getReservations(reversationStatus: reversationStatus, page: page) { [weak self] result, page in
            guard let self = self else { return }
            self.presenter?.didFetchReservations(with: result, totalPages: page)
        }
    }
    
    func approveReservation(reservationId: Int, indexPath: IndexPath) {
        reservationWorker.approveReservation(reservationId: reservationId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didApproveReservation(with: result, indexPath: indexPath)
        }
    }
    
    func rejectReservation(reservationId: Int, indexPath: IndexPath) {
        reservationWorker.rejectedReservation(reservationId: reservationId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRejectReservation(with: result, indexPath: indexPath)
        }
    }
    
    func addOfferRate(offerId: Int, rate: Double, indexPath: IndexPath) {
        offersWorker.addOfferRate(offerId: offerId, rate: rate) {  [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddOfferRate(with: result, indexPath: indexPath)
        }
    }
}
