//
//  MyReservationPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum ReservationStatus {
    case pending
    case approved
    case rejected
}

class ReservationRequestsPresenter: ReservationRequestsPresenterProtocol, ReservationRequestsInteractorOutputProtocol {
 
    weak var view: ReservationRequestsViewProtocol?
    private let interactor: ReservationRequestsInteractorInputProtocol
    private let router: ReservationRequestsProtocol
    
    private var reservationStatus: ReservationStatus = .pending
    private var isPullToRefresh: Bool = false
    private var pendingReversations: [ReservationViewModel] = []
    private var approvedReversations: [ReservationViewModel] = []
    private var rejectedReversations: [ReservationViewModel] = []
    
    private var pendingCurrentPage = 1
    private var approvedCurrentPage = 1
    private var rejectedCurrentPage = 1
    
    private var pendingTotalPages = 1
    private var approvedTotalPages = 1
    private var rejectedTotalPages = 1
    
    var numberOfPendigRows: Int {
        return pendingReversations.count
    }
    
    var numberOfApprovedRows: Int {
        return approvedReversations.count
    }
    
    var numbersOfRejectedRows: Int {
        return rejectedReversations.count
    }
    
    init(view: ReservationRequestsViewProtocol, interactor: ReservationRequestsInteractorInputProtocol, router: ReservationRequestsProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getReservations(reversationStatus: reservationStatus, page: pendingCurrentPage)
    }
    
    func pullToRefresh() {
        isPullToRefresh = true
        
        switch reservationStatus {
        case .pending:
            pendingCurrentPage = 1
            pendingTotalPages = 1
            view?.setEmptyText(text: "", reservationStatus: reservationStatus)
            interactor.getReservations(reversationStatus: reservationStatus, page: pendingCurrentPage)

        case .approved:
            approvedCurrentPage = 1
            approvedTotalPages = 1
            view?.setEmptyText(text: "", reservationStatus: reservationStatus)
            interactor.getReservations(reversationStatus: reservationStatus, page: approvedCurrentPage)
            
        case .rejected:
            rejectedCurrentPage = 1
            rejectedTotalPages = 1
            view?.setEmptyText(text: "", reservationStatus: reservationStatus)
            interactor.getReservations(reversationStatus: reservationStatus, page: rejectedCurrentPage)
        }
    }
    
    func loadNextPage() {
        switch reservationStatus {
        case .pending:
            if pendingCurrentPage < pendingTotalPages {
                pendingCurrentPage += 1
                view?.showActivityIndicator(isUserInteractionEnabled: true)
                interactor.getReservations(reversationStatus: reservationStatus, page: pendingCurrentPage)
            }
        case .approved:
            if approvedCurrentPage < approvedTotalPages {
                approvedCurrentPage += 1
                view?.showActivityIndicator(isUserInteractionEnabled: true)
                interactor.getReservations(reversationStatus: reservationStatus, page: approvedCurrentPage)
            }
        case .rejected:
            if rejectedCurrentPage < rejectedTotalPages {
                rejectedCurrentPage += 1
                view?.showActivityIndicator(isUserInteractionEnabled: true)
                interactor.getReservations(reversationStatus: reservationStatus, page: rejectedCurrentPage)
            }
        }
    }
    
    func rejectButtonTapped(at indexPath: IndexPath) {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
       interactor.rejectReservation(reservationId: pendingReversations[indexPath.row].id, indexPath: indexPath)
    }
    
    func approveButtonTapped(at indexPath: IndexPath) {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
         interactor.approveReservation(reservationId: pendingReversations[indexPath.row].id, indexPath: indexPath)
    }
    
    func addRateButtonTapped(at indexPath: IndexPath) {
        view?.showAddRateView(with: approvedReversations[indexPath.row], indexPath: indexPath)
    }
    
    func addOfferRateButtonTapped(rate: Double, offerId: Int, indexPath: IndexPath) {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.addOfferRate(offerId: offerId, rate: rate, indexPath: indexPath)
    }
    
    func editButtonTapped(at indexPath: IndexPath) {
        switch reservationStatus {
        case .pending:
            let reservationOrder = pendingReversations[indexPath.row]
            router.navigateToRequestReservation(reservation: reservationOrder)
        case .rejected:
            let reservationOrder = rejectedReversations[indexPath.row]
            router.navigateToRequestReservation(reservation: reservationOrder)
        case .approved:
            break
        }
    }
    
 
    func swichOffersSegmentedControl(index: Int) {
        switch index {
        case 0:
            reservationStatus = .pending
            loadData(currentArray: pendingReversations, currentPage: pendingCurrentPage)
        case 1:
            reservationStatus = .approved
            loadData(currentArray: approvedReversations, currentPage: approvedCurrentPage)
        case 2 :
            reservationStatus = .rejected
            loadData(currentArray: rejectedReversations, currentPage: rejectedCurrentPage)
        default:
            reservationStatus = .pending
        }
    }
    
    private func loadData(currentArray: [ReservationViewModel], currentPage: Int) {
        if currentArray.count == 0 {
            view?.showActivityIndicator(isUserInteractionEnabled: true)
            view?.setEmptyText(text: "", reservationStatus: reservationStatus)
            interactor.getReservations(reversationStatus: reservationStatus, page: currentPage)
        }
    }
    
    func didFetchReservations(with result: Result<[Reservation]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        
        switch result {
        case .success(let value):
            
            if isPullToRefresh {
                switch reservationStatus {
                case .pending:
                    pendingReversations = []
                case .approved:
                    approvedReversations = []
                case .rejected:
                    rejectedReversations = []
                }
                isPullToRefresh = false
            }

            
            switch reservationStatus {
            case .pending:
                pendingReversations.append(contentsOf: value.map({ReservationViewModel(reservation: $0)}))
                pendingTotalPages = totalPages
                
                if pendingReversations.count == 0 {
                    view?.setEmptyText(text: "thereAraNoReservations".localized(), reservationStatus: .pending)
                }
                
            case .approved:
                approvedReversations.append(contentsOf: value.map({ReservationViewModel(reservation: $0)}))
                approvedTotalPages = totalPages
                
                if approvedReversations.count == 0 {
                   view?.setEmptyText(text: "thereAraNoReservations".localized(), reservationStatus: .approved)
                }
                
            case .rejected:
                rejectedReversations.append(contentsOf: value.map({ReservationViewModel(reservation: $0)}))
                rejectedTotalPages = totalPages
                
                if rejectedReversations.count == 0 {
                    view?.setEmptyText(text: "thereAraNoReservations".localized(), reservationStatus: .rejected)
                }
                
            }
            view?.reloadTableView(reservationStatus: reservationStatus)
        case .failure(let error):
            isPullToRefresh = false
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didApproveReservation(with result: Result<Data>, indexPath: IndexPath) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            pendingReversations.remove(at: indexPath.row)
            if pendingReversations.count == 0 {
                view?.setEmptyText(text: "thereAraNoReservations".localized(), reservationStatus: reservationStatus)
            }
            view?.reloadTableView(reservationStatus: .pending)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didRejectReservation(with result: Result<Data>, indexPath: IndexPath) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            pendingReversations.remove(at: indexPath.row)
            if pendingReversations.count == 0 {
                view?.setEmptyText(text: "thereAraNoReservations".localized(), reservationStatus: reservationStatus)
            }
            view?.reloadTableView(reservationStatus: .pending)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didAddOfferRate(with result: Result<Data>, indexPath: IndexPath) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            approvedReversations[indexPath.row].isRated = true
            view?.reloadRowAt(at: indexPath)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<ReservationViewModel>, at indexPath: IndexPath) {
        switch reservationStatus {
        case .pending:
            cell.configure(model: pendingReversations[indexPath.row])
        case .approved:
            cell.configure(model: approvedReversations[indexPath.row])
        case .rejected:
            cell.configure(model: rejectedReversations[indexPath.row])
        }
    }
    
    
}
