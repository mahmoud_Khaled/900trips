//
//  MyReservationView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ReservationRequestsView: UIView {
    
    lazy var segmentedControl: UISegmentedControl = {
        let items = [
            "pending".localized(),
            "approved".localized(),
            "rejected".localized()
        ]
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.tintColor = .coral
        let font = DinNextFont.regular.getFont(ofSize: 15)
        segmentedControl.setTitleTextAttributes([.font: font], for: .normal)
        return segmentedControl
    }()
    
    let pendingRefreshControl = UIRefreshControl()
    
    lazy var pendingReservationTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(PendingReservationCell.self, forCellReuseIdentifier: PendingReservationCell.className)
        tableView.refreshControl = pendingRefreshControl
        tableView.delaysContentTouches = false
        tableView.estimatedRowHeight = 0
        return tableView
    }()
    
    let approvedRefreshControl = UIRefreshControl()
    lazy var approvedReservastionTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(ApprovedReservationCell.self, forCellReuseIdentifier: ApprovedReservationCell.className)
        tableView.refreshControl = approvedRefreshControl
        tableView.delaysContentTouches = false
        tableView.estimatedRowHeight = 0
        return tableView
    }()
    
    let rejectedRefreshControl = UIRefreshControl()
    lazy var rejectedReservationTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(RejectedReservationCell.self, forCellReuseIdentifier: RejectedReservationCell.className)
        tableView.refreshControl = rejectedRefreshControl
        tableView.delaysContentTouches = false
        tableView.estimatedRowHeight = 0
        return tableView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(segmentedControl)
        addSubview(pendingReservationTableView)
        addSubview(approvedReservastionTableView)
        addSubview(rejectedReservationTableView)
    }
    
    private func setupSegmentedControllConstraints() {
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 10),
            segmentedControl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            segmentedControl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            segmentedControl.heightAnchor.constraint(equalToConstant: 38)
        ])
    }
    
    private func setupPendingTableViewConstraints() {
        NSLayoutConstraint.activate([
            pendingReservationTableView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10),
            pendingReservationTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            pendingReservationTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            pendingReservationTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5)
        ])
    }
    
    private func setupApprovedTableViewConstraints() {
        NSLayoutConstraint.activate([
            approvedReservastionTableView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10),
            approvedReservastionTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            approvedReservastionTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            approvedReservastionTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5)
        ])
    }
    
    private func setupRejectedTableViewConstraints() {
        NSLayoutConstraint.activate([
            rejectedReservationTableView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10),
            rejectedReservationTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            rejectedReservationTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            rejectedReservationTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5)
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupSegmentedControllConstraints()
        setupPendingTableViewConstraints()
        setupApprovedTableViewConstraints()
        setupRejectedTableViewConstraints()
    }
    
}
