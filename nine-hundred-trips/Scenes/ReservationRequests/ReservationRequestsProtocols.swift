//
//  MyReservationProtocols.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 7/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ReservationRequestsProtocol: class {
    func showAlert(with message: String)
    func navigateToRequestReservation(reservation: ReservationViewModel)
}

protocol ReservationRequestsPresenterProtocol: class {
    var view: ReservationRequestsViewProtocol? { get set }
    var numberOfPendigRows: Int { get }
    var numberOfApprovedRows: Int { get }
    var numbersOfRejectedRows: Int { get }
    func swichOffersSegmentedControl(index: Int)
    func pullToRefresh()
    func configure(cell: AnyConfigurableCell<ReservationViewModel>, at indexPath: IndexPath)
    func viewDidLoad()
    func loadNextPage()
    func approveButtonTapped(at indexPath: IndexPath)
    func rejectButtonTapped(at indexPath: IndexPath)
    func addRateButtonTapped(at indexPath: IndexPath)
    func addOfferRateButtonTapped(rate: Double, offerId: Int, indexPath: IndexPath)
    func editButtonTapped(at indexPath: IndexPath)
}

protocol ReservationRequestsInteractorInputProtocol: class {
    var presenter: ReservationRequestsInteractorOutputProtocol? { get set }
    func getReservations(reversationStatus: ReservationStatus, page: Int)
    func rejectReservation(reservationId: Int, indexPath: IndexPath)
    func approveReservation(reservationId: Int, indexPath: IndexPath)
    func addOfferRate(offerId: Int, rate: Double, indexPath: IndexPath)
}

protocol ReservationRequestsInteractorOutputProtocol: class {
    func didFetchReservations(with result: Result<[Reservation]>, totalPages: Int)
    func didApproveReservation(with result: Result<Data>, indexPath: IndexPath)
    func didRejectReservation(with result: Result<Data>, indexPath: IndexPath)
    func didAddOfferRate(with result: Result<Data>, indexPath: IndexPath )
}

protocol ReservationRequestsViewProtocol: class {
    var presenter: ReservationRequestsPresenterProtocol! { get set }
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func endRefreshing()
    func reloadTableView(reservationStatus: ReservationStatus)
    func showAddRateView(with reservationOffer: ReservationViewModel, indexPath: IndexPath)
    func reloadRowAt(at indexPath: IndexPath)
    func setEmptyText(text: String, reservationStatus: ReservationStatus)
}
