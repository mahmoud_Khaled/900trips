//
//  TourismGuideDetailsProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol TourismGuideDetailsRouterProtocol: class {
    
}

protocol TourismGuideDetailsPresenterProtocol: class {
    var view: TourismGuideDetailsViewProtocol? { get set }
    func configure(cell: AnyConfigurableCell<URL?>, at indexPath: IndexPath)
    var numberOfRows: Int { get }
    func shareTourismPlace()
}

protocol TourismGuideDetailsInteractorInputProtocol: class {
    var presenter: TourismGuideDetailsInteractorOutputProtocol? { get set }
}

protocol TourismGuideDetailsInteractorOutputProtocol: class {
    
}

protocol TourismGuideDetailsViewProtocol: class {
    var presenter: TourismGuideDetailsPresenterProtocol! { get set }
}
