//
//  TourismGuideDetailsView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import MapKit

class TourismGuideDetailsView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrolView = UIScrollView()
        scrolView.translatesAutoresizingMaskIntoConstraints = false
        scrolView.contentInsetAdjustmentBehavior = .never
        scrolView.delaysContentTouches = false
        scrolView.showsHorizontalScrollIndicator = false
        scrolView.showsVerticalScrollIndicator = false
        return scrolView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var imagesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
        return collectionView
    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.pageIndicatorTintColor = .white
        pageControl.currentPageIndicatorTintColor = .coral
        return pageControl
    }()
    
    private lazy var mapContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var mapKit: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(imagesCollectionView)
        containerView.addSubview(pageControl)
        containerView.addSubview(mapContainerView)
        mapContainerView.addSubview(mapKit)
        containerView.addSubview(descriptionLabel)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupImagesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            imagesCollectionView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            imagesCollectionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            imagesCollectionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            imagesCollectionView.heightAnchor.constraint(equalToConstant: 200)
        ])
    }
    
    private func setupPageControlConstraints() {
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: imagesCollectionView.bottomAnchor, constant: -10),
            pageControl.centerXAnchor.constraint(equalTo: imagesCollectionView.centerXAnchor)
        ])
    }
    
    private func setupMapContainerConstraints() {
        NSLayoutConstraint.activate([
            mapContainerView.topAnchor.constraint(equalTo: imagesCollectionView.bottomAnchor, constant: 15),
            mapContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            mapContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            mapContainerView.heightAnchor.constraint(equalToConstant: 157)
        ])
    }
    
    private func setupMapKitConstraints() {
        NSLayoutConstraint.activate([
            mapKit.topAnchor.constraint(equalTo: mapContainerView.topAnchor),
            mapKit.leadingAnchor.constraint(equalTo: mapContainerView.leadingAnchor),
            mapKit.trailingAnchor.constraint(equalTo: mapContainerView.trailingAnchor),
            mapKit.bottomAnchor.constraint(equalTo: mapContainerView.bottomAnchor)
        ])
    }
    
    private func setupDescriptionLabelConstrainrts() {
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: mapContainerView.bottomAnchor, constant: 15),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupImagesCollectionViewConstraints()
        setupPageControlConstraints()
        setupMapContainerConstraints()
        setupMapKitConstraints()
        setupDescriptionLabelConstrainrts()
    }
    
    func configure(tourismPlaceViewModel: TourismPlaceViewModel) {
        pageControl.numberOfPages = tourismPlaceViewModel.imageUrls.count
        descriptionLabel.text = tourismPlaceViewModel.description
        if tourismPlaceViewModel.latitude > -89 && tourismPlaceViewModel.latitude < 89 && tourismPlaceViewModel.longitude > -179 && tourismPlaceViewModel.longitude < 179 {
            let center = CLLocationCoordinate2D(latitude: tourismPlaceViewModel.latitude, longitude: tourismPlaceViewModel.longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: center, span: span)
            self.mapKit.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = center
            mapKit.addAnnotation(annotation)
        }
    }
    
}
