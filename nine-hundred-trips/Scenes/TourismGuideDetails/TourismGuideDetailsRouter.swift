//
//  TourismGuideDetailsRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismGuideDetailsRouter: TourismGuideDetailsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(tourismPlaceViewModel: TourismPlaceViewModel) -> UIViewController {
        let view = TourismGuideDetailsViewController(viewModel: tourismPlaceViewModel)
        let interactor = TourismGuideDetailsInteractor()
        let router = TourismGuideDetailsRouter()
        let presenter = TourismGuideDetailsPresenter(view: view, interactor: interactor, router: router, viewModel: tourismPlaceViewModel)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
}
