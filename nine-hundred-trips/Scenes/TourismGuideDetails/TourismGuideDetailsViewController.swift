//
//  TourismGuideDetailsViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismGuideDetailsViewController: BaseViewController, TourismGuideDetailsViewProtocol {    
    
    private let mainView = TourismGuideDetailsView()
    var presenter: TourismGuideDetailsPresenterProtocol!
    
    private let tourismPlaceViewModel: TourismPlaceViewModel
    private var currentIndexOfPageControll: Int = 0
    
    init(viewModel: TourismPlaceViewModel) {
        self.tourismPlaceViewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        setupCollectionView()
        navigationItem.title = tourismPlaceViewModel.title
        mainView.configure(tourismPlaceViewModel: tourismPlaceViewModel)
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func setupNavigationBarButtons() {
        let shareButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_share"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(shareButtonTapped))
            .build()
        
        navigationItem.rightBarButtonItem = shareButton
    }
    
    private func setupCollectionView() {
        mainView.imagesCollectionView.delegate = self
        mainView.imagesCollectionView.dataSource = self
    }
    
    @objc private func shareButtonTapped() {
        presenter.shareTourismPlace()
    }
    
}

extension TourismGuideDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.className, for: indexPath) as? ImageCell  else { return UICollectionViewCell() }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainView.imagesCollectionView {
            currentIndexOfPageControll = Int(scrollView.contentOffset.x / mainView.imagesCollectionView.frame.size.width)
            mainView.pageControl.currentPage = currentIndexOfPageControll
        }
    }

}
