//
//  TourismGuideDetailsPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TourismGuideDetailsPresenter: TourismGuideDetailsPresenterProtocol, TourismGuideDetailsInteractorOutputProtocol {
    
    weak var view: TourismGuideDetailsViewProtocol?
    private let interactor: TourismGuideDetailsInteractorInputProtocol
    private let router: TourismGuideDetailsRouterProtocol
    
    private let tourismPlaceViewModel: TourismPlaceViewModel
    var numberOfRows: Int {
        return tourismPlaceViewModel.imageUrls.count
    }
    
    init(view: TourismGuideDetailsViewProtocol, interactor: TourismGuideDetailsInteractorInputProtocol, router: TourismGuideDetailsRouterProtocol, viewModel: TourismPlaceViewModel) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.tourismPlaceViewModel = viewModel
    }
    
    func configure(cell: AnyConfigurableCell<URL?>, at indexPath: IndexPath) {
        cell.configure(model: tourismPlaceViewModel.imageUrls[indexPath.row])
    }
    
    func shareTourismPlace() {
        
    }
}
