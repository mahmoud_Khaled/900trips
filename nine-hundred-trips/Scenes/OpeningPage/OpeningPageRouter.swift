//
//  OpeningPageRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum OpeningPageDestination {
    case register, login, countryAndCurrency
}

class OpeningPageRouter: OpeningPageRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = OpeningPageViewController()
        let router = OpeningPageRouter()
        let presenter = OpeningPagePresenter(view: view, router: router)
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func naviagte(to destination: OpeningPageDestination) {
        switch destination {
        case .register:
            viewController?.navigationController?.pushViewController(RegisterRouter.createModule(), animated: true)
        case .login:
            viewController?.navigationController?.pushViewController(LoginRouter.createModule(), animated: true)
        case .countryAndCurrency:
            viewController?.present(CountryAndCurrencyRouter.createModule(), animated: true, completion: nil)
        }
    }
}
