//
//  OpeningPagePresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class OpeningPagePresenter: OpeningPagePresenterProtocol {
    
    weak var view: OpeningPageViewProtocol?
    private let router: OpeningPageRouterProtocol
    
    init(view: OpeningPageViewProtocol, router: OpeningPageRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func registerButtonTapped() {
        router.naviagte(to: .register)
    }
    
    func loginButtonTapped() {
        router.naviagte(to: .login)
    }
    
    func skipButtonTapped() {
        router.naviagte(to: .countryAndCurrency)
    }
}
