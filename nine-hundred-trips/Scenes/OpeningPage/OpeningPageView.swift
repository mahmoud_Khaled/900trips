//
//  OpeningPageView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OpeningPageView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "background"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var registerNowLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "registerNow".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("register".localized(), for: .normal)
        button.setTitleColor(.cyan, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .clear
        button.layer.cornerRadius = 30
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.cyan.cgColor
        return button
    }()
    
    private lazy var haveAccountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "ifYouHaveAnAccount".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 22)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("login".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 30
        return button
    }()
    
    lazy var skipButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("skip".localized(), for: .normal)
        button.setTitleColor(.cyan, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "ic_skip")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        button.imageView?.translatesAutoresizingMaskIntoConstraints = false
        button.imageView?.contentMode = .scaleAspectFit
        NSLayoutConstraint.activate([
            button.imageView!.heightAnchor.constraint(equalToConstant: 30),
            button.imageView!.widthAnchor.constraint(equalToConstant: 30),
            button.imageView!.centerYAnchor.constraint(equalTo: button.centerYAnchor, constant: 4
            ),
            button.imageView!.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: 12)
        ])
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(logoImageView)
        containerView.addSubview(registerNowLabel)
        containerView.addSubview(registerButton)
        containerView.addSubview(haveAccountLabel)
        containerView.addSubview(loginButton)
        containerView.addSubview(skipButton)
    }
    
    private func setupBackgroundImageViewConstraints(){
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupLogoImageViewConstraints() {
        NSLayoutConstraint.activate([
            logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 70),
            logoImageView.widthAnchor.constraint(equalToConstant: 174),
            logoImageView.heightAnchor.constraint(equalToConstant: 201)
        ])
    }
    
    private func setupRegiterNowLabelConstraints() {
        NSLayoutConstraint.activate([
            registerNowLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            registerNowLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 10.0),
            registerNowLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20.0),
            registerNowLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20)
        ])
    }
    
    private func setupRegisterButtonConstraints() {
        NSLayoutConstraint.activate([
            registerButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            registerButton.topAnchor.constraint(equalTo: registerNowLabel.bottomAnchor, constant: 20.0),
            registerButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20.0),
            registerButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            registerButton.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupHaveAccountLabelConstraints() {
        NSLayoutConstraint.activate([
            haveAccountLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            haveAccountLabel.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 15.0),
            haveAccountLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20.0),
            haveAccountLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20)
        ])
    }
    
    private func setupLoginButtonConstraints() {
        NSLayoutConstraint.activate([
            loginButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            loginButton.topAnchor.constraint(equalTo: haveAccountLabel.bottomAnchor, constant: 20.0),
            loginButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20.0),
            loginButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            loginButton.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func setupSkipButtonConstraints(){
        NSLayoutConstraint.activate([
            skipButton.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 20.0),
            skipButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15),
            skipButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -25
            ),
            skipButton.heightAnchor.constraint(equalToConstant: 60),
            skipButton.widthAnchor.constraint(equalToConstant: 120)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupLogoImageViewConstraints()
        setupRegiterNowLabelConstraints()
        setupRegisterButtonConstraints()
        setupHaveAccountLabelConstraints()
        setupLoginButtonConstraints()
        setupSkipButtonConstraints()
    }
    
}
