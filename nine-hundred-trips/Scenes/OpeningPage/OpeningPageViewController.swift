//
//  OpeningPageViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OpeningPageViewController: BaseViewController, OpeningPageViewProtocol {

    
    private let  mainView = OpeningPageView()
    var presenter: OpeningPagePresenterProtocol!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func addTargets() {
        mainView.registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        mainView.loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        mainView.skipButton.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)
    }
    
    @objc private func registerButtonTapped() {
        presenter.registerButtonTapped()
    }
    
    @objc private func loginButtonTapped() {
        presenter.loginButtonTapped()
    }
    
    @objc private func skipButtonTapped() {
        presenter.skipButtonTapped()
    }
    
}
