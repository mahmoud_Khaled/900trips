//
//  OpeningPageProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 4/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol OpeningPageRouterProtocol: class {
    func naviagte(to destination: OpeningPageDestination)
}

protocol OpeningPagePresenterProtocol: class {
    var view: OpeningPageViewProtocol? { get set }
    func registerButtonTapped()
    func loginButtonTapped()
    func skipButtonTapped()
}

protocol OpeningPageViewProtocol: class {
    var presenter: OpeningPagePresenterProtocol! { get set }
}
