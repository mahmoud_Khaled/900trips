//
//  TourismPlaceLocationInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import CoreLocation

class TourismPlaceLocationInteractor: TourismPlaceLocationInteractorInputProtocol {
    
    weak var presenter: TourismPlaceLocationInteractorOutputProtocol?
    
    func reverseGeocodeLocation(lat: Double, long: Double) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: long)
        geoCoder.reverseGeocodeLocation(location) { [weak self] placemarks, error in
            guard let self = self else { return }
            if let error = error {
                self.presenter?.didReverseGeocodeLocation(with: .failure(error))
            } else {
                guard let placemarks = placemarks else { return }
                guard placemarks.count > 0 else {
                    self.presenter?.didReverseGeocodeLocation(with: .failure(NetworkError.emptyPlacemarks))
                    return
                }
                
                var address = ""
                let placemark = placemarks.first!
                
                if let placemarkName = placemark.name {
                    address += placemarkName
                }
                
                if let city = placemark.locality {
                    address += " - " + city
                }
                
                if let state = placemark.administrativeArea {
                    address += " - " + state
                }
                
                self.presenter?.didReverseGeocodeLocation(with: .success(address))
            }
        }
    }
}
