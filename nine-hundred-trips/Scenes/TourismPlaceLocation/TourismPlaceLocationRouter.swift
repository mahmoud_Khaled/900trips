//
//  TourismPlaceLocationRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TourismPlaceLocationRouter: TourismPlaceLocationRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(delgate: GetTourismPlaceLocationProtocol) -> UIViewController {
        let view = TourismPlaceLocationViewController()
        let interactor = TourismPlaceLocationInteractor()
        let router = TourismPlaceLocationRouter()
        let locationManager = LocationManager()
        let presenter = TourismPlaceLocationPresenter(view: view, interactor: interactor, router: router, locationManager: locationManager, delgate: delgate)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        locationManager.delegate = presenter
        return view
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showPermissionAlert(with title: String, message: String) {
        AlertBuilder(title: title, message: message, preferredStyle: .alert)
            .addAction(title: "Cancel", style: .cancel)
            .addAction(title: "Settings", style: .default) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
            .build()
            .show()
    }
    
}
