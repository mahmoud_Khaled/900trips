//
//  TourismPlaceLocationView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import MapKit

class TourismPlaceLocationView: UIView {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var tapGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer()
        return tapGesture
    }()
    
    lazy var mapKit: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.addGestureRecognizer(tapGesture)
        return mapView
    }()
    
    lazy var bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumGray
        return view
    }()
    
    private lazy var locationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(locationImageView)
        stackView.addArrangedSubview(locationLabel)
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var locationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_selectlocation")
        imageView.isHidden = true
        return imageView
    }()
    
    lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.textColor = .darkGray
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var markButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 251/255, green: 144/255, blue: 95/255, alpha: 1.0).cgColor
        button.setTitle("mark".localized(), for: .normal)
        button.setTitleColor(.coral, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.layer.cornerRadius = 25
        button.isEnabled = false
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(bottomContainerView)
        containerView.addSubview(mapKit)
        bottomContainerView.addSubview(locationStackView)
        bottomContainerView.addSubview(markButton)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0)
        ])
    }
    
    private func setupMapKitConstraints() {
        NSLayoutConstraint.activate([
            mapKit.topAnchor.constraint(equalTo: containerView.topAnchor),
            mapKit.bottomAnchor.constraint(equalTo: bottomContainerView.topAnchor),
            mapKit.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            mapKit.leadingAnchor.constraint(equalTo: containerView.leadingAnchor)
        ])
    }
    
    private func setupBottomContainerViewConstraints() {
        NSLayoutConstraint.activate([
            bottomContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            bottomContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            bottomContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            bottomContainerView.heightAnchor.constraint(equalToConstant: 125)
        ])
    }
    
   
    
    private func setupLocationStackViewConstraints() {
        NSLayoutConstraint.activate([
            locationStackView.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 10),
            locationStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            locationStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        ])
    }

    private func setupLocationImageViewConstraints() {
        NSLayoutConstraint.activate([
            locationImageView.heightAnchor.constraint(equalToConstant: 25),
            locationImageView.widthAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func setupMarkButtonConstraints() {
        NSLayoutConstraint.activate([
            markButton.bottomAnchor.constraint(equalTo: bottomContainerView.bottomAnchor, constant: -10),
            markButton.centerXAnchor.constraint(equalTo: bottomContainerView.centerXAnchor),
            markButton.heightAnchor.constraint(equalToConstant: 50),
            markButton.widthAnchor.constraint(equalToConstant: 106)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupBottomContainerViewConstraints()
        setupMapKitConstraints()
        setupLocationStackViewConstraints()
        setupLocationImageViewConstraints()
        setupMarkButtonConstraints()
    }
    
    func setLocationAddress(address: String) {
        locationImageView.isHidden = false
        markButton.isEnabled = true
        locationLabel.text = address
    }
}
