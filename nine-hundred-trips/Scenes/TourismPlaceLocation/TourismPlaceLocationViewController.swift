//
//  TourismPlaceLocationViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class TourismPlaceLocationViewController: BaseViewController, TourismPlaceLocationViewProtocol, UIGestureRecognizerDelegate {
    
    private let mainView = TourismPlaceLocationView()
    var presenter: TourismPlaceLocationPresenterProtocol!
    
    let annotation = MKPointAnnotation()
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "selectPlaceLocation".localized()
        setupNavigationBarStyle()
        presenter.viewDidLoad()
        addTargets()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
    }
    
    func setMapRegion(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: center, span: span)
        mainView.mapKit.setRegion(region, animated: true)
        annotation.coordinate = center
        mainView.mapKit.addAnnotation(annotation)
        presenter.reverseGeocode(location: location)
    }
    
    private func addTargets() {
        mainView.markButton.addTarget(self, action: #selector(markButtonTapped), for: .touchUpInside)
        mainView.tapGesture.addTarget(self, action: #selector(handleTap(gestureReconizer:)))
    }
    
    func setLocationAddress(address: String) {
        mainView.setLocationAddress(address: address)
    }
    
    @objc private func markButtonTapped() {
        presenter.dismiss()
    }

    @objc private func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        let location = gestureReconizer.location(in: mainView.mapKit)
        let coordinate = mainView.mapKit.convert(location,toCoordinateFrom: mainView.mapKit)
        annotation.coordinate = coordinate
        mainView.mapKit.addAnnotation(annotation)
        presenter.reverseGeocode(location: CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude))
    }
}
