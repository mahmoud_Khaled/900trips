//
//  TourismPlaceLocationPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import CoreLocation

class TourismPlaceLocationPresenter: TourismPlaceLocationPresenterProtocol, TourismPlaceLocationInteractorOutputProtocol, LocationManagerDelegate {
    
    weak var view: TourismPlaceLocationViewProtocol?
    private let interactor: TourismPlaceLocationInteractorInputProtocol
    private let router: TourismPlaceLocationRouterProtocol
    private weak var delgate: GetTourismPlaceLocationProtocol?
    private let locationManager: LocationManagerProtocol
    private var locationAddress: String = ""
    private var locationLatitude: Double = 0.0
    private var locationLongitude: Double = 0.0
    
    init(view: TourismPlaceLocationViewProtocol, interactor: TourismPlaceLocationInteractorInputProtocol, router: TourismPlaceLocationRouterProtocol, locationManager: LocationManagerProtocol, delgate: GetTourismPlaceLocationProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.locationManager = locationManager
        self.delgate = delgate
    }
    
    func viewDidLoad() {
        locationManager.getUserLocation()
    }
    
    func dismiss() {
        delgate?.getLocation(latitude: locationLatitude, longitude: locationLongitude, address: locationAddress)
        router.dismiss()
    }
    
    func handleLocationServicesDisabled() {
        router.showPermissionAlert(with: "enableLocation".localized(), message: "locationPermisstion".localized())
    }
    
    func didFetchLocation(location: CLLocation) {
        locationManager.stopLocationUpdates()
        view?.setMapRegion(location: location)
    }
    
    func didFail(with error: Error) {
        router.showAlert(with: "noInternetConnection".localized())
    }
    
    func reverseGeocode(location: CLLocation) {
        interactor.reverseGeocodeLocation(lat: location.coordinate.latitude, long: location.coordinate.longitude)
        locationLatitude = location.coordinate.latitude
        locationLongitude = location.coordinate.longitude
    }
    
    func didReverseGeocodeLocation(with result: Result<String>) {
        switch result {
        case .success(let address):
            view?.setLocationAddress(address: address)
            locationAddress = address
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
}
