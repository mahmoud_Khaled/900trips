//
//  TourismPlaceLocationProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import CoreLocation

protocol TourismPlaceLocationRouterProtocol: class {
    func dismiss()
    func showAlert(with message: String)
    func showPermissionAlert(with title: String, message: String)
}

protocol TourismPlaceLocationPresenterProtocol: class {
    var view: TourismPlaceLocationViewProtocol? { get set }
    func dismiss()
    func viewDidLoad()
    func reverseGeocode(location: CLLocation)
}

protocol TourismPlaceLocationInteractorInputProtocol: class {
    var presenter: TourismPlaceLocationInteractorOutputProtocol? { get set }
    func reverseGeocodeLocation(lat: Double, long: Double)
}

protocol TourismPlaceLocationInteractorOutputProtocol: class {
    func didReverseGeocodeLocation(with result: Result<String>)
}

protocol TourismPlaceLocationViewProtocol: class {
    var presenter: TourismPlaceLocationPresenterProtocol! { get set }
    func setMapRegion(location: CLLocation)
    func setLocationAddress(address: String)
}
