//
//  OfferDetailCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class OfferDetailsCell: UITableViewCell, ConfigurableCell {
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "offerNumber".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var seperatorVerticalView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "1234"
        label.textColor = .coral
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var offerImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_flight_direction")?.imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var seperatorHorizontalView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var offerRateView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.starSize = 20
        view.settings.fillMode = .precise
        view.settings.updateOnTouch = false
        view.settings.filledColor = .coral
        view.settings.filledBorderColor = .coral
        view.settings.emptyColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.emptyBorderColor = UIColor.coral.withAlphaComponent(0.30)
        view.settings.starMargin = 3
        return view
    }()
    
    private var offerImageViewWidthConstraint: NSLayoutConstraint!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(nameLabel)
        addSubview(seperatorVerticalView)
        addSubview(valueLabel)
        addSubview(offerImageView)
        addSubview(seperatorHorizontalView)
        addSubview(offerRateView)
    }
    
    private func setupNameLabelConstraints() {
        NSLayoutConstraint.activate([
            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            nameLabel.widthAnchor.constraint(equalToConstant: 120)
        ])
    }
    
    private func setupSeperatorVerticalViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorVerticalView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            seperatorVerticalView.bottomAnchor.constraint(equalTo: seperatorHorizontalView.topAnchor, constant: 0),
            seperatorVerticalView.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor, constant: 1),
            seperatorVerticalView.widthAnchor.constraint(equalToConstant: 1),
        ])
    }
    
    private func setupValueLabelConstraints() {
        NSLayoutConstraint.activate([
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            valueLabel.leadingAnchor.constraint(equalTo: seperatorVerticalView.trailingAnchor, constant: 10),
            valueLabel.trailingAnchor.constraint(equalTo: offerImageView.leadingAnchor, constant: -4),
        ])
    }
    
    private func setupOfferImageViewConstraints() {
        offerImageViewWidthConstraint = offerImageView.widthAnchor.constraint(equalToConstant: 32)
        NSLayoutConstraint.activate([
            offerImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            offerImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            offerImageViewWidthConstraint,
            offerImageView.heightAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    private func setupSeperatorHorizontalViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorHorizontalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            seperatorHorizontalView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            seperatorHorizontalView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            seperatorHorizontalView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupOfferRateViewConstraints() {
        NSLayoutConstraint.activate([
            offerRateView.centerYAnchor.constraint(equalTo: centerYAnchor),
            offerRateView.leadingAnchor.constraint(equalTo: seperatorVerticalView.trailingAnchor, constant: 10),
        ])
    }
    
    
    private func layoutUI() {
        addSubviews()
        setupNameLabelConstraints()
        setupSeperatorVerticalViewConstraints()
        setupValueLabelConstraints()
        setupOfferImageViewConstraints()
        setupSeperatorHorizontalViewConstraints()
        setupOfferRateViewConstraints()
    }
    
    func configure(model: FlightDetail) {
        nameLabel.text = model.title
        valueLabel.text = model.value
        offerRateView.rating = model.offerRate ?? 0.0
        
        if model.imageIsHidden {
            offerImageView.isHidden = true
            offerImageViewWidthConstraint.constant = 0
        } else {
            offerImageView.load(url: model.imageUrl)
            offerImageView.isHidden = false
        }
        
        if model.offerRate == nil {
            valueLabel.isHidden = false
            offerRateView.isHidden = true
        } else {
            if model.offerRate == 0.0{
                valueLabel.isHidden = false
                offerRateView.isHidden = true
                valueLabel.text = "notRatedYet".localized()
            } else {
                valueLabel.isHidden = true
                offerRateView.isHidden = false
            }
        }
    }
    
}
