//
//  OfferDetailsViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OfferDetailsViewController: BaseViewController, OfferDetailsViewProtocol {

    
    private let mainView = OfferDetailsView()
    var presenter: OfferDetailsPresenterProtocol!
    
    private var number1: String?
    private var number2: String?
    private var offerDetailsViewModel: OfferDetailsViewModel?
    
    private let isMyOffer: Bool
    
    init(isMyOffer: Bool) {
        self.isMyOffer = isMyOffer
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        if isMyOffer {
            mainView.prepareForMyOffer()
        }
        setupCollectionsView()
        setupTableView()
        setupPageController()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        presenter.viewDidLoad()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupCollectionsView() {
        mainView.imagesCollectionView.delegate = self
        mainView.imagesCollectionView.dataSource = self
        
        mainView.servicesCollectionView.delegate = self
        mainView.servicesCollectionView.dataSource = self
    }
    
    private func setupTableView() {
        mainView.offerDetailsTableView.delegate = self
        mainView.offerDetailsTableView.dataSource = self
    }
    
    private func addTargets() {
        mainView.callButton.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        mainView.companyViewGesture.addTarget(self, action: #selector(companyViewTapped))
        mainView.messageButton.addTarget(self, action: #selector(messageButtonTapped), for: .touchUpInside)
        mainView.requestReservationButton.addTarget(self, action: #selector(requestResrevationButtonTapped), for: .touchUpInside)
    }
    
    private func setupPageController() {
        mainView.pageControl.currentPage = 0
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupNavigationBarButtons() {
        let favouriteButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_favourite_white"))
            .height(20)
            .width(20)
            .action(target: self, selector: #selector(favouriteButtonTapped))
            .build()
        
        let shareButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_share"))
            .height(20)
            .width(20)
            .action(target: self, selector: #selector(shareButtonTapped))
            .build()
        
        if isMyOffer || UserDefaultsHelper.userType == .company {
            navigationItem.rightBarButtonItems = [shareButton]
        } else {
            navigationItem.rightBarButtonItems = [favouriteButton, shareButton]
        }
        
    }
    
    
    @objc private func favouriteButtonTapped() {
       
        if UserDefaultsHelper.isLoggedIn {
            offerDetailsViewModel?.isLiked.toggle()
            (navigationItem.rightBarButtonItems?.first?.customView as? UIButton)?.setImage(offerDetailsViewModel?.favouriteButtonImage, for: .normal)
            presenter.favouriteButtonTapped(isLiked: offerDetailsViewModel?.isLiked ?? false)
            
        } else {
            presenter.showLoginAlert()
        }
    }
    
    @objc private func shareButtonTapped() {
        presenter.shareButtonTapped()
    }
    
    @objc private func callButtonTapped() {
        presenter.callButtonTapped(number1: number1 ?? "", number2: number2 ?? "")
    }
    
    @objc private func companyViewTapped() {
        presenter.companyViewTapped()
    }
    
    @objc private func messageButtonTapped() {
        presenter.messageButtonTapped()
    }
    
    @objc private func requestResrevationButtonTapped() {
        presenter.requestResrevationButtonTapped()
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func setOfferDetailsData(with viewModel: OfferDetailsViewModel) {
        
        offerDetailsViewModel = viewModel
        number1 = viewModel.companyNumber1
        number2 = viewModel.companyNumber2
        
        title = viewModel.title
        if !isMyOffer {
            if UserDefaultsHelper.userType == .client {
                (navigationItem.rightBarButtonItems?.first?.customView as? UIButton)?.setImage(viewModel.favouriteButtonImage, for: .normal)
            }
        }
        
        mainView.configure(viewModel: viewModel)
        if viewModel.companyId == UserDefaultsHelper.id {
            mainView.prepareForMyOffer()
        }
        
        if UserDefaultsHelper.userType == .company {
            mainView.updateReservationButtonHeight()
        }
    }
    
    func reloadCollectionViews() {
        mainView.imagesCollectionView.reloadData()
        mainView.servicesCollectionView.reloadData()
        mainView.pageControl.numberOfPages = presenter.offerImagesNumberOfRows
    }
    
    func reloadTableView() {
        mainView.offerDetailsTableView.reloadData()
    }
}

extension OfferDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == mainView.imagesCollectionView {
           return presenter.offerImagesNumberOfRows
        } else {
            return presenter.offerServicesNumberOfRows
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainView.imagesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.className, for: indexPath) as? ImageCell  else { return UICollectionViewCell() }
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
            
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServiceCell.className, for: indexPath) as? ServiceCell  else { return UICollectionViewCell() }
            presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == mainView.imagesCollectionView {
            
            let width = collectionView.frame.width
            let height = collectionView.frame.height
            return CGSize(width: width, height: height)
            
        } else {
            
            let height = collectionView.frame.height
            return CGSize(width: 80, height: height)
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainView.imagesCollectionView {
            mainView.pageControl.currentPage = Int(scrollView.contentOffset.x / mainView.imagesCollectionView.frame.size.width)
        }
    }
}

extension OfferDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.flightDetailsNumberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OfferDetailsCell.className, for: indexPath) as? OfferDetailsCell else { return UITableViewCell() }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}
