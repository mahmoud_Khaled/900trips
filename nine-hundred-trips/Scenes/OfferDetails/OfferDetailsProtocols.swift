//
//  OfferDetailsProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol OfferDetailsRouterProtocol: class {
    func showAlert(with message: String)
    func showActionSheet(number1: String, number2: String)
    func navigateTo(destination: OfferDetailsDestination)
}

protocol OfferDetailsPresenterProtocol: class {
    var view: OfferDetailsViewProtocol? { get set }
    var offerImagesNumberOfRows: Int { get }
    var offerServicesNumberOfRows: Int { get }
    var flightDetailsNumberOfRows: Int { get  }
    func viewDidLoad()
    func configure(cell: AnyConfigurableCell<URL?>, at indexPath: IndexPath)
    func configure(cell: AnyConfigurableCell<FlightDetail>, at indextPath: IndexPath)
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath)
    func callButtonTapped(number1: String, number2: String)
    func favouriteButtonTapped(isLiked: Bool)
    func showLoginAlert()
    func companyViewTapped()
    func messageButtonTapped()
    func shareButtonTapped()
    func requestResrevationButtonTapped()
}

protocol OfferDetailsInteractorInputProtocol: class {
    var presenter: OfferDetailsInteractorOutputProtocol? { get set }
    func getOfferDetails(offerId: Int)
    func addFavourite(offerId: Int)
    func removeFavourite(offerId: Int)
}

protocol OfferDetailsInteractorOutputProtocol: class {
    func didFetchOfferDetails(with result: Result<OfferDetails>)
    func didChangeFavourite(with result: Result<Data>)
}

protocol OfferDetailsViewProtocol: class {
    var presenter: OfferDetailsPresenterProtocol! { get set }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func setOfferDetailsData(with viewModel: OfferDetailsViewModel)
    func reloadCollectionViews()
    func reloadTableView()

}

@objc protocol UpdateOfferProtocol: class {
    @objc optional func updateOffer(indexPath: IndexPath, isLiked: Bool)
}
