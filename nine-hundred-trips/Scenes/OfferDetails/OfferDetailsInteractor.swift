//
//  OfferDetailsInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class OfferDetailsInteractor: OfferDetailsInteractorInputProtocol {
    
    weak var presenter: OfferDetailsInteractorOutputProtocol?
    private let offersWorker = OffersWorker()
    
    func getOfferDetails(offerId: Int) {
        
        offersWorker.getDetails(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchOfferDetails(with: result)
        }
        
    }
    
    func addFavourite(offerId: Int) {
        offersWorker.addFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didChangeFavourite(with: result)
        }
    }
    
    func removeFavourite(offerId: Int) {
        offersWorker.removeFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didChangeFavourite(with: result)
        }
    }
}
