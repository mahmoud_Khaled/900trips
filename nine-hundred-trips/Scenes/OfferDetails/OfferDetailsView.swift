//
//  OfferDetailsView.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Cosmos

class OfferDetailsView: UIView {
    
    private var companyDetailsViewHeight: NSLayoutConstraint!
    private var conactsViewHeight: NSLayoutConstraint!
    private var requestReservationButtonHeight: NSLayoutConstraint!
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var imagesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
        return collectionView
    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.numberOfPages = 3
        pageControl.pageIndicatorTintColor = .white
        pageControl.currentPageIndicatorTintColor = .coral
        return pageControl
    }()
    
    lazy var residenceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 20
        return view
    }()
    
     private lazy var residenceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_house")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
        
    }()
    
    private lazy var residenceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "residence".localized()
        return label
    }()
    
    lazy var ticketView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 20
        return view
    }()
    
    private lazy var ticketImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_flight")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var ticketLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "flightTicket".localized()
        return label
    }()
    
    private lazy var residenceStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(residenceView)
        stackView.addArrangedSubview(ticketView)
        stackView.axis = .horizontal
        stackView.spacing = 15
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var countriesView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var countriesSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var flightImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_flight_direction")?.imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var fromCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "from".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var toCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "to".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
     lazy var fromCountryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 32
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    lazy var toCountryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 32
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var fromCountryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----"
        label.textColor = .customBlue
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()

    lazy var toCountryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----"
        label.textColor = .customBlue
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var flightDateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var flightDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "---------------"
        label.textColor = .babyGreen
        label.textAlignment = .center
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()

    
    lazy var transiteRoundedView: SemiCirleView = {
        let view = SemiCirleView(frame: CGRect(x: 0, y: 0, width: 100, height: 41))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    private lazy var transiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "transit".localized()
        label.textColor = .white
        label.textAlignment = .center
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var transiteCountriesView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var transitedCountryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 23
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var pausedImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_pause"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
     lazy var transitedCountryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----"
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var transitedTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "-----"
        label.textColor = .babyGreen
        label.font = DinNextFont.regular.getFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var transiteStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(transiteRoundedView)
        stackView.addArrangedSubview(transiteCountriesView)
        stackView.axis = .vertical
        stackView.spacing = 17.5
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var offerDetailsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    lazy var offerDetailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.register(OfferDetailsCell.self, forCellReuseIdentifier: OfferDetailsCell.className)
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    private lazy var servicesView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var serviceImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_services"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var servicesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.text = "services".localized()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var servicesCollectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(ServiceCell.self, forCellWithReuseIdentifier: ServiceCell.className)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    
    private lazy var hotelDetailsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var hotelImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_hotel_logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = 37
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
   
    lazy var hotelRateView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.starSize = 16
        view.settings.fillMode = .precise
        view.settings.updateOnTouch = false
        view.settings.filledColor = UIColor(red: 255/255, green: 255/255, blue: 0, alpha: 1)
        view.settings.filledBorderColor = UIColor(red: 255/255, green: 255/255, blue: 0, alpha: 1)
        view.settings.starMargin = 0
        return view
    }()
    
    lazy var hotelRateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 22)
        label.text = "-- Stars"
        label.numberOfLines = 0
        return label
    }()
    
    lazy var hotelNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 20)
        label.text = "-----"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var adultView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    private lazy var adultSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var adultImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_adult"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    
    private lazy var adultPriceTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "adultPrice".localized()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var adultDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 10)
        label.text = "adultPerson".localized()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var adultPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.text = "---"
        label.numberOfLines = 0
        return label
    }()
    
    
    private lazy var childView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        return view
    }()
    
    
    private lazy var childSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var childImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_child"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var childPriceTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "childPrice".localized()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var childPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.bold.getFont(ofSize: 25)
        label.text = "---"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var childDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 10)
        label.text = "childPerson".localized()
        label.numberOfLines = 0
        return label
    }()
    
    
    private lazy var priceStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(adultView)
        stackView.addArrangedSubview(childView)
        stackView.axis = .horizontal
        stackView.spacing = 15
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    lazy var companyViewGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer()
        return tapGesture
    }()
    
    private lazy var companyDetailsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .coral
        view.layer.cornerRadius = 13
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 4, spread: 0)
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(companyViewGesture)
        return view
    }()
    
    lazy var companyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 32
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.medium.getFont(ofSize: 20)
        label.text = "-----"
        label.numberOfLines = 1
        return label
    }()
    
    lazy var countryOfCompanyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.text = "-----"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var moreImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_skip")?.withRenderingMode(.alwaysTemplate).imageFlippedForRightToLeftLayoutDirection())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .white
        return imageView
    }()

    
    lazy var requestReservationButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("requestAReservation".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        button.backgroundColor = .coral
        button.setImage(UIImage(named: "ic_tag")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 25
        button.imageView?.translatesAutoresizingMaskIntoConstraints = false
        
        if L102Language.currentLanguage == "en" {
            NSLayoutConstraint.activate([
                button.imageView!.centerXAnchor.constraint(equalTo: button.centerXAnchor, constant: -110),
                button.imageView!.centerYAnchor.constraint(equalTo: button.centerYAnchor,constant: 5)
            ])
        } else {
            NSLayoutConstraint.activate([
                button.imageView!.centerXAnchor.constraint(equalTo: button.centerXAnchor, constant: 50),
                button.imageView!.centerYAnchor.constraint(equalTo: button.centerYAnchor,constant: 5)
            ])
            
        }
        
        return button
    }()

    private lazy var contactsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .babyGray
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var seperatorVerticallView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var callButton: UIButton = {
        let button = UIButton(type: .system)
         button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_call")?.withRenderingMode(.alwaysOriginal).imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        return button
    }()

    lazy var messageButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_message")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    private lazy var callLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "call".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var messageabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "message".localized()
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        if L102Language.currentLanguage == "ar" {
            imagesCollectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(imagesCollectionView)
        containerView.addSubview(pageControl)
        residenceView.addSubview(residenceImageView)
        residenceView.addSubview(residenceLabel)
        ticketView.addSubview(ticketImageView)
        ticketView.addSubview(ticketLabel)
        containerView.addSubview(residenceStackView)
        containerView.addSubview(countriesView)
        countriesView.addSubview(countriesSeperatorView)
        countriesView.addSubview(flightImageView)
        countriesView.addSubview(fromCountryLabel)
        countriesView.addSubview(toCountryLabel)
        countriesView.addSubview(fromCountryImageView)
        countriesView.addSubview(toCountryImageView)
        countriesView.addSubview(toCountryNameLabel)
        countriesView.addSubview(fromCountryNameLabel)
        countriesView.addSubview(flightDateView)
        flightDateView.addSubview(flightDateLabel)
        transiteCountriesView.addSubview(transitedCountryImageView)
        transiteCountriesView.addSubview(transitedCountryNameLabel)
        transiteCountriesView.addSubview(transitedTimeLabel)
        transiteCountriesView.addSubview(pausedImageView)
        containerView.addSubview(transiteStackView)
        containerView.addSubview(transiteLabel)
        
        containerView.addSubview(offerDetailsView)
        offerDetailsView.addSubview(offerDetailsTableView)
        
        containerView.addSubview(servicesView)
        servicesView.addSubview(serviceImageView)
        servicesView.addSubview(servicesLabel)
        servicesView.addSubview(servicesCollectionView)
        
        containerView.addSubview(hotelDetailsView)
        hotelDetailsView.addSubview(hotelImageView)
        hotelDetailsView.addSubview(hotelNameLabel)
        hotelDetailsView.addSubview(hotelRateView)
        hotelDetailsView.addSubview(hotelRateLabel)
        
        adultView.addSubview(adultSeperatorView)
        adultView.addSubview(adultImageView)
        adultView.addSubview(adultPriceTitle)
        adultView.addSubview(adultDescriptionLabel)
        adultView.addSubview(adultPriceLabel)
        
        childView.addSubview(childSeperatorView)
        childView.addSubview(childImageView)
        childView.addSubview(childPriceTitle)
        childView.addSubview(childDescriptionLabel)
        childView.addSubview(childPriceLabel)
        
        containerView.addSubview(priceStackView)
        containerView.addSubview(companyDetailsView)
        
        companyDetailsView.addSubview(companyImageView)
        companyDetailsView.addSubview(companyNameLabel)
        companyDetailsView.addSubview(countryOfCompanyNameLabel)
        companyDetailsView.addSubview(moreImageView)
        
        containerView.addSubview(requestReservationButton)
        
        containerView.addSubview(contactsView)
        contactsView.addSubview(seperatorVerticallView)
        contactsView.addSubview(callButton)
        contactsView.addSubview(messageButton)
        contactsView.addSubview(callLabel)
        contactsView.addSubview(messageabel)
        
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupImagesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            imagesCollectionView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            imagesCollectionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            imagesCollectionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            imagesCollectionView.heightAnchor.constraint(equalToConstant: 175)
        ])
    }
    
    private func setupPageControlConstraints() {
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: imagesCollectionView.bottomAnchor, constant: -10),
            pageControl.centerXAnchor.constraint(equalTo: imagesCollectionView.centerXAnchor)
        ])
    }
    
    private func setupResidenceViewContainerConstraints() {
        NSLayoutConstraint.activate([
            residenceView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupTicketViewContainerConstraints() {
        NSLayoutConstraint.activate([
            ticketView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupResidenceImageViewConstraints() {
        NSLayoutConstraint.activate([
            residenceImageView.leadingAnchor.constraint(equalTo: residenceView.leadingAnchor, constant: 10),
            residenceImageView.centerYAnchor.constraint(equalTo: residenceView.centerYAnchor),
            residenceImageView.heightAnchor.constraint(equalToConstant: 22),
            residenceImageView.widthAnchor.constraint(equalToConstant: 22)
        ])
    }
    
    private func setupResidenceLabelViewConstraints() {
        NSLayoutConstraint.activate([
            residenceLabel.leadingAnchor.constraint(equalTo: residenceImageView.trailingAnchor, constant: 5),
            residenceLabel.trailingAnchor.constraint(equalTo: residenceView.trailingAnchor, constant: -10),
            residenceLabel.centerYAnchor.constraint(equalTo: residenceImageView.centerYAnchor),
        ])
    }
    
    private func setupTicketImageViewConstraints() {
        NSLayoutConstraint.activate([
            ticketImageView.leadingAnchor.constraint(equalTo: ticketView.leadingAnchor, constant: 10),
            ticketImageView.centerYAnchor.constraint(equalTo: ticketView.centerYAnchor),
            ticketImageView.heightAnchor.constraint(equalToConstant: 21),
            ticketImageView.widthAnchor.constraint(equalToConstant: 21)
        ])
    }
    
    private func setupTicketLabelViewConstraints() {
        NSLayoutConstraint.activate([
            ticketLabel.leadingAnchor.constraint(equalTo: ticketImageView.trailingAnchor, constant: 5),
            ticketLabel.trailingAnchor.constraint(equalTo: ticketView.trailingAnchor, constant: -10),
            ticketLabel.centerYAnchor.constraint(equalTo: ticketImageView.centerYAnchor),
        ])
    }
    
    private func setupStackViewConstraints() {
        NSLayoutConstraint.activate([
            residenceStackView.topAnchor.constraint(equalTo: imagesCollectionView.bottomAnchor, constant: 5),
            residenceStackView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)

        ])
    }
    
    private func setupCountriesViewConstraints() {
        NSLayoutConstraint.activate([
            countriesView.topAnchor.constraint(equalTo: residenceStackView.bottomAnchor, constant: 10),
            countriesView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            countriesView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            countriesView.heightAnchor.constraint(equalToConstant: 165)
        ])
    }
    
    private func setupCountriesSeperatorViewConstraints() {
        NSLayoutConstraint.activate([
            countriesSeperatorView.centerXAnchor.constraint(equalTo: countriesView.centerXAnchor),
            countriesSeperatorView.centerYAnchor.constraint(equalTo: countriesView.centerYAnchor,  constant: -5),
            countriesSeperatorView.widthAnchor.constraint(equalToConstant: 1),
            countriesSeperatorView.heightAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    private func setupFlightImageViewConstraints() {
        NSLayoutConstraint.activate([
            flightImageView.centerXAnchor.constraint(equalTo: countriesView.centerXAnchor),
            flightImageView.centerYAnchor.constraint(equalTo: countriesView.centerYAnchor, constant: -10),
            flightImageView.widthAnchor.constraint(equalToConstant: 40),
            flightImageView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupFromCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            fromCountryLabel.topAnchor.constraint(equalTo: countriesView.topAnchor, constant: 5),
            fromCountryLabel.leadingAnchor.constraint(equalTo: countriesView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupToCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            toCountryLabel.topAnchor.constraint(equalTo: countriesView.topAnchor, constant: 5),
            toCountryLabel.leadingAnchor.constraint(equalTo: countriesSeperatorView.trailingAnchor, constant: 10)
        ])
    }
    
    
    private func setupFromCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            fromCountryImageView.centerYAnchor.constraint(equalTo: countriesView.centerYAnchor, constant: -15),
            fromCountryImageView.trailingAnchor.constraint(equalTo: countriesSeperatorView.leadingAnchor, constant: -50),
            fromCountryImageView.heightAnchor.constraint(equalToConstant: 64),
            fromCountryImageView.widthAnchor.constraint(equalToConstant: 64)
        ])
    }
    
    private func setupToCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            toCountryImageView.centerYAnchor.constraint(equalTo: countriesView.centerYAnchor, constant: -15),
            toCountryImageView.leadingAnchor.constraint(equalTo: countriesSeperatorView.trailingAnchor, constant: 50),
            toCountryImageView.heightAnchor.constraint(equalToConstant: 64),
            toCountryImageView.widthAnchor.constraint(equalToConstant: 64)
        ])
    }
    
    private func setupFromCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            fromCountryNameLabel.topAnchor.constraint(equalTo: fromCountryImageView.bottomAnchor, constant: 2),
            fromCountryNameLabel.centerXAnchor.constraint(equalTo: fromCountryImageView.centerXAnchor)
        ])
    }
    
    private func setupToCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            toCountryNameLabel.topAnchor.constraint(equalTo: toCountryImageView.bottomAnchor, constant: 2),
            toCountryNameLabel.centerXAnchor.constraint(equalTo: toCountryImageView.centerXAnchor)
        ])
    }
    
    private func setupFlightDateViewConstraints() {
        NSLayoutConstraint.activate([
            flightDateView.heightAnchor.constraint(equalToConstant: 25),
            flightDateView.bottomAnchor.constraint(equalTo: countriesView.bottomAnchor, constant: -10),
            flightDateView.centerXAnchor.constraint(equalTo: countriesView.centerXAnchor)
        ])
    }
    
    private func setupFlightDateLabelConstraints() {
        NSLayoutConstraint.activate([
            
            flightDateLabel.leadingAnchor.constraint(equalTo: flightDateView.leadingAnchor, constant: 20),
            flightDateLabel.trailingAnchor.constraint(equalTo: flightDateView.trailingAnchor, constant: -20),
            flightDateLabel.centerYAnchor.constraint(equalTo: flightDateView.centerYAnchor)
        ])
    }
    
    
    
    private func setupTransiteRoundedViewConstraints() {
        NSLayoutConstraint.activate([
            transiteRoundedView.widthAnchor.constraint(equalToConstant: 100),
            transiteRoundedView.heightAnchor.constraint(equalToConstant: 41)
        ])
    }
    
    private func setupTransiteLabelConstraints() {
        NSLayoutConstraint.activate([
            transiteLabel.centerXAnchor.constraint(equalTo: transiteRoundedView.centerXAnchor),
            transiteLabel.centerYAnchor.constraint(equalTo: transiteRoundedView.centerYAnchor),
        ])
    }
    
    private func setupTransiteCountryViewConstraints() {
        NSLayoutConstraint.activate([
            transiteCountriesView.heightAnchor.constraint(equalToConstant: 58),
            transiteCountriesView.leadingAnchor.constraint(equalTo: transiteStackView.leadingAnchor, constant: 0),
            transiteCountriesView.trailingAnchor.constraint(equalTo: transiteStackView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupTransitedCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            transitedCountryImageView.centerYAnchor.constraint(equalTo: transiteCountriesView.centerYAnchor),
            transitedCountryImageView.leadingAnchor.constraint(equalTo: transiteCountriesView.leadingAnchor, constant: 10),
            transitedCountryImageView.heightAnchor.constraint(equalToConstant: 46),
            transitedCountryImageView.widthAnchor.constraint(equalToConstant: 46)
        ])
    }
    
    private func setupPauseImageViewConstraints() {
        NSLayoutConstraint.activate([
            pausedImageView.centerYAnchor.constraint(equalTo: transiteCountriesView.centerYAnchor),
            pausedImageView.trailingAnchor.constraint(equalTo: transiteCountriesView.trailingAnchor, constant: -10),
            pausedImageView.heightAnchor.constraint(equalToConstant: 27),
            pausedImageView.widthAnchor.constraint(equalToConstant: 27)
        ])
    }
    
    private func setupTransitedCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            transitedCountryNameLabel.centerYAnchor.constraint(equalTo: transitedCountryImageView.centerYAnchor, constant: -10),
            transitedCountryNameLabel.leadingAnchor.constraint(equalTo: transitedCountryImageView.trailingAnchor, constant: 10),
        ])
    }
    
    private func setupTransitedTimeLabelConstraints() {
        NSLayoutConstraint.activate([
            transitedTimeLabel.centerYAnchor.constraint(equalTo: transitedCountryImageView.centerYAnchor, constant: 10),
            transitedTimeLabel.leadingAnchor.constraint(equalTo: transitedCountryImageView.trailingAnchor, constant: 10),
        ])
    }
    
    private func setupTransiteStackViewConstraints() {
        NSLayoutConstraint.activate([
            transiteStackView.topAnchor.constraint(equalTo: countriesView.bottomAnchor, constant: 0),
            transiteStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            transiteStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
        ])
    }
    
    
    private func setupOfferDetailsViewConstraints() {
        NSLayoutConstraint.activate([
            offerDetailsView.topAnchor.constraint(equalTo: transiteStackView.bottomAnchor, constant: 15),
            offerDetailsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            offerDetailsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            offerDetailsView.heightAnchor.constraint(equalToConstant: 206.25)
        ])
    }
    
    private func setupOfferDetailsTableViewConstraints() {
        NSLayoutConstraint.activate([
            offerDetailsTableView.topAnchor.constraint(equalTo: offerDetailsView.topAnchor, constant: 4),
            offerDetailsTableView.leadingAnchor.constraint(equalTo: offerDetailsView.leadingAnchor, constant: 4),
            offerDetailsTableView.trailingAnchor.constraint(equalTo: offerDetailsView.trailingAnchor, constant: -4),
            offerDetailsTableView.bottomAnchor.constraint(equalTo: offerDetailsView.bottomAnchor, constant: -4)
        ])
    }
    
    private func setupServicesViewConstraints() {
        NSLayoutConstraint.activate([
            servicesView.topAnchor.constraint(equalTo: offerDetailsView.bottomAnchor, constant: 15),
            servicesView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            servicesView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            servicesView.heightAnchor.constraint(equalToConstant: 155)
        ])
    }
    
    private func setupServiceImageViewConstraints() {
        NSLayoutConstraint.activate([
            serviceImageView.topAnchor.constraint(equalTo: servicesView.topAnchor, constant: 10),
            serviceImageView.leadingAnchor.constraint(equalTo: servicesView.leadingAnchor, constant: 10),
            serviceImageView.heightAnchor.constraint(equalToConstant: 32),
            serviceImageView.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    private func setupServicesLabelConstraints() {
        NSLayoutConstraint.activate([
            servicesLabel.centerYAnchor.constraint(equalTo: serviceImageView.centerYAnchor),
            servicesLabel.leadingAnchor.constraint(equalTo: serviceImageView.trailingAnchor, constant: 8)
        ])
    }
    
    private func setupServicesCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            servicesCollectionView.topAnchor.constraint(equalTo: serviceImageView.bottomAnchor, constant: 15),
            servicesCollectionView.trailingAnchor.constraint(equalTo: servicesView.trailingAnchor, constant: -10),
            servicesCollectionView.leadingAnchor.constraint(equalTo: servicesView.leadingAnchor, constant: 10),
            servicesCollectionView.bottomAnchor.constraint(equalTo: servicesView.bottomAnchor, constant: -20),
        ])
    }
    
    private func setupHotelDetailsViewConstraints() {
        NSLayoutConstraint.activate([
            hotelDetailsView.topAnchor.constraint(equalTo: servicesView.bottomAnchor, constant: 15),
            hotelDetailsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            hotelDetailsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            hotelDetailsView.heightAnchor.constraint(equalToConstant: 93)
        ])
    }
    
    private func setupHotelImageViewConstraints() {
        NSLayoutConstraint.activate([
            hotelImageView.centerYAnchor.constraint(equalTo: hotelDetailsView.centerYAnchor),
            hotelImageView.leadingAnchor.constraint(equalTo: hotelDetailsView.leadingAnchor, constant: 10),
            hotelImageView.heightAnchor.constraint(equalToConstant: 74),
            hotelImageView.widthAnchor.constraint(equalToConstant: 74)
        ])
    }
    
    private func setupHotelNameLabelConstraints() {
        NSLayoutConstraint.activate([
            hotelNameLabel.centerYAnchor.constraint(equalTo: hotelImageView.centerYAnchor, constant: -15),
            hotelNameLabel.leadingAnchor.constraint(equalTo: hotelImageView.trailingAnchor, constant: 10),
            hotelNameLabel.trailingAnchor.constraint(equalTo: hotelDetailsView.trailingAnchor, constant: -4)
        ])
    }
    
    private func setupHotelRateViewConstraints() {
        NSLayoutConstraint.activate([
            hotelRateView.leadingAnchor.constraint(equalTo: hotelImageView.trailingAnchor, constant: 10),
            hotelRateView.topAnchor.constraint(equalTo: hotelNameLabel.bottomAnchor, constant: 5)
        ])
    }
    
    private func setupHotelRateLabelConstraints() {
        NSLayoutConstraint.activate([
            hotelRateLabel.centerYAnchor.constraint(equalTo: hotelRateView.centerYAnchor, constant: -3),
            hotelRateLabel.leadingAnchor.constraint(equalTo: hotelRateView.trailingAnchor, constant: 4),
            hotelRateLabel.trailingAnchor.constraint(equalTo: hotelDetailsView.trailingAnchor, constant: 0)
        ])
    }
    
    
    private func setupAdultViewConstraints() {
        NSLayoutConstraint.activate([
            adultView.heightAnchor.constraint(equalToConstant: 111),
        ])
    }
    
    private func setupAdultSperatorViewConstraints() {
        NSLayoutConstraint.activate([
            adultSeperatorView.centerYAnchor.constraint(equalTo: adultView.centerYAnchor),
            adultSeperatorView.leadingAnchor.constraint(equalTo: adultView.leadingAnchor, constant: 0),
            adultSeperatorView.trailingAnchor.constraint(equalTo: adultView.trailingAnchor, constant: 0),
            adultSeperatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    
    private func setupAdultImageViewConstraints() {
        NSLayoutConstraint.activate([
            adultImageView.topAnchor.constraint(equalTo: adultView.topAnchor, constant: 10),
            adultImageView.leadingAnchor.constraint(equalTo: adultView.leadingAnchor, constant: 5),
            adultImageView.heightAnchor.constraint(equalToConstant: 32),
            adultImageView.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    private func setupAdultPriceTitleConstraints() {
        NSLayoutConstraint.activate([
            adultPriceTitle.centerYAnchor.constraint(equalTo: adultImageView.centerYAnchor, constant: -10),
            adultPriceTitle.leadingAnchor.constraint(equalTo: adultImageView.trailingAnchor, constant: 5),
            adultPriceTitle.trailingAnchor.constraint(equalTo: adultView.trailingAnchor, constant: 0)

        ])
    }
    
    private func setupAdultDescriptionConstraints() {
        NSLayoutConstraint.activate([
            adultDescriptionLabel.centerYAnchor.constraint(equalTo: adultImageView.centerYAnchor, constant: 10),
            adultDescriptionLabel.leadingAnchor.constraint(equalTo: adultImageView.trailingAnchor, constant: 5),
            adultDescriptionLabel.trailingAnchor.constraint(equalTo: adultView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupAdultPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            adultPriceLabel.centerXAnchor.constraint(equalTo: adultView.centerXAnchor),
            adultPriceLabel.topAnchor.constraint(equalTo: adultSeperatorView.bottomAnchor, constant: 5),
        ])
    }
    
    private func setupChildSperatorViewConstraints() {
        NSLayoutConstraint.activate([
            childSeperatorView.centerYAnchor.constraint(equalTo: childView.centerYAnchor),
            childSeperatorView.leadingAnchor.constraint(equalTo: childView.leadingAnchor, constant: 0),
            childSeperatorView.trailingAnchor.constraint(equalTo: childView.trailingAnchor, constant: 0),
            childSeperatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    private func setupChildImageViewConstraints() {
        NSLayoutConstraint.activate([
            childImageView.topAnchor.constraint(equalTo: childView.topAnchor, constant: 10),
            childImageView.leadingAnchor.constraint(equalTo: childView.leadingAnchor, constant: 5),
            childImageView.heightAnchor.constraint(equalToConstant: 32),
            childImageView.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    private func setupChildPriceTitleConstraints() {
        NSLayoutConstraint.activate([
            childPriceTitle.centerYAnchor.constraint(equalTo: childImageView.centerYAnchor, constant: -10),
            childPriceTitle.leadingAnchor.constraint(equalTo: childImageView.trailingAnchor, constant: 5),
            childPriceTitle.trailingAnchor.constraint(equalTo: childView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupChildDescriptionLabelConstraints() {
        NSLayoutConstraint.activate([
            childDescriptionLabel.centerYAnchor.constraint(equalTo: childImageView.centerYAnchor, constant: 10),
            childDescriptionLabel.leadingAnchor.constraint(equalTo: childImageView.trailingAnchor, constant: 5),
            childDescriptionLabel.trailingAnchor.constraint(equalTo: childView.trailingAnchor, constant: 0)
        ])
    }
    
    
    private func setupChildPriceLabelConstraints() {
        NSLayoutConstraint.activate([
            childPriceLabel.centerXAnchor.constraint(equalTo: childView.centerXAnchor),
            childPriceLabel.topAnchor.constraint(equalTo: childSeperatorView.bottomAnchor, constant: 5),
        ])
    }
    
    
    private func setupPriceStackViewConstraints() {
        NSLayoutConstraint.activate([
            priceStackView.topAnchor.constraint(equalTo: hotelDetailsView.bottomAnchor, constant: 15),
            priceStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            priceStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
        ])
    }
    
    private func setupCompanyDetailsViewConstraints() {
        companyDetailsViewHeight = companyDetailsView.heightAnchor.constraint(equalToConstant: 80)
        NSLayoutConstraint.activate([
            companyDetailsViewHeight,
            companyDetailsView.topAnchor.constraint(equalTo: priceStackView.bottomAnchor, constant: 15),
            companyDetailsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            companyDetailsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10)
        ])
    }
    
    private func setupCompanyImageViewConstraints() {
        NSLayoutConstraint.activate([
            companyImageView.centerYAnchor.constraint(equalTo: companyDetailsView.centerYAnchor),
            companyImageView.leadingAnchor.constraint(equalTo: companyDetailsView.leadingAnchor, constant: 5),
            companyImageView.heightAnchor.constraint(equalToConstant: 64),
            companyImageView.widthAnchor.constraint(equalToConstant: 64)
        ])
    }
    
    private func setupCompanyNameLabelConstraints() {
        NSLayoutConstraint.activate([
            companyNameLabel.centerYAnchor.constraint(equalTo: companyImageView.centerYAnchor, constant: -10),
            companyNameLabel.leadingAnchor.constraint(equalTo: companyImageView.trailingAnchor, constant: 5),
            companyNameLabel.trailingAnchor.constraint(equalTo: moreImageView.leadingAnchor, constant: 4)
        ])
    }
   
    private func setupCountryOfCountryNameLabelConstraints() {
        NSLayoutConstraint.activate([
            countryOfCompanyNameLabel.centerYAnchor.constraint(equalTo: companyImageView.centerYAnchor, constant: 10),
            countryOfCompanyNameLabel.leadingAnchor.constraint(equalTo: companyImageView.trailingAnchor, constant: 5),
            countryOfCompanyNameLabel.trailingAnchor.constraint(equalTo: companyDetailsView.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupMoreImageViewConstraints() {
        NSLayoutConstraint.activate([
            moreImageView.centerYAnchor.constraint(equalTo: companyImageView.centerYAnchor),
            moreImageView.trailingAnchor.constraint(equalTo: companyDetailsView.trailingAnchor, constant: -5),
            moreImageView.heightAnchor.constraint(equalToConstant: 30),
            moreImageView.widthAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    
    private func setupRequestReservationButtonConstraints() {

        requestReservationButtonHeight = requestReservationButton.heightAnchor.constraint(equalToConstant: 50)
        NSLayoutConstraint.activate([
            requestReservationButtonHeight,
            requestReservationButton.topAnchor.constraint(equalTo: companyDetailsView.bottomAnchor, constant: 15),
            requestReservationButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            requestReservationButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
        ])
    }
    

    private func setupContactsViewConstraints() {
        conactsViewHeight = contactsView.heightAnchor.constraint(equalToConstant: 85)
        NSLayoutConstraint.activate([
            conactsViewHeight,
            contactsView.topAnchor.constraint(equalTo: requestReservationButton.bottomAnchor, constant: 15),
            contactsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            contactsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            contactsView.bottomAnchor.constraint(lessThanOrEqualTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupSperatorVertivalViewConstraints() {
        NSLayoutConstraint.activate([
            seperatorVerticallView.topAnchor.constraint(equalTo: contactsView.topAnchor, constant: 0),
            seperatorVerticallView.centerXAnchor.constraint(equalTo: contactsView.centerXAnchor),
            seperatorVerticallView.bottomAnchor.constraint(equalTo: contactsView.bottomAnchor, constant: 0),
            seperatorVerticallView.widthAnchor.constraint(equalToConstant: 1),
        ])
    }
    
    
    private func setupCallButtonConstraints() {
        NSLayoutConstraint.activate([
            callButton.centerYAnchor.constraint(equalTo: contactsView.centerYAnchor, constant: -15),
            callButton.trailingAnchor.constraint(equalTo: seperatorVerticallView.leadingAnchor, constant: -60),
            callButton.widthAnchor.constraint(equalToConstant: 40),
            callButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func setupMessageButtonConstraints() {
        NSLayoutConstraint.activate([
            messageButton.centerYAnchor.constraint(equalTo: contactsView.centerYAnchor, constant: -15),
            messageButton.leadingAnchor.constraint(equalTo: seperatorVerticallView.trailingAnchor, constant: 60),
            messageButton.widthAnchor.constraint(equalToConstant: 40),
            messageButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    
    private func setupCallLabelConstraints() {
        NSLayoutConstraint.activate([
            callLabel.centerXAnchor.constraint(equalTo: callButton.centerXAnchor),
            callLabel.topAnchor.constraint(equalTo: callButton.bottomAnchor, constant: 5),
        ])
    }
    
    private func setupMessageabelConstraints() {
        NSLayoutConstraint.activate([
            messageabel.centerXAnchor.constraint(equalTo: messageButton.centerXAnchor),
            messageabel.topAnchor.constraint(equalTo: messageButton.bottomAnchor, constant: 5),
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupImagesCollectionViewConstraints()
        setupPageControlConstraints()
        setupResidenceViewContainerConstraints()
        setupTicketViewContainerConstraints()
        setupResidenceImageViewConstraints()
        setupTicketImageViewConstraints()
        setupResidenceLabelViewConstraints()
        setupTicketLabelViewConstraints()
        setupStackViewConstraints()
        setupCountriesViewConstraints()
        setupCountriesSeperatorViewConstraints()
        setupFlightImageViewConstraints()
        setupTransiteRoundedViewConstraints()
        setupFromCountryLabelConstraints()
        setupToCountryLabelConstraints()
        setupToCountryImageViewConstraints()
        setupFromCountryImageViewConstraints()
        setupFromCountryNameLabelConstraints()
        setupToCountryNameLabelConstraints()
        setupFlightDateViewConstraints()
        setupFlightDateLabelConstraints()
        setupTransiteRoundedViewConstraints()
        setupTransiteLabelConstraints()
        setupTransiteCountryViewConstraints()
        setupTransitedCountryImageViewConstraints()
        setupPauseImageViewConstraints()
        setupTransitedCountryNameLabelConstraints()
        setupTransitedTimeLabelConstraints()
        setupTransiteStackViewConstraints()
        
        setupOfferDetailsViewConstraints()
        setupOfferDetailsTableViewConstraints()
        setupServicesViewConstraints()
        setupServiceImageViewConstraints()
        setupServicesLabelConstraints()
        setupServicesCollectionViewConstraints()
        
        setupHotelDetailsViewConstraints()
        setupHotelImageViewConstraints()
        setupHotelNameLabelConstraints()
        setupHotelRateViewConstraints()
        setupHotelRateLabelConstraints()
        
        setupAdultViewConstraints()
        setupAdultSperatorViewConstraints()
        setupAdultImageViewConstraints()
        setupAdultPriceTitleConstraints()
        setupAdultDescriptionConstraints()
        setupAdultPriceLabelConstraints()
        
        setupChildSperatorViewConstraints()
        setupChildImageViewConstraints()
        setupChildPriceTitleConstraints()
        setupChildDescriptionLabelConstraints()
        setupChildPriceLabelConstraints()
        
        setupPriceStackViewConstraints()
        
        setupCompanyDetailsViewConstraints()
        setupCompanyImageViewConstraints()
        setupCompanyNameLabelConstraints()
        setupCountryOfCountryNameLabelConstraints()
        setupMoreImageViewConstraints()
        
        setupRequestReservationButtonConstraints()
        
        setupContactsViewConstraints()
        setupSperatorVertivalViewConstraints()
        setupCallButtonConstraints()
        setupMessageButtonConstraints()
        setupCallLabelConstraints()
        setupMessageabelConstraints()
        
    }
    
    func configure(viewModel: OfferDetailsViewModel) {
        
        residenceView.isHidden = !viewModel.hasResidence
        ticketView.isHidden = !viewModel.hasFlight
        fromCountryImageView.load(url: viewModel.fromCityImageUrl)
        toCountryImageView.load(url: viewModel.toCityImageUrl)
        fromCountryNameLabel.text = viewModel.fromCityName
        toCountryNameLabel.text = viewModel.toCityName
        flightDateLabel.text = viewModel.offerPeriod
        transiteRoundedView.isHidden = !viewModel.isTransit
        transiteCountriesView.isHidden = !viewModel.isTransit
        transitedCountryImageView.load(url: viewModel.transitCountryImageUrl)
        transitedCountryNameLabel.text = viewModel.transitCountryName
        transitedTimeLabel.text = viewModel.transitTime
        hotelNameLabel.text = viewModel.hotelName
        hotelRateLabel.text = viewModel.hotelRateText
        hotelRateView.rating = viewModel.hotelRate
        hotelRateView.settings.totalStars = Int(viewModel.hotelRate)
        adultPriceLabel.text = viewModel.adultPriceString
        childPriceLabel.text = viewModel.childPriceString
        companyImageView.load(url: viewModel.companyImageUrl)
        companyNameLabel.text = viewModel.companyName
        countryOfCompanyNameLabel.text = viewModel.countryOfCompany
        
    }
    
    func prepareForMyOffer() {
        companyDetailsViewHeight.constant = 0
        companyDetailsView.isHidden = true
        conactsViewHeight.constant = 0
        contactsView.isHidden = true
        requestReservationButtonHeight.constant = 0
    }
    
    func updateReservationButtonHeight() {
        requestReservationButtonHeight.constant = 0
    }
    
}
