//
//  OfferDetailsPresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class OfferDetailsPresenter: OfferDetailsPresenterProtocol, OfferDetailsInteractorOutputProtocol {
    
    weak var view: OfferDetailsViewProtocol?
    private let interactor: OfferDetailsInteractorInputProtocol
    private let router: OfferDetailsRouterProtocol
    private let offerId: Int
    private let indexPath: IndexPath
    private weak var delegate: UpdateOfferProtocol?
    private var offerImages =  [URL?]()
    private var offerServices = [ServiceViewModel]()
    private var flightDetails = [FlightDetail]()
    private var offerDetailsViewModel: OfferDetailsViewModel!
    private var userViewModel: UserViewModel!
    
    var offerImagesNumberOfRows: Int {
        return offerImages.count
    }
    
    var offerServicesNumberOfRows: Int {
        return offerServices.count
    }
    
    var flightDetailsNumberOfRows: Int {
        return flightDetails.count
    }
    
    init(view: OfferDetailsViewProtocol, interactor: OfferDetailsInteractorInputProtocol, router: OfferDetailsRouterProtocol, offerId: Int, delegate: UpdateOfferProtocol?, indexPath: IndexPath?) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.offerId = offerId
        self.delegate = delegate 
        self.indexPath = indexPath ?? IndexPath(row: 0, section: 0)
    }
    
    func viewDidLoad() {
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        interactor.getOfferDetails(offerId: offerId)
    }
    
    func didFetchOfferDetails(with result: Result<OfferDetails>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            userViewModel = UserViewModel(user: value.company)
            offerDetailsViewModel = OfferDetailsViewModel(offerDetails: value)
            offerImages = L102Language.currentLanguage == "ar" ? offerDetailsViewModel.images.reversed() : offerDetailsViewModel.images
            offerServices = offerDetailsViewModel.services
            flightDetails = offerDetailsViewModel.flightDetails
            view?.setOfferDetailsData(with: offerDetailsViewModel)
            view?.reloadCollectionViews()
            view?.reloadTableView()
            
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didChangeFavourite(with result: Result<Data>) {
        switch result {
        case .success:
            delegate?.updateOffer?(indexPath: indexPath, isLiked: offerDetailsViewModel.isLiked)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<URL?>, at indexPath: IndexPath) {
        cell.configure(model: offerImages[indexPath.item])
    }
    
    func configure(cell: AnyConfigurableCell<FlightDetail>, at indextPath: IndexPath) {
        cell.configure(model: flightDetails[indextPath.row])
    }
    
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath) {
        cell.configure(model: offerServices[indexPath.item])
    }
    
    func callButtonTapped(number1: String, number2: String) {
        guard userViewModel != nil else { return }
        router.showActionSheet(number1: number1, number2: number2)
    }
    
    func favouriteButtonTapped(isLiked: Bool) {
        if isLiked {
            interactor.addFavourite(offerId: offerId)
        } else {
            interactor.removeFavourite(offerId: offerId)
        }
    }
    
    func companyViewTapped() {
        guard userViewModel != nil else { return }
        router.navigateTo(destination: .companyDetails(viewModel: userViewModel))
    }
    
    func messageButtonTapped() {
        if UserDefaultsHelper.isLoggedIn {
            guard userViewModel != nil else { return }
            router.navigateTo(destination: .chat(userId: userViewModel.id, userImage: userViewModel.imageUrl, username: userViewModel.username, rate: userViewModel.rate))
        } else {
            router.showAlert(with: "loginFirst".localized())
        }
    }
    
    func requestResrevationButtonTapped() {
        guard userViewModel != nil else { return }
        guard offerDetailsViewModel != nil else { return }
        router.navigateTo(destination: .requestReservation(offerDetailsViewModel: offerDetailsViewModel))
    }
    
    func showLoginAlert() {
        router.showAlert(with: "loginFirst".localized())
    }
    
    func shareButtonTapped() {
        guard offerDetailsViewModel != nil else { return }
        guard let link = URL(string: "\(Constants.url)\(Constants.offerDetailsEndpoint)/\(offerDetailsViewModel.id)") else { return }
        router.navigateTo(destination: .share(url: link))
    }
}
