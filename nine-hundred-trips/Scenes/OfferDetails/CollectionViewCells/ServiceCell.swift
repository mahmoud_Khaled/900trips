//
//  ServiceCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/5/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ServiceCell: UICollectionViewCell, ConfigurableCell {
    
    private lazy var serviceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var serviceNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColor
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.text = "اكلات متنوعه شرقيه"
        label.textAlignment = .center
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    private func addSubViews() {
        addSubview(serviceImageView)
        addSubview(serviceNameLabel)
    }
    
    private func setupOfferImageViewConstraints() {
        NSLayoutConstraint.activate([
            serviceImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            serviceImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            serviceImageView.heightAnchor.constraint(equalToConstant: 50),
            serviceImageView.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupServiceNameConstraints() {
        NSLayoutConstraint.activate([
            serviceNameLabel.topAnchor.constraint(equalTo: serviceImageView.bottomAnchor, constant: 4),
            serviceNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            serviceNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
        ])
    }
    
    
    private func layoutUI() {
        addSubViews()
        setupOfferImageViewConstraints()
        setupServiceNameConstraints()
    }
    
    func configure(model: ServiceViewModel) {
        serviceImageView.load(url: model.imageUrl)
        serviceNameLabel.text = model.name
    }
}
