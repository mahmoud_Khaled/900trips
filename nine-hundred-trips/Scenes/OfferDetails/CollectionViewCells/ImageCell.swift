//
//  ImageCell.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell, ConfigurableCell {
    
    private lazy var offerImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "offerimage"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    private func addSubViews() {
        addSubview(offerImageView)
    }
    
    private func setupOfferImageeViewConstraints() {
        NSLayoutConstraint.activate([
            offerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            offerImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            offerImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            offerImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
    }

    
    private func layoutUI() {
        addSubViews()
        setupOfferImageeViewConstraints()
    }
    
    func configure(model: URL?) {
        offerImageView.load(url: model)
    }
    
}
