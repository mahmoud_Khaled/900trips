//
//  OfferDetailsRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum OfferDetailsDestination {
    case companyDetails(viewModel: UserViewModel)
    case chat(userId: Int, userImage: URL?, username: String, rate: String)
    case share(url: URL)
    case requestReservation(offerDetailsViewModel: OfferDetailsViewModel)
}

class OfferDetailsRouter: OfferDetailsRouterProtocol {
   
    weak var viewController: UIViewController?
    
    static func createModule(isMyOffer: Bool, indexPath: IndexPath?, offerId: Int, delegate: UpdateOfferProtocol?) -> UIViewController {
        let view = OfferDetailsViewController(isMyOffer: isMyOffer)
        let interactor = OfferDetailsInteractor()
        let router = OfferDetailsRouter()
        let presenter = OfferDetailsPresenter(view: view, interactor: interactor, router: router, offerId: offerId, delegate: delegate, indexPath: indexPath)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
    func showActionSheet(number1: String, number2: String) {
        
        let actionSheet = AlertBuilder(title: "numbers".localized(), message: nil, preferredStyle: .actionSheet)
        
        if !number1.isEmpty {
            _ =  actionSheet.addAction(title: number1, style: .default) { [weak self] in
                guard let self = self else { return }
                self.callNumber(number: number1)
            }
        }
        
        if !number2.isEmpty {
           _ = actionSheet.addAction(title: number2, style: .default) { [weak self] in
                guard let self = self else { return }
                self.callNumber(number: number2)
            }
        }
        
        if number1.isEmpty && number2.isEmpty {
            showAlert(with: "noPhoneNumbers".localized())
            return
        }
        
        actionSheet.addAction(title: "cancel".localized(), style: .cancel)
            .build()
            .show()
    }
    
    private func callNumber(number: String) {
        guard let url = URL(string: ("tel://" + number)) else { return }
        UIApplication.shared.open(url)
    }
    
    func navigateTo(destination: OfferDetailsDestination) {
        switch destination {
        case .companyDetails(let viewModel):
            viewController?.navigationController?.pushViewController(CompanyDetailsRouter.createModule(with: viewModel), animated: true)
        case .chat(let userId, let userImage, let username, let rate):
            viewController?.tabBarController?.navigationController?.pushViewController(ChatRouter.createModule(with: userId, userImage: userImage, username: username, conversationId: nil, rate: rate), animated: true)
        case .share(let url):
            viewController?.present(UIActivityViewController(activityItems: [url], applicationActivities: nil), animated: true, completion: nil)
        case .requestReservation(let offerDetailsViewModel):
            viewController?.navigationController?.pushViewController(RequestReservationRouter.createModule(offerDetails: offerDetailsViewModel), animated: true)
        }
        
    }
    
}
