//
//  RequestQuotationCompletionProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol RequestQuotationCompletionRouterProtocol: class {
    func showAlert(with message: String)
}

protocol RequestQuotationCompletionPresenterProtocol: class {
    var view: RequestQuotationCompletionViewProtocol? { get set }
    func getCountries()
    func getCities(countryId: Int, for fetchType: FetchType)
    
    func requestQuotation(
        departureCityId: Int,
        arrivalCityId: Int,
        transitType: String,
        adultPassengersCount: String,
        childPassengersCount: String,
        babyPassengersCount: String,
        residenceType: String,
        roomsCount: String,
        personsInRoom: String,
        description: String,
        name: String,
        phone: String,
        email: String
    )
}

protocol RequestQuotationCompletionInteractorInputProtocol: class {
    var presenter: RequestQuotationCompletionInteractorOutputProtocol? { get set }
    func getCountries()
    func getCities(countryId: Int)
    func requestQuotation(model: RequestQuotation)
}

protocol RequestQuotationCompletionInteractorOutputProtocol: class {
    func didFetchCountries(with result: Result<[Country]>)
    func didFetchCities(with result: Result<[City]>)
    func didRequestQuotation(with result: Result<Order>)
}

protocol RequestQuotationCompletionViewProtocol: class {
    var presenter: RequestQuotationCompletionPresenterProtocol! { get set }
    func showActivityIndicator()
    func hideActivityIndicator()
    func didFetch(countries: [CountryViewModel])
    func didFetch(cities: [CityViewModel], for fetchType: FetchType)
    func showSuccessView()
    func resetInputsValues()
}
