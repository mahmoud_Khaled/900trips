//
//  RequestQuotationCompletionViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationCompletionViewController: BaseViewController, RequestQuotationCompletionViewProtocol {
    
    private let mainView = RequestQuotationCompletionView()
    var presenter: RequestQuotationCompletionPresenterProtocol!
    
    private var departureCountryId = 0
    private var departureCityId = 0
    
    private var arrivalCountryId = 0
    private var arrivalCityId = 0
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "requestQuotation".localized()
        presenter.getCountries()
        addTargets()
        setupNavigationBarStyle()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func addTargets() {
        
        mainView.departureCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCountryId = item.id
            self.mainView.departureCityTextField.text = ""
            self.departureCityId = 0
            self.presenter.getCities(countryId: self.departureCountryId, for: .departure)
        }
        
        mainView.departureCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.departureCityId = item.id
        }
        
        mainView.arrivalCountryTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCountryId = item.id
            self.mainView.arrivalCityTextField.text = ""
            self.arrivalCityId = 0
            self.presenter.getCities(countryId: self.arrivalCountryId, for: .arrival)
        }
        
        mainView.arrivalCityTextField.didSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.arrivalCityId = item.id
        }
        
        mainView.doneButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        
    }
    
    @objc private func doneButtonTapped() {
        presenter.requestQuotation(
            departureCityId: departureCityId,
            arrivalCityId: arrivalCityId,
            transitType: mainView.transitTypeTextField.text!,
            adultPassengersCount: mainView.travellersFormView.firstTextField.text!,
            childPassengersCount: mainView.travellersFormView.secondTextField.text!,
            babyPassengersCount: mainView.travellersFormView.thirdTextField.text!,
            residenceType: mainView.residenceFormView.firstTextField.text!,
            roomsCount: mainView.residenceFormView.secondTextField.text!,
            personsInRoom: mainView.residenceFormView.thirdTextField.text!,
            description: mainView.tripDescriptionTextView.text,
            name: mainView.personalInfoFormView.firstTextField.text!,
            phone: mainView.personalInfoFormView.secondTextField.text!,
            email: mainView.personalInfoFormView.thirdTextField.text!
        )
    }
    
    
    func showActivityIndicator() {
        view?.showProgressHUD(isUserInteractionEnabled: true)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
    func didFetch(countries: [CountryViewModel]) {
        mainView.departureCountryTextField.items = countries
        mainView.arrivalCountryTextField.items = countries
    }
    
    func didFetch(cities: [CityViewModel], for fetchType: FetchType) {
        switch fetchType {
        case .departure:
            mainView.departureCityTextField.items = cities
        case .arrival:
            mainView.arrivalCityTextField.items = cities
        }
    }
    
    func showSuccessView() {
        show(popupView: SuccessView(), model: "yourOrderHasBeenPlaced".localized(), on: tabBarController?.view, for: 2) {
            
        }
    }
    
    func resetInputsValues() {
        departureCountryId = 0
        departureCityId = 0
        arrivalCountryId = 0
        arrivalCityId = 0
        mainView.resetInputsValues()
    }
}

