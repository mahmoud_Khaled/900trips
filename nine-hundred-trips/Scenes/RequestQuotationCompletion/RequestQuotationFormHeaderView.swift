//
//  RequestQuotationFormHeaderView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationFormHeaderView: UIView {
    private lazy var headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mediumTurquoise
        return view
    }()
    
    private lazy var headerNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = DinNextFont.regular.getFont(ofSize: 17)
        label.text = "المسافرين"
        return label
    }()
    
    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_travellers"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        addSubview(headerView)
        headerView.addSubview(headerNameLabel)
        addSubview(headerImageView)
    }
    
    private func setupHeaderViewConstraints() {
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerView.heightAnchor.constraint(equalToConstant: 52)
            ])
    }
    
    private func setupHeaderNameLabelConstraints() {
        NSLayoutConstraint.activate([
            headerNameLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 4),
            headerNameLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor)
            ])
    }
    
    private func setupHeaderImageViewConstraints() {
        NSLayoutConstraint.activate([
            headerImageView.topAnchor.constraint(equalTo: headerNameLabel.bottomAnchor, constant: 8),
            headerImageView.widthAnchor.constraint(equalToConstant: 51),
            headerImageView.heightAnchor.constraint(equalToConstant: 51),
            headerImageView.centerXAnchor.constraint(equalTo: headerNameLabel.centerXAnchor)
            ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupHeaderViewConstraints()
        setupHeaderNameLabelConstraints()
        setupHeaderImageViewConstraints()
    }
    
    func configure(title: String, imageName: String) {
        headerNameLabel.text = title
        headerImageView.image = UIImage(named: imageName)
    }
    
}
