//
//  RequestQuotationCompletionInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationCompletionInteractor: RequestQuotationCompletionInteractorInputProtocol {
    
    weak var presenter: RequestQuotationCompletionInteractorOutputProtocol?
    
    private let applicationDataWorker = ApplicationDataWorker()
    private let orderWorker = OrderWorker()
    
    func getCountries() {
        applicationDataWorker.getCountries { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCountries(with: result)
        }
    }
    
    func getCities(countryId: Int) {
        applicationDataWorker.getCities(countryId: countryId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchCities(with: result)
        }
    }
    
    func requestQuotation(model: RequestQuotation) {
        orderWorker.requestQuotation(model: model) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRequestQuotation(with: result)
        }
    }
}
