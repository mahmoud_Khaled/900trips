//
//  RequestQuotationCompletionView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class RequestQuotationCompletionView: UIView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: IQPreviousNextView = {
        let view = IQPreviousNextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var departureContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var departureCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseDepartureCountry".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var departureCountryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var departureCityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseDepartureCity".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var departureCityTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "city".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var flightImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_flight_colored"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var arrivalContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 13
        return view
    }()
    
    private lazy var arrivalCountryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseArrivalCountry".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var arrivalCountryTextField: PickerViewTextField<CountryViewModel> = {
        let textField = PickerViewTextField<CountryViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "country".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var arrivalCityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "chooseArrivalCity".localized()
        label.font = DinNextFont.regular.getFont(ofSize: 13)
        label.textColor = .darkGray
        return label
    }()
    
    lazy var arrivalCityTextField: PickerViewTextField<CityViewModel> = {
        let textField = PickerViewTextField<CityViewModel>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .textColor
        textField.placeholder = "city".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    lazy var transitTypeTextField: PickerViewTextField<String> = {
        let textField = PickerViewTextField<String>()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.items = [
            "transit".localized(),
            "direct".localized()
        ]
        textField.font = DinNextFont.regular.getFont(ofSize: 20)
        textField.textColor = .coral
        textField.placeholder = "selectTransitType".localized()
        textField.layer.applySketchShadow(color: .lightCyan, alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        textField.layer.cornerRadius = 30.5
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 5))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5))
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var travellersView: RequestQuotationFormHeaderView = {
        let view = RequestQuotationFormHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(title: "travellers".localized(), imageName: "ic_travellers")
        return view
    }()
    
    lazy var travellersFormView: RequestQuotationFormView = {
        let view = RequestQuotationFormView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(
            firstInputOptions: TextFieldOptions(placeholder: "numberOfAdults".localized(), keyboardType: .asciiCapableNumberPad),
            secondInputOptions: TextFieldOptions(placeholder: "numberOfTravellersUnderTwelve".localized(), keyboardType: .asciiCapableNumberPad),
            thirdInputOptions: TextFieldOptions(placeholder: "numberOfTravellersUnderTwo".localized(), keyboardType: .asciiCapableNumberPad)
        )
        return view
    }()
    
    private lazy var residenceView: RequestQuotationFormHeaderView = {
        let view = RequestQuotationFormHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(title: "residenceForm".localized(), imageName: "ic_residence")
        return view
    }()
    
    lazy var residenceFormView: RequestQuotationFormView = {
        let view = RequestQuotationFormView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(
            firstInputOptions: TextFieldOptions(placeholder: "residenceType".localized()),
            secondInputOptions: TextFieldOptions(placeholder: "numberOfRooms".localized(), keyboardType: .asciiCapableNumberPad),
            thirdInputOptions: TextFieldOptions(placeholder: "numberOfPersonsInEachRoom".localized(), keyboardType: .asciiCapableNumberPad)
        )
        return view
    }()
    
    private lazy var tripDescriptionView: RequestQuotationFormHeaderView = {
        let view = RequestQuotationFormHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(title: "tripDescription".localized(), imageName: "ic_trip_description")
        return view
    }()
    
    private lazy var tripDescriptionContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var tripDescriptionTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .white
        textView.font = DinNextFont.regular.getFont(ofSize: 20)
        textView.textColor = .textColor
        textView.placeholder = "tripDescription".localized()
        textView.minHeight = 95
        textView.maxHeight = 95
        return textView
    }()
    
    private lazy var personalInfoView: RequestQuotationFormHeaderView = {
        let view = RequestQuotationFormHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(title: "personalInfo".localized(), imageName: "ic_personal_info")
        return view
    }()
    
    lazy var personalInfoFormView: RequestQuotationFormView = {
        let view = RequestQuotationFormView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.configure(
            firstInputOptions: TextFieldOptions(placeholder: "name".localized()),
            secondInputOptions: TextFieldOptions(placeholder: "phoneNumber".localized(), keyboardType: .phonePad),
            thirdInputOptions: TextFieldOptions(placeholder: "email".localized(), keyboardType: .emailAddress)
        )
        return view
    }()
    
    lazy var doneButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("done".localized(), for: .normal)
        button.backgroundColor = .coral
        button.layer.cornerRadius = 25
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = DinNextFont.medium.getFont(ofSize: 20)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(departureContainerView)
        departureContainerView.addSubview(departureCountryLabel)
        departureContainerView.addSubview(departureCountryTextField)
        departureContainerView.addSubview(departureCityLabel)
        departureContainerView.addSubview(departureCityTextField)
        containerView.addSubview(flightImageView)
        containerView.addSubview(arrivalContainerView)
        arrivalContainerView.addSubview(arrivalCountryLabel)
        arrivalContainerView.addSubview(arrivalCountryTextField)
        arrivalContainerView.addSubview(arrivalCityLabel)
        arrivalContainerView.addSubview(arrivalCityTextField)
        containerView.addSubview(transitTypeTextField)
        containerView.addSubview(travellersView)
        containerView.addSubview(travellersFormView)
        containerView.addSubview(residenceView)
        containerView.addSubview(residenceFormView)
        containerView.addSubview(tripDescriptionView)
        containerView.addSubview(tripDescriptionContainerView)
        tripDescriptionContainerView.addSubview(tripDescriptionTextView)
        containerView.addSubview(personalInfoView)
        containerView.addSubview(personalInfoFormView)
        containerView.addSubview(doneButton)
    }
    
    private func setupScrollViewConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        ])
    }
    
    private func setupDepartureContainerViewConstraints() {
        NSLayoutConstraint.activate([
            departureContainerView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 26),
            departureContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            departureContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupDepartureCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            departureCountryLabel.topAnchor.constraint(equalTo: departureContainerView.topAnchor, constant: 13),
            departureCountryLabel.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupDepartureCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            departureCountryTextField.topAnchor.constraint(equalTo: departureCountryLabel.bottomAnchor, constant: 13),
            departureCountryTextField.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 14.5),
            departureCountryTextField.trailingAnchor.constraint(equalTo: departureContainerView.trailingAnchor, constant: -8),
            departureCountryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupDepartureCityLabelConstraints() {
        NSLayoutConstraint.activate([
            departureCityLabel.topAnchor.constraint(equalTo: departureCountryTextField.bottomAnchor, constant: 14),
            departureCityLabel.leadingAnchor.constraint(equalTo: departureCountryLabel.leadingAnchor, constant: 0),
        ])
    }
    
    private func setupDepartureCityTextFieldConstraints() {
        NSLayoutConstraint.activate([
            departureCityTextField.topAnchor.constraint(equalTo: departureCityLabel.bottomAnchor, constant: 13),
            departureCityTextField.leadingAnchor.constraint(equalTo: departureCountryTextField.leadingAnchor, constant: 0),
            departureCityTextField.trailingAnchor.constraint(equalTo: departureCountryTextField.trailingAnchor, constant: 0),
            departureCityTextField.heightAnchor.constraint(equalToConstant: 61),
            departureCityTextField.bottomAnchor.constraint(equalTo: departureContainerView.bottomAnchor, constant: -26)
        ])
    }
    
    private func setupFlightImageViewConstraints() {
        NSLayoutConstraint.activate([
            flightImageView.heightAnchor.constraint(equalToConstant: 40),
            flightImageView.widthAnchor.constraint(equalToConstant: 40),
            flightImageView.topAnchor.constraint(equalTo: departureContainerView.bottomAnchor, constant: 19),
            flightImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupArrivalContainerViewConstraints() {
        NSLayoutConstraint.activate([
            arrivalContainerView.topAnchor.constraint(equalTo: flightImageView.bottomAnchor, constant: 19),
            arrivalContainerView.leadingAnchor.constraint(equalTo: departureContainerView.leadingAnchor, constant: 0),
            arrivalContainerView.trailingAnchor.constraint(equalTo: departureContainerView.trailingAnchor, constant: 0),
            arrivalContainerView.heightAnchor.constraint(equalToConstant: 241)
        ])
    }
    
    
    private func setupArrivalCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            arrivalCountryLabel.topAnchor.constraint(equalTo: arrivalContainerView.topAnchor, constant: 13),
            arrivalCountryLabel.leadingAnchor.constraint(equalTo: arrivalContainerView.leadingAnchor, constant: 15)
        ])
    }
    
    private func setupArrivalCountryTextFieldConstraints() {
        NSLayoutConstraint.activate([
            arrivalCountryTextField.topAnchor.constraint(equalTo: arrivalCountryLabel.bottomAnchor, constant: 13),
            arrivalCountryTextField.leadingAnchor.constraint(equalTo: arrivalContainerView.leadingAnchor, constant: 14.5),
            arrivalCountryTextField.trailingAnchor.constraint(equalTo: arrivalContainerView.trailingAnchor, constant: -8),
            arrivalCountryTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupArrivalCityLabelConstraints() {
        NSLayoutConstraint.activate([
            arrivalCityLabel.topAnchor.constraint(equalTo: arrivalCountryTextField.bottomAnchor, constant: 14),
            arrivalCityLabel.leadingAnchor.constraint(equalTo: arrivalCountryLabel.leadingAnchor, constant: 0),
        ])
    }
    
    private func setupArrivalCityTextFieldConstraints() {
        NSLayoutConstraint.activate([
            arrivalCityTextField.topAnchor.constraint(equalTo: arrivalCityLabel.bottomAnchor, constant: 13),
            arrivalCityTextField.leadingAnchor.constraint(equalTo: arrivalCountryTextField.leadingAnchor, constant: 0),
            arrivalCityTextField.trailingAnchor.constraint(equalTo: arrivalCountryTextField.trailingAnchor, constant: 0),
            arrivalCityTextField.heightAnchor.constraint(equalToConstant: 61),
            arrivalCityTextField.bottomAnchor.constraint(equalTo: arrivalContainerView.bottomAnchor, constant: -26)
        ])
    }
    
    private func setupTransitTypeTextFieldConstraints() {
        NSLayoutConstraint.activate([
            transitTypeTextField.topAnchor.constraint(equalTo: arrivalContainerView.bottomAnchor, constant: 16),
            transitTypeTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
            transitTypeTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16),
            transitTypeTextField.heightAnchor.constraint(equalToConstant: 61)
        ])
    }
    
    private func setupTravellersViewConstraints() {
        NSLayoutConstraint.activate([
            travellersView.topAnchor.constraint(equalTo: transitTypeTextField.bottomAnchor, constant: 18),
            travellersView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            travellersView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            travellersView.heightAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupTravellersFormViewConstraints() {
        NSLayoutConstraint.activate([
            travellersFormView.topAnchor.constraint(equalTo: travellersView.bottomAnchor, constant: 18),
            travellersFormView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            travellersFormView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupResidenceViewConstraints() {
        NSLayoutConstraint.activate([
            residenceView.topAnchor.constraint(equalTo: travellersFormView.bottomAnchor, constant: 18),
            residenceView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            residenceView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            residenceView.heightAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupResidenceFormViewConstraints() {
        NSLayoutConstraint.activate([
            residenceFormView.topAnchor.constraint(equalTo: residenceView.bottomAnchor, constant: 18),
            residenceFormView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            residenceFormView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupTripDescriptionViewConstraints() {
        NSLayoutConstraint.activate([
            tripDescriptionView.topAnchor.constraint(equalTo: residenceFormView.bottomAnchor, constant: 18),
            tripDescriptionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            tripDescriptionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            tripDescriptionView.heightAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupTripDescriptionContainerViewConstraints() {
        NSLayoutConstraint.activate([
            tripDescriptionContainerView.topAnchor.constraint(equalTo: tripDescriptionView.bottomAnchor, constant: 18),
            tripDescriptionContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            tripDescriptionContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupTripDescriptionTextViewConstraints() {
        NSLayoutConstraint.activate([
            tripDescriptionTextView.topAnchor.constraint(equalTo: tripDescriptionContainerView.topAnchor, constant: 8),
            tripDescriptionTextView.leadingAnchor.constraint(equalTo: tripDescriptionContainerView.leadingAnchor, constant: 8),
            tripDescriptionTextView.trailingAnchor.constraint(equalTo: tripDescriptionContainerView.trailingAnchor, constant: -8),
            tripDescriptionTextView.bottomAnchor.constraint(equalTo: tripDescriptionContainerView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupPersonalInfoView() {
        NSLayoutConstraint.activate([
            personalInfoView.topAnchor.constraint(equalTo: tripDescriptionContainerView.bottomAnchor, constant: 18),
            personalInfoView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            personalInfoView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            personalInfoView.heightAnchor.constraint(equalToConstant: 86)
        ])
    }
    
    private func setupPersonalInfoFormViewConstraints() {
        NSLayoutConstraint.activate([
            personalInfoFormView.topAnchor.constraint(equalTo: personalInfoView.bottomAnchor, constant: 18),
            personalInfoFormView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            personalInfoFormView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
        ])
    }
    
    private func setupDoneButtonConstraints() {
        NSLayoutConstraint.activate([
            doneButton.topAnchor.constraint(equalTo: personalInfoFormView.bottomAnchor, constant: 22),
            doneButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            doneButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            doneButton.heightAnchor.constraint(equalToConstant: 50),
            doneButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupDepartureContainerViewConstraints()
        setupDepartureCountryLabelConstraints()
        setupDepartureCountryTextFieldConstraints()
        setupDepartureCityLabelConstraints()
        setupDepartureCityTextFieldConstraints()
        setupFlightImageViewConstraints()
        setupArrivalContainerViewConstraints()
        setupArrivalCountryLabelConstraints()
        setupArrivalCountryTextFieldConstraints()
        setupArrivalCityLabelConstraints()
        setupArrivalCityTextFieldConstraints()
        setupTransitTypeTextFieldConstraints()
        setupTravellersViewConstraints()
        setupTravellersFormViewConstraints()
        setupResidenceViewConstraints()
        setupResidenceFormViewConstraints()
        setupTripDescriptionViewConstraints()
        setupTripDescriptionContainerViewConstraints()
        setupTripDescriptionTextViewConstraints()
        setupPersonalInfoView()
        setupPersonalInfoFormViewConstraints()
        setupDoneButtonConstraints()
    }
    
    func resetInputsValues() {
        departureCountryTextField.text = ""
        departureCityTextField.text = ""
        arrivalCountryTextField.text = ""
        arrivalCityTextField.text = ""
        transitTypeTextField.text = ""
        travellersFormView.firstTextField.text = ""
        travellersFormView.secondTextField.text = ""
        travellersFormView.thirdTextField.text = ""
        residenceFormView.firstTextField.text = ""
        residenceFormView.secondTextField.text = ""
        residenceFormView.thirdTextField.text = ""
        tripDescriptionTextView.text = ""
        personalInfoFormView.firstTextField.text = ""
        personalInfoFormView.secondTextField.text = ""
        personalInfoFormView.thirdTextField.text = ""
    }
}
