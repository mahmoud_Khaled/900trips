//
//  RequestQuotationCompletionRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class RequestQuotationCompletionRouter: RequestQuotationCompletionRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(with price: Double) -> UIViewController {
        let view = RequestQuotationCompletionViewController()
        let interactor = RequestQuotationCompletionInteractor()
        let router = RequestQuotationCompletionRouter()
        let presenter = RequestQuotationCompletionPresenter(view: view, interactor: interactor, router: router, price: price)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
}
