//
//  RequestQuotationFormView.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

struct TextFieldOptions {
    var placeholder: String
    var keyboardType: UIKeyboardType? = nil
    
    init(placeholder: String, keyboardType: UIKeyboardType? = nil) {
        self.placeholder = placeholder
        self.keyboardType = keyboardType
    }
}

class RequestQuotationFormView: UIView {
    
    lazy var firstTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        return textField
    }()
    
    lazy var firstLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 246/255, blue: 246/255, alpha: 1.0)
        return view
    }()
    
    lazy var secondTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        return textField
    }()
    
    lazy var secondLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 248/255, green: 246/255, blue: 246/255, alpha: 1.0)
        return view
    }()
    
    lazy var thirdTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = DinNextFont.regular.getFont(ofSize: 17)
        return textField
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 10
        layer.applySketchShadow(color: UIColor.black.withAlphaComponent(0.07), alpha: 1.0, x: 0, y: 0, blur: 4, spread: 0)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(firstTextField)
        addSubview(firstLineView)
        addSubview(secondTextField)
        addSubview(secondLineView)
        addSubview(thirdTextField)
    }
    
    private func setupFirstTextFieldConstraints() {
        NSLayoutConstraint.activate([
            firstTextField.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            firstTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            firstTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
            ])
    }
    
    private func setupFirstLineViewConstraints() {
        NSLayoutConstraint.activate([
            firstLineView.topAnchor.constraint(equalTo: firstTextField.bottomAnchor, constant: 16),
            firstLineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            firstLineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            firstLineView.heightAnchor.constraint(equalToConstant: 1)
            ])
    }
    
    private func setupSecondTextFieldConstraints() {
        NSLayoutConstraint.activate([
            secondTextField.topAnchor.constraint(equalTo: firstLineView.bottomAnchor, constant: 16),
            secondTextField.leadingAnchor.constraint(equalTo: firstTextField.leadingAnchor),
            secondTextField.trailingAnchor.constraint(equalTo: firstTextField.trailingAnchor)
            ])
    }
    
    private func setupSecondLineViewConstraints() {
        NSLayoutConstraint.activate([
            secondLineView.topAnchor.constraint(equalTo: secondTextField.bottomAnchor, constant: 16),
            secondLineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            secondLineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            secondLineView.heightAnchor.constraint(equalToConstant: 1)
            ])
    }
    
    private func setupThirdTextFieldConstraints() {
        NSLayoutConstraint.activate([
            thirdTextField.topAnchor.constraint(equalTo: secondLineView.bottomAnchor, constant: 16),
            thirdTextField.leadingAnchor.constraint(equalTo: firstTextField.leadingAnchor),
            thirdTextField.trailingAnchor.constraint(equalTo: firstTextField.trailingAnchor),
            thirdTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
            ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupFirstTextFieldConstraints()
        setupFirstLineViewConstraints()
        setupSecondTextFieldConstraints()
        setupSecondLineViewConstraints()
        setupThirdTextFieldConstraints()
    }
    
    func configure(firstInputOptions: TextFieldOptions, secondInputOptions: TextFieldOptions, thirdInputOptions: TextFieldOptions) {
        
        firstTextField.placeholder = firstInputOptions.placeholder
        if let keyboardType = firstInputOptions.keyboardType {
            firstTextField.keyboardType = keyboardType
        }
        
        secondTextField.placeholder = secondInputOptions.placeholder
        if let keyboardType = secondInputOptions.keyboardType {
            secondTextField.keyboardType = keyboardType
        }
        
        thirdTextField.placeholder = thirdInputOptions.placeholder
        if let keyboardType = thirdInputOptions.keyboardType {
            thirdTextField.keyboardType = keyboardType
        }
    }
}

