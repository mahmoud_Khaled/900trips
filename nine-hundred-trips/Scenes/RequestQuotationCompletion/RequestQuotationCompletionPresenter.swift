//
//  RequestQuotationCompletionPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class RequestQuotationCompletionPresenter: RequestQuotationCompletionPresenterProtocol, RequestQuotationCompletionInteractorOutputProtocol {
    
    weak var view: RequestQuotationCompletionViewProtocol?
    private let interactor: RequestQuotationCompletionInteractorInputProtocol
    private let router: RequestQuotationCompletionRouterProtocol
    
    private let price: Double
    private var fetchType: FetchType!

    init(view: RequestQuotationCompletionViewProtocol, interactor: RequestQuotationCompletionInteractorInputProtocol, router: RequestQuotationCompletionRouterProtocol, price: Double) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.price = price
    }
    
    func getCountries() {
        view?.showActivityIndicator()
        interactor.getCountries()
    }
    
    func getCities(countryId: Int, for fetchType: FetchType) {
        view?.showActivityIndicator()
        self.fetchType = fetchType
        interactor.getCities(countryId: countryId)
    }
    
    func didFetchCountries(with result: Result<[Country]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let countries):
            view?.didFetch(countries: countries.map(CountryViewModel.init))
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchCities(with result: Result<[City]>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let cities):
            view?.didFetch(cities: cities.map(CityViewModel.init), for: fetchType)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func requestQuotation(
        departureCityId: Int,
        arrivalCityId: Int,
        transitType: String,
        adultPassengersCount: String,
        childPassengersCount: String,
        babyPassengersCount: String,
        residenceType: String,
        roomsCount: String,
        personsInRoom: String,
        description: String,
        name: String,
        phone: String,
        email: String
    ) {
        if departureCityId == 0 {
            router.showAlert(with: "emptyDepartureCity".localized())
        } else if arrivalCityId == 0 {
            router.showAlert(with: "emptyArrivalCity".localized())
        } else if transitType.isEmpty {
            router.showAlert(with: "emptyTransitType".localized())
        } else if adultPassengersCount.isEmpty {
            router.showAlert(with: "emptyAdultPassengersCount".localized())
        } else if childPassengersCount.isEmpty {
            router.showAlert(with: "emptyChildPassengersCount".localized())
        } else if babyPassengersCount.isEmpty {
            router.showAlert(with: "emptyBabyPassengersCount".localized())
        } else if residenceType.isEmpty {
            router.showAlert(with: "emptyResidenceType".localized())
        } else if roomsCount.isEmpty {
            router.showAlert(with: "emptyRoomsCount".localized())
        } else if personsInRoom.isEmpty {
            router.showAlert(with: "emptyPersonsInRoom".localized())
        } else if description.isEmpty {
            router.showAlert(with: "emptyDescription".localized())
        } else if name.isEmpty {
            router.showAlert(with: "emptyName".localized())
        } else if !NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: email) {
            router.showAlert(with: "incorrectEmail".localized())
        } else if phone.isEmpty {
            router.showAlert(with: "emptyPhone".localized())
        } else {
            let requestQuotation = RequestQuotation(
                departureCityId: departureCityId,
                arrivalCityId: arrivalCityId,
                isTransit: transitType == "transit".localized() ? true : false,
                adultPassengersCount: adultPassengersCount,
                childPassengersCount: childPassengersCount,
                babyPassengersCount: babyPassengersCount,
                residenceType: residenceType,
                roomsCount: roomsCount,
                personsInRoom: personsInRoom,
                description: description,
                name: name,
                phone: phone.englishDigits,
                email: email
            )
            view?.showActivityIndicator()
            interactor.requestQuotation(model: requestQuotation)
        }
    }
    
    func didRequestQuotation(with result: Result<Order>) {
        view?.hideActivityIndicator()
        view?.resetInputsValues()
        switch result {
        case .success(let order):
            if price == 0 {
                view?.showSuccessView()
            } else {
                print("Proceed to payment with order id \(order.id)")
            }
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
}
