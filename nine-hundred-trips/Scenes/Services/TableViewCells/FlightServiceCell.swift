//
//  Services.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FlightServiceCell: UITableViewCell, ConfigurableCell {
    
    private lazy var serviceImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "offerimage"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 23
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var serviceNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = DinNextFont.medium.getFont(ofSize: 17)
        label.text = "Dams for tourism "
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var selectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "ic_success")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(serviceImageView)
        addSubview(serviceNameLabel)
        addSubview(selectedImageView)
    }
    
    private func setupCountryImageViewConstraints() {
        NSLayoutConstraint.activate([
            serviceImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            serviceImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            serviceImageView.widthAnchor.constraint(equalToConstant: 46),
            serviceImageView.heightAnchor.constraint(equalToConstant: 46)
        ])
    }
    
    private func setupCountryLabelConstraints() {
        NSLayoutConstraint.activate([
            serviceNameLabel.leadingAnchor.constraint(equalTo: serviceImageView.trailingAnchor, constant: 10),
            serviceNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
        
    private func setupSelectedImageConstraints() {
        NSLayoutConstraint.activate([
            selectedImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            selectedImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            selectedImageView.widthAnchor.constraint(equalToConstant: 25),
            selectedImageView.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupCountryImageViewConstraints()
        setupCountryLabelConstraints()
        setupSelectedImageConstraints()
    }
    
    func configure(model: ServiceViewModel) {
        serviceImageView.load(url: model.imageUrl)
        serviceNameLabel.text = model.name
        selectedImageView.isHidden = !model.isSelected
    }
}
