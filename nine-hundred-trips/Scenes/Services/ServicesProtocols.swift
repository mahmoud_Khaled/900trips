//
//  ServicesProtocols.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ServicesRouterProtocol: class {
    func dismiss()
}

protocol ServicesPresenterProtocol: class {
    var view: ServicesViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath)
    func confirmButtonTapped()
    func didSelectRow(at indexPath: IndexPath)
}

protocol ServicesInteractorInputProtocol: class {
    var presenter: ServicesInteractorOutputProtocol? { get set }
}

protocol ServicesInteractorOutputProtocol: class {
    
}

protocol ServicesViewProtocol: class {
    var presenter: ServicesPresenterProtocol! { get set }
    func reloadData() 
}
