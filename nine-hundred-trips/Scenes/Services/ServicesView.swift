//
//  ServicesView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ServicesView: UIView {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.register(FlightServiceCell.self, forCellReuseIdentifier: FlightServiceCell.className)
        tableView.estimatedRowHeight = 0
        tableView.rowHeight = 68
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        return tableView
    }()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(tableView)
    }
    
    private func setupTableViewConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }

    
    private func layoutUI() {
        addSubviews()
        setupTableViewConstraints()
    }
    
}
