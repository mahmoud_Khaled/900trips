//
//  ServicesRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ServicesRouter: ServicesRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(services: [ServiceViewModel], selectedServices:[ServiceViewModel], delegate: ChooseServiceDelegate) -> UIViewController {
        let view = ServicesViewController()
        let interactor = ServicesInteractor()
        let router = ServicesRouter()
        let presenter = ServicesPresenter(view: view, interactor: interactor, router: router, services: services, selectedServices: selectedServices, delegate: delegate)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func dismiss() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
}
