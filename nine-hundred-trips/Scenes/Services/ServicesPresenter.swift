//
//  ServicesPresenter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ServicesPresenter: ServicesPresenterProtocol, ServicesInteractorOutputProtocol {
    
    weak var view: ServicesViewProtocol?
    private let interactor: ServicesInteractorInputProtocol
    private let router: ServicesRouterProtocol
    private var services: [ServiceViewModel]
    private var selectedServices:[ServiceViewModel]
    weak var delegate: ChooseServiceDelegate?
    
    var numberOfRows: Int {
        return services.count
    }
    
    init(view: ServicesViewProtocol, interactor: ServicesInteractorInputProtocol, router: ServicesRouterProtocol, services: [ServiceViewModel], selectedServices:[ServiceViewModel], delegate: ChooseServiceDelegate) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.services = services
        self.delegate = delegate
        self.selectedServices = selectedServices
    }
    
    func configure(cell: AnyConfigurableCell<ServiceViewModel>, at indexPath: IndexPath) {
        cell.configure(model: services[indexPath.row])
    }
    
    func confirmButtonTapped() {
        delegate?.didChooseServices(services: services, selectedServices: selectedServices)
        router.dismiss()
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
        if services[indexPath.row].isSelected {
            services[indexPath.row].isSelected = false
            selectedServices = selectedServices.filter({$0.id != services[indexPath.row].id })
        } else {
            services[indexPath.row].isSelected = true
            selectedServices.append(services[indexPath.row])
        }
        
        view?.reloadData()
        
    }
    
}
