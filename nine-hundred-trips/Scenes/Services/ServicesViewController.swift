//
//  ServicesViewController.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 6/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController, ServicesViewProtocol {
    
    private var mainView = ServicesView()
    var presenter: ServicesPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        setupTableView()
        title = "flightServices".localized()
    }
    
    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    func reloadData() {
         mainView.tableView.reloadData()
    }
    
    private func setupNavigationBarButtons() {
        let confirmButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_done"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(confirmButtonTapped))
            .build()
        navigationItem.rightBarButtonItem = confirmButton
    }
    
    @objc private func confirmButtonTapped() {
        presenter.confirmButtonTapped()
    }

    
}

extension ServicesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FlightServiceCell.className, for: indexPath) as? FlightServiceCell else { return UITableViewCell() }
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
}
