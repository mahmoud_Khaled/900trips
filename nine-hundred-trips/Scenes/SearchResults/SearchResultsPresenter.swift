//
//  SearchResultsPresenter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class SearchResultsPresenter: SearchResultsPresenterProtocol, SearchResultsInteractorOutputProtocol, UpdateOfferProtocol {
    
    weak var view: SearchResultsViewProtocol?
    private let interactor: SearchResultsInteractorInputProtocol
    private let router: SearchResultsRouterProtocol
    private var searchResults: [OfferViewModel]
    
    private var fromCityId: Int?
    private var toCityId: Int?
    private var totalPages = 1
    private var currentPage = 1
    
    var numberOfRows: Int {
        return searchResults.count
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        return dateFormatter
    }()
    
    init(view: SearchResultsViewProtocol, interactor: SearchResultsInteractorInputProtocol, router: SearchResultsRouterProtocol, searchResults: [OfferViewModel], totalPages: Int, fromCityId: Int, toCityId: Int) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.searchResults = searchResults
        self.totalPages = totalPages
        self.fromCityId = fromCityId
        self.toCityId = toCityId
    }
    
    func configure(cell: AnyConfigurableCell<OfferViewModel>, for indexPath: IndexPath) {
        cell.configure(model: searchResults[indexPath.row])
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let offerId = searchResults[indexPath.row].id
        router.navigateToOfferDetails(indexPath: indexPath, id: offerId, delegate: self)
    }
    
    func updateOffer(indexPath: IndexPath, isLiked: Bool) {
        searchResults[indexPath.row].isLiked = isLiked
        NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
            "offerId": searchResults[indexPath.row].id,
            "likeStatus": searchResults[indexPath.row].isLiked
        ])
        view?.reloadData()
    }
    
    func toggleFavourite(indexPath: IndexPath) {
        guard UserDefaultsHelper.isLoggedIn else {
            router.showAlert(with: "loginFirst".localized())
            return
        }
        view?.showActivityIndicator(isUserInteractionEnabled: true)
        if searchResults[indexPath.row].isLiked {
            interactor.removeFavourite(offerId: searchResults[indexPath.row].id, index: indexPath.row)
        } else {
            interactor.addFavourite(offerId: searchResults[indexPath.row].id, index: indexPath.row)
        }
    }
    
    func didAddFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            searchResults[index].isLiked = true
            view?.reloadData()
            NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
                "offerId": searchResults[index].id,
                "likeStatus": searchResults[index].isLiked
            ])
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didRemoveFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            searchResults[index].isLiked = false
            view?.reloadData()
            NotificationCenter.default.post(name: .updateOfferLikeStatus, object: nil, userInfo: [
                "offerId": searchResults[index].id,
                "likeStatus": searchResults[index].isLiked
            ])
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let value):
            self.totalPages = totalPages
            searchResults.append(contentsOf:value.map { OfferViewModel(offer: $0, dateFormatter: self.dateFormatter, stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}") })
            view?.reloadData()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func loadNextPage() {
        if currentPage < totalPages {
            view?.showActivityIndicator(isUserInteractionEnabled: true)
            currentPage += 1
            interactor.searchOffers(fromCityId: fromCityId ?? 0, toCityId: toCityId ?? 0, page: currentPage)
        }
    }
}
