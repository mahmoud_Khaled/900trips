//
//  SearchResultsRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SearchResultsRouter: SearchResultsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(with results: [OfferViewModel], totalPages: Int, fromCityId: Int, toCityId: Int) -> UIViewController {
        let view = SearchResultsViewController()
        let interactor = SearchResultsInteractor()
        let router = SearchResultsRouter()
        let presenter = SearchResultsPresenter(view: view, interactor: interactor, router: router, searchResults: results, totalPages: totalPages, fromCityId: fromCityId, toCityId: toCityId)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToOfferDetails(indexPath: IndexPath, id: Int, delegate: UpdateOfferProtocol) {
        viewController?.navigationController?.pushViewController(OfferDetailsRouter.createModule(isMyOffer: false, indexPath: indexPath, offerId: id, delegate: delegate), animated: true)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}
