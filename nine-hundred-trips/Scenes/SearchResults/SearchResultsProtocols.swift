//
//  SearchResultsProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol SearchResultsRouterProtocol: class {
    func navigateToOfferDetails(indexPath: IndexPath, id: Int, delegate: UpdateOfferProtocol)
    func showAlert(with message: String)
}

protocol SearchResultsPresenterProtocol: class {
    var view: SearchResultsViewProtocol? { get set }
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<OfferViewModel>, for indexPath: IndexPath)
    func didSelectRow(at indexPath: IndexPath)
    func toggleFavourite(indexPath: IndexPath)
    func loadNextPage()
}

protocol SearchResultsInteractorInputProtocol: class {
    var presenter: SearchResultsInteractorOutputProtocol? { get set }
    func addFavourite(offerId: Int, index: Int)
    func removeFavourite(offerId: Int, index: Int)
    func searchOffers(fromCityId: Int, toCityId: Int, page: Int)
}

protocol SearchResultsInteractorOutputProtocol: class {
    func didAddFavourite(with result: Result<Data>, index: Int)
    func didRemoveFavourite(with result: Result<Data>, index: Int)
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int)
}

protocol SearchResultsViewProtocol: class {
    var presenter: SearchResultsPresenterProtocol! { get set }
    func reloadData()
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
}
