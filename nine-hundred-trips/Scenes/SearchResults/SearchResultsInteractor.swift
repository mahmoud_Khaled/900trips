//
//  SearchResultsInteractor.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SearchResultsInteractor: SearchResultsInteractorInputProtocol {

    weak var presenter: SearchResultsInteractorOutputProtocol?
    
    private let offersWorker = OffersWorker()
    
    func addFavourite(offerId: Int, index: Int) {
        offersWorker.addFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddFavourite(with: result, index: index)
        }
    }
    
    func removeFavourite(offerId: Int, index: Int) {
        offersWorker.removeFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRemoveFavourite(with: result, index: index)
        }
    }
    
    func searchOffers(fromCityId: Int, toCityId: Int, page: Int) {
        offersWorker.searchOffers(fromCityId: fromCityId, toCityId: toCityId, page: page) { [weak self] result, pages  in
            guard let self = self else { return }
            self.presenter?.didFetchOffers(with: result, totalPages: pages)
        }
    }
}
