//
//  SearchResultsViewController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SearchResultsViewController: BaseViewController, SearchResultsViewProtocol {
    
    private let mainView = SearchResultsView()
    var presenter: SearchResultsPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "searchResults".localized()
        setupTableView()
        mainView.tableView.reloadData()
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    func reloadData() {
        mainView.tableView.reloadData()
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view?.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view?.hideProgressHUD()
    }
    
}

extension SearchResultsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.className, for: indexPath) as! OfferCell
        presenter.configure(cell: AnyConfigurableCell(cell), for: indexPath)
        cell.favouriteButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.toggleFavourite(indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
    
}
