//
//  HomeRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum HomeDestination {
    case offerDetails(indexPath: IndexPath?, id: Int, delegate: UpdateOfferProtocol?, isMyOffer: Bool)
    case search
    case notification
}

class HomeRouter: HomeRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = HomeViewController()
        view.tabBarItem = UITabBarItem(title: "offers".localized(), image: UIImage(named: "ic_offers"), selectedImage: UIImage(named: "ic_offers"))
        view.tabBarItem.setTitleTextAttributes([ NSAttributedString.Key.font: DinNextFont.regular.getFont(ofSize: 13)], for:UIControl.State.normal)
        view.tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        view.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2.0)
        let interactor = HomeInteractor()
        let router = HomeRouter()
        let presenter = HomePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return CustomNavigationController(rootViewController: view)
    }
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }

    func navigate(to destination: HomeDestination) {
        switch destination {
        case .offerDetails(let indexPath, let id, let delegate, let isMyOffer):
            viewController?.navigationController?.pushViewController(OfferDetailsRouter.createModule(isMyOffer: isMyOffer, indexPath: indexPath, offerId: id, delegate: delegate), animated: true)
        case .search:
            viewController?.navigationController?.pushViewController(SearchRouter.createModule(), animated: true)
        case .notification:
            viewController?.navigationController?.pushViewController(NotificationsRouter.createModule(), animated: true)
        }
    }
    
    func switchToFirstTab() {
        viewController?.tabBarController?.selectedIndex = 0
    }
}
