//
//  HomePresenter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class HomePresenter: HomePresenterProtocol, HomeInteractorOutputProtocol, UpdateOfferProtocol {
    
    weak var view: HomeViewProtocol?
    private let interactor: HomeInteractorInputProtocol
    private let router: HomeRouterProtocol
    
    private var segmentedIndex = 0
    private var offersViewModels = [OfferViewModel]()
    private var seasonalViewModels = [OfferViewModel]()
    private var specialsViewModels = [OfferViewModel]()
    private var dailyViewModels = [OfferViewModel]()
    private var seasonalTotalPages = 1
    private var specialTotalPages = 1
    private var dailyTotalPages = 1
    private var seasonalCurrentPage = 1
    private var specialCurrentPage = 1
    private var dailyCurrentPage = 1
    private var isLoading = false
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    var numberOfRows: Int {
        return offersViewModels.count
    }
    
    private var countryId: Int {
        return UserDefaultsHelper.countryId
    }

    init(view: HomeViewProtocol, interactor: HomeInteractorInputProtocol, router: HomeRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didUpdateOfferLikeStatus(notification:)),
            name: .updateOfferLikeStatus,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleOpenOfferDetails),
            name: .handleOpenOfferDetails,
            object: nil
        )
    }
    
    func viewDidLoad() {
        isLoading = true
        view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
        interactor.getSeasonalOffers(countryId: countryId, page: seasonalCurrentPage)
    }
    
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int) {
        view?.hideActivityIndicator()
        view?.endRefreshing()
        switch result {
        case .success(let offers):
            view?.changeTableViewUserInteraction(isUserInteractionEnabled: true)
            if segmentedIndex == 0 {
                seasonalTotalPages = totalPages
                seasonalViewModels.append(contentsOf: offers.map {
                    OfferViewModel(
                        offer: $0,
                        dateFormatter: self.dateFormatter,
                        stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}"
                    )
                })
                offersViewModels = seasonalViewModels
            } else if segmentedIndex == 1 {
                specialTotalPages = totalPages
                specialsViewModels.append(contentsOf: offers.map {
                    OfferViewModel(
                        offer: $0,
                        dateFormatter: self.dateFormatter,
                        stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}"
                    )
                })
                offersViewModels = specialsViewModels
            } else if segmentedIndex == 2 {
                dailyTotalPages = totalPages
                dailyViewModels.append(contentsOf: offers.map {
                    OfferViewModel(
                        offer: $0,
                        dateFormatter: self.dateFormatter,
                        stringFormat: "from".localized() + " {{fromDate}} " + "to".localized() + " {{toDate}}"
                    )
                })
                offersViewModels = dailyViewModels
            }
            isLoading = false
            view?.reloadTableView()
            
            if(offersViewModels.count == 0) {
                view?.setEmptyTextToTableView(message: "noOffers".localized())
            }

        case .failure(let error):
            isLoading = false
            view?.resetTableViewOffset()
            router.showAlert(with: error.localizedDescription)
        }
       
    }
    
    func toggleFavourite(indexPath: IndexPath) {
        guard UserDefaultsHelper.isLoggedIn else {
            router.showAlert(with: "loginFirst".localized())
            return
        }
        view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
        if offersViewModels[indexPath.row].isLiked == true {
            interactor.removeFavourite(offerId: offersViewModels[indexPath.row].id, index: indexPath.row)
        } else {
            interactor.addFavourite(offerId: offersViewModels[indexPath.row].id, index: indexPath.row)
        }
    }
    
    func didAddFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            offersViewModels[index].isLiked = true
            if segmentedIndex == 0 {
                seasonalViewModels[index].isLiked = true
            } else if segmentedIndex == 1 {
                specialsViewModels[index].isLiked = true
            } else {
                dailyViewModels[index].isLiked = true
            }
            view?.reloadTableView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func didRemoveFavourite(with result: Result<Data>, index: Int) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            offersViewModels[index].isLiked = false
            if segmentedIndex == 0 {
                seasonalViewModels[index].isLiked = false
            } else if segmentedIndex == 1 {
                specialsViewModels[index].isLiked = false
            } else {
                dailyViewModels[index].isLiked = false
            }
            view?.reloadTableView()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath) {
        guard !isLoading else { return }
        let offerViewModel = offersViewModels[indexPath.row]
        cell.configure(model: offerViewModel)
    }
    
    func swichOffersSegmentedControl(index: Int) {
        segmentedIndex = index
        view?.setEmptyTextToTableView(message: "")
        if index == 0 {
            if seasonalViewModels.count > 0 {
                offersViewModels = seasonalViewModels
                view?.reloadTableView()
            } else {
                view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
                interactor.getSeasonalOffers(countryId: countryId, page: seasonalCurrentPage)
            }
        } else if index == 1 {
            if specialsViewModels.count > 0 {
                offersViewModels = specialsViewModels
                view?.reloadTableView()
            } else {
                view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
                interactor.getSpecialOffers(countryId: countryId, page: specialCurrentPage)
            }
        } else if index == 2 {
            if dailyViewModels.count > 0 {
                offersViewModels = dailyViewModels
                view?.reloadTableView()
            } else {
                view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
                interactor.getDailyOffers(countryId: countryId, page: dailyCurrentPage)
            }
        }
    }
    
    func pullToRefresh() {
        seasonalCurrentPage = 1
        specialCurrentPage = 1
        dailyCurrentPage = 1
        isLoading = true
        view?.changeTableViewUserInteraction(isUserInteractionEnabled: false)
        view?.setEmptyTextToTableView(message: "")
        offersViewModels = []
        if segmentedIndex == 0 {
            seasonalViewModels = []
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: true)
            interactor.getSeasonalOffers(countryId: countryId, page: seasonalCurrentPage)
        } else if segmentedIndex == 1 {
            specialsViewModels = []
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: true)
            interactor.getSpecialOffers(countryId: countryId, page: specialCurrentPage)
        } else {
            dailyViewModels = []
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: true)
            interactor.getDailyOffers(countryId: countryId, page: dailyCurrentPage)
        }
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let offer = offersViewModels[indexPath.row]
        router.navigate(to: .offerDetails(indexPath: indexPath, id: offer.id, delegate: self, isMyOffer: offer.userId == UserDefaultsHelper.id))
    }
    
    func updateOffer(indexPath: IndexPath, isLiked: Bool) {
        offersViewModels[indexPath.row].isLiked = isLiked
        if segmentedIndex == 0 {
            seasonalViewModels[indexPath.row].isLiked = isLiked
        } else if segmentedIndex == 1 {
            specialsViewModels[indexPath.row].isLiked = isLiked
        } else {
            dailyViewModels[indexPath.row].isLiked = isLiked
        }
        view?.reloadTableView()
    }
    
    func searchButtonTapped() {
        router.navigate(to: .search)
    }
    
    func notificationButtonTapped() {
        router.navigate(to: .notification)
    }
    
    func loadNextPage() {
        if segmentedIndex == 0 && seasonalCurrentPage < seasonalTotalPages {
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
            seasonalCurrentPage += 1
            interactor.getSeasonalOffers(countryId: countryId, page: seasonalCurrentPage)
        } else if segmentedIndex == 1 && specialCurrentPage < specialTotalPages {
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
            specialCurrentPage += 1
            interactor.getSeasonalOffers(countryId: countryId, page: seasonalCurrentPage)
        } else if segmentedIndex == 2 && dailyCurrentPage < dailyTotalPages {
            view?.showActivityIndicator(isUserInteractionEnabled: false, blockUIOnly: false)
            dailyCurrentPage += 1
            interactor.getDailyOffers(countryId: countryId, page: dailyCurrentPage)
        }
    }
    
    @objc private func didUpdateOfferLikeStatus(notification: NSNotification) {
        let data = notification.userInfo
        guard
            let offerId = data?["offerId"] as? Int,
            let likeStatus = data?["likeStatus"] as? Bool
        else { return }
        
        let offerViewModel = offersViewModels.first(where: { $0.id == offerId })
        let seasonlViewModel = seasonalViewModels.first(where: { $0.id == offerId })
        let specialViewModel = specialsViewModels.first(where: { $0.id == offerId })
        let dailyViewModel = dailyViewModels.first(where: { $0.id == offerId })
        
        offerViewModel?.isLiked = likeStatus
        seasonlViewModel?.isLiked = likeStatus
        specialViewModel?.isLiked = likeStatus
        dailyViewModel?.isLiked = likeStatus
        
        view?.reloadTableView()
    }
    
    @objc private func handleOpenOfferDetails(notification: NSNotification) {
        guard let id = notification.userInfo?["offerId"] as? Int else { return }
        router.switchToFirstTab()
        router.navigate(to: .offerDetails(indexPath: nil, id: id, delegate: nil, isMyOffer: false))
    }
}
