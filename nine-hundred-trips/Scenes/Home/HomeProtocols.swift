//
//  HomeProtocols.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol HomeRouterProtocol: class {
    func showAlert(with message: String)
    func navigate(to destination: HomeDestination)
    func switchToFirstTab()
}

protocol HomePresenterProtocol: class {
    var view: HomeViewProtocol? { get set }
    func viewDidLoad()
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<OfferViewModel>, at indexPath: IndexPath)
    func swichOffersSegmentedControl(index: Int)
    func toggleFavourite(indexPath: IndexPath)
    func pullToRefresh()
    func didSelectRow(at indexPath: IndexPath)
    func searchButtonTapped()
    func notificationButtonTapped()
    func loadNextPage()
}

protocol HomeInteractorInputProtocol: class {
    var presenter: HomeInteractorOutputProtocol? { get set }
    func getSeasonalOffers(countryId: Int, page: Int) 
    func getSpecialOffers(countryId: Int, page: Int)
    func getDailyOffers(countryId: Int, page: Int)
    func addFavourite(offerId: Int, index: Int)
    func removeFavourite(offerId: Int, index: Int)
}

protocol HomeInteractorOutputProtocol: class {
    func didFetchOffers(with result: Result<[Offer]>, totalPages: Int)
    func didAddFavourite(with result: Result<Data>, index: Int)
    func didRemoveFavourite(with result: Result<Data>, index: Int)
}

protocol HomeViewProtocol: class {
    var presenter: HomePresenterProtocol! { get set }
    func reloadTableView()
    func showActivityIndicator(isUserInteractionEnabled: Bool, blockUIOnly: Bool)
    func hideActivityIndicator()
    func endRefreshing()
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool)
    func setEmptyTextToTableView(message: String)
    func resetTableViewOffset()
}
