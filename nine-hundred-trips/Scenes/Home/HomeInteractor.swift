//
//  HomeInteractor.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class HomeInteractor: HomeInteractorInputProtocol {
    
    weak var presenter: HomeInteractorOutputProtocol?
    private let offersWorker = OffersWorker()
    
    func getSeasonalOffers(countryId: Int, page: Int) {
        offersWorker.getSeasonalOffers(countryId: countryId, page: page) { [weak self] result, page  in
            guard let self = self else { return }
            self.presenter?.didFetchOffers(with: result, totalPages: page)
        }
    }
    
    func getSpecialOffers(countryId: Int, page: Int) {
        offersWorker.getSpecialOffers(countryId: countryId, page: page) { [weak self] result, page  in
            guard let self = self else { return }
            self.presenter?.didFetchOffers(with: result, totalPages: page)
        }
    }
    
    func getDailyOffers(countryId: Int, page: Int) {
        offersWorker.getDailyOffers(countryId: countryId, page: page) { [weak self] result, page  in
            guard let self = self else { return }
            self.presenter?.didFetchOffers(with: result, totalPages: page)
        }
    }
    
    func addFavourite(offerId: Int, index: Int) {
        offersWorker.addFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddFavourite(with: result, index: index)
        }
    }
    
    func removeFavourite(offerId: Int, index: Int) {
        offersWorker.removeFavourite(offerId: offerId) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didRemoveFavourite(with: result, index: index)
        }
    }

}
