//
//  HomeViewController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, HomeViewProtocol {

    private let mainView = HomeView()
    var presenter: HomePresenterProtocol!

    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "offers".localized()
        hideBackButtonTitle()
        setupNavigationBarStyle()
        setupNavigationBarButtons()
        setupTableView()
        addTargets()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    private func addTargets() {
        mainView.segmentedControl.addTarget(self, action: #selector(swichOffersSegmentedControl), for: .valueChanged)
        mainView.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    @objc private func swichOffersSegmentedControl(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        presenter.swichOffersSegmentedControl(index: index)
    }

    private func setupNavigationBarStyle() {
        navigationController?.navigationBar.barTintColor = .coral
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupNavigationBarButtons() {
        let dismissButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_notification"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(notificationButtonTapped))
            .build()
        
        
        let confirmButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_search"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(searchButtonTapped))
            .build()
        
        navigationItem.rightBarButtonItems = [confirmButton, dismissButton]
    }
    
    @objc private func notificationButtonTapped() {
        presenter.notificationButtonTapped()
    }
    
    @objc private func searchButtonTapped() {
        presenter.searchButtonTapped()
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool, blockUIOnly: Bool) {
        view.showProgressHUD(isUserInteractionEnabled: isUserInteractionEnabled, blockUIOnly: blockUIOnly)
    }
    
    func hideActivityIndicator() {
        view.hideProgressHUD()
    }
    
    func changeTableViewUserInteraction(isUserInteractionEnabled: Bool) {
        mainView.tableView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    @objc private func pullToRefresh() {
        presenter.pullToRefresh()
    }
    
    func endRefreshing() {
        mainView.refreshControl.endRefreshing()
    }
    
    func setEmptyTextToTableView(message: String) {
        mainView.tableView.emptyMessage(message: message)
    }
    
    func resetTableViewOffset() {
        mainView.tableView.setContentOffset(.zero, animated: true)
    }
    
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.className, for: indexPath) as! OfferCell
        presenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        cell.favouriteButtonTapped = { [weak self] in
            guard let self = self else { return }
            self.presenter.toggleFavourite(indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter.numberOfRows - 1 {
            presenter.loadNextPage()
        }
    }
    
}
