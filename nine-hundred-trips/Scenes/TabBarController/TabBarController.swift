//
//  TabBarController.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

import UIKit

class TabBarController: UITabBarController {
    
    private lazy var shadowView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.applySketchShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 1), alpha: 0.1, x: 0, y: 2, blur: 14.7, spread: 0)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabBar()
        hideBackButtonTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func initTabBar() {
        tabBar.tintColor = .coral
        tabBar.clipsToBounds = false
        tabBar.backgroundImage = UIImage()
        setupShadowView()
    }
    
    private func setupShadowView() {
        tabBar.addSubview(shadowView)
        NSLayoutConstraint.activate([
            shadowView.topAnchor.constraint(equalTo: tabBar.topAnchor, constant: -1),
            shadowView.leadingAnchor.constraint(equalTo: tabBar.leadingAnchor),
            shadowView.trailingAnchor.constraint(equalTo: tabBar.trailingAnchor),
            shadowView.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor)
        ])
        tabBar.sendSubviewToBack(shadowView)
    }
}

