//
//  TabBarControllerRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

import UIKit

class TabBarControllerRouter {
    
    static func createModule(withViewControllers viewControllers: [UIViewController]) -> UIViewController {
        let tabBarController = TabBarController()
        tabBarController.viewControllers = viewControllers
        return tabBarController
    }
    
}
