//
//  L102Language.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIApplication {
    class var isRTL: Bool {
        return UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft
    }
}

class L102Localizer: NSObject {
    
    class func doTheMagic() {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
        MethodSwizzleGivenClassName(cls: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.cstm_userInterfaceLayoutDirection))
        MethodSwizzleGivenClassName(cls: UITextField.self, originalSelector: #selector(UITextField.layoutSubviews), overrideSelector: #selector(UITextField.cstmlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UITextView.self, originalSelector: #selector(UITextView.layoutSubviews), overrideSelector: #selector(UITextView.cstmlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(UILabel.layoutSubviews), overrideSelector: #selector(UILabel.cstmlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UIButton.self, originalSelector: #selector(UIButton.layoutSubviews), overrideSelector: #selector(UIButton.customLayoutSubviews))
        MethodSwizzleGivenClassName(cls: UITabBar.self, originalSelector: #selector(UITabBar.layoutSubviews), overrideSelector: #selector(UITabBar.customLayoutSubviews))
    }
}

extension UILabel {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        if self.isKind(of: NSClassFromString("UITextFieldLabel")!) {
            return // handle special case with uitextfields
        }
        if self.tag <= 0  {
            if UIApplication.isRTL  {
                if self.textAlignment == .right {
                    return
                }
            } else {
                if self.textAlignment == .left {
                    return
                }
            }
        }
        if self.tag <= 0 {
            if UIApplication.isRTL  {
                if self.textAlignment != .center{
                    self.textAlignment = .right
                }
                
            } else {
                if self.textAlignment != .center{
                    self.textAlignment = .left
                }
                
            }
        }
    }
}


extension UITextField {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        guard textAlignment != .center else { return }
        if self.tag <= 0 {
            if UIApplication.isRTL  {
                if self.textAlignment == .right { return }
                self.textAlignment = .right
            } else {
                if self.textAlignment == .left { return }
                self.textAlignment = .left
            }
        }
    }
}

extension UITextView {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        guard textAlignment != .center else { return }
        if self.tag <= 0 {
            if UIApplication.isRTL  {
                if self.textAlignment == .right { return }
                self.textAlignment = .right
            } else {
                if self.textAlignment == .left { return }
                self.textAlignment = .left
            }
        }
    }
}

extension UIButton {
    @objc public func customLayoutSubviews() {
        customLayoutSubviews()
        if tag == -1 {
            if contentHorizontalAlignment == .center {
                contentHorizontalAlignment = .center
                if UIApplication.isRTL {
                    contentHorizontalAlignment = .right
                    titleEdgeInsets.right = titleEdgeInsets.left
                    titleEdgeInsets.left = 0
                } else {
                    contentHorizontalAlignment = .left
                    titleEdgeInsets.left = titleEdgeInsets.right
                    titleEdgeInsets.right = 0
                }
            } else {
                if UIApplication.isRTL {
                    if contentHorizontalAlignment == .right { return }
                    contentHorizontalAlignment = .right
                    titleEdgeInsets.right = titleEdgeInsets.left
                    titleEdgeInsets.left = 0
                } else {
                    if contentHorizontalAlignment == .left { return }
                    contentHorizontalAlignment = .left
                    titleEdgeInsets.left = titleEdgeInsets.right
                    titleEdgeInsets.right = 0
                }
            }
        }
    }
}

extension UITabBar {
    @objc public func customLayoutSubviews() {
        customLayoutSubviews()
        if UIApplication.isRTL {
            semanticContentAttribute = .forceRightToLeft
        } else {
            semanticContentAttribute = .forceLeftToRight
        }
    }
}


extension UIApplication {
    @objc var cstm_userInterfaceLayoutDirection : UIUserInterfaceLayoutDirection {
        get {
            var direction = UIUserInterfaceLayoutDirection.leftToRight
            if L102Language.currentLanguage == "ar" {
                direction = .rightToLeft
            }
            return direction
        }
    }
}

extension Bundle {
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        if self == Bundle.main {
            let currentLanguage = L102Language.currentLanguage
            var bundle = Bundle();
            if let _path = Bundle.main.path(forResource: L102Language.currentLanguage, ofType: "lproj") {
                bundle = Bundle(path: _path)!
            }else
                if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                    bundle = Bundle(path: _path)!
                } else {
                    let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                    bundle = Bundle(path: _path)!
            }
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        } else {
            return (self.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
}


func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
