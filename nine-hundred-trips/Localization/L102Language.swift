//
//  L102Language.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class L102Language {
    
    class var currentLanguage: String {
        let lang = UserDefaults.standard.array(forKey: "AppleLanguages")!
        let firstLanguage = lang[0] as! String
        if firstLanguage.lowercased().range(of: "ar") != nil {
            return "ar"
        } else {
            return "en"
        }
    }
    
    class var isRTL: Bool {
        return L102Language.currentLanguage == "ar"
    }
    
    class func setLanguage(code: String) {
        UserDefaults.standard.set([code], forKey: "AppleLanguages")
    }
    
}
