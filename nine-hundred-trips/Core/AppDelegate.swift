//
//  AppDelegate.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var window: UIWindow?
    
    private var rootViewController: UINavigationController? {
        return (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)
    }
    
    private var topViewController: UIViewController? {
        return rootViewController?.children.last
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        getUserToken()
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.backgroundColor = .white
        
        L102Localizer.doTheMagic()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledToolbarClasses = [ChatViewController.self]
        IQKeyboardManager.shared.disabledTouchResignedClasses = [ChatViewController.self]
        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [ChatViewController.self]
        
        UINavigationBar.appearance().titleTextAttributes = [
            .font: DinNextFont.medium.getFont(ofSize: 17),
            .foregroundColor: UIColor.white
        ]
        
        startApplication()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (_, error) in
            if error == nil {
               print("Push notifications permission granted")
            }
        }
        
        UNUserNotificationCenter.current().delegate = self
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBHandler()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard let pushNotification = PushNotification(userInfo: notification.request.content.userInfo) else { return }
        
        if pushNotification.type == .message {
            do {
                guard let pushNotificationData = pushNotification.data else { return }
                let message = try DictionaryDecoder().decode(Message.self, from: pushNotificationData)
                if let topViewController = topViewController as? ChatViewController {
                    
                    handleNewMessage(message: message)
                    if message.senderUser.id == topViewController.userId {
                        completionHandler([])
                    } else {
                        completionHandler([.alert,.badge,.sound])
                    }
                } else {
                    handleNewMessage(message: message)
                    completionHandler([.alert,.badge,.sound])
                }
            } catch { }
        } else {
            completionHandler([.alert,.badge,.sound])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard let pushNotification = PushNotification(userInfo: response.notification.request.content.userInfo) else { return }
        do {
            guard let pushNotificationData = pushNotification.data else { return }
            let message = try DictionaryDecoder().decode(Message.self, from: pushNotificationData)
            handleNewMessage(message: message)
            if let topViewController = topViewController as? ChatViewController, topViewController.userId != message.senderUser.id {
                
                topViewController.navigationController?.popViewController(animated: false)
                pushChatViewController(user: message.senderUser, conversationId: message.id, animated: false)
                
            } else {
                if let topViewController = topViewController as? ChatViewController, topViewController.userId == message.senderUser.id {
                    return
                }
                pushChatViewController(user: message.senderUser, conversationId: message.id)
            }
        } catch { }
        
        completionHandler()
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if UIApplication.shared.applicationState != .active {
            guard let pushNotification = PushNotification(userInfo: userInfo) else { return }
            do {
                guard let pushNotificationData = pushNotification.data else { return }
                let message = try DictionaryDecoder().decode(Message.self, from: pushNotificationData)
                handleNewMessage(message: message)
            } catch { }
        }
        
        completionHandler(.newData)
    }
    
    func startApplication() {
        if UserDefaultsHelper.countryId != 0 {
            setRootViewController(createTabBarController(), animated: false)
        } else {
            setRootViewController(ChooseLanguageRouter.createModule(), animated: false)
        }
    }
    
    func setRootViewController(_ viewController: UIViewController, animated: Bool) {
        
        window?.subviews.forEach {
            $0.removeFromSuperview()
        }

        if animated {
            let transition = UIView.AnimationOptions.transitionCrossDissolve
            window?.rootViewController = viewController
            UIView.transition(with: window!, duration: 0.5, options: transition, animations: {})
        } else {
            window?.rootViewController = viewController
        }
        
    }
    
    func createTabBarController() -> UIViewController {
        var tabBarControllers = [
            HomeRouter.createModule(),
            TourismGuideRouter.createModule(),
            CompaniesRouter.createModule()
        ]
        
        if UserDefaultsHelper.isLoggedIn {
            tabBarControllers.append(ConversationsRouter.createModule())
        }
        
        if UserDefaultsHelper.userType == .client {
            tabBarControllers.append(UserProfileRouter.createModule())
        } else {
            tabBarControllers.append(CompanyProfileRouter.createModule())
        }
        
        
        let tabBarController = TabBarControllerRouter.createModule(withViewControllers: tabBarControllers)
        
        
        return UINavigationController.create(rootViewController: tabBarController)
    }
    
    private func FBHandler() {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    private func getUserToken(isRefreshToken: Bool = false) {
        InstanceID.instanceID().instanceID { [weak self] result, error in
            guard let self = self else { return }
            if let error = error {
                print("Couldn't obtain firebase token reason \(error.localizedDescription)")
            } else {
                guard let result = result else { return }
                self.FBHandler()
                UserDefaultsHelper.firebaseToken = result.token
                if isRefreshToken {
                    let userWorker = UserWorker()
                    let identifier = UIApplication.shared.beginBackgroundTask {
                        userWorker.cancelAllRequests()
                    }
                    
                    userWorker.refreshToken(token: result.token) { [weak self] result in
                        guard let self = self else { return }
                        switch result {
                        case .success:
                            UIApplication.shared.endBackgroundTask(identifier)
                        case .failure(let error):
                            if let networkError = error as? NetworkError, case NetworkError.unauthorized = networkError {
                                return
                            }
                            self.getUserToken(isRefreshToken: true)
                        }
                    }
                    
                }
            }
        }
    }
    
    @objc private func refreshToken(notification: NSNotification) {
        getUserToken(isRefreshToken: true)
    }
    
    private func handleNewMessage(message: Message) {
        NotificationCenter.default.post(name: .newMessage, object: nil, userInfo: [
            "message": message
        ])

        NotificationCenter.default.post(name: .updateConversations, object: nil, userInfo: [
            "conversationId": message.id,
            "userId": message.senderUser.id ?? 0,
            "userName": message.senderUser.username ?? "",
            "userImage": message.senderUser.imageUrl ?? "",
            "time": message.time,
            "content": message.type == .photo ? "photo".localized() : message.message,
            "rate": message.senderUser.rate ?? ""
        ])
    }
    
    private func pushChatViewController(user: User, conversationId: Int, animated: Bool = true) {
        let viewModel = UserViewModel(user: user)
        rootViewController?.pushViewController(ChatRouter.createModule(with: viewModel.id, userImage: viewModel.imageUrl, username: viewModel.username, conversationId: conversationId, rate: viewModel.rate), animated: animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        let statusBarRect = UIApplication.shared.statusBarFrame
        guard let touchPoint = event?.allTouches?.first?.location(in: self.window) else { return }
        
        if statusBarRect.contains(touchPoint) {
            NotificationCenter.default.post(name: .statusBarTapped, object: nil)
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlString = url.absoluteString
        if urlString.contains("ninehundredtrips://") {
            guard
                let targetUrl = urlString.components(separatedBy: "ninehundredtrips://").last,
                let targetScreen = URLSchemeTarget(rawValue: targetUrl.components(separatedBy: "/").first ?? ""),
                let targetId = Int(targetUrl.components(separatedBy: "/").last ?? "")
            else { return false }
            switch targetScreen {
            case .offer:
                NotificationCenter.default.post(name: .handleOpenOfferDetails, object: nil, userInfo: [
                    "offerId": targetId
                ])
            }
        }
        return false
    }
}
