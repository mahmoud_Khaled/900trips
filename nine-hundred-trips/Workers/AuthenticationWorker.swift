//
//  AuthenticationWorker.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/8/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AuthenticationWorker {
    
    private let networkHandler = NetworkHandler()
    
    func register(userType: UserType, username: String, companyName: String, email: String, password: String, confirmPassword: String, countryId: Int, currencyId: Int, companyType: Int, completionHandler: @escaping (Result<Data>)->()) {
        
        var parameters: [String: Any] = [:]
        if userType == .client {
            
            parameters = [
                "username": username,
                "email": email,
                "country_id": countryId,
                "currency_id": currencyId ,
                "password": password,
                "user_type_id": userType.rawValue,
                "password_confirmation": confirmPassword
            ]
            
        } else if userType == .company {
            parameters = [
                "username": companyName,
                "email": email,
                "country_id": countryId,
                "currency_id": currencyId ,
                "company_type": companyType,
                "password": password,
                "user_type_id": userType.rawValue,
                "password_confirmation": confirmPassword
            ]
        }
        
        let registerRequest = AuthenticationRouter.register(parameters: parameters)
        networkHandler.request(registerRequest).observe(with: completionHandler)
    }
    
    func verify(email: String, verificationCode: String, deviceToken: String, completionHandler: @escaping (Result<User>) -> ()) {
        
        var parameters: [String: Any] = [:]
        
        parameters = [
            "email": email,
            "activation_code": verificationCode,
            "device_token": deviceToken,
            "device_type": "ios"
        ]
        
        let registerVerificationRequest = AuthenticationRouter.registerVerification(parameters: parameters)
        do {
            try networkHandler.request(registerVerificationRequest).decoded(toType: User.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
        
        
    }
    
    func login(email: String, password: String, deviceToken: String, completionHandler: @escaping (Result<User>) -> ()) {
        let parameters = [
            "email": email,
            "password": password,
            "device_token": deviceToken,
            "device_type": "ios"
        ]
        
        let loginRequest = AuthenticationRouter.login(parameters: parameters)
        do {
            try networkHandler.request(loginRequest).decoded(toType: User.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func resetPassword(email: String, completionHandler: @escaping ((Result<Data>) -> ())) {
        networkHandler.request(AuthenticationRouter.resetPassword(email: email)).observe(with: completionHandler)
    }
    
    func setNewPassword(email: String, activationCode: String, newPassword: String, confirmNewPassword: String, completionHandler: @escaping ((Result<Data>) -> ())) {
        let parameters = [
            "email": email,
            "activation_code": activationCode,
            "password": newPassword,
            "password_confirmation": confirmNewPassword
        ]
        let setNewPasswordRequest = AuthenticationRouter.setNewPassword(parameters: parameters)
        networkHandler.request(setNewPasswordRequest).observe(with: completionHandler)
    }
    
}
