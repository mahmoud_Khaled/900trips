//
//  ApplicationDataWorker.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ApplicationDataWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getCountries(completionHandler: @escaping (Result<[Country]>) -> ()) {
        let countriesRequest = ApplicationDataRouter.getCountries
        do {
            try networkHandler.request(countriesRequest).decoded(toType: [Country].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getCities(countryId: Int, completionHandler: @escaping (Result<[City]>) -> ()) {
        let citiesRequest = ApplicationDataRouter.getCities(countryId: countryId)
        do {
            try networkHandler.request(citiesRequest).decoded(toType: [City].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getCountriesAndCurrenceies(completionHandler: @escaping (Result<CountryAndCurrency>) -> ()) {
        
        let getCountriesAndCurrenceiesRequest = ApplicationDataRouter.getCountriesAndCurrencies
        do {
            try networkHandler.request(getCountriesAndCurrenceiesRequest).decoded(toType: CountryAndCurrency.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getCompanyTypes(completionHandler: @escaping (Result<[CompanyType]>) -> ()) {
        
        let getCompanyTypesRequest = ApplicationDataRouter.getCompanyTypes
        do {
            try networkHandler.request(getCompanyTypesRequest).decoded(toType: [CompanyType].self).observe(with: completionHandler)
            
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getMessageTypes(completionHandler: @escaping (Result<[MessageType]>) -> ()) {
        let messageTypesRequest = ApplicationDataRouter.getMessageTypes
        do {
            try networkHandler.request(messageTypesRequest, debug: true).decoded(toType: [MessageType].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getAboutApplication(completionHandler: @escaping (Result<Page>) -> ()) {
        let aboutApplicationRequest = ApplicationDataRouter.getAboutApplication
        do {
            try networkHandler.request(aboutApplicationRequest, debug: true).decoded(toType: Page.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getTermsAndConditions(completionHandler: @escaping (Result<Page>) -> ()) {
        let termsAndConditionsRequest = ApplicationDataRouter.getTermsAndConditions
        do {
            try networkHandler.request(termsAndConditionsRequest, debug: true).decoded(toType: Page.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
}
