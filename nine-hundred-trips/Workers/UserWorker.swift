//
//  UserWorker.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class UserWorker {
    
    private let networkHandler = NetworkHandler()
    
    func logout(completionHandler: @escaping  (Result<Data>) -> ()) {
        let logout = UserRouter.logout
        networkHandler.request(logout).observe(with: completionHandler)
    }
    
    func getUserData(completionHandler: @escaping  (Result<User>) -> ()) {
        let getUserData = UserRouter.getUserData
        do {
            try networkHandler.request(getUserData).decoded(toType: User.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func changeLanguage(code: String, completionHandler: @escaping (Result<Data>) -> ()) {
        networkHandler.request(UserRouter.changeLanguage(code: code), debug: true).observe(with: completionHandler)
    }
    
    func contactUs(name: String, email: String, messageTypeId: Int, content: String, completionHandler: @escaping (Result<Data>) -> ()) {
        let sendMessageRequest = UserRouter.contactUs(name: name, email: email, messageTypeId: messageTypeId, content: content)
        networkHandler.request(sendMessageRequest, debug: true).observe(with: completionHandler)
    }
    
    func searchCompanies(query: String, page: Int, completionHandler: @escaping (Result<[User]>, Int) -> ()) {
        let searchRequest = UserRouter.searchCompanies(query: query, page: page)
        do {
            try networkHandler.request(searchRequest).decoded(toType: [User].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }

    func getFavourites(page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        let getFavouritesRequest = UserRouter.getFavourites(page: page)
        do {
            try networkHandler.request(getFavouritesRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func editPassword(oldPassword: String, newPassword: String, confirmPassword: String,  completionHandler: @escaping (Result<Data>) -> ()) {
        let editPasswordRequest = UserRouter.editPassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)
        networkHandler.request(editPasswordRequest, debug: true).observe(with: completionHandler)
    }

    func getCompanyDetails(id: Int, page: Int, completionHandler: @escaping (Result<CompanyDetails>, Int) -> ()) {
        let getCompanyDetailsRequest = UserRouter.getCompanyDetails(id: id, page: page)
        do {
            try networkHandler.request(getCompanyDetailsRequest).decoded(toType: CompanyDetails.self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func updateUserProfile(userName: String, email: String, countryId: Int, currencyId: Int, companyType: Int, photo: [File]?, completionHandler: @escaping (Result<Data>) -> ()) {
        let updateProfileRequest = UserRouter.updateProfile(userName: userName, email: email, countryId: countryId, currencyId: currencyId, companyType: companyType)
        networkHandler.upload(files: photo, to: updateProfileRequest).observe(with: completionHandler)
    }
    
    func updateCompanyPhoneNumbers(phone: String, secondPhoneNumber: String, completionHandler: @escaping (Result<Data>) -> ()) {
        let updatePhoneNumbersRequest = UserRouter.updatePhoneNumbers(phone: phone, secondPhoneNumber: secondPhoneNumber)
        networkHandler.request(updatePhoneNumbersRequest, debug: true).observe(with: completionHandler)
    }
    
    func updateSocialLinks(facebook: String, twitter: String, instagram: String, website: String, completionHandler: @escaping (Result<Data>) -> ()) {
        let updateSocialLinksRequest = UserRouter.updateSocialLinks(facebook: facebook, twitter: twitter, instagram: instagram, website: website)
        networkHandler.request(updateSocialLinksRequest, debug: true).observe(with: completionHandler)
    }
    
    func downloadBudgetAndAccountsFile(completionHandler: @escaping (Result<URL?>) -> ()) {
        networkHandler.download(url: Constants.budgetAndAccountsPdfFile).observe { url in
            completionHandler(url)
        }
    }
    
    func refreshToken(token: String, completionHandler: @escaping (Result<Data>) -> ()) {
        let refreshTokenRequest = UserRouter.refreshToken(token: token)
        networkHandler.request(refreshTokenRequest).observe(with: completionHandler)
    }
    
    func cancelAllRequests() {
        networkHandler.invalidateAllRequests()
    }
}

