//
//  OffersWorker.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class OffersWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getSeasonalOffers(countryId: Int, page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        let getOffersRequest = OffersRouter.getSeasonalOffers(countryId: countryId, page: page)
        do {
            try networkHandler.request(getOffersRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func getSpecialOffers(countryId: Int, page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        let getOffersRequest = OffersRouter.getSpecialOffers(countryId: countryId, page: page)
        do {
            try networkHandler.request(getOffersRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func getDailyOffers(countryId: Int, page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        let getOffersRequest = OffersRouter.getDailyOffers(countryId: countryId, page: page)
        do {
            try networkHandler.request(getOffersRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func getDetails(offerId: Int, completionHandler: @escaping (Result<OfferDetails>) -> ()) {
        
        let getOfferDetailsRequest = OffersRouter.details(offerId: offerId)
        do {
            try networkHandler.request(getOfferDetailsRequest).decoded(toType: OfferDetails.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
        
    }
    
    func addFavourite(offerId: Int, completionHandler: @escaping (Result<Data>) -> ()) {
        let addFavouriteRequest = OffersRouter.addFavourite(offerId: offerId)
        networkHandler.request(addFavouriteRequest).observe(with: completionHandler)
    }
    
    
    func removeFavourite(offerId: Int, completionHandler: @escaping (Result<Data>) -> ()) {
        let addFavouriteRequest = OffersRouter.removeFavourite(offerId: offerId)
        networkHandler.request(addFavouriteRequest).observe(with: completionHandler)
    }

    
    func searchOffers(fromCityId: Int, toCityId: Int, page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        let searchOffersRequest = OffersRouter.searchOffers(fromCityId: fromCityId, toCityId: toCityId, page: page)
        do {
            try networkHandler.request(searchOffersRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func getMyOffers(page: Int, completionHandler: @escaping (Result<[Offer]>, Int) -> ()) {
        
        let getMyOffersRequest = OffersRouter.getMyOffers(page: page)
        
        do {
            try networkHandler.request(getMyOffersRequest).decoded(toType: [Offer].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
        
    }
    
    func getDataOfAddOffer(completionHandler: @escaping (Result<AddOfferData>)->()) {
        let getAddOfferDataRequest = OffersRouter.getAddOfferData
        do {
           try networkHandler.request(getAddOfferDataRequest).decoded(toType: AddOfferData.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func uploadOfferImages(files: [File], completionHandler: @escaping (Result<[String]>) -> ()) {
        let uploadImagesRequest = OffersRouter.uploadImages
        do {
            try networkHandler.upload(files: files, to: uploadImagesRequest, debug: true).decoded(toType: [String].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func addOffer(parameters: [String: Any], completionHandler: @escaping (Result<Data>) -> ()) {
        let addOfferRequest = OffersRouter.addOffer(parameters: parameters)
        networkHandler.request(addOfferRequest).observe(with: completionHandler)
    }
    
    func deleteOffer(offerId: Int, completionHandler: @escaping (Result<Data>)->()) {
        let deleteOfferRequest = OffersRouter.deleteOffer(offerId: offerId)
        networkHandler.request(deleteOfferRequest).observe(with: completionHandler)
    }
    
    func markOfferAsSpecial(offerId: Int, completionHandler: @escaping (Result<Data>)->()) {
        let markOfferAsSpecialRequest = OffersRouter.markOfferAsSpecial(offerId: offerId)
        networkHandler.request(markOfferAsSpecialRequest).observe(with: completionHandler)
    }
    
    func addOfferRate(offerId: Int, rate: Double, completionHandler: @escaping (Result<Data>) -> ()) {
        let addRateRequest = OffersRouter.addRate(offerId: offerId, rate: rate)
        networkHandler.request(addRateRequest).observe(with: completionHandler)
    }
    
}
