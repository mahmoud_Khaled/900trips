//
//  OrderWorker.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class OrderWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getRequestQuotationPrice(completionHandler: @escaping (Result<RequestQuotationPrice>) -> ()) {
        let getRequestQuotationPriceRequest = OrderRouter.getRequestQuotationPrice
        
        do {
           try networkHandler.request(getRequestQuotationPriceRequest).decoded(toType: RequestQuotationPrice.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func requestQuotation(model: RequestQuotation, completionHandler: @escaping (Result<Order>) -> ()) {
        let requestQuotationRequest = OrderRouter.requestQuotation(model: model)
        
        do {
            try networkHandler.request(requestQuotationRequest).decoded(toType: Order.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
}
