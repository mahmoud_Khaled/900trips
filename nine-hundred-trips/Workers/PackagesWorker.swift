//
//  File.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/29/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation


class PackagesWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getPackagesAndSubscritions(completionHandler: @escaping (Result<PackageAndSubscription>) -> ()) {
        let getPackagesAndSubscriptionsRequest = PackagesRouter.getPackagesAndSubscriptions
        do {
            try networkHandler.request(getPackagesAndSubscriptionsRequest).decoded(toType: PackageAndSubscription.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
}

