//
//  NotificationWorker.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
class NotificationWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getAllNotifications(page: Int, completionHandler: @escaping  (Result<[Notification]>, Int) -> ()) {
        
        let getNotificationsRequest = NotificationRouter.getNotifications(page: page)
        
        do {
            try networkHandler.request(getNotificationsRequest).decoded(toType: [Notification].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
        
    }
    
    func deleteNotification(notificationId: Int, completionHandler: @escaping  (Result<Data>) -> ()) {
        let deleteNotificationRequest = NotificationRouter.deleteNotification(notificationId: notificationId)
        networkHandler.request(deleteNotificationRequest).observe { result in
            completionHandler(result)
        }
    }
    
    func deleteAllNotifications(completionHandler: @escaping (Result<Data>) -> ()) {
        let deleteAllNotificationsRequest = NotificationRouter.deleteAllNotifications
        networkHandler.request(deleteAllNotificationsRequest).observe { result in
            completionHandler(result)
        }
    }
}
