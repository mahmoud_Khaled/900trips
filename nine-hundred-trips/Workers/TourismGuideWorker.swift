//
//  PlaceGuideWorker.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class TourismGuideWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getPlacesCategories(completionHandler: @escaping (Result<[PlaceCategory]>) -> ()) {
        let getPlacesCategoriesRequest = TourismRouter.getPlacesCategories
        do {
            try networkHandler.request(getPlacesCategoriesRequest).decoded(toType: [PlaceCategory].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func getTourismPlaces(categoryId: Int, cityId: Int, searchText: String, page: Int, completionHandler: @escaping (Result<[TourismPlace]>, Int) -> ()) {
        let getTourismPlacesRequest = TourismRouter.getPlaces(categoryId: categoryId, cityId: cityId, searchText: searchText, page: page)
        do {
            try networkHandler.request(getTourismPlacesRequest).decoded(toType: [TourismPlace].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            print("error is: ", error)
            completionHandler(.failure(error), 0)
        }
    }
    
    func uploadTourismImages(files: [File], completionHandler: @escaping (Result<[String]>) -> ()) {
        let uploadImagesRequest = TourismRouter.uploadImages
        do {
            try networkHandler.upload(files: files, to: uploadImagesRequest).decoded(toType: [String].self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func addTourismPlace(parameters: [String: Any], completionHandler: @escaping (Result<Data>) -> ()) {
        let addTourismPlaceRequest = TourismRouter.addTourismPlace(parameters: parameters)
        networkHandler.request(addTourismPlaceRequest).observe { result in
            completionHandler(result)
        }
    }
    
    func downloadTourismGuideFile(completionHandler: @escaping (Result<URL?>) -> ()) {
        networkHandler.download(url: Constants.tourismGuidePdfFile).observe { url in
            completionHandler(url)
        }
    }
}
