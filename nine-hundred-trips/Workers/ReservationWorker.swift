//
//  ReservationWorker.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 7/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ReservationWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getReservations(reversationStatus: ReservationStatus, page: Int, completionHandler: @escaping (Result<[Reservation]>, Int) -> ()) {
        let getReservationsRequest: ReservationRouter
        switch reversationStatus {
        case .pending:
            getReservationsRequest = .getPendingReservations(page: page)
        case .approved:
            getReservationsRequest = .getApprovedReservations(page: page)
        case .rejected:
            getReservationsRequest = .getRejectedReservations(page: page)
        }
        
        do {
            try networkHandler.request(getReservationsRequest).decoded(toType: [Reservation].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func approveReservation(reservationId: Int, completionHandler: @escaping (Result<Data>) -> ()) {
        let approveRequest = ReservationRouter.approveReservation(reservationId: reservationId)
        networkHandler.request(approveRequest).observe(with: completionHandler)
    }
    
    func rejectedReservation(reservationId: Int, completionHandler: @escaping (Result<Data>) -> ()) {
        let rejectRequest = ReservationRouter.rejectReservation(reservation: reservationId)
        networkHandler.request(rejectRequest).observe(with: completionHandler)
    }
    
    func requestReservation(parameters: [String: Any], completionHandler: @escaping (Result<Data>) -> ()) {
        let requestReservationRequest = ReservationRouter.requestReservation(parameters: parameters)
        networkHandler.request(requestReservationRequest).observe(with: completionHandler)
    }
    
}
