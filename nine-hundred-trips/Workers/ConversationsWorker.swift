//
//  ConversationsWorker.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ConversationsWorker {
    
    private let networkHandler = NetworkHandler()
    
    func getAllConvesations(page: Int, completionHandler: @escaping (Result<[Conversation]>, Int) -> ()) {
        
        let getConversationsRequest = ConversationRouter.getConversations(page: page)
        
        do {
            try networkHandler.request(getConversationsRequest).decoded(toType: [Conversation].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
        
    }
    
    func delteConvesation(conversationId: Int, completionHandler: @escaping (Result<Data>) -> ()) {
        
        let deleteConversationRequest = ConversationRouter.deleteConversation(id: conversationId)
                networkHandler.request(deleteConversationRequest).observe(with: completionHandler)
    }
    
    
    func getMessages(with userId: Int, page: Int, completionHandler: @escaping (Result<[Message]>, Int) -> ()) {
        let getMessagesRequest = ConversationRouter.getMessages(userId: userId, page: page)
        do {
            try networkHandler.request(getMessagesRequest).decoded(toType: [Message].self).observe { [weak self] result in
                guard let self = self else { return }
                completionHandler(result, self.networkHandler._totalPages)
            }
        } catch {
            completionHandler(.failure(error), 0)
        }
    }
    
    func sendMessage(to userId: Int, content: String, completionHandler: @escaping (Result<Data>) -> ()) {
        let sendMessageRequest = ConversationRouter.sendMessage(userId: userId, content: content)
        networkHandler.request(sendMessageRequest).observe(with: completionHandler)
    }
    
    func sendImage(to userId: Int, file: File, completionHandler: @escaping (Result<Data>) -> ()) {
        let sendImageRequest = ConversationRouter.sendImage(userId: userId)
        networkHandler.upload(files: [file], to: sendImageRequest).observe(with: completionHandler)
    }
    
    func rate(userId: Int, rate: Double, completionHandler: @escaping (Result<Data>) -> ()) {
        let rateRequest = ConversationRouter.rate(userId: userId, rate: rate)
        networkHandler.request(rateRequest).observe(with: completionHandler)
    }
}
