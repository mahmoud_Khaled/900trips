//
//  UserType.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 4/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum UserType: String {
    case client = "4"
    case company = "3"
}

extension UserType: Codable {
    public init(from decoder: Decoder) throws {
        self = try UserType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .client
    }
}
