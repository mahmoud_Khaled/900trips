//
//  DinNextFont.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum DinNextFont: String {
    case heavy = "Heavy"
    case regular = "Regular"
    case bold = "Bold"
    case medium = "Medium"
    case light = "Light"
    case ultralight = "UltraLight"
    case black = "Black"
    
    func getFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "DINNextLTW23-\(self.rawValue)", size: size) ?? .systemFont(ofSize: size)
    }
}
