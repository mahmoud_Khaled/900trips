//
//  CustomNavigationController.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.barStyle = .black
    }
    
}
