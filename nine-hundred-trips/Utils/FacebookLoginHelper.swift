//
//  FacebookLoginHelper.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import FBSDKLoginKit

protocol FacebookLoginHelperProtocol {
    func login(withPermissions permissions: [Permissions], fields: [Fields])
}

protocol FacebookLoginHelperDelegate: class {
    func didAuthenticate(info: [String: AnyObject])
    func didFail(with error: Error)
}

enum Permissions: String {
    case publicProfile = "public_profile"
    case email
}

enum Fields {
    case id
    case firstName
    case lastName
    case picture(width: Int)
    case phone
    case email
    
    var value: String {
        switch self {
        case .id:
            return "id"
        case .firstName:
            return "first_name"
        case .lastName:
            return "last_name"
        case .phone:
            return "phone"
        case .email:
            return "email"
        case .picture(let width):
            return "picture.width(\(width))"
        }
    }
}

class FacebookLoginHelper: FacebookLoginHelperProtocol {
    
    private let facebookLoginManager = FBSDKLoginManager()
    weak var delegate: FacebookLoginHelperDelegate?
    private let owner: UIViewController
    
    init(owner: UIViewController) {
        self.owner = owner
    }
    
    private func fetchFacebookProfile(fields: String, completionHandler: @escaping (Result<[String: AnyObject]>) -> ()) {
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": fields]).start { _, result, error in
            if let error = error {
                completionHandler(.failure(error))
            } else {
                guard var userInfo = result as? [String: AnyObject] else { return }
                if FBSDKAccessToken.current()?.tokenString != nil {
                    userInfo["access_token"] = FBSDKAccessToken.current()?.tokenString as AnyObject
                }
                completionHandler(.success(userInfo))
            }
        }
    }
    
    func login(withPermissions permissions: [Permissions], fields: [Fields]) {
        facebookLoginManager.logOut()
        let permissionsStrings = permissions.map { $0.rawValue }
        let requestedFields = fields.map { $0.value }.joined(separator: ",")
        facebookLoginManager.logIn(withReadPermissions: permissionsStrings, from: owner) { [weak self] result, error in
            guard let self = self else { return }
            if let error = error {
                self.delegate?.didFail(with: error)
            } else {
                guard let result = result else { return }
                if !result.isCancelled {
                    self.fetchFacebookProfile(fields: requestedFields) { [weak self] result in
                        guard let self = self else { return }
                        switch result {
                        case .success(let info):
                            self.delegate?.didAuthenticate(info: info)
                        case .failure(let error):
                            self.delegate?.didFail(with: error)
                        }
                    }
                } else {
                    self.delegate?.didFail(with: NetworkError.cancelled)
                }
            }
        }
    }
}
