//
//  LocationManager.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerProtocol {
    func getUserLocation()
    func stopLocationUpdates()
}

protocol LocationManagerDelegate: class {
    func handleLocationServicesDisabled()
    func didFetchLocation(location: CLLocation)
    func didFail(with error: Error)
}

class LocationManager: NSObject, LocationManagerProtocol, CLLocationManagerDelegate {
    
    weak var delegate: LocationManagerDelegate?
    private lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        return locationManager
    }()
    
    
    func getUserLocation() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        } else {
            delegate?.handleLocationServicesDisabled()
        }
    }
    
    func stopLocationUpdates() {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            delegate?.handleLocationServicesDisabled()
        } else if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            delegate?.didFetchLocation(location: location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didFail(with: error)
    }
}
