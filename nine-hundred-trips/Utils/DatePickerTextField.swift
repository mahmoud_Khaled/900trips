import UIKit

class DatePickerTextField: UITextField, UITextFieldDelegate {
    
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        return datePicker
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: L102Language.currentLanguage)
        return dateFormatter
    }()
    
    var datePickerMode: UIDatePicker.Mode = .date {
        didSet {
            datePicker.datePickerMode = datePickerMode
        }
    }
    
    var minimumDate: Date? {
        didSet {
            datePicker.minimumDate = minimumDate
        }
    }
    
    var maximumDate: Date? {
        didSet {
            datePicker.maximumDate = maximumDate
        }
    }
    
    var didSelectDate: ((Date) -> ())?
    
    init(dateFormat: String) {
        super.init(frame: .zero)
        inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        dateFormatter.dateFormat = dateFormat
        delegate = self
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setDate(_ date: Date, animated: Bool) {
        datePicker.setDate(date, animated: animated)
    }
    
    @objc private func handleDatePicker(_ picker: UIDatePicker) {
        text = dateFormatter.string(from: picker.date)
        didSelectDate?(picker.date)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        handleDatePicker(datePicker)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
