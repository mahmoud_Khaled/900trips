//
//  CustomTextField.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class PickerViewTextField<Item: CustomStringConvertible>: UITextField, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()
    
    private var selectedRow = 0
    
    var didSelectItem: ((Item) -> ())?
    
    var items = [Item]() {
        didSet {
            pickerView.reloadAllComponents()
            if isFirstResponder == true {
                pickerView(pickerView, didSelectRow: selectedRow, inComponent: 0)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        inputView = pickerView
        delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(describing: items[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard items.count > 0 else { return }
        selectedRow = row
        text = String(describing: items[row])
        didSelectItem?(items[row])
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        pickerView(pickerView, didSelectRow: selectedRow, inComponent: 0)
    }
    
    func setSelectedItem<T: CustomStringConvertible>(_ item: T) {
        let selectedItem = items.first(where: { $0.description == item.description })
        selectedRow = items.firstIndex(where: { $0.description == selectedItem?.description }) ?? 0
        pickerView.selectRow(selectedRow, inComponent: 0, animated: false)
        pickerView(pickerView, didSelectRow: selectedRow, inComponent: 0)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
