//
//  UserDefaultsHelper.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    
    static private let defaults = UserDefaults.standard
    
    static var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: "isLoggedIn")
        } set {
            defaults.set(newValue, forKey: "isLoggedIn")
        }
    }
    
    static var token: String? {
        get {
            return defaults.string(forKey: "accessToken")
        } set {
            defaults.set(newValue, forKey: "accessToken")
        }
    }
    
    static var id: Int {
        get {
            return defaults.integer(forKey: "id")
        } set {
            defaults.set(newValue, forKey: "id")
        }
    }
    
    static var userType: UserType {
        get {
            return UserType(rawValue: defaults.string(forKey: "userType") ?? "") ?? .client
        } set {
            defaults.set(newValue.rawValue, forKey: "userType")
        }
    }
    
    static var countryId: Int {
        get {
            return defaults.integer(forKey: "countryId")
        } set {
            defaults.set(newValue, forKey: "countryId")
        }
    }
    
    static var currencyId: Int {
        get {
            return defaults.integer(forKey: "currencyId")
        } set {
            defaults.set(newValue, forKey: "currencyId")
        }
    }
    
    static var companyId: Int {
        get {
            return defaults.integer(forKey: "companyId")
        } set {
            defaults.set(newValue, forKey: "companyId")
        }
    }
    
    static var firebaseToken: String {
        get {
            return defaults.string(forKey: "firebaseToken") ?? ""
        } set {
            defaults.set(newValue, forKey: "firebaseToken")
        }
    }
}
