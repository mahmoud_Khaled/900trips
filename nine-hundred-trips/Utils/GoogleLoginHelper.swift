//
//  GoogleLoginHelper.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import GoogleSignIn

protocol GoogleLoginHelperProtocol {
    func login()
}

protocol GoogleLoginHelperDelegate: class {
    func didAuthenticate(with user: GIDGoogleUser)
    func didFail(with error: Error)
}

class GoogleLoginHelper: NSObject, GIDSignInUIDelegate, GIDSignInDelegate, GoogleLoginHelperProtocol {
    
    private let owner: UIViewController
    weak var delegate: GoogleLoginHelperDelegate?
    
    init(owner: UIViewController) {
        self.owner = owner
        super.init()
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    func login() {
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if error.localizedDescription.contains("canceled") {
                delegate?.didFail(with: NetworkError.cancelled)
            } else {
                delegate?.didFail(with: error)
            }
        } else {
            delegate?.didAuthenticate(with: user)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        owner.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        owner.dismiss(animated: true, completion: nil)
    }
    
}
