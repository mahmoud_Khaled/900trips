//
//  CustomFlowLayout.swift
//  nine-hundred-trips
//
//  Created by Vortex on 6/1/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CustomFlowLayout: UICollectionViewFlowLayout {
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attr = layoutAttributesForItem(at: itemIndexPath)
        attr?.alpha = 1
        return attr
    }
}
