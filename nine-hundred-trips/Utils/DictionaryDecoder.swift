//
//  DictionaryDecoder.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 5/28/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class DictionaryDecoder {
    private let jsonDecoder = JSONDecoder()
    
    func decode<T>(_ type: T.Type, from json: Any) throws -> T where T: Decodable {
        let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
        return try jsonDecoder.decode(type, from: jsonData)
    }
}
