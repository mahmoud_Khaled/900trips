//
//  PopupViewProtocols.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

protocol PopupViewProtocol where Self: UIView {
    associatedtype Model
    var dismissesAutomtically: Bool { get }
    var isConfigurable: Bool { get }
    func configure(model: Model)
}

extension PopupViewProtocol {
    func configure(model: String) {
        
    }
}

protocol PopupViewActionProtocol: PopupViewProtocol {
    var confirmAction: (() -> ())? { get set }
    var cancelAction: (() -> ())? { get set }
}
