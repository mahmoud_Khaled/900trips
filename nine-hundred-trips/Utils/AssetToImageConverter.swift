//
//  AssetToImageConverter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Photos

protocol AssetToImageConverterProtocol {
    func convert(assets: [PHAsset], targetSize: CGSize) -> [UIImage]
}

class AssetToImageConverter: AssetToImageConverterProtocol {
    
    func convert(assets: [PHAsset], targetSize: CGSize) -> [UIImage] {
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        var images = [UIImage]()
        assets.forEach {
            manager.requestImage(for: $0, targetSize: targetSize, contentMode: .aspectFill, options: options) {
                result, _ in
                images.append(result!)
            }
        }
        return images
    }
    
}
