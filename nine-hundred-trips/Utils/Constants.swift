//
//  Constants.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct Constants {
    
    static let url = "http://900trips.com/"
    static let apiEndpoint = "api/v1/"
    static let baseUrl = "\(url)\(apiEndpoint)"

    
    //MARK:- Application Data Endpoints
    static let countryAnCurrencyEndpoint = "currencies-countries"
    static let countriesEndpoint = "countries"
    static let citiesEndpoint = "cities"
    static let messageTypesEndpoint = "contact-categories"
    static let companyTypesEndpoint = "company-types"
    static let aboutApplicationEndpoint = "about"
    static let termsAndConditionsEndpoint = "terms"
    

    //MARK:- Auth Endpoints
    static let registerEndpoint = "register"
    static let registerVerificationEndPoint = "activate"
    static let loginEndPoint = "login"
    static let resetPasswordEndpoint = "forget"
    static let setNewPasswordEndpoint = "set-password"
    static let logoutEndpoint = "logout"

    //MARK:- Offers Endpoints
    static let dailyOffers = "daily-offers"
    static let specialOffers = "adv-offers"
    static let seasonalOffers = "seasonal-offers"
    static let addFavourite = "like-offer"
    static let removeFavourite = "unlike-offer"
    static let offerDetailsEndpoint = "offer"
    static let searchOffersEndpoint = "search-offers"
    static let myOffersEndpoint = "my-offers"
    static let addOfferDataEndpoint = "add-offer-services"
    static let addOfferEndpoint = "add-offer"
    static let deleteOfferEndpoint = "delete-offer"
    static let offerSpecialEndPoint = "add-offer-adv"
    static let addOfferRateEndpoint = "add-offer-rating"

    //MARK:- User Endpoitns
    static let userDataEndpoint = "current-user"
    static let changeLanguageEndpoint = "change-lang"
    static let contactUsEndpoint = "contact-us"
    static let companiesEndPoint = "companies"
    static let favouritesEndpoint = "favourite-offers"
    static let companyDetailsEndpoint = "company-details"
    static let changePasswordEndpoint = "change-password"
    static let updateProfileEndpoint = "update-profile"
    static let updatePhoneNumbersEndpoint = "phones"
    static let updateSocialLinksEndpoint = "social-links"
    static let budgetAndAccountsPdfFile = "http://900trips.com/images/budget-accounts.xls"
    static let refreshTokenEndpoint = "update-device-token"
    
    //MARK:- Tourism Guide Endpoints
    static let placesCategoriesEndpoint = "places-categories"
    static let placesEndpoint = "country-places"
    static let uploadImagesEndpoint = "upload-photos"
    static let addTourismPlaceEndpoint = "add-place"
    static let tourismGuidePdfFile = "http://900trips.com/images/file-pdf.pdf"

    //MARK:- Order Endpoints
    static let requestQuotationPriceEndpoint = "order-price-quota"
    static let requestQuotationEndpoint = "add-order"

    //MARK:- Conversations Endpoints
    static let conversationsEndpoint = "all-messages"
    static let messagesEndpoint = "messages-user"
    static let sendMessageEndpoint = "add-message"
    static let deleteConversationEndpoint = "delete-messages"
    static let rateConversationEndpoint = "add-user-rating"
    
    //MARK:- Notifications Endpoints
    static let notificationsEndpoint = "notifications"
    static let deleteNotificationEndpoint = "delete-notification"
    static let deleteAllNotificationsEndpoint = "delete-all-notification"
    
    //MARK:- Reservations ENdpoints
    static let myReservationsEndpoint = UserDefaultsHelper.userType == .company ? "my-new-reservations"  :"my-new-reservations-user"
    static let myApprovedReservationsEndpoint = UserDefaultsHelper.userType == .company ? "my-approved-reservations" : "my-approved-reservations-user"
    static let myRejectedReservationsEndpoint = UserDefaultsHelper.userType == .company ? "my-refused-reservations" : "my-refused-reservations-user"
    static let requestReservationEndpoint = "add-reservation"
    
    static let approveResevationEndpoint = "approve-reservation"
    static let rejectReservationEndpoint = "refuse-reservation"
    
    //MARK:- Packages
    static let packagesAndSubScriptionEndpoint = "packages"
    
    //Domain Regex
    static let domainRegex = "^(http|https|ftp|Http|Https|Ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$"
}
