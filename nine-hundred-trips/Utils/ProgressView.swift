//
//  ProgressView.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/12/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ProgressView: UIView {
    
    private let shapeLayer = CAShapeLayer()
    private let trackLayer = CAShapeLayer()
    private var circularPath: UIBezierPath!
    private let radius: CGFloat
    private let strokeColor: UIColor
    private let lineWidth: CGFloat
    
    var strokeEnd: Float = 0.5 {
        didSet {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            shapeLayer.strokeEnd = CGFloat(strokeEnd)
            CATransaction.commit()
        }
    }
    
    init(radius: CGFloat, strokeColor: UIColor, lineWidth: CGFloat, frame: CGRect) {
        self.radius = radius
        self.strokeColor = strokeColor
        self.lineWidth = lineWidth
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle:  0, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineWidth = lineWidth
        trackLayer.lineCap = .round

        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineCap = .round
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)

        shapeLayer.position = center
        trackLayer.position = center
        
        layer.addSublayer(trackLayer)
        layer.addSublayer(shapeLayer)

        backgroundColor = .clear

    }
}
