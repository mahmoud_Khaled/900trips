//
//  AuthenticationRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum AuthenticationRouter: URLRequestConvertible {
    
    case register(parameters: [String: Any])
    case registerVerification(parameters: [String: Any])
    case login(parameters: [String: Any])
    case resetPassword(email: String)
    case setNewPassword(parameters: [String: Any])
    
    var method: HTTPMethod {
        return .post
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .register(let parameters):
            return parameters
        case .registerVerification(let parameters):
            return parameters
        case .login(let parameters):
            return parameters
        case .resetPassword(let email):
            return [
                "email": email
            ]
        case .setNewPassword(let parameters):
            return parameters
        }
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .register:
            relativePath = Constants.registerEndpoint
        case .registerVerification:
            relativePath = Constants.registerVerificationEndPoint
        case .login:
            relativePath = Constants.loginEndPoint
        case .resetPassword:
            relativePath = Constants.resetPasswordEndpoint
        case .setNewPassword:
            relativePath = Constants.setNewPasswordEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
