//
//  ApplicationDataRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/14/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire
enum ApplicationDataRouter: URLRequestConvertible {
    
    case getCountries
    case getCities(countryId: Int)
    case getCountriesAndCurrencies
    case getCompanyTypes
    case getMessageTypes
    case getAboutApplication
    case getTermsAndConditions
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .getCountries:
            relativePath = Constants.countriesEndpoint
        case .getCities(let id):
            relativePath = "\(Constants.citiesEndpoint)/\(id)"
        case .getCountriesAndCurrencies:
            relativePath = Constants.countryAnCurrencyEndpoint
        case .getCompanyTypes:
            relativePath = Constants.companyTypesEndpoint
        case .getMessageTypes:
            relativePath = Constants.messageTypesEndpoint
        case .getAboutApplication:
            relativePath = Constants.aboutApplicationEndpoint
        case .getTermsAndConditions:
            relativePath = Constants.termsAndConditionsEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
