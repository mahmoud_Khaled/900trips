//
//  ConversationRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum ConversationRouter: URLRequestConvertible {
    
    case getConversations(page: Int)
    case deleteConversation(id: Int)
    case getMessages(userId: Int, page: Int)
    case sendMessage(userId: Int, content: String)
    case sendImage(userId: Int)
    case rate(userId: Int, rate: Double)
    
    var method: HTTPMethod {
        switch self {
        case .getConversations, .getMessages:
            return .get
        case .sendMessage, .sendImage, .deleteConversation, .rate:
            return .post
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getConversations(let page):
            return [
                "page": page
            ]
        case .deleteConversation(let id):
            return [
                "message_id": id
            ]
        case .getMessages(_, let page):
            return [
                "page": page
            ]
        case .sendMessage(let userId, let content):
            return [
                "to": userId,
                "message": content,
                "message_type": "0"
            ]
        case .sendImage(let userId):
            return [
                "to": userId,
                "message_type": "1"
            ]
        case .rate(let userId, let rate):
            return [
                "rated_user": userId,
                "rate": rate
            ]
        }
    }
    
    var url: URL {
        let relativePath : String
        switch self {
        case .getConversations:
            relativePath = Constants.conversationsEndpoint
        case .deleteConversation:
            relativePath = Constants.deleteConversationEndpoint
        case .getMessages(let userId, _):
            relativePath = "\(Constants.messagesEndpoint)/\(userId)"
        case .sendMessage, .sendImage:
            relativePath = Constants.sendMessageEndpoint
        case .rate:
            relativePath = Constants.rateConversationEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getConversations:
            return URLEncoding.queryString
        case .getMessages:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
