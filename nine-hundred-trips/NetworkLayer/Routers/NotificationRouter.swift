//
//  NotificationRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 6/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum NotificationRouter: URLRequestConvertible {
    case getNotifications(page: Int)
    case deleteNotification(notificationId: Int)
    case deleteAllNotifications
    
    var method: HTTPMethod {
        switch self {
        case .getNotifications:
            return .get
        case .deleteNotification, .deleteAllNotifications:
            return .post
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getNotifications(let page):
            return [
                "page": page
            ]
        case .deleteNotification(let notificationId):
            return [
                "notification_id": notificationId
            ]
        default:
            return nil
        }
    }
    
    var url: URL {
        let relativePath : String
        switch self {
        case .getNotifications:
            relativePath = Constants.notificationsEndpoint
        case .deleteNotification:
            relativePath = Constants.deleteNotificationEndpoint
        case .deleteAllNotifications:
            relativePath = Constants.deleteAllNotificationsEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getNotifications:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}
