//
//  ReservationRouter.swift
//  nine-hundred-trips
//
//  Created by iOS ibtdi.com on 7/16/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum ReservationRouter: URLRequestConvertible {
    
    case getPendingReservations(page: Int)
    case getApprovedReservations(page: Int)
    case getRejectedReservations(page: Int)
    case approveReservation(reservationId: Int)
    case rejectReservation(reservation: Int)
    case requestReservation(parameters: [String: Any])
    
    var method: HTTPMethod {
        switch self {
        case .getPendingReservations, .getApprovedReservations, .getRejectedReservations:
            return .get
            
        case .approveReservation, .rejectReservation, .requestReservation:
            return .post
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getPendingReservations(let page), .getApprovedReservations(let page), .getRejectedReservations(let page):
            return [
                "page": page
            ]
            
        case .approveReservation(let reservationId), .rejectReservation(let reservationId):
            return [
                "reservation_id": reservationId
            ]
        case .requestReservation(let parameters):
            return parameters
        }
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .getPendingReservations:
            relativePath = Constants.myReservationsEndpoint
        case .getApprovedReservations:
            relativePath = Constants.myApprovedReservationsEndpoint
        case .getRejectedReservations:
            relativePath = Constants.myRejectedReservationsEndpoint
        case .approveReservation:
            relativePath = Constants.approveResevationEndpoint
        case .rejectReservation:
             relativePath = Constants.rejectReservationEndpoint
        case .requestReservation:
            relativePath = Constants.requestReservationEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getPendingReservations, .getApprovedReservations, .getRejectedReservations:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    
}
