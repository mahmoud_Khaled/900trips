//
//  OrderRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum OrderRouter: URLRequestConvertible {
    
    case getRequestQuotationPrice
    case requestQuotation(model: RequestQuotation)
    
    var method: HTTPMethod {
        switch self {
        case .getRequestQuotationPrice:
            return .get
        case .requestQuotation:
            return .post
        } 
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getRequestQuotationPrice:
            return nil
        case .requestQuotation(let model):
            return model.toParameters()
        }
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .getRequestQuotationPrice:
            relativePath = Constants.requestQuotationPriceEndpoint
        case .requestQuotation:
            relativePath = Constants.requestQuotationEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getRequestQuotationPrice, .requestQuotation:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
