//
//  TouristGuideRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/6/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum TourismRouter: URLRequestConvertible {
    
    case getPlacesCategories
    case getPlaces(categoryId: Int, cityId: Int, searchText: String, page: Int)
    case uploadImages
    case addTourismPlace(parameters: [String: Any])
    
    var method: HTTPMethod {
        switch self {
        case .uploadImages, .addTourismPlace:
            return .post
        default:
            return .get
        }

    }
    
    var parameters: [String : Any]? {
        switch self {
        case .addTourismPlace(let params):
            return params
        case .getPlaces( _, _, let searchText, let page):
            return [
                "title": searchText,
                "page": page
            ]
        default:
            return nil
        }

    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .getPlacesCategories:
            relativePath = Constants.placesCategoriesEndpoint
        case .getPlaces(let categoryId, let cityId, _, _):
            relativePath = "\(Constants.placesEndpoint)/\(UserDefaultsHelper.countryId)/\(categoryId)/\(cityId)"
        case .uploadImages:
            relativePath = Constants.uploadImagesEndpoint
        case .addTourismPlace:
            relativePath = Constants.addTourismPlaceEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getPlaces:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }

}
