//
//  PackagesRouter.swift
//  nine-hundred-trips
//
//  Created by Mahmoud Khaled on 5/29/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum PackagesRouter: URLRequestConvertible {
    
    case getPackagesAndSubscriptions
    
    var method: HTTPMethod {
        switch self {
        case .getPackagesAndSubscriptions:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var url: URL{
        let relativePath: String
        switch self {
        case .getPackagesAndSubscriptions:
            relativePath = Constants.packagesAndSubScriptionEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding{
        return JSONEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    
}
