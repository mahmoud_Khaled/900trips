//
//  UserRouter.swift
//  nine-hundred-trips
//
//  Created by ibtdi.com on 5/2/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum UserRouter: URLRequestConvertible {
    
    case logout
    case getUserData
    case changeLanguage(code: String)
    case contactUs(name: String, email: String, messageTypeId: Int, content: String)
    case searchCompanies(query: String, page: Int)
    case getFavourites(page: Int)
    case getCompanyDetails(id: Int, page: Int)
    case editPassword(oldPassword: String, newPassword: String, confirmPassword: String)
    case updateProfile(userName: String, email: String, countryId: Int, currencyId: Int, companyType: Int)
    case updatePhoneNumbers(phone: String, secondPhoneNumber: String)
    case updateSocialLinks(facebook: String, twitter: String, instagram: String, website: String)
    case refreshToken(token: String)
    
    var method: HTTPMethod {
        switch self {
        case .contactUs, .logout, .changeLanguage, .editPassword, .updateProfile, .updatePhoneNumbers, .updateSocialLinks, .refreshToken:
            return .post
        case .getUserData, .searchCompanies, .getFavourites, .getCompanyDetails:
            return .get
        }
    }

    var parameters: [String : Any]? {
        switch self {
        case .contactUs(let name, let email, let messageTypeId, let content):
            return [
                "name": name,
                "email": email,
                "message_type_id": messageTypeId,
                "message": content
            ]
        case .changeLanguage(let code):
            return [
                "lang": code
            ]
        case .searchCompanies(let searchQuery, let page):
            return [
                "username": searchQuery,
                "page": page
            ]
        case .getFavourites(let page):
            return [
                "page": page
            ]
        case .getCompanyDetails(_, let page):
            return [
                "page": page
            ]
        case .getUserData:
            return nil
        case .editPassword(let oldPassword, let newPassword, let confirmPassword):
            return [
                "old_password": oldPassword,
                "password" :newPassword,
                "password_confirmation" :confirmPassword
            ]
        case .updateProfile(let userName, let email, let countryId, let currencyId, let companyType):
            return [
                "username": userName,
                "email" :email,
                "country_id" :countryId,
                "currency_id": currencyId,
                "company_type": companyType,
            ]
        case .updatePhoneNumbers(let phone, let secondPhoneNumber):
           return [
                "phone": phone,
                "phone1": secondPhoneNumber
            ]
        case .updateSocialLinks(let facebook, let twitter, let instagram, let website):
            return [
                "facebook": facebook,
                "twitter": twitter,
                "instagram": instagram,
                "domain": website
            ]
        case .refreshToken(let token):
            return [
                "device_token": token
            ]
        default:
            return nil
        }
    }

    var url: URL {
        let relativePath: String
        switch self {
        case .logout:
            relativePath = Constants.logoutEndpoint
        case .getUserData:
            relativePath = Constants.userDataEndpoint
        case .changeLanguage:
            relativePath = Constants.changeLanguageEndpoint
        case .contactUs:
            relativePath = Constants.contactUsEndpoint
        case .searchCompanies:
            relativePath = Constants.companiesEndPoint
        case .getFavourites:
            relativePath = Constants.favouritesEndpoint
        case .getCompanyDetails(let id, _):
            relativePath = "\(Constants.companyDetailsEndpoint)/\(id)"
        case .editPassword:
            relativePath  = Constants.changePasswordEndpoint
        case .updateProfile:
            relativePath = Constants.updateProfileEndpoint
        case .updatePhoneNumbers:
            relativePath = Constants.updatePhoneNumbersEndpoint
        case .updateSocialLinks:
            relativePath = Constants.updateSocialLinksEndpoint
        case .refreshToken:
            relativePath = Constants.refreshTokenEndpoint
        }
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .searchCompanies, .getFavourites, .getCompanyDetails:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}
