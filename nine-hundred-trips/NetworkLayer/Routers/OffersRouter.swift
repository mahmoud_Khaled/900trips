//
//  OffersRouter.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/9/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum OffersRouter: URLRequestConvertible {
    
    case getSeasonalOffers(countryId:Int, page: Int)
    case getSpecialOffers(countryId: Int, page: Int)
    case getDailyOffers(countryId: Int, page: Int)
    case details(offerId: Int)
    case addFavourite(offerId: Int)
    case removeFavourite(offerId: Int)
    case searchOffers(fromCityId: Int, toCityId: Int, page: Int)
    case getMyOffers(page: Int)
    case getAddOfferData
    case uploadImages
    case addOffer(parameters: [String: Any])
    case deleteOffer(offerId: Int)
    case markOfferAsSpecial(offerId: Int)
    case addRate(offerId: Int, rate: Double)
    
    var method: HTTPMethod {
        switch self {
        case .searchOffers, .addFavourite, .removeFavourite, .deleteOffer, .uploadImages, .addOffer, .markOfferAsSpecial, .addRate:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getDailyOffers( _ , let page), .getSpecialOffers( _ , let page), .getSeasonalOffers( _ , let page):
            return [
                "page": page
            ]
        case .searchOffers(let fromCityId, let toCityId, _):
            return [
                "from_city": fromCityId,
                "to_city": toCityId
            ]
        case .addFavourite(let offerId):
            return [
                "offer_id": offerId
            ]
        case .removeFavourite(let offerId):
            return [
                "offer_id": offerId
            ]
        case .getMyOffers(let page):
            return [
                "page": page
            ]
        case .addOffer(let parameters):
            return parameters
        
        case .deleteOffer(let offerId):
            return [
                "offer_id": offerId
            ]
        case .markOfferAsSpecial(let offerId):
            return [
                "offer_id": offerId
            ]
        case .addRate(let offerId, let rate):
            return [
                "offer_id": offerId,
                "rate": rate
            ]
       
        default:
            return nil
        }
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .getSeasonalOffers(let id, _ ):
            relativePath = "\(Constants.seasonalOffers)/\(id)"
        case .getSpecialOffers(let id, _):
            relativePath = "\(Constants.specialOffers)/\(id)"
        case .getDailyOffers(let id, _):
            relativePath = "\(Constants.dailyOffers)/\(id)"
        case .details(let id):
            relativePath = "\(Constants.offerDetailsEndpoint)/\(id)"
        case .addFavourite:
            relativePath = Constants.addFavourite
        case .removeFavourite:
            relativePath = Constants.removeFavourite
        case .searchOffers:
            relativePath = Constants.searchOffersEndpoint
        case .getMyOffers:
            relativePath = Constants.myOffersEndpoint
        case .getAddOfferData:
            relativePath = Constants.addOfferDataEndpoint
        case .uploadImages:
            relativePath = Constants.uploadImagesEndpoint
        case .addOffer:
            relativePath = Constants.addOfferEndpoint
        case .deleteOffer:
            relativePath = Constants.deleteOfferEndpoint
        case .markOfferAsSpecial:
            relativePath = Constants.offerSpecialEndPoint
        case .addRate:
            relativePath = Constants.addOfferRateEndpoint
        }
        switch self {
        case .searchOffers( _, _, let page):
            let url = URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
            return url.appending("page", value: "\(page)")
        default:
            return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
        }
        
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getDailyOffers, .getSeasonalOffers, .getSpecialOffers, .getMyOffers:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
