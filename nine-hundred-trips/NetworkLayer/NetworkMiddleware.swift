//
//  NetworkMiddleware.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

class NetworkMiddleware: RequestAdapter {
    
    let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForResource = 60
        configuration.timeoutIntervalForRequest = 60
        return SessionManager(configuration: configuration)
    }()
    
    private var accessToken: String {
        get {
            guard let token: String = UserDefaults.standard.string(forKey: "accessToken") else { return "" }
            return token
        } set {
            UserDefaults.standard.set(newValue, forKey: "accessToken")
        }
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if urlRequest.url?.absoluteString.contains("pdf") == true || urlRequest.url?.absoluteString.contains("xls") == true || urlRequest.url?.absoluteString.contains("upload-photos") == true || urlRequest.url?.absoluteString.contains("update-profile") == true {
            urlRequest.timeoutInterval = 300
        }
        
        urlRequest.setValue(L102Language.currentLanguage, forHTTPHeaderField: "Accept-Language")
        
        if !UserDefaultsHelper.isLoggedIn {
            urlRequest.setValue("\(UserDefaultsHelper.currencyId)", forHTTPHeaderField: "Accept-Currency")
        }
        
        if accessToken != "" {
            urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        }
        
        return urlRequest
        
    }
}
