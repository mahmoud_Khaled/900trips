//
//  FutureDecoded.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension Future where Value == Data {
    func decoded<NextValue: Decodable>(toType type: NextValue.Type, withKeyPath keyPath: String = "", debug: Bool = false) throws -> Future<NextValue> {
        return transformed {
            do {
                return try JSONDecoder().decode(NextValue.self, from: $0)
            } catch {
                throw error
            }
        }
    }
}
