//
//  DownloadRequestExtension.swift
//  nine-hundred-trips
//
//  Created by Vortex on 5/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

extension DownloadRequest {
    open class func suggestedDownloadDestination(
        for directory: FileManager.SearchPathDirectory = .documentDirectory,
        in domain: FileManager.SearchPathDomainMask = .userDomainMask,
        with options: DownloadOptions)
        -> DownloadFileDestination
    {
        return { temporaryURL, response in
            let destination = DownloadRequest.suggestedDownloadDestination(for: directory, in: domain)(temporaryURL, response)
            return (destination.destinationURL, options)
        }
    }
}
